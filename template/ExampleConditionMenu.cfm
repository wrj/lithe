[,]
	<cfif>
		<cfcondition>currentrow%3 eq 1</cfcondition>
		<ul class="row-fluid">
	</cfif>
	<li class="span4 portfolio-item all-course">
		<div class="picture">
			<a href="{{LINKURL}}" title="{{NAME}}"><img src="/assets/upload/image/{{IMAGEFILE}}" alt=""/>
				<div class="image-overlay-link"></div>
			</a>
		</div>
		<cfif>
			<cfcondition>{{description}} neq ""</cfcondition>
			<div class="hero-unit">
				<div class="cousre-intro">
					<p>{{DESCRIPTION}}</p>
				</div>
			</div>
		</cfif>
[,]
	</li>
	<cfif><cfcondition>currentrow%3 eq 0</cfcondition>
		</ul>
	</cfif>
[,]

