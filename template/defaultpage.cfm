<div class="pagination hidden-phone pagination-left">
  <ul>
<!--- previous button group --->
      {{desktop-first-group}}
      {{desktop-prev-group}}
      <!--- Loop Number of page --->
      {{desktop-number-group}}
      <!--- Next Button group --->
      {{desktop-next-group}}
      {{desktop-last-group}}
  </ul>
</div>
<div class="pagination pagination-centered hidden-tablet hidden-desktop">
  <ul>
    <!--- previous button group --->
    {{desktop-prev-group}}
    <!--- Next Button group --->
    {{desktop-next-group}}
  </ul>
</div>