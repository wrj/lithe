<cfimport taglib="/cfc/tags" prefix="lithe">
<cfset activepage = 'News'>
<cfinclude template="include/header.cfm">
<div class="container">
	<div class="row-fluid">
		<lithe:lithe-breadcrumb homeurl="web" template="defaultbreadcrumb"/>
	</div>
	<div>
		<cfoutput>
			<lithe:lithe-post-lists limit=12 language="english">
			<div class="row-fluid">
				<cfif posts["totalrecord"] gt 0>
					<lithe:lithe-post-list>
						<cfif (post["currentrow"]%4) eq 1>
							<ul class="thumbnails">
						</cfif>
							<li class="span3">
								<div class="product">
								    <div class="image-product">
										<a href="#buildlink(post["slug"],'web/blogitem.cfm')#"><img src="#post["sthumb"]#" /></a>
									</div>
									<div class="posttitle">
										<a href="#buildlink(post["slug"],'web/blogitem.cfm')#"><h4>#post["title"]#</h4></a>
									</div>
									<div class="postprice">
										<p class="tag-p">#post["INTRO"]#</p>
									</div>
									<div class="tags-product">
								        <p class="tag-p"><i class="icon-tags"></i> Tags : 
									        <lithe:lithe-tag>
									  			<cfif tag["totalrecord"] neq 0>
													<a href="tag.cfm?tags=#tag["name"]#">
														#tag["name"]#
													</a>
													<cfif tag["currentrow"]	neq tag["totalrecord"]>
														,
													</cfif>
												</cfif>
											</lithe:lithe-tag>
										</p>
								    </div>
								</div>
							</li>
						<cfif (post["currentrow"]%4) eq 0>
							</ul>
						</cfif>
					</lithe:lithe-post-list>
				</cfif>
			</div>
			<div class="row-fluid">
				<lithe:lithe-post-list-page>
			</div>
		</script>
		</lithe:lithe-post-lists>
		</cfoutput>
	</div>
</div>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">