<cfimport taglib="/cfc/tags" prefix="lithe">
<cfset activepage = 'Blog'>
<cfinclude template="include/header.cfm">
<lithe:lithe-post-detail>
<div class="container">
	<cfoutput>
	<div class="row-fluid">
		<lithe:lithe-breadcrumb homeurl="web" slug="#slug#"  template="defaultbreadcrumb"/>
	</div>
	<div class="row-fluid">
		<cfif post["totalrecord"] gt 0>
			<div class="blog-item span9">
				<h3>#post["TITLE"]#</h3>
				<img src="#post["LTHUMB"]#" class="blog-image">
				<p>#post["DETAIL"]#</p>
				<div class="blog-item-panel">
	                <ul>
	                    <li class="date">
	                      <p><i class="icon-calendar"></i>#post["UPDATEDAT"]#</p></li>
	                    <li><p><i class="icon-user"></i>#post["AUTHOR"]#</p></li>
	                    <li><p><i class="icon-tags"></i>
							<lithe:lithe-tag>
								<cfif tag["totalrecord"] neq 0>
									<a href="tag.cfm?tags=#tag["name"]#">#tag["name"]#</a>
									<cfif tag["currentrow"]	neq tag["totalrecord"]>
										,
									</cfif>
								</cfif>
							</lithe:lithe-tag>
	                    </p></li>
	                </ul>
	            </div>
	            <div class="blog-item-panel">
                    <lithe:lithe-gallery-image>
                    	<cfif image['totalrecord'] gt 0>
							<cfif (image["currentrow"]%3) eq 1>
					            <ul class="thumbnails">
							</cfif>
				            <li class="span4">
					            <div class="product">
					                <div class="image-product">
					                    <img src="#image["image"]#" />
					                </div>
					                <div class="posttitle">
						                <a href="#image["link"]#"><h4>#image["title"]#</h4></a>
						            </div>
				                    <div class="postdetail"><p>#image["detail"]#</p></div>
					            </div>
					        </li>
							<cfif (image["currentrow"]%3) eq 0>
					            </ul>
							</cfif>
						</cfif>
			        </lithe:lithe-gallery-image>
	        	</div>
	            <div class="comments row-fluid">
	    			<div id="disqus_thread"></div>
					<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
					<a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
	    		</div>
			</div>
			<div class="span3">
				<ul class="relate-blog-item">
					<cfif post["relateditemtotalrecord"] gt 0>
						<lithe:lithe-relateditem>
							<li>
								<h4>#relateditem["TITLE"]#</h4>
								<img src="#relateditem["STHUMB"]#">
								<p class="relate-blog-intro">#relateditem["INTRO"]#</p>
								<a class="post-more" href="#buildlink(relateditem["SLUG"],'web/blogitem.cfm')#">Continue Reading&nbsp;&raquo;</a>
							</li>
						</lithe:lithe-relateditem>
					</cfif>
				</ul>
			</div>
		<cfelse>
			<h3>No data</h3>
		</cfif>
	</div>
	</cfoutput>
</div>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">
</lithe:lithe-post-detail>
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
    var disqus_shortname = 'skoodelithesample'; // required: replace example with your forum short name

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>