<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/3/13 AD
  Time: 10:40 AM
  To change this template use File | Settings | File Templates.
--->

<cfimport taglib="/cfc/tags" prefix="lithe">
<cfprocessingdirective pageEncoding="utf-8"/>
<cfset activepage = 'Form'>
<cfinclude template="include/header.cfm">
<div class="container">
	<div class="row-fluid">
		<cfoutput>
			<ul class="breadcrumb">
				<li><a href="#buildotherlink("web")#">Home</a> <span class="divider">/</span></li>
				<li class="active">Form New</li>
			</ul>
		</cfoutput>
	</div>
	<div class="row-fluid">
		<cfoutput>
			<!--- error, warning, notice --->
			<cfif flashKeyExists("success")>
				<div class="alert alert-success">
				#flash("success")#
				</div>
			</cfif>
			<cfif flashKeyExists("error")>
				<cfset ERROR = true>
				<div class="alert alert-error">
				#flash("error")#
				</div>
			</cfif>
		</cfoutput>
			<cfoutput>
				<lithe:lithe-form-new collectionname="myform" formaction="/web/formsave.cfm" formclass="form-horizontal row-fluid" recaptcha=true emailtocustomer=false emailcustomertemplate="order.txt" emailtoadmin=false emailadmintemplate="order.txt">
					<div class="control-group">
						<label class="control-label">Title</label>
						<div class="controls">
							<input type="text" name="title" class="span11" required>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Email</label>
						<div class="controls">
							<input type="email" name="email" class="span11" required>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Gender</label>
						<div class="controls">
							<label class="radio"><input type="radio" name="gender" value="Male" required>Male</label>
							<label class="radio"><input type="radio" name="gender" value="Female" required>Female</label>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">National</label>
						<div class="controls">
							<select name="national" class="span11">
								<option value="Thailand">Thailand</option>
								<option value="Malaysia">Malaysia</option>
								<option value="Vietnam">Vietnam</option>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Detail</label>
						<div class="controls">
							<textarea name="detail" class="span11"></textarea>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label"></label>
						<div class="controls">
							<lithe:recaptcha/>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label"></label>
						<div class="controls">
							<input type="submit" value="Submit" class="btn btn-primary">
							<input type="reset" value="Reset" class="btn">
						</div>
					</div>
				</lithe:lithe-form-new>
			</cfoutput>
	</div>
</div>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">
