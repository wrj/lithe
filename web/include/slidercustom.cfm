<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 8/28/13 AD
  Time: 2:27 PM
  To change this template use File | Settings | File Templates.
--->
<cfimport taglib="/cfc/tags" prefix="lithe">
<lithe:lithe-slider slug="Slider">
<cfif slider["totalrecord"] gt 0>
<div class="row-fluid slideshow">
    <div class="slider-box">
        <div id="myCarousel" class="carousel slide">
            <!-- Carousel items -->
            <div class="carousel-inner">
                <cfoutput>
                <lithe:lithe-gallery-image>
	                <cfif image["totalrecord"] gt 0>
	                    <div class="item <cfif image['currentRow'] == 1>active</cfif>">
	                        <img src="#image["IMAGE"]#" class="slidethumb">
	                        <div class="carousel-caption">
	                            <a href="#image["LINK"]#">
		                            <h4>#image["TITLE"]#</h4>
		                            <p>#image["DETAIL"]#</p>
		                        </a>
	                        </div>
	                    </div>
	                </cfif>
                </lithe:lithe-gallery-image>
                </cfoutput>
            </div>
            <!-- Carousel nav -->
            <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
            <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
        </div>
    </div>
</div>
</cfif>
</lithe:lithe-slider>