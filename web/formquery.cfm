<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/17/13 AD
  Time: 10:50 AM
  To change this template use File | Settings | File Templates.
--->
<cfparam name="url.dateformat" default="dd-mm-yyyy">
<cfinclude template="../cfc/dbregist.cfm">
<cfset sortvalue = 1/>
<cfif not isDefined('url.iSortCol_0')>
	<cfset url.iSortCol_0 = 0>
	<cfset url.sSortDir_0 = 1>
	<cfset url.sSearch = "">
	<cfset url.sEcho = 1>
	<cfset url.iDisplayStart =0>
	<cfset url.iDisplayLength = 10>
</cfif>
<cfset participantcount = 0>
<cfset participantdata = arrayNew()>
<cfset searchOr = structNew()>
<cfset searchArr = arrayNew()>
<cfset reg = structNew()>
<cfset realcollectionname = "form_#url.collectionname#">
<cfset readstruct = MongoCollectionFindOne(datasource=databasename,collection=realcollectionname,query={})>
<cfif isdefined("readstruct")>
	<cfset datakeylist = structkeylist(readstruct,',')>
	<cfset sortlabel = listGetAt(datakeylist,(Val(url.iSortCol_0) + 1))/>
	<cfif url.sSortDir_0 eq "desc">
		<cfset sortvalue = -1/>
	</cfif>
	<cfset sortst = structNew()>
	<cfset sortst[sortlabel] = sortvalue>
	<cfif url.sSearch IS NOT "">
		<cfset reg['$regex'] = ".*#url.sSearch#.*">
		<cfset reg['$options']= "i">
		<cfset ignorelist="_id,CREATEDAT,UPDATEDAT">
		<cfloop list="#datakeylist#" delimiters="," index="item">
			<cfif listfindnocase(ignorelist,item,',') eq 0>
				<cfset search = structNew()>
				<cfset search[item] = reg>
				<cfset arrayAppend(searchArr,search)>
			</cfif>
		</cfloop>
		<cfset searchOr['$or'] = searchArr>
		<cfset mydatatotal = MongoCollectioncount(datasource=databasename,collection=realcollectionname,query=searchOr)>
		<cfset mydata = MongoCollectionFind(datasource=databasename,collection=realcollectionname,query=searchOr,skip=url.iDisplayStart,size=url.iDisplayLength,sort=sortst)>
		<cfelse>
		<cfset mydatatotal = MongoCollectioncount(datasource=databasename,collection=realcollectionname,query=searchOr)>
		<cfset mydata = MongoCollectionFind(datasource=databasename,collection=realcollectionname,query=searchOr,skip=url.iDisplayStart,size=url.iDisplayLength,sort=sortst)>
	</cfif>
	<cfset k=0>
	<cfsavecontent variable="output">
		<cfoutput>
			{ "sEcho":#url.sEcho#,"iTotalRecords":"#mydatatotal#","iTotalDisplayRecords": "#mydatatotal#","aaData":[
			<cfloop array="#mydata#" index="i">
				<cfif k GT 0>,</cfif>
				[
				<cfloop list="#url.showfield#" delimiters="," index="field">
					<cfif field EQ "CREATEDAT">
						"<cfif structKeyExists(i,field)>#dateformat(i[field],url.dateformat)#</cfif>",
						<cfelseif field EQ "UPDATEDAT">
						"<cfif structKeyExists(i,field)>#dateformat(i[field],url.dateformat)#</cfif>",
						<cfelse>
						"<cfif structKeyExists(i,field)>#jsStringFormat(i[field])#</cfif>",
					</cfif>
				</cfloop>

				"<a class='edit_row' href='<cfif right(url.edittemplate,1) IS "?">#url.edittemplate#key=#i['_id'].toString()#&collectionname=#url.collectionname#<cfelse>#url.edittemplate#&key=#i['_id'].toString()#&collectionname=#url.collectionname#</cfif>'>Edit</a>",
				"<a class='delete_row' href='<cfif right(url.deletetemplate,1) IS "?">#url.deletetemplate#key=#i['_id'].toString()#&collectionname=#url.collectionname#<cfelse>#url.deletetemplate#&key=#i['_id'].toString()#&collectionname=#url.collectionname#</cfif>'>Delete</a>"
				]
				<cfset k++>
			</cfloop>
		</cfoutput>
		]}
	</cfsavecontent>
	<cfelse>
	<cfsavecontent variable="output">
		<cfoutput>
			{"sEcho":#url.sEcho#,"iTotalRecords":"0","iTotalDisplayRecords": "0","aaData":[]}
		</cfoutput>
	</cfsavecontent>
</cfif>

<cfoutput>#output#</cfoutput>