<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 8/28/13 AD
  Time: 3:50 PM
  To change this template use File | Settings | File Templates.
--->

<cfimport taglib="/cfc/tags" prefix="lithe">
<cfset activepage = 'Gallery'>
<cfinclude template="include/header.cfm">
<div class="container">
<div class="row-fluid">
	<cfoutput>
	    <ul class="breadcrumb">
	        <li><a href="#buildotherlink("web")#">Home</a> <span class="divider">/</span></li>
	        <li class="active">Gallery</li>
	    </ul>
	</cfoutput>
</div>
<div>
<cfoutput>
    <lithe:lithe-gallery-lists limit=12>
    <div class="row-fluid">
	<cfif galleries["totalrecord"] gt 0>
            <lithe:lithe-gallery-list>
			<cfif (gallery["currentrow"]%4) eq 1>
                <ul class="thumbnails">
			</cfif>
                <li class="span3">
	                <div class="gallerydetail">
	                    <div class="image-product">
	                        <a href="gallerydetail.cfm?slug=#gallery["slug"]#"><img src="#gallery["sthumb"]#" /></a>
			            </div>
			            <div class="posttitle">
		                    <a href="gallerydetail.cfm?slug=#gallery["slug"]#"><h4>#gallery["title"]#</h4></a>
			            </div>
		            </div>
                </li>
				<cfif (gallery["currentrow"]%4) eq 0>
	                </ul>
				</cfif>
            </lithe:lithe-gallery-list>
	</cfif>
    </div>
        <div class="row-fluid">
            <lithe:lithe-list-page template="defaultpage"/>
        </div>
    </lithe:lithe-gallery-lists>
</cfoutput>
        </div>
</div>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">