<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/4/13 AD
  Time: 2:38 PM
  To change this template use File | Settings | File Templates.
--->

<cfimport taglib="/cfc/tags" prefix="lithe">
<cfprocessingdirective pageEncoding="utf-8"/>
<cfset activepage = 'About'>
<cfinclude template="include/header.cfm">
<cfoutput>
	<div class="container">
		<div class="row-fluid">
			<ul class="breadcrumb">
				<li><a href="index.cfm">Home</a> <span class="divider">/</span></li>
				<li class="active">Categories</li>
			</ul>
		</div>
		<div class="row-fluid">
			<cfoutput>
				<!--- error, warning, notice --->
				<cfif flashKeyExists("success")>
						<div class="alert alert-success">
							#flash("success")#
						</div>
				</cfif>
				<cfif flashKeyExists("error")>
					<cfset ERROR = true>
						<div class="alert alert-error">
							#flash("error")#
						</div>
				</cfif>
			</cfoutput>
			<form action="checklogin.cfm" method="post" class="form-horizontal row-fluid loginform">
				<div class="control-group">
					<label class="control-label">Username</label>
					<div class="controls">
						<input type="text" name="username" class="username span5" required>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">Password</label>
					<div class="controls">
						<input type="password" name="password" class="password span5" required>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"></label>
					<div class="controls">
						<button type="submit" class="btn btn-primary">Login</button>
					</div>
				</div>
			</form>

		</div>
	</div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">