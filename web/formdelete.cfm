<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/4/13 AD
  Time: 10:54 AM
  To change this template use File | Settings | File Templates.
--->
<cfimport taglib="/cfc/tags" prefix="lithe">
<cfprocessingdirective pageEncoding="utf-8"/>
<cfset activepage = 'FormUpdate'>
<cfinclude template="include/header.cfm">
<cfinclude template="../cfc/formcontroller.cfm">
<cfset output = formdelete(url.collectionname,url.key)>
<cfif output IS 1>
	<cfset flashinsert('success','Delete success')>
<cfelse>
	<cfset flashinsert('error','Delete error')>
</cfif>
<cflocation url = "formlist.cfm">
<cfinclude template="include/footer.cfm">