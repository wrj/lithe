<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/4/13 AD
  Time: 11:38 AM
  To change this template use File | Settings | File Templates.
--->

<cfimport taglib="/cfc/tags" prefix="lithe">
<cfprocessingdirective pageEncoding="utf-8"/>
<cfset activepage = 'FormEdit'>
<cfinclude template="include/header.cfm">
<div class="container">
	<div class="row-fluid">
		<cfoutput>
			<ul class="breadcrumb">
				<li><a href="#buildotherlink("web")#">Home</a> <span class="divider">/</span></li>
				<li class="active">Form Edit</li>
			</ul>
		</cfoutput>
	</div>
	<div class="row-fluid">

<!---		<cfif structKeyExists(session,"form_username") >--->
			<cfinclude template="../cfc/formcontroller.cfm">
			<cfset formdata = formedit(url.collectionname,url.key)>
			<cfif formdata.status NEQ 0 >
<!---				<cfdump var="#formdata#">--->
<!---				<cfabort>--->
			<cfoutput>
				<form action="formupdate.cfm" method="post" class="form-horizontal row-fluid">
					<div class="control-group">
						<label class="control-label">Title</label>
						<div class="controls">
							<input type="text" name="title" class="span11" value="#formdata['TITLE']#" required>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Email</label>
						<div class="controls">
							<input type="email" name="email" class="span11" value="#formdata['EMAIL']#" required>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Gender</label>
						<div class="controls">
							<cfif formdata['GENDER'] IS "Male">
								<label class="radio"><input type="radio" name="gender" value="Male" required checked>Male</label>
								<label class="radio"><input type="radio" name="gender" value="Female" required>Female</label>
							<cfelseif formdata['GENDER'] IS "Female">
								<label class="radio"><input type="radio" name="gender" value="Male" required>Male</label>
								<label class="radio"><input type="radio" name="gender" value="Female" required checked>Female</label>
							</cfif>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">National</label>
						<div class="controls">
							<select name="national" class="span11">
								<cfif formdata['NATIONAL'] IS "Thailand">
										<option value="Thailand" selected="">Thailand</option>
										<option value="Malaysia">Malaysia</option>
										<option value="Vietnam">Vietnam</option>
								<cfelseif formdata['NATIONAL'] IS "Malaysia">
										<option value="Thailand">Thailand</option>
										<option value="Malaysia" selected>Malaysia</option>
										<option value="Vietnam">Vietnam</option>
								<cfelseif formdata['NATIONAL'] IS "Vietnam">
										<option value="Thailand">Thailand</option>
										<option value="Malaysia">Malaysia</option>
										<option value="Vietnam" selected>Vietnam</option>
								</cfif>
							</select>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">Detail</label>
						<div class="controls">
							<textarea name="detail" class="span11">#formdata['DETAIL']#</textarea>
						</div>
					</div>

					<div class="control-group">
						<div class="controls">
							<input type="hidden" name="key" value="#formdata['id']#"/><br/>
							<input type="hidden" name="collectionname" value="#url['collectionname']#"/><br/>
							<button type="submit" class="btn">Update</button><br/>
						</div>
					</div>
				</form>
			</cfoutput>
			</cfif>

<!---</cfif>--->

	</div>
</div>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">