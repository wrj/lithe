<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 11/20/13 AD
  Time: 5:02 PM
  To change this template use File | Settings | File Templates.
--->

<cfimport taglib="/cfc/tags" prefix="lithe">
<cfset activepage = 'Categories'>
<cfinclude template="include/header.cfm">
	<cfoutput>
		<div class="container">
			<div class="row-fluid">
				<ul class="breadcrumb">
					<li><a href="#buildotherlink("web")#">Home</a> <span class="divider">/</span></li>
					<li class="active">Categories</li>
				</ul>
			</div>
			<div class="row-fluid">
				<lithe:lithe-category-lists url="web/categorydetail.cfm">
					<lithe:lithe-category-list>
						<div class="gallerydetail">
							<div class="posttitle">
								<img src="#category['IMAGEREF']#" alt="" width=40> #category['LINK']#  <br><br>
							</div>
						</div>
					</lithe:lithe-category-list>
				</lithe:lithe-category-lists>
			</div>
		</div>
	</cfoutput>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">
