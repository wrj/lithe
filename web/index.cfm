<cfimport taglib="/cfc/tags" prefix="lithe">
<cfset activepage = 'Home'>
<cfinclude template="include/header.cfm">
<div class="container">
	<cfinclude template="include/slidercustom.cfm">
	<cfoutput>
	        <lithe:lithe-post-lists limit=8 category="News" random=false>
	            <h3>News</h3>
			<cfif posts["totalrecord"] gt 0>
	                <lithe:lithe-post-list>
					<cfif (post["currentrow"]%4) eq 1>
	                    <ul class="thumbnails">
					</cfif>
	                    <li class="span3">
	                    <div class="product">
	                    <div class="image-product">
	                            <a href="#buildlink(post["slug"],'web/blogitem.cfm')#"><img src="#post['STHUMB']#" /></a>
	                </div>
	                <div class="posttitle">
	                        <a href="#buildlink(post["slug"],'web/blogitem.cfm')#"><h4>#post['TITLE']#</h4></a>
	                </div>
<!---	                <div class="postprice">--->
<!---	                <p class="tag-p"> Price : #post['PRICE']#</p>--->
<!---	                </div>--->
	                <div class="tags-product">
	                <p class="tag-p"><i class="icon-tags"></i> Tags :
	                <lithe:lithe-tag>
						<cfif tag["totalrecord"] neq 0>
	                            <a href="#buildotherlink("web/tag.cfm","tags",tag["name"])#">
								#tag["name"]#
	                            </a>
							<cfif tag["currentrow"]	neq tag["totalrecord"]>
	                                ,
							</cfif>
						</cfif>
	                    </lithe:lithe-tag>
	                    </p>
	                    </div>
	                    </div>
	                    </li>
					<cfif (post["currentrow"]%4) eq 0>
	                    </ul>
					</cfif>
	                </lithe:lithe-post-list>
			</cfif>
	        </lithe:lithe-post-lists>
	</cfoutput>
	<cfoutput>
		<h3>Tags Clouds</h3>
		<lithe:lithe-tag-clouds>
			<cfif tagclouds["totalrecord"] NEQ 0>
				<lithe:lithe-tag>
					<cfif tag["totalrecord"] NEQ 0>
							<a href="#buildotherlink("web/tag.cfm","tags",tag["name"])#">#tag["name"]#</a>
						<cfif tag["currentrow"]	NEQ tag["totalrecord"]>
							,
						</cfif>
					</cfif>
				</lithe:lithe-tag>
			</cfif>
		</lithe:lithe-tag-clouds>
	</cfoutput>
</div>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">
<script type="text/javascript">
    jQuery(function() {
        jQuery('#myCarousel').carousel({})
    });
</script>