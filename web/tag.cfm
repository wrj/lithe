<cfimport taglib="/cfc/tags" prefix="lithe">
<cfset activepage = ''>
<cfinclude template="include/header.cfm">
<div class="container">
	<div class="row-fluid">
		<ul class="breadcrumb">
			<li><a href="#buildotherlink("web")#">Home</a> <span class="divider">/</span></li>
			<li class="active">Tag : <cfoutput>#tags#</cfoutput></li>
		</ul>
	</div>		
	<cfoutput>
		<lithe:lithe-post-lists limit=12>
			<cfif posts["totalrecord"] gt 0>
				<div class="row-fluid">
					<lithe:lithe-post-list>
						<cfif (post["currentrow"]%4) eq 1>
							<ul class="thumbnails">	
						</cfif>
							<li class="span3">
								<div class="product">
								    <div class="image-product">
										<a href="item.cfm?slug=#post["slug"]#"><img src="#post["STHUMB"]#" /></a>
									</div>
									<div class="posttitle">
										<a href="item.cfm?slug=#post["SLUG"]#"><h4>#post["TITLE"]#</h4></a>
										<p class="tag-p"> Price : #post["PRICE"]#</p>
									</div>
									<div class="postprice">
										<p class="tag-p"> Price : #post["PRICE"]#</p>
									</div>
									<div class="tags-product">
								        <p class="tag-p"><i class="icon-tags"></i> Tags : 
											<lithe:lithe-tag>
									  			<cfif tag["totalrecord"] gt 0>
									  				<a href="tag.cfm?tags=#tag["name"]#">
									  					#tag["name"]#
								  					</a>
									  				<cfif tag["currentrow"] lt tag["totalrecord"]>
									  					,
									  				</cfif>
									  			</cfif>
											</lithe:lithe-tag>
										</p>
								    </div>
								</div>
							</li>
						<cfif (post["currentrow"]%4) eq 0>
							</ul>	
						</cfif>
					</lithe:lithe-post-list>
				</div>
				<div class="row-fluid">
					<lithe:lithe-post-list-page>
				</div>
			<cfelse>
				<div class="row-fluid">
					<h3>No data</h3>
				</div>
			</cfif>
		</lithe:lithe-post-lists>
	</cfoutput>
</div>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">