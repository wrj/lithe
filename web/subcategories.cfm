<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 11/20/13 AD
  Time: 5:20 PM
  To change this template use File | Settings | File Templates.
--->

<cfimport taglib="/cfc/tags" prefix="lithe">
<cfset activepage = 'Work'>
<cfinclude template="include/header.cfm">
<div class="container">
	<div class="row-fluid">
		<ul class="breadcrumb">
			<li><a href="index.cfm">Home</a> <span class="divider">/</span></li>
			<li class="active">Work</li>
		</ul>
	</div>
	<div>
		<cfoutput>
			<lithe:lithe-subcategory-lists category="" limit=12>
				<div class="row-fluid">
					<cfif subcategories["totalrecord"] gt 0>
							<lithe:lithe-subcategory-list>
							<cfif (subcategory["currentrow"]%4) eq 1>
								<ul class="thumbnails">
							</cfif>
								<li class="span3">
									<div class="gallerydetail">
										<div class="image-product">
											<a href="#buildotherlink("web/news.cfm","category",subcategory["SLUG"])#"><img src="#subcategory["IMAGEREF"]#"/></a>
										</div>
										<div class="posttitle">
											<a href="#buildotherlink("web/news.cfm","category",subcategory["SLUG"])#"><h4>#subcategory["TITLE"]#</h4></a>
										</div>
									</div>
								</li>
							<cfif (subcategory["currentrow"]%4) eq 0>
								</ul>
							</cfif>
							</lithe:lithe-subcategory-list>
					</cfif>
				</div>
				<div class="row-fluid">
					<lithe:lithe-list-page template="defaultpage"/>
				</div>
			</lithe:lithe-subcategory-lists>
		</cfoutput>
	</div>
</div>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">