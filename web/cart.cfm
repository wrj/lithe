<cfset activepage = 'Shopping Cart'>
<cfinclude template="include/header.cfm">
<div class="container">
	<div class="row-fluid">
		<ul class="lithe-breadcrumbs breadcrumb"
		data-lithe-category="Blog" >
			<script type="text/x-jsrender">
				<li><a href="index.cfm">Home</a> <span class="divider">/</span></li>
				<li class="active"><cfoutput>#activepage#</cfoutput></li>
			</script>
		</ul>
	</div>

	<div class="lithe-shopping-cart" data-lithe-updatefield-class="updatetext" 
		            data-lithe-remove-allproduct-button-class="btn btn-danger removeall" 
		            data-lithe-limit="5">
	    <script type="text/x-jsrender">
	        {{for shoppingcart}}
		        <div class="row-fluid">
		        	<div class="span5 totalitem">
		            	<p>You have <span class="totallist">{{:total_item}}</span> item in your shopping cart</p>
		        	</div>
		        </div>
	            <div class="row-fluid">
					<div class="pagination pagination-left">
	            		{{pagination model=#parent.data.pagination/}}
	            	</div> 
				</div>
				<div class="row-fluid">
					<div class="span12">
						<table class="table table-bordered tableshoppingcart">
							<thead>
								<tr class="tablehead">
									<th class="tableitemsimg ">Items</th>
									<th class="tabledetail ">Detail</th>
									<th class="tableprice ">Price</th>
									<th class="tablequatity ">Quantity</th>
									<th class="tabletotalprice ">Total</th>
									<th class="tableremoveproduct ">Remove</th>
								</tr>
							</thead>
							<tbody>
								{{for products}}
									<tr>
										<td class="tableitemsimg ">
												<img src="{{:OTHER.STHUMB}}" alt="{{:NAME}}" class="shoppig-img span12">
										</td>
										<td class="tabledetail ">
											<div>{{:NAME}} - {{:OTHER.CATEGORYTITLE}}</div>
											<div>SKU : {{:OTHER.SKU}}</div>
											
										</td>
										<td class="tableprice ">{{numberFormat model=PRICE/}}</td>
										<td class="tablequatity textaligncenter ">
											
											<div class="textaligncenter">{{:UPDATE}}<div>
										</td>
										<td class="tabletotalprice">{{numberFormat model=TOTAL/}}</td>
										<td class="tableremoveproduct ">{{:REMOVE}}</td>
									</tr>
								{{/for}}
								{{if products.length == 0}}
									<tr>
										<td colspan="6">
											<a href="product.cfm?category=Product">Shopping cart is empty. Go Back</a>
										</td>
									</tr>
								{{/if}}
							</tbody>
						</table>
					</div>
				</div>
	        {{/for}}
	        <div class="row-fluid">
	            <div class="span2 offset10">
	            	<div class="totalprice"><p class="grandtotal">Grand Total : {{numberFormat model=shoppingcart.total_price/}}</p></div>
	            </div>
	        </div>
	        
		   	<div class="row-fluid">
		   		<div class="span12">
		   			<div class="continueshopping span3">
		   				<a href="product.cfm?category=Product" class="btn"><span class="">Continue Shopping</span></a>
		   			</div>
		   			<div class="btnremoveallproduct span3">
		   				{{if shoppingcart.products.length > 0}}
		            		{{:shoppingcart.remove_allproduct}}
		            	{{/if}}
		            </div>	
		   			<div class="pull-right">
		   				{{if shoppingcart.products.length > 0}}
		   					<a href="payment.cfm" class="btn btn-primary">Shopping Payment</a>
		   				{{/if}}
		   			</div>
		   		</div>
		   	</div>	
	    </script>
	</div>
</div>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">