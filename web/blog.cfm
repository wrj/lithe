<cfimport taglib="/cfc/tags" prefix="lithe">
<cfset activepage = 'Blog'>
<cfinclude template="include/header.cfm">
<div class="container">
	<div class="row-fluid">
		<lithe:lithe-breadcrumb homeurl="web" category="Blog"  template="defaultbreadcrumb"/>
	</div>
	<div class="row-fluid">
		<lithe:lithe-post-lists category="Blog" limit=5>
			<cfoutput>
			<cfif posts["totalrecord"] gt 0>
				<lithe:lithe-list-page classdesktop="pagination-right"/>
				<lithe:lithe-post-list>
					<div class="blog-item">
						<h3>#post["title"]#</h3>
						<img src="#post["lthumb"]#">
						<p>#post["intro"]#</p>
						<a class="post-more" href="#buildlink(post["slug"],'web/blogitem.cfm')#">Continue Reading&nbsp;&raquo;</a>
						<div class="blog-item-panel">
	                        <ul>
	                            <li class="date">
	                                <p><i class="icon-calendar"></i>
	                                #post["updatedat"]#
	                                </p>
	                            </li>
							    <li>
							        <p><i class="icon-user"></i>
							            #post["AUTHOR"]#
							        </p>
							    </li>
							    <li><p><i class="icon-tags"></i>
							        <lithe:lithe-tag>
							            <cfif tag["totalrecord"] gt 0>
							                #tag["name"]#
							                <cfif tag["currentrow"] lt tag["totalrecord"]>
							                    ,
							                </cfif>
							            </cfif>
									</lithe:lithe-tag>
							  </p></li>
							  </ul>
						  </div>
					</div>
				</lithe:lithe-post-list>
				<lithe:lithe-list-page template="defaultpage" />
			</cfif>
			</cfoutput>
		</lithe:lithe-post-lists>
	</div>
</div>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">