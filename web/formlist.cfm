<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/4/13 AD
  Time: 10:26 AM
  To change this template use File | Settings | File Templates.
--->

<cfimport taglib="/cfc/tags" prefix="lithe">
<cfprocessingdirective pageEncoding="utf-8"/>
<cfset activepage = 'Form'>
<cfinclude template="include/header.cfm">
<div class="container">
	<div class="row-fluid">
		<cfoutput>
			<ul class="breadcrumb">
				<li><a href="#buildotherlink("web")#">Home</a> <span class="divider">/</span></li>
				<li class="active">Form List</li>
			</ul>
		</cfoutput>
	</div>
	<div class="row-fluid">

<!---<cfif structKeyExists(session,"form_username") >--->
		<cfoutput>
			<!--- error, warning, notice --->
			<cfif flashKeyExists("success")>
				<div class="alert alert-success">
				#flash("success")#
				</div>
			</cfif>
			<cfif flashKeyExists("error")>
				<cfset ERROR = true>
				<div class="alert alert-error">
				#flash("error")#
				</div>
			</cfif>
		</cfoutput>
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped createTableData display applicationTable">
			<thead>
				<tr>
					<th width="">title</th>
					<th width="">email</th>
					<th width="">create</th>
					<th width="">update</th>
					<th width="">delete</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="7" class="dataTables_empty">โปรดรอ ...</td>
				</tr>
			</tbody>
			<tfoot>
			<tr>
				<th width="">title</th>
				<th width="">email</th>
				<th width="">create</th>
				<th width="">update</th>
				<th width="">delete</th>
			</tr>
			</tfoot>
		</table>

<!---</cfif>--->
	</div>
</div>

<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">

<cfoutput>
	<lithe:lithe-form-lists
			showfield="TITLE,EMAIL,CREATEDAT"
			collectionname="myform"
			tableclass="applicationTable"
			service="http://#CGI.HTTP_HOST#/web/formquery.cfm"
		/>
</cfoutput>