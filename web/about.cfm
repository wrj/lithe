<cfimport taglib="/cfc/tags" prefix="lithe">
<cfset activepage = 'About'>
<cfinclude template="include/header.cfm">
<lithe:lithe-html slug="About">
	<cfoutput>
	<div class="container">
		<div class="row-fluid">
			<ul class="breadcrumb">
				<li><a href="#buildotherlink("web")#">Home</a> <span class="divider">/</span></li>
				<li class="active">About</li>
			</ul>
		</div>
		<div class="row-fluid">
			<h3>#html["title"]#</h3>
			<div class="thumbnail-about">
				<img src="#html["LTHUMB"]#">
			</div>
			<p>#html["detail"]#</p>
			<lithe:lithe-gallery-image>
				<cfif image["totalrecord"] gt 0>
					<cfif (image["currentrow"]%4) eq 1>
			            <ul class="thumbnails">
					</cfif>
		                <li class="span3">
			                <div class="product">
				                <div class="image-product">
				                        <img src="#image["image"]#" />
					            </div>
					            <div class="posttitle">
				                    <a href="#image["link"]#"><h4>#image["title"]#</h4></a>
					            </div>
								<div class="postdetail"><p>#image["detail"]#</p></div>
				            </div>
			            </li>
					<cfif (image["currentrow"]%4) eq 0>
		             </ul>
		            </cfif>
				</cfif>
			</lithe:lithe-gallery-image>
		</div>
	</div>
	</cfoutput>
</lithe:lithe-html>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">
