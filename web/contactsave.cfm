<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 9/17/13 AD
  Time: 4:49 PM
  To change this template use File | Settings | File Templates.
--->

<cfprocessingdirective pageEncoding="utf-8"/>
<cfimport taglib="/cfc/tags" prefix="lithe">
<lithe:recaptcha action="check"/>
<cfif form.option_userecaptcha IS TRUE AND recaptcha IS FALSE>
	<cfset message="error=#form.option_errorrecaptchamessage#">
	<cfelse>
	<cfset save_filepath="">
	<cfset message="">
	<cfset imageuploadurl="">
	<cfif isdefined("form.file") AND NOT form.file IS "">
		<cfhttp url="http://#CGI['HTTP_HOST']#/cfc/services/contactus.cfc?" method="post" result="objdata">
			<cfhttpparam type="url" name="method" value="uploadFile"/>
			<cfhttpparam type="file" name="file" file="#form.file#"/>
		</cfhttp>
		<cfset save_filepath = objdata['filecontent']>
		<cfset imageuploadurl="http://#CGI.HTTP_HOST#/assets/upload/contactus/#deserializeJSON(save_filepath)['file']#">
	</cfif>
	<cfif isDefined("form.title") AND isDefined("form.email") AND isDefined("form.detail")>
		<cfif IsValid("email", form.email)>
			<cfhttp url="http://#CGI['HTTP_HOST']#/cfc/services/contactus.cfc?" method="post" result="objdata2">
				<cfhttpparam type="url" name="method" value="savecontactus"/>
				<cfhttpparam type="url" name="title" value="#form.title#"/>
				<cfhttpparam type="url" name="email" value="#form.email#"/>
				<cfhttpparam type="url" name="detail" value="#form.detail#"/>
				<cfhttpparam type="url" name="filepath" value="#save_filepath#"/>
			</cfhttp>
			<cfset issavedata = deserializeJSON(objdata2['filecontent'])>
			<cfif issavedata.status IS 1>
				<cfset message="success=#form.option_successmessage#">
			</cfif>
			<cfif form.option_useemail >
				<cfhttp url="http://#CGI['HTTP_HOST']#/cfc/services/contactus.cfc?" method="post" result="objdata3">
					<cfhttpparam type="url" name="method" value="sendemail"/>
					<cfhttpparam type="url" name="template" value="#form.option_emailtemplate#"/>
					<cfhttpparam type="url" name="title" value="#form.title#"/>
					<cfhttpparam type="url" name="email" value="#form.email#"/>
					<cfhttpparam type="url" name="detail" value="#form.detail#"/>
					<cfhttpparam type="url" name="filepath" value="#imageuploadurl#"/>
				</cfhttp>
				<cfset issendemaildata = deserializeJSON(objdata3['filecontent'])>
				<cfif issendemaildata.status IS 1>
					<cfset message="success=#form.option_successmessage#">
				</cfif>
			</cfif>
			<cfelse>
			<cfset message="error=#form.option_errormessage#">
		</cfif>
		<cfelse>
		<cfset message="error=#form.option_errormessage#">
	</cfif>
</cfif>
<cflocation url = "#form.option_formredirect#?#message#">