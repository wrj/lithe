<cfprocessingdirective pageEncoding="utf-8"/>
<cfimport taglib="/cfc/tags" prefix="lithe">
<cfset activepage = 'Contact Us'>
<cfinclude template="include/header.cfm">
<lithe:lithe-html slug="Contact">
<div class="container">
	<div class="row-fluid">
		<cfoutput>
			<ul class="breadcrumb">
				<li><a href="#buildotherlink("web")#">Home</a> <span class="divider">/</span></li>
				<li class="active">Contact Us</li>
			</ul>
		</cfoutput>
	</div>
	<div>
		<div class="flash-session">
		<cfif isdefined("success")>
			<cfoutput>
					<div class="alert alert-success">#success#</div>
			</cfoutput>
		</cfif>
		<cfif isdefined("error")>
			<cfoutput>
					<div class="alert alert-error">#error#</div>
			</cfoutput>
		</cfif>
		</div>
	</div>
	<div class="row-fluid">

		<div class="span4">
			<lithe:lithe-contact recaptcha=true email=false>
    			<h3>Contact Us</h3>

		        <div class="control-group">
		        	<label class="control-label">Subject
		        	</label>
		        	<div class="controls">
				        <select name="title" required="required">
				            <option value="ติดต่อแจ้งข้อผิดพลาด">ติดต่อแจ้งข้อผิดพลาด</option>
				            <option value="ติดต่อขอรับบริการ">ติดต่อขอรับบริการ</option>
			            </select>
		        	</div>
		        </div>
		        <div class="control-group" required="required">
		        	<label class="control-label">Email
		        	</label>
		        	<div class="controls">
				        <input type="text" id="inputName" placeholder="Email" name="email"/>
		        	</div>
		        </div>
		        <div class="control-group" required="required">
		        	<label class="control-label">Detail
		        	</label>
		        	<div class="controls">
				        <textarea name="detail" ></textarea>
		        	</div>
		        </div>
				<div class="control-group">
					<label class="control-label">File
					</label>
					<div class="controls">
						<input type='file' name='file'>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">
					</label>
					<div class="controls">
						<lithe:recaptcha/>
					</div>
				</div>
		        <div><input type="submit" class="btn" value="Send Message"></div>

			</lithe:lithe-contact>
		</div>
		<div class="span8">
			<h3>Map</h3>
			<div class="map">
				<img src="img/map.png">
			</div>
		</div>
	</div>
</div>
</lithe:lithe-html>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">