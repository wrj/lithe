<cfset activepage = 'Shopping Cart'>
<cfinclude template="include/header.cfm">
<div class="container">
    <div class="row-fluid">
        <ul class="lithe-breadcrumbs breadcrumb"
        data-lithe-category="Blog" >
            <script type="text/x-jsrender">
                <li><a href="index.cfm">Home</a> <span class="divider">/</span></li>
                <li class="active">Payment</li>
            </script>
        </ul>
    </div>
    <div class="col-wrapper">
        <div class="row show-grid">
            <!-- START LEFT-SIDE CONTACT FORM AREA -->
            <div class="contact-form span12">
                <h3>Payment</h3>
                <!-- <form id="validForm"> -->
                <div class="lithe-shopping-payment" 
                data-lithe-shopping-name-attrbutes="placeholder='Name'"
                data-lithe-shopping-address-attrbutes="placeholder='Address'"
                data-lithe-shopping-address-continued-attrbutes="placeholder='Address  Continued'"
                data-lithe-shopping-city-attrbutes="placeholder='City'"
                data-lithe-shopping-zip-attrbutes="placeholder='Zip'"
                data-lithe-shopping-phone-number-attrbutes="placeholder='Phone Number'"
                data-lithe-billing-name-attrbutes="placeholder='Name'"
                data-lithe-billing-address-attrbutes="placeholder='Address'"
                data-lithe-billing-address-continued-attrbutes="placeholder='Address  Continued'"
                data-lithe-billing-city-attrbutes="placeholder='City'"
                data-lithe-billing-zip-attrbutes="placeholder='Zip'"
                data-lithe-billing-phone-number-attrbutes="placeholder='Phone Number'"
                data-lithe-submit-goto="/quotereceipt.cfm"
                data-lithe-submit-class="btn btn-primary"
                
                >
                    <script type="text/x-jsrender">
                    <div class="{{:facebook.notloginclass}}">
    					Please login with facebook before payment.
    				</div>
    				<div class="{{:facebook.loggedinclass}}">
                        {{for shoppingpayment}}
                        {{:startform}}

                        <div class="row form-horizontal">
                            {{for shipping}}
                            <div class="span6 ">
                            	<div class="paymentshippingaddress">
                            		<h3>Your Shipping Address </h3>

                                <div class="control-group">
                                    <label class="control-label">Name (first and last)<sup>*</sup></label>

                                    <div class="controls">
                                        {{:name}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label"
                                          >Address<sup>*</sup></label>

                                    <div class="controls">
                                        {{:address}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Address
                                        Continued<sup>*</sup></label>

                                    <div class="controls">
                                        {{:address_continued}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">State<sup>*</sup></label>

                                    <div class="controls">
                                        {{:state}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">City<sup>*</sup></label>

                                    <div class="controls">
                                        {{:city}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Tambon<sup>*</sup></label>

                                    <div class="controls">
                                        {{:tambon}}
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Zip<sup>*</sup></label>

                                    <div class="controls">
                                        {{:zip_code}}
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Phone
                                        Number<sup>*</sup></label>

                                    <div class="controls">
                                        {{:phone_number}}
                                    </div>

                                </div>
                            	</div>
                                
                            </div>
                            {{/for}}
                            {{for billing}}
                            <div class="span6 ">
                            	<div class="paymentbillingaddress">
                            		<div class="row-fluid">
                            			<div class="span6">
                            				<h3 class="">Your billing Address</h3>
                            			</div>
                            			 

                                			<div class="span6">

                                    			<div class="">
                                       				 <label class="checkbox">{{:same_shipping_address}} Same as my shipping address.</label>
                                    			</div>
                               				 </div>
                            		</div>

                                <div class="control-group">
                                    <label class="control-label" for="inputName">Name<sup>*</sup></label>

                                    <div class="controls">
                                        {{:name}}
                                        <!--                                                 <input type="text" class="span4" id="inputName" placeholder="Name"> -->
                                        <!--<span class="help-inline">Please fill your address</span>-->
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="inputName">Address<sup>*</sup></label>

                                    <div class="controls">
                                        {{:address}}
                                        <!--                                                 <input type="text" class="span4" id="inputName" placeholder="Name"> -->
                                        <!--<span class="help-inline">Please fill your address</span>-->
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Address
                                        Continued<sup>*</sup></label>

                                    <div class="controls">
                                        {{:address_continued}}
                                    </div>
                                </div>
    							<div class="control-group">
                                    <label for="textarea" class="control-label">State<sup>*</sup></label>

                                    <div class="controls">
                                        {{:state}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="textarea" class="control-label">City<sup>*</sup></label>

                                    <div class="controls">
                                        {{:city}}
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="textarea" class="control-label">Tambon<sup>*</sup></label>

                                    <div class="controls">
                                        {{:tambon}}
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="textarea" class="control-label">Zip<sup>*</sup></label>

                                    <div class="controls">
                                        {{:zip_code}}
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="textarea" class="control-label">Phone
                                        Number<sup>*</sup></label>

                                    <div class="controls">
                                        {{:phone_number}}
                                    </div>
                                </div>
                            	</div>
                                
                            </div>
                            {{/for}}
                        </div>
                        <div class="control-group form-button-offset btnsaveformpayment">
                            {{:submit}}
                            <input type="reset" class="btn" value="Clear Form">
                        </div>
                        {{:endform}}
                        {{/for}}
                        </div>
                    </script>
                </div>
                <!-- </form> -->
            </div>
            <!-- END LEFT-SIDE CONTACT FORM AREA -->
        </div>
    </div>
</div>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">