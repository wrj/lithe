<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 8/28/13 AD
  Time: 3:50 PM
  To change this template use File | Settings | File Templates.
--->

<cfimport taglib="/cfc/tags" prefix="lithe">
<cfset activepage = 'Gallery'>
<cfinclude template="include/header.cfm">
<cfoutput>
<div class="container">
<div class="row-fluid">
    <div class="row-fluid">
        <ul class="breadcrumb">
            <li><a href="#buildotherlink("web")#">Home</a> <span class="divider">/</span></li>
            <li><a href="#buildotherlink("web/gallery.cfm")#">Gallery</a> <span class="divider">/</span></li>
            <li class="active">#slug#</li>
        </ul>
    </div>
</div>
<div>

    <lithe:lithe-gallery-detail>
    <div class="row-fluid">
	<cfif gallery["totalrecord"] gt 0>
            <lithe:lithe-gallery-image>
			<cfif (image["currentrow"]%4) eq 1>
                <ul class="thumbnails">
					</cfif>
		                <li class="span3">
	                <div class="gallerydetail">
		                <div class="image-product">
                            <a class="fancybox" href="#image["image"]#" data-fancybox-group="gallery" title="#image["detail"]#"><img src="#image["image"]#" alt="#image["title"]#"/></a>
			            </div>
			            <div class="posttitle">
		                    <a href="#image["link"]#">
								<h4>#image["title"]#</h4>
							</a>
			            </div>
		            </div>
		            </li>
					<cfif (image["currentrow"]%4) eq 0>
                </ul>
			</cfif>
            </lithe:lithe-gallery-image>
	</cfif>
    </div>

    </lithe:lithe-gallery-detail>

        </div>
</div>

</cfoutput>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">

<script type="text/javascript">
    $(document).ready(function() {
        $('.fancybox').fancybox();
    });
</script>