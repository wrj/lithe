<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 11/20/13 AD
  Time: 6:06 PM
  To change this template use File | Settings | File Templates.
--->

<cfimport taglib="/cfc/tags" prefix="lithe">
<lithe:lithe-category-detail>
	<cfset activepage = 'Categories'>
	<cfinclude template="include/header.cfm">
	<cfoutput>
		<div class="container">
			<div class="row-fluid">
				<lithe:lithe-breadcrumb homeurl="web" category="#url.category#"  template="defaultbreadcrumb"/>
			</div>
			<div class="row-fluid">
				<cfif category["totalrecord"] NEQ 0>
					<h3>#category["TITLE"]#</h3>
					<div class="thumbnail-about">
						<img src="#category["IMAGEREF"]#">
					</div>
					<p>#category["DETAIL"]#</p>
				</cfif>
			</div>
		</div>
	</cfoutput>
</lithe:lithe-category-detail>
<cfinclude template="include/footer.cfm">
<cfinclude template="include/js.cfm">
