<cfimport taglib="/cfc/tags" prefix="lithe">
<cfset activepage = 'Product'>
<cfinclude template="include/header.cfm">
<lithe:lithe-product-detail>
<div class="container">
	<cfoutput>
		<div class="row-fluid">
			<lithe:lithe-breadcrumb homeurl="home" slug="#slug#"  template="defaultbreadcrumb"/>
		</div>
		<div class="row-fluid">
			<div class="blog-item-image span6">
				<img src="#product["LTHUMB"]#">
			</div>
			<div class="blog-item span6">
				<h4>#product["TITLE"]#</h4>
				<p>#product["INTRO"]#</p>
				<p>Price : #product["PRICE"]#</p>
				<lithe:lithe-product-detail-cart>
				<div class="clear_both"></div>
				<div class="blog-item-panel">
                    <ul>
                        <li class="date">
                          <p><i class="icon-th-list"></i>
                          	#product["CATEGORYTITLE"]#
                          </p>
                      	</li>
                        <li>
                        	<p><i class="icon-tags"></i>
								<lithe:lithe-tag>
								<cfif tag["totalrecord"] neq 0>
									<a href="tag.cfm?tags=#tag["name"]#">#tag["name"]#</a>
									<cfif tag["currentrow"]	neq tag["totalrecord"]>
										,
									</cfif>
								</cfif>
							</lithe:lithe-tag>
                        	</p>
                    	</li>
                    </ul>
                </div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="blog-item-description span12">
				<h4>Product Description</h4>
				<p>#product["DETAIL"]#</p>
			</div>
		</div>
		<h4>Galleries</h4>
		<div class="row-fluid">
		    <lithe:lithe-gallery-image>
		    	<cfif image['totalrecord'] gt 0>
					<cfif (image["currentrow"]%3) eq 1>
			            <ul class="thumbnails">
					</cfif>
			            <li class="span4">
				            <div class="product">
					            <div class="image-product">
					                    <img src="#image["image"]#" />
						        </div>
						        <div class="posttitle">
						                <a href="#image["link"]#"><h4>#image["title"]#</h4></a>
						        </div>
					            <div class="postdetail"><p>#image["detail"]#</p></div>
					        </div>
				        </li>
					<cfif (image["currentrow"]%3) eq 0>
			            </ul>
	            	</cfif>
				</cfif>
	        </lithe:lithe-gallery-image>
		</div>
		<div class="row-fluid">
			<div class="span12">
				<h4>Related Product</h4>
			</div>
			<cfif product["relateditemtotalrecord"] gt 0>
				<lithe:lithe-relateditem>
					<cfif (relateditem["currentRow"]%4) eq 1>
						<ul class="thumbnails">
					</cfif>
					<li class="span3">
							<div class="product">
							    <div class="image-product">
									<a href="item.cfm?slug=#relateditem["SLUG"]#"><img src="#relateditem["STHUMB"]#" /></a>
								</div>
								<div class="posttitle">
									<a href="item.cfm?slug=#relateditem["SLUG"]#"><h4>#relateditem["TITLE"]#</h4></a>
								</div>
								<div class="postprice">
									<p class="tag-p"> Price : #relateditem["PRICE"]#</p>
								</div>
								<div class="tags-product">
							        <p class="tag-p"><i class="icon-tags"></i> Tags : 
								        <lithe:lithe-relatedtag>
								        	<cfif relatedtag["totalrecord"] neq 0>
												<a href="tag.cfm?tags=#relatedtag["name"]#">#relatedtag["name"]#</a>
												<cfif relatedtag["currentrow"]	neq relatedtag["totalrecord"]>
													,
												</cfif>
											</cfif>
								        </lithe:lithe-relatedtag>
									</p>
							    </div>
							</div>
						</li>
					<cfif (relateditem["currentRow"]%4) eq 0>
						</ul>
					</cfif>
				</lithe:lithe-relateditem>
			</cfif>
		</div>
	</cfoutput>
</div>
<cfinclude template="include/footer.cfm">
</lithe:lithe-product-detail>
<cfinclude template="include/js.cfm">
<script>
window.addCustomProduct(true,"add success")
</script>
