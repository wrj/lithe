<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 8/29/13 AD
  Time: 11:54 AM
  To change this template use File | Settings | File Templates.
--->

<cfimport taglib="/cfc/tags" prefix="lithe">
<cfinclude template="/cfc/globalfunction.cfm"/>
<cfset titlepage = "">
<cfif isDefined("slug")>
	<cfset titlepage = "| #slug#">
</cfif>
<!DOCTYPE html>
<html lang="en">
<head>
<title><lithe:sitetitle/><cfoutput>#titlepage#</cfoutput></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<lithe:lithe-metadescription>
	<meta name="author" content="">

	<!-- Le styles -->
<!--- <link href='http://fonts.googleapis.com/css?family=Ropa+Sans|Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'> --->
	<link href="../public/stylesheets/bootstrap.min.css" rel="stylesheet">
	<link href="../public/stylesheets/bootstrap-responsive.min.css" rel="stylesheet">
	<link href="../private/fancybox/jquery.fancybox.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Le fav and touch icons -->
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
	<link rel="icon" href="img/favicon.ico" type="image/x-icon">
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top">
<div class="navbar-inner">
<div class="container">
	<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	<a class="brand" href="index.cfm">Project name</a>
<div class="nav-collapse collapse lithe-menu" data-lithe-menu="mainmenu">
<script id="menuTemplate" type="text/x-jsrender">
<ul class="{{:menu_class}}">
	{{for menu}}
	<li {{if name == "<cfoutput>#activepage#</cfoutput>"}}class="active"{{/if}}><a href="{{>linkurl}}">{{>name}}</a></li>
	{{/for}}
</ul>
</script>
</div>
<ul class="nav pull-right">
<li class="dropdown">
<a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0);">
	<i class="icon-flag icon-white"></i>
<cfoutput>
	#session['language']#
</cfoutput>
	<b class="caret"></b>
</a>
<ul class="dropdown-menu">
<cfloop list="#session["weblanguage"]#" delimiters="," index="lang">
	<cfoutput>
			<li><a href="javascript:void(0)" onclick="changelang('#lang#')">#lang#</a></li>
	</cfoutput>
</cfloop>
</ul>
</li>
</ul>
	<!--/.nav-collapse -->
</div>
</div>
</div>
<script type="text/javascript">
	function changelang(lang)
	{
		$.ajax({
			url:"/changelang.cfm",
			data: { language: lang,format:'json'},
			success: function(data) {
				location.reload();
			}
		})
	}
</script>

