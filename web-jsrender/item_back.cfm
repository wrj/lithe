<cfset activepage = 'Product'>
<cfinclude template="../web/include/header.cfm">
<div class="container">
	<div class="lithe-product-detail" data-lithe-addbutton-name="Add to Cart">
		<script type="text/x-jsrender">
			<div class="row-fluid">
				<ul class="lithe-breadcrumbs breadcrumb">
					<li><a href="../web/index.cfm">Home</a> <span class="divider">/</span></li>
					<li><a href="../web/blog.cfm">Product</a> <span class="divider">/</span></li>
					<li class="active">{{:TITLE}}</li>
				</ul>
			</div>
			<div class="row-fluid">
				<div class="blog-item-image span6">
					<img src="{{:LTHUMB}}">
				</div>
				<div class="blog-item span6">
					<h4>{{:TITLE}}</h4>
					<p>{{:INTRO}}</p>
					<p>Price : {{:PRICE}}</p>
					<p>{{:ADD}}</p>
					<div class="clear_both"></div>
					<div class="blog-item-panel">
	                    <ul>
	                        <li class="date">
	                          <p><i class="icon-th-list"></i>{{:CATEGORYTITLE}}</p></li>
	                        <li><p><i class="icon-tags"></i>
								{{for TAG ~taglength=TAG.length-1}}
					        		<a href="../web/tag.cfm?tags={{:#data}}">{{:#data}}</a>
					        		{{if #index != ~taglength}},{{/if}} 
					        	{{/for}}
	                        </p></li>
	                    </ul>
	                </div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="blog-item-description span12">
					<h4>Product Description</h4>
					<p>{{:DETAIL}}</p>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span12">
					<h4>Relate Product</h4>
				</div>
				{{for RELATEITEM}}
					{{if #index%4 == 0}}
						<ul class="thumbnails">
					{{/if}}
						<li class="span3">
							<div class="product">
							    <div class="image-product">
									<a href="../web/item.cfm?slug={{>SLUG}}"><img src="{{:STHUMB}}" /></a>
								</div>
								<div class="posttitle">
									<a href="../web/item.cfm?slug={{>SLUG}}"><h4>{{:TITLE}}</h4></a>
								</div>
								<div class="postprice">
									<p class="tag-p"> Price : {{:PRICE}}</p>
								</div>
								<div class="tags-product">
							        <p class="tag-p"><i class="icon-tags"></i> Tags : 
								        {{for TAG ~taglength=TAG.length-1}}
								        <a href="../web/tag.cfm?tags={{:#data}}">{{:#data}}</a>
								        	{{if #index != ~taglength}},{{/if}} 
								        {{/for}}
									</p>
							    </div>
							</div>
						</li>
					{{if #index%4 == 3}}
						</ul>
					{{/if}}
				{{/for}}
			</div>
		</script>
	</div>
</div>
<cfinclude template="../web/include/footer.cfm">
<cfinclude template="../web/include/js.cfm">