<div class="row-fluid lithe-posts slideshow" data-lithe-category="Slider">
	<script type="text/x-jsrender">
		<div class="slider-box">
		    <div id="myCarousel" class="carousel slide">
			    <!-- Carousel items -->
			    <div class="carousel-inner">
			    	{{for posts}}
			    		<div class="item {{if #index == 0}}active{{/if}}">
			    			<img src="{{:LTHUMB}}" class="slidethumb">
			    			<div class="carousel-caption">
								<h4>{{:TITLE}}</h4>
							</div>
			    		</div>
			    	{{/for}}
			    </div>
			    <!-- Carousel nav -->
			    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
			    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
		    </div>
	    </div>
	</script>
</div>
<style>
.slider-box{
    font-size: 12px;
    font-weight: bold;
    left: -1px;
    padding: 3px 7px;
    top: -1px;
}
.slide {
	width: 900px;
	margin: 0 auto;
}
.carousel-inner{
	width: 900px;
}
.slideshow{
	margin-bottom: 20px;
}
</style>