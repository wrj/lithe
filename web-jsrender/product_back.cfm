<cfset activepage = 'Product'>
<cfinclude template="../web/include/header.cfm">
<div class="container">
	<div class="row-fluid">
		<ul class="lithe-breadcrumbs breadcrumb"
		data-lithe-category="<cfoutput>#category#</cfoutput>" >
			<script type="text/x-jsrender">
				<li><a href="../web/index.cfm">Home</a> <span class="divider">/</span></li>
				<li class="active">{{:TITLE}}</li>
			</script>
		</ul>
	</div>
	<div class="lithe-posts" data-lithe-withparam="true" data-lithe-limit="12">
		<script type="text/x-jsrender">
			<div class="row-fluid">
				{{for posts}}
					{{if #index%4 == 0}}
						<ul class="thumbnails">
					{{/if}}
						<li class="span3">
							<div class="product">
							    <div class="image-product">
									<a href="../web/item.cfm?slug={{>SLUG}}"><img src="{{:STHUMB}}" /></a>
								</div>
								<div class="posttitle">
									<a href="../web/item.cfm?slug={{>SLUG}}"><h4>{{:TITLE}}</h4></a>
								</div>
								<div class="postprice">
									<p class="tag-p"> Price : {{:PRICE}}</p>
								</div>
								<div class="tags-product">
							        <p class="tag-p"><i class="icon-tags"></i> Tags : 
								        {{for TAG ~taglength=TAG.length-1}}
								        <a href="../web/tag.cfm?tags={{:#data}}">{{:#data}}</a>
								        	{{if #index != ~taglength}},{{/if}} 
								        {{/for}}
									</p>
							    </div>
							</div>
						</li>
					{{if #index%4 == 3}}
						</ul>
					{{/if}}
				{{/for}}
			</div>
			<div class="row-fluid">
				{{pagination model=pagination class="pagination-left"/}}
			</div>
		</script>
	</div>
</div>
<cfinclude template="../web/include/footer.cfm">
<cfinclude template="../web/include/js.cfm">