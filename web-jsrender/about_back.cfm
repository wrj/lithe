<cfset activepage = 'About'>
<cfinclude template="../web/include/header.cfm">
<div class="container">
	<div class="row-fluid">
		<ul class="breadcrumb">
			<li><a href="../web/index.cfm">Home</a> <span class="divider">/</span></li>
			<li class="active">About</li>
		</ul>
	</div>
	<div class="row-fluid">
		<div class="lithe-page-detail" data-lithe-slug="About">
			<script type="text/x-jsrender">
				<h3>{{:TITLE}}</h3>
				<div class="thumbnail-about">
					<img src="{{:LTHUMB}}">
				</div>
				<p>{{:DETAIL}}</p>
			</script>
		</div>
	</div>
</div>
<cfinclude template="../web/include/footer.cfm">
<cfinclude template="../web/include/js.cfm">
