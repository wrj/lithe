<cfset activepage = 'Blog'>
<cfinclude template="../web/include/header.cfm">
<div class="container">
	<div class="lithe-post-detail">
		<script type="text/x-jsrender">
			<div class="row-fluid">
				<ul class="lithe-breadcrumbs breadcrumb">
					<li><a href="../web/index.cfm">Home</a> <span class="divider">/</span></li>
					<li><a href="../web/blog.cfm">Blog</a> <span class="divider">/</span></li>
					<li class="active">{{:TITLE}}</li>
				</ul>
			</div>
			<div class="row-fluid">
				<div class="blog-item span9">
					<h3>{{:TITLE}}</h3>
					<img src="{{:LTHUMB}}" class="blog-image">
					<p>{{:DETAIL}}</p>
					<div class="blog-item-panel">
	                    <ul>
	                        <li class="date">
	                          <p><i class="icon-calendar"></i>{{dateFormat model=UPDATEDAT format="date"/}}</p></li>
	                        <li><p><i class="icon-user"></i>{{:AUTHOR}}</p></li>
	                        <li><p><i class="icon-tags"></i>
								{{for TAG ~taglength=TAG.length-1}}
					        		<a>{{:#data}}</a>
					        		{{if #index != ~taglength}},{{/if}} 
					        	{{/for}}
	                        </p></li>
	                    </ul>
	                </div>
	                <div class="comments row-fluid">
		    			<div id="disqus_thread"></div>
						<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
						<a href="http://disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
		    		</div>
				</div>
				<div class="span3">
					<ul class="relate-blog-item">

					</ul>
				</div>
				
				<script type="text/javascript">
					$(function(){
		    			var sidebar = '';
		    			{{for RELATEITEM}}
						sidebar+='<li>';
						sidebar+='<h4>{{:TITLE}}</h4>';
						sidebar+='<img src="{{:STHUMB}}">';
						sidebar+='<p class="relate-blog-intro">{{:INTRO}}</p>';
						sidebar+='<a class="post-more" href="../web/blogitem.cfm?slug={{>SLUG}}">Continue Reading&nbsp;&raquo;</a>';
						sidebar+='</li>';
						{{/for}}
						$('.relate-blog-item').html(sidebar);
					});
				</script>
			</div>
		</script>
	</div>
</div>
<cfinclude template="../web/include/footer.cfm">
<cfinclude template="../web/include/js.cfm">
<script type="text/javascript">
    /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
    var disqus_shortname = 'skoodelithesample'; // required: replace example with your forum short name

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>