<cfset activepage = 'Contact Us'>
<cfinclude template="../web/include/header.cfm">
<div class="container">
	<div class="row-fluid">
		<ul class="breadcrumb">
			<li><a href="../web/index.cfm">Home</a> <span class="divider">/</span></li>
			<li class="active">Contact</li>
		</ul>
	</div>
	<div class="row-fluid">
		<div class="lithe-contactform span4" data-lithe-title-attrbutes='id="inputName" placeholder="Name"' data-lithe-email-attrbutes='id="inputName" placeholder="Email"' data-lithe-submit-name="Send Message">
    		<script type="text/x-jsrender">
    			<h3>Contact Us</h3>
		        {{:startform}}
		        <div class="control-group">
		        	<label class="control-label">Name
		        	</label>
		        	<div class="controls">
		        		{{:title}}
		        	</div>
		        </div>
		        <div class="control-group">
		        	<label class="control-label">Email
		        	</label>
		        	<div class="controls">
		        		{{:email}}
		        	</div>
		        </div>
		        <div class="control-group">
		        	<label class="control-label">Detail
		        	</label>
		        	<div class="controls">
		        		{{:detail}}
		        	</div>
		        </div>
		        <div>{{:submit}}</div>
		        {{:endform}}
    		</script>
		</div>
		<div class="span8">
			<h3>Map</h3>
			<div class="map">
				<img src="../web/img/map.png">
			</div>
		</div>
	</div>
</div>
<cfinclude template="../web/include/footer.cfm">
<cfinclude template="../web/include/js.cfm">