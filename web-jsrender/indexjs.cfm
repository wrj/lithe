<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 8/29/13 AD
  Time: 10:15 AM
  To change this template use File | Settings | File Templates.
--->

<cfset activepage = 'Home'>
<cfinclude template="../web/include/header.cfm">
<div class="container">
<cfinclude template="include/slider.cfm">
    <div class="row-fluid lithe-posts" data-lithe-category="Product" data-lithe-limit="8">
        <script type="text/x-jsrender">
            <h3>Product</h3>
            {{for posts}}
            {{if #index%4 == 0}}
            <ul class="thumbnails">
                {{/if}}
                <li class="span3">
                    <div class="product">
                        <div class="image-product">
                            <a href="../web/item.cfm?slug={{>SLUG}}"><img src="{{:STHUMB}}" /></a>
                        </div>
                        <div class="posttitle">
                            <a href="../web/item.cfm?slug={{>SLUG}}"><h4>{{:TITLE}}</h4></a>
                        </div>
                        <div class="postprice">
                            <p class="tag-p"> Price : {{:PRICE}}</p>
                        </div>
                        <div class="tags-product">
                            <p class="tag-p"><i class="icon-tags"></i> Tags :
                                {{for TAG ~taglength=TAG.length-1}}
                                <a href="../web/tag.cfm?tags={{:#data}}">{{:#data}}</a>
                                {{if #index != ~taglength}},{{/if}}
                                {{/for}}
                            </p>
                        </div>
                    </div>
                </li>
                {{if #index%4 == 3}}
            </ul>
            {{/if}}
            {{/for}}
        </script>
    </div>
</div>
<cfinclude template="../web/include/footer.cfm">
<cfinclude template="../web/include/js.cfm">
<script type="text/javascript">
    jQuery(function() {
        jQuery('#myCarousel').carousel({})
    });
</script>