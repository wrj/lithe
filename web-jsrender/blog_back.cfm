<cfset activepage = 'Blog'>
<cfinclude template="../web/include/header.cfm">
<div class="container">
	<div class="row-fluid">
		<ul class="lithe-breadcrumbs breadcrumb"
		data-lithe-category="Blog" >
			<script type="text/x-jsrender">
				<li><a href="../web/index.cfm">Home</a> <span class="divider">/</span></li>
				<li class="active">{{:TITLE}}</li>
			</script>
		</ul>
	</div>
	<div class="row-fluid lithe-posts" data-lithe-category="Blog" data-lithe-limit="5">
		<script type="text/x-jsrender">
			{{for posts}}
				<div class="blog-item">
					<h3>{{:TITLE}}</h3>
					<img src="{{:LTHUMB}}">
					<p>{{:INTRO}}</p>
					<a class="post-more" href="../web/blogitem.cfm?slug={{>SLUG}}">Continue Reading&nbsp;&raquo;</a>
					<div class="blog-item-panel">
                        <ul>
                            <li class="date">
                              <p><i class="icon-calendar"></i>{{dateFormat model=UPDATEDAT format="date"/}}</p></li>
                            <li><p><i class="icon-user"></i>{{:AUTHOR}}</p></li>
                            <li><p><i class="icon-tags"></i>
								{{for TAG ~taglength=TAG.length-1}}
					        		<a>{{:#data}}</a>
					        		{{if #index != ~taglength}},{{/if}} 
					        	{{/for}}
                            </p></li>
                        </ul>
                    </div>
				</div>
			{{/for}}
			<div class="row-fluid">
				{{pagination model=pagination class="pagination-left"/}}
			</div>
		</script>
	</div>
</div>
<cfinclude template="../web/include/footer.cfm">
<cfinclude template="../web/include/js.cfm">