<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 9/16/13 AD
  Time: 2:46 PM
  To change this template use File | Settings | File Templates.
--->



<cfimport taglib="/cfc/tags" prefix="lithe">
<link rel="stylesheet" media="screen" href="../public/stylesheets/bootstrap.min.css">
<cfoutput>
	<lithe:lithe-tag-clouds>
		<cfif tagclouds["totalrecord"] neq 0>
			<ul>
				<lithe:lithe-tag>
					<cfif tag["totalrecord"] neq 0>
						<a href="tag.cfm?tags=#tag["name"]#">#tag["name"]#</a>
						<cfif tag["currentrow"]	neq tag["totalrecord"]>
								,
						</cfif>
					</cfif>
				</lithe:lithe-tag>
			</ul>
		</cfif>
	</lithe:lithe-tag-clouds>
</cfoutput>
<script type="text/javascript" src="../private/javascripts/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="../public/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="../private/javascripts/head.min.js"></script>
<script type="text/javascript" src="../private/javascripts/system.js"></script>