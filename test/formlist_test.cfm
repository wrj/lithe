<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/4/13 AD
  Time: 10:26 AM
  To change this template use File | Settings | File Templates.
--->

<cfprocessingdirective pageEncoding="utf-8"/>
<cfcontent type="text/html; charset=utf-8">
<cfimport taglib="/cfc/tags" prefix="lithe">
<cfinclude template="/cfc/globalfunction.cfm"/>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<!-- start: Meta -->
	<link href="../public/stylesheets/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<cfif structKeyExists(session,"form_username") >

	<cfoutput>
		<!--- error, warning, notice --->
		<cfif flashKeyExists("success")>
			<div class="alert alert-success">
			#flash("success")#
			</div>
		</cfif>
		<cfif flashKeyExists("error")>
			<cfset ERROR = true>
			<div class="alert alert-error">
			#flash("error")#
			</div>
		</cfif>
	</cfoutput>

		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped createTableData display applicationTable">
			<thead>
			<tr>
				<th width="">title</th>
				<th width="">email</th>
				<th width="">detail</th>
				<th width="">create</th>
				<th width="">update</th>
				<th width="">delete</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td colspan="7" class="dataTables_empty">โปรดรอ ...</td>
			</tr>
			</tbody>
			<tfoot>
			<tr>
				<th width="">title</th>
				<th width="">email</th>
				<th width="">detail</th>
				<th width="">create</th>
				<th width="">update</th>
				<th width="">delete</th>
			</tr>
			</tfoot>
		</table>

</cfif>
<script src="../private/javascripts/jquery-1.8.3.min.js"></script>
<!---<script type="text/javascript" src="../public/javascripts/less-1.3.3.min.js"></script>--->
<script src="../public/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js"></script>
<script src="../public/javascripts/bootstrap.js"></script>

<script type="text/javascript" src="../private/javascripts/jquery.dataTables.js"></script>

<cfoutput>
<lithe:lithe-form-lists
		showfield="TITLE,EMAIL,DETAIL,CREATEDAT"
		collectionname="formdb"
		tableclass="applicationTable"
		service="http://#CGI.HTTP_HOST#/test/formquery.cfm"
		/>
</cfoutput>



<script type="text/javascript" src="../private/javascripts/head.min.js"></script>
<script type="text/javascript" src="../private/javascripts/system.js"></script>
</body>
</html>