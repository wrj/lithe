<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/4/13 AD
  Time: 1:45 PM
  To change this template use File | Settings | File Templates.
--->
<cfprocessingdirective pageEncoding="utf-8"/>
<cfcontent type="text/html; charset=utf-8">
<cfimport taglib="/cfc/tags" prefix="lithe">
<cfinclude template="/cfc/globalfunction.cfm"/>

<cfif structKeyExists(session,"form_username") >

<cfset ignorelist = "FIELDNAMES,RECAPTCHA,KEY,COLLECTIONNAME">
<cfset updateform=structNew()>
<cfloop list="#structkeylist(FORM,',')#" index="item">
	<cfif listfindnocase(ignorelist,item,',') eq 0 AND REFindNoCase("OPTION_",item) IS FALSE>
		<cfset updateform[item] = form[item]>
	</cfif>
</cfloop>

	<cfinclude template="../cfc/formcontroller.cfm">
<cfset output = formupdate(form.collectionname,form.key,updateform)>

<cfif output IS 1>
<!---	<cfset message="success=Update success">--->
	<cfset flashinsert('success','Update success')>
<cfelse>
<!---	<cfset message="error=Update error">--->
	<cfset flashinsert('error','Update error')>
</cfif>
<cflocation url = "formlist_test.cfm">

</cfif>