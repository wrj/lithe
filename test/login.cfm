<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/4/13 AD
  Time: 2:38 PM
  To change this template use File | Settings | File Templates.
--->
<cfprocessingdirective pageEncoding="utf-8"/>
<cfoutput>

</cfoutput>
<cfif isdefined("error")>
	<div class="alert alert-error alert_error_custom">
	<cfoutput><ul>#error#</ul></cfoutput>
	</div>
</cfif>
<cfif isdefined("success")>
	<div class="alert alert-success alert_success_custom">
	<cfoutput>#success#</cfoutput>
	</div>
</cfif>
<form action="checklogin.cfm" method="post" class="form-horizontal row-fluid loginform">
	<div class="control-group">
		<label class="control-label">Username</label>
		<div class="controls">
			<input type="text" name="username" class="username span11" required>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">Password</label>
		<div class="controls">
			<input type="password" name="password" class="password span11" required>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label"></label>
		<div class="controls">
			<button type="submit" class="btn btn-primary">เข้าสู่ระบบ</button>
		</div>
	</div>
</form>