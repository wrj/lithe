<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/2/13 AD
  Time: 5:58 PM
  To change this template use File | Settings | File Templates.
--->
<cfinclude template="/cfc/globalfunction.cfm"/>
<cfprocessingdirective pageEncoding="utf-8"/>
<cfimport taglib="/cfc/tags" prefix="lithe">
<lithe:recaptcha action="check"/>
<cfif form.option_userecaptcha IS TRUE AND recaptcha IS FALSE>
	<cfset flashinsert('error','#form.option_errormessage#')>
<cfelse>
	<cfset save_filepath="">
	<cfset imageuploadurl="">

<!---	<cfif isdefined("form.file") AND NOT form.file IS "">--->
<!---		<cfhttp url="http://#CGI['HTTP_HOST']#/cfc/services/contactus.cfc?" method="post" result="objdata">--->
<!---			<cfhttpparam type="url" name="method" value="uploadFile"/>--->
<!---			<cfhttpparam type="file" name="file" file="#form.file#"/>--->
<!---		</cfhttp>--->
<!---		<cfset save_filepath = objdata['filecontent']>--->
<!---		<cfset imageuploadurl="http://#CGI.HTTP_HOST#/assets/upload/contactus/#deserializeJSON(save_filepath)['file']#">--->
<!---	</cfif>--->
<!---	<cfif isDefined("form.title") AND isDefined("form.email") AND isDefined("form.detail")>--->
			<cfset ignorelist = "FIELDNAMES,RECAPTCHA">
			<cfset rawdata=structNew()>
			<cfloop list="#structkeylist(FORM,',')#" index="item">
				<cfif listfindnocase(ignorelist,item,',') eq 0 AND REFindNoCase("OPTION_",item) IS FALSE>
					<cfset rawdata[item] = form[item]>
				</cfif>
			</cfloop>
			<cfset insertdata=serializeJSON(rawdata)>
			<cfset emaildata=serializeJSON(rawdata)>
			<cfhttp url="http://#CGI['HTTP_HOST']#/cfc/services/form.cfc?" method="post" result="objdata2">
				<cfhttpparam type="url" name="method" value="insertform"/>
				<cfhttpparam type="url" name="collectionname" value="#form.option_collectionname#"/>
				<cfhttpparam type="url" name="insertdata" value="#insertdata#"/>
			</cfhttp>
			<cfset issavedata = deserializeJSON(objdata2['filecontent'])>
			<cfif issavedata.status IS 1>
				<cfset flashinsert('success','#form.option_successmessage#')>
			</cfif>
			<cfif form.option_emailtocustomer>
				<cfif isdefined("form.email") AND IsValid("email", form.email) AND isdefined("form.title")>
					<cfhttp url="http://#CGI['HTTP_HOST']#/cfc/services/form.cfc?" method="post" result="objdata3">
						<cfhttpparam type="url" name="method" value="sendemailtocustomer"/>
						<cfhttpparam type="url" name="template" value="#form.option_emailcustomertemplate#"/>
						<cfhttpparam type="url" name="email" value="#form.email#"/>
						<cfhttpparam type="url" name="subject" value="#form.title#"/>
						<cfhttpparam type="url" name="emaildata" value="#emaildata#"/>
					</cfhttp>
					<cfset issendemaildata = deserializeJSON(objdata3['filecontent'])>
					<cfif issendemaildata.status IS 1>
						<cfset flashinsert('success','#form.option_successmessage#')>
					<cfelse>
						<cfset flashinsert('error','#form.option_errormessage#')>
					</cfif>
				<cfelse>
					<cfset flashinsert('error','#form.option_errormessage#')>
				</cfif>
			</cfif>
			<cfif form.option_emailtoadmin>
				<cfif isdefined("form.email") AND IsValid("email", form.email) AND isdefined("form.title")>
					<cfhttp url="http://#CGI['HTTP_HOST']#/cfc/services/form.cfc?" method="post" result="objdata4">
						<cfhttpparam type="url" name="method" value="sendmultiemailtoadmin"/>
						<cfhttpparam type="url" name="template" value="#form.option_emailadmintemplate#"/>
						<cfhttpparam type="url" name="email" value="#form.email#"/>
						<cfhttpparam type="url" name="subject" value="#form.title#"/>
						<cfhttpparam type="url" name="emaildata" value="#emaildata#"/>
					</cfhttp>
					<cfset issendemaildata = deserializeJSON(objdata4['filecontent'])>
					<cfif issendemaildata.status IS 1>
						<cfset flashinsert('success','#form.option_successmessage#')>
					<cfelse>
						<cfset flashinsert('error','#form.option_errormessage#')>
					</cfif>
				<cfelse>
					<cfset flashinsert('error','#form.option_errormessage#')>
				</cfif>
			</cfif>
<!---		<cfelse>--->
<!---		<cfset message="error=#form.option_errormessage#">--->
<!---	</cfif>--->
</cfif>
<cflocation url = "#form.option_formredirect#">

