<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 9/13/13 AD
  Time: 11:14 AM
  To change this template use File | Settings | File Templates.
--->
<cfimport taglib="/cfc/tags" prefix="lithe">
<link rel="stylesheet" media="screen" href="../public/stylesheets/bootstrap.min.css">
<cfset activepagex="test1">
<cfoutput>

	<lithe:lithe-menu menu="mainmenu" >
<!---		<cfdump var="#menu#">--->
		<div class="navbar navbar-inverse navbar-fixed-top">
			<div class="navbar-inner">
				<div class="nav-collapse collapse">
					<lithe:lithe-menu-list template="defaultmenu" activepage="#activepagex#" allchildren=true />
				</div>
			</div>
		</div>
	</lithe:lithe-menu>

</cfoutput>
<script type="text/javascript" src="../private/javascripts/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="../public/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="../private/javascripts/head.min.js"></script>
<script type="text/javascript" src="../private/javascripts/system.js"></script>
