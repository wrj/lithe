<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/3/13 AD
  Time: 10:40 AM
  To change this template use File | Settings | File Templates.
--->
<cfinclude template="/cfc/globalfunction.cfm"/>
<cfimport taglib="/cfc/tags" prefix="lithe">

<cfoutput>
	<!--- error, warning, notice --->
	<cfif flashKeyExists("success")>
		<div class="alert alert-success">
		#flash("success")#
		</div>
	</cfif>
	<cfif flashKeyExists("error")>
		<cfset ERROR = true>
		<div class="alert alert-error">
		#flash("error")#
		</div>
	</cfif>
</cfoutput>

<link rel="stylesheet" media="screen" href="../public/stylesheets/bootstrap.min.css">
<cfoutput>
	<lithe:lithe-form-new recaptcha=true emailtocustomer=false emailcustomertemplate="order.txt" emailtoadmin=false emailadmintemplate="order.txt">
		title<select name="title">
		<option value="subject1">subject1</option>
		<option value="subject2">subject2</option>
	</select><br/>
		email<input type="text" name="email" required/><br/>
		detail<textarea name="detail"></textarea><br/>
		<lithe:recaptcha/>
		<input type="submit" value="submit">
	</lithe:lithe-form-new>
</cfoutput>
<script type="text/javascript" src="../private/javascripts/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="../public/javascripts/bootstrap.min.js"></script>
<script type="text/javascript" src="../private/javascripts/head.min.js"></script>
<script type="text/javascript" src="../private/javascripts/system.js"></script>
