<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/17/13 AD
  Time: 11:12 AM
  To change this template use File | Settings | File Templates.
--->

<cfset configfile = "#expandPath('/cfc/config.json')#">
<cfset systemconfig = deserializeJSON(fileRead( "#configfile#" )) />
<cfset databasename = systemconfig.database.databasename>
<cfset databaseusername = systemconfig.database.databaseusername>
<cfset databasepassword = systemconfig.database.databasepassword>
<cfset databaseport = systemconfig.database.databaseport>
<cfset databasehost = systemconfig.database.databasehost>
<cfif MongoIsvalid(databasename) eq 'NO'>
	<cfif databaseusername neq '' AND databasepassword neq ''>
		<cfif databaseport neq ''>
			<cfset MongoRegister( name=databasename, server=databasehost,db=databasename,port=databaseport,username=databaseusername,password=databasepassword)>
		<cfelse>
			<cfset MongoRegister( name=databasename, server=databasehost,db=databasename,username=databaseusername,password=databasepassword)>
		</cfif>
	<cfelse>
		<cfif databaseport neq ''>
			<cfset MongoRegister( name=databasename, server=databasehost, db=databasename,port=databaseport)>
		<cfelse>
			<cfset MongoRegister( name=databasename, server=databasehost, db=databasename)>
		</cfif>
	</cfif>
</cfif>
