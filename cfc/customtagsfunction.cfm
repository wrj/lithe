<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 11/6/13 AD
  Time: 11:56 AM
  To change this template use File | Settings | File Templates.
--->

<cffunction name="cfcondition" output="false" access="public" returntype="string">
	<cfargument name="text" type="string" required="true">
	<cfargument name="keydata" type="any" required="true">
	<cfargument name="index" type="numeric" required="true">
	<cfargument name="totalrecord" type="numeric" required="true">
	<cfset var text = arguments.text>
	<cfset var iftags = rematch('<\s*?cfif\b[^>]*>(.*?)</cfif\b[^>]*>',text)>
	<cfloop array="#iftags#" index="iftag">
		<cfset var condition = rematch('<\s*?cfcondition\b[^>]*>(.*?)</cfcondition\b[^>]*>',iftag)>
		<cfset var operators =replaceNoCase(condition[1],"currentrow","")>
		<cfset operators =replaceNoCase(operators,"<cfcondition>","")>
		<cfset operators =replaceNoCase(operators,"</cfcondition>","")>
		<cfset var operatorsator = listToArray(operators," ",true)>
		<cfif arraylen(operatorsator) GT 1>
			<cfif left(operatorsator[1],1) IS "%">
				<cfset var numberoperators = removeChars(operatorsator[1],1,1)>
				<cfif operatorsator[2] IS "EQ">
					<!---	EQ % operatorsetor	--->
					<cfif arguments.index%numberoperators EQ lsParseNumber(operatorsator[3])>
						<cfset text = cutcfif(text,condition[1],iftag)>
					<cfelse>
						<cfset text=replaceNoCase(text,iftag,"","one")>
					</cfif>
				<cfelseif operatorsator[2] IS "NEQ">
					<!---	NEQ % operatorsetor	--->
					<cfif arguments.index%numberoperators NEQ lsParseNumber(operatorsator[3])>
						<cfset text = cutcfif(text,condition[1],iftag)>
					<cfelse>
						<cfset text=replaceNoCase(text,iftag,"","one")>
					</cfif>
				</cfif>
			<cfelseif operatorsator[1] IS "">
				<cfif operatorsator[2] IS "EQ">
					<!---			TOTALRECORD		--->
					<cfif operatorsator[3] IS "totalrecord">
						<cfif arguments.index EQ arguments.totalrecord>
							<cfset text = cutcfif(text,condition[1],iftag)>
						<cfelse>
							<cfset text=replaceNoCase(text,iftag,"","one")>
						</cfif>
						<!---			NUMBER		--->
					<cfelseif isNumeric(operatorsator[3])>
						<cfif arguments.index EQ operatorsator[3]>
							<cfset text = cutcfif(text,condition[1],iftag)>
						<cfelse>
							<cfset text=replaceNoCase(text,iftag,"","one")>
						</cfif>
					</cfif>
				<cfelseif operatorsator[2] IS "NEQ">
					<!---			NEQ TOTALRECORD		--->
					<cfif operatorsator[3] IS "totalrecord">
						<cfif arguments.index NEQ arguments.totalrecord>
							<cfset text = cutcfif(text,condition[1],iftag)>
						<cfelse>
							<cfset text=replaceNoCase(text,iftag,"","one")>
						</cfif>
						<!---			NEQ NUMBER		--->
					<cfelseif isNumeric(operatorsator[3])>
						<cfif arguments.index NEQ operatorsator[3]>
							<cfset text = cutcfif(text,condition[1],iftag)>
						<cfelse>
							<cfset text=replaceNoCase(text,iftag,"","one")>
						</cfif>
					</cfif>
				</cfif>
			<cfelse>
				<cfif operatorsator[2] IS "EQ">
					<!---		TEXT STRING		--->
					<cfset var field = replaceNoCase(operatorsator[1],'{{','',"all")>
					<cfset field = replaceNoCase(field,'}}','',"all")>
					<cfset eqvalue = replaceNoCase(operatorsator[3],'"','',"all")>
					<cfif arguments.keydata[field] EQ eqvalue>
						<cfset text = cutcfif(text,condition[1],iftag)>
					<cfelse>
						<cfset text=replaceNoCase(text,iftag,"","one")>
					</cfif>
				<cfelseif operatorsator[2] IS "NEQ">
					<!---		TEXT STRING		--->
					<cfset var field = replaceNoCase(operatorsator[1],'{{','',"all")>
					<cfset field = replaceNoCase(field,'}}','',"all")>
					<cfset eqvalue = replaceNoCase(operatorsator[3],'"','',"all")>
					<cfif arguments.keydata[field] NEQ eqvalue>
						<cfset text = cutcfif(text,condition[1],iftag)>
					<cfelse>
						<cfset text=replaceNoCase(text,iftag,"","one")>
					</cfif>
				</cfif>
			</cfif>
		<cfelseif findnocase("isdefined",operatorsator[1])>
			<cfset var key = replaceNoCase(operatorsator[1],"isdefined","")>
			<cfset key = removeChars(key,1,1)>
			<cfset key = removeChars(key,len(key),1)>
			<cfif structKeyExists(arguments.keydata,key)>
				<cfset text = cutcfif(text,condition[1],iftag)>
			<cfelse>
				<cfset text=replaceNoCase(text,iftag,"","one")>
			</cfif>
		</cfif>
	</cfloop>
	<cfreturn text>
</cffunction>

<cffunction name="cutcfif" output="false" access="public" returntype="string">
	<cfargument name="text" type="string" required="true">
	<cfargument name="conditiontag" type="any" required="true">
	<cfargument name="iftag" type="any" required="true">
	<cfset var rmtag=replaceNoCase(iftag,arguments.conditiontag,"","one")>
	<cfset rmtag=replaceNoCase(rmtag,"<cfif>","","one")>
	<cfset rmtag=replaceNoCase(rmtag,"</cfif>","","one")>
	<cfset var text=replaceNoCase(arguments.text,arguments.iftag,rmtag,"one")>
	<cfreturn text>
</cffunction>