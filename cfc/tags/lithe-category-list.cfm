<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 9/23/13 AD
  Time: 1:58 PM
  To change this template use File | Settings | File Templates.
--->


<cfif thisTag.executionMode EQ "start">
	<cfset data = getBaseTagData("cf_lithe-category-lists")>
	<cfset categorydata = ArrayNew()>
	<cfif structKeyExists(data, "categorydata")>
		<cfset categorydata = data["categorydata"]>
	</cfif>
	<cfset currentRow = 1>
	<cfset CALLER["category"] = structNew()>
	<cfif arraylen(categorydata) gt 0>
		<cfset CALLER["category"] = categorydata[currentRow]>
		<cfset CALLER["category"]["currentRow"] = currentRow>
	</cfif>
</cfif>
<cfif thisTag.executionMode EQ "end">
	<cfset currentRow++>
	<cfif currentRow LE arraylen(categorydata)>
		<cfset CALLER["category"] = categorydata[currentRow]>
		<cfset CALLER["category"]["currentRow"] = currentRow>
		<cfexit method="loop">
	<cfelse>
		<cfexit method="exittag">
	</cfif>
</cfif>