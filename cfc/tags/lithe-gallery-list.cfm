<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 8/28/13 AD
  Time: 10:49 AM
  To change this template use File | Settings | File Templates.
--->

<cfif thisTag.executionMode EQ "start">
	<cfinclude template="/cfc/globalfunction.cfm" runonce="true"/>
	<cfset data = getBaseTagData("cf_lithe-gallery-lists")>
	<cfset gallerydata = ArrayNew()>
	<cfif structKeyExists(data, "gallerydata")>
		<cfset gallerydata = data["gallerydata"]>
	</cfif>
	<cfset currentRow = 1>
	<cfset CALLER["gallery"] = structNew()>
	<cfif arraylen(gallerydata) gt 0>
		<cfset currentgallery = gallerydata[currentRow]>
		<cfset StrEscUtils = createObject("java", "org.apache.commons.lang.StringEscapeUtils") />
		<cfset detailtext = StrEscUtils.unescapeHTML(currentgallery['DETAIL'])>
		<cfset detailtext = replaceNoCase(detailtext, "##", "####", 'all')>
		<cfset detailtext = replaceNoCase(detailtext, "####buildlink", "##buildlink", 'all')>
		<cfset detailtext = replaceNoCase(detailtext, ")####", ")##", 'all')>
		<cfset currentgallery["DETAIL"] = evaluate(DE(detailtext))>
		<cfset CALLER["gallery"] = currentgallery>
		<cfset CALLER["gallery"]["currentRow"] = currentRow>
	</cfif>
</cfif>
<cfif thisTag.executionMode EQ "end">
	<cfset currentRow++>
	<cfif currentRow LE arraylen(gallerydata)>
		<cfset currentgallery = gallerydata[currentRow]>
		<cfset StrEscUtils = createObject("java", "org.apache.commons.lang.StringEscapeUtils") />
		<cfset detailtext = StrEscUtils.unescapeHTML(currentgallery['DETAIL'])>
		<cfset detailtext = replaceNoCase(detailtext, "##", "####", 'all')>
		<cfset detailtext = replaceNoCase(detailtext, "####buildlink", "##buildlink", 'all')>
		<cfset detailtext = replaceNoCase(detailtext, ")####", ")##", 'all')>
		<cfset currentgallery["DETAIL"] = evaluate(DE(detailtext))>
		<cfset CALLER["gallery"] = currentgallery>
		<cfset CALLER["gallery"]["currentRow"] = currentRow>
		<cfexit method="loop">
		<cfelse>
		<cfexit method="exittag">
	</cfif>
</cfif>