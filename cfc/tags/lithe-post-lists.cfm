<cfparam name="attributes.orderby" type="string" default="CREATEDAT" />
<cfparam name="attributes.sort" type="string" default="desc" />
<cfparam name="attributes.thispage" type="numeric" default=1 />
<cfparam name="attributes.limit" type="numeric" default=20 />
<cfparam name="attributes.tags" type="string" default="" />
<cfparam name="attributes.category" type="string" default="" />
<cfparam name="attributes.skip" type="numeric" default=0/>
<cfparam name="attributes.subcategory" type="boolean" default=false/>
<cfparam name="attributes.random" type="boolean" default=false/>
<cfparam name="attributes.useparams" type="boolean" default=true/>
<cfoutput>
	<cfif thisTag.executionMode EQ "start">
		<cfif attributes['useparams']>
			<cfif attributes['thispage'] eq 1 && isDefined("page")>
				<cfset attributes["thispage"] = page>
			</cfif>
			<cfif attributes['category'] eq "" && isDefined("category")>
				<cfset attributes["category"] = category>
			</cfif>
			<cfif attributes['tags'] eq "" && isDefined("tags")>
				<cfset attributes["tags"] = tags>
			</cfif>
		</cfif>
			<cfset methodname = "postslisttest"/>
			<cfif attributes.random eq true>
				<cfset methodname = "postsrandom"/>
			</cfif>
		<cfif (attributes["category"] neq "") || (attributes["tags"] neq "")>
			<cfhttp url="http://#CGI['HTTP_HOST']#/cfc/services/post.cfc" method="get" result="objdata">
				<cfhttpparam type="url" name="method" value="#methodname#"/>
				<cfhttpparam type="url" name="orderby" value="#attributes.orderby#"/>
				<cfhttpparam type="url" name="sort" value="#attributes.sort#"/>
				<cfhttpparam type="url" name="thispage" value="#attributes.thispage#"/>
				<cfhttpparam type="url" name="limit" value="#attributes.limit#"/>
				<cfhttpparam type="url" name="tags" value="#attributes.tags#"/>
				<cfhttpparam type="url" name="category" value="#attributes.category#"/>
				<cfhttpparam type="url" name="language" value="#session["language"]#"/>
				<cfhttpparam type="url" name="skip" value="#attributes.skip#"/>
				<cfhttpparam type="url" name="subcategory" value="#attributes.subcategory#"/>
			</cfhttp>
<!---
			<cfoutput>
				#objdata['filecontent']#
			</cfoutput>
			<cfabort/>
--->
			<cfset rawdata = deserializeJSON(objdata['filecontent'])>
			<cfset postdata = rawdata["contents"]>
			<cfset CALLER["posts"] = StructNew()>
			<cfset CALLER["posts"]["totalrecord"] = rawdata["totalrecord"]>		
		<cfelse>
			<cfset CALLER["posts"] = StructNew()>
			<cfset CALLER["posts"]["totalrecord"] = 0>		
		</cfif>
	</cfif>
</cfoutput>