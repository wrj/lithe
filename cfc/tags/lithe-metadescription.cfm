<cfsilent>
<cfif thisTag.executionMode EQ "start">
	<cfset metastring = "">
	<cfset basetaglist = getBaseTagList()>
	<cfif listFindNoCase(basetaglist, "cf_lithe-product-detail", ',') gt 0>
		<cfset data = getBaseTagData("cf_lithe-product-detail")>
	<cfelseif listFindNoCase(basetaglist, "cf_lithe-post-detail", ',') gt 0>
		<cfset data = getBaseTagData("cf_lithe-post-detail")>
	<cfelseif listFindNoCase(basetaglist, "cf_lithe-page-detail", ',') gt 0>
		<cfset data = getBaseTagData("cf_lithe-page-detail")>
	<cfelse>
		<cfset data = structnew()>
	</cfif>
	<cfif structKeyExists(data, "rawdata")>
		<cfif structKeyExists(data['rawdata'], "TAG")>
			<cfif isArray(data["rawdata"]["TAG"])>
				<cfset metastring = "<meta name='description' content='#ArrayToList(data['rawdata']['TAG'])#'>">
			</cfif>
		</cfif>
	</cfif>
</cfif>
</cfsilent><cfoutput>#metastring#</cfoutput>