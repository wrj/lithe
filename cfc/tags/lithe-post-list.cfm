<cfif thisTag.executionMode EQ "start">
	<cfinclude template="/cfc/globalfunction.cfm" runonce="true"/>
    <cfset data = getBaseTagData("cf_lithe-post-lists")>
    <cfset postdata = ArrayNew()>
    <cfif structKeyExists(data, "postdata")>
        <cfset postdata = data["postdata"]>
    </cfif>
    <cfset currentRow = 1>
    <cfset CALLER["post"] = structNew()>
    <cfif arraylen(postdata) gt 0>
	    <cfset currentpost = postdata[currentRow]>
	    <cfset StrEscUtils = createObject("java", "org.apache.commons.lang.StringEscapeUtils") />
	    <cfset detailtext = StrEscUtils.unescapeHTML(currentpost['DETAIL'])>
	    <cfset detailtext = replaceNoCase(detailtext, "##", "####", 'all')>
	    <cfset detailtext = replaceNoCase(detailtext, "####buildlink", "##buildlink", 'all')>
	    <cfset detailtext = replaceNoCase(detailtext, ")####", ")##", 'all')>
	    <cfset currentpost["DETAIL"] = evaluate(DE(detailtext))>
        <cfset CALLER["post"] = currentpost>
        <cfset CALLER["post"]["currentRow"] = currentRow>
        <cfif isArray(postdata[currentRow]["TAG"])>
            <cfset tag = postdata[currentRow]["TAG"]>
        <cfelse>
            <cfset tag = arraynew()>
        </cfif>
    </cfif>
</cfif>
<cfif thisTag.executionMode EQ "end">
    <cfset currentRow++>
    <cfif currentRow LE arraylen(postdata)>
	    <cfset currentpost = postdata[currentRow]>
	    <cfset StrEscUtils = createObject("java", "org.apache.commons.lang.StringEscapeUtils") />
	    <cfset detailtext = StrEscUtils.unescapeHTML(currentpost['DETAIL'])>
	    <cfset detailtext = replaceNoCase(detailtext, "##", "####", 'all')>
	    <cfset detailtext = replaceNoCase(detailtext, "####buildlink", "##buildlink", 'all')>
	    <cfset detailtext = replaceNoCase(detailtext, ")####", ")##", 'all')>
	    <cfset currentpost["DETAIL"] = evaluate(DE(detailtext))>
	    <cfset CALLER["post"] = currentpost>
        <cfset CALLER["post"]["currentRow"] = currentRow>
        <cfif isArray(postdata[currentRow]["TAG"])>
            <cfset tag = postdata[currentRow]["TAG"]>
        <cfelse>
            <cfset tag = arraynew()>
        </cfif>
        <cfexit method="loop">
    <cfelse>
        <cfexit method="exittag">
    </cfif>
</cfif>