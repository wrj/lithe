<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/15/13 AD
  Time: 1:57 PM
  To change this template use File | Settings | File Templates.
--->

<cfparam name="attributes.service" default="http://#CGI.HTTP_HOST#/formquery.cfm">
<cfparam name="attributes.includedatatable" default=false>
<cfparam name="attributes.showfield" default="">
<cfparam name="attributes.collectionname" default="formdb">
<cfparam name="attributes.edittemplate" default="formedit.cfm?">
<cfparam name="attributes.deletetemplate" default="formdelete.cfm?">
<cfparam name="attributes.pipe" default=1>
<cfparam name="attributes.pagination" default="full_numbers" pattern="two_button|full_numbers">
<cfparam name="attributes.tableclass" default="">
<cfparam name="attributes.ordercolumn" default=1>
<cfparam name="attributes.sort" default="asc">


	<cfif thisTag.executionMode EQ "start">
<!---		<cfdump var="#attributes#">--->
<!---		<cfabort>--->
		<cfif attributes.includedatatable>
			<script type="text/javascript" src="../private/javascripts/jquery.dataTables.js"></script>
		</cfif>
		<script type="text/javascript">

			var applicationArr=[];
			var applicationDataTable;
			var CurrentParticipants=""
			$(document).ready(function() {
		<!--- --------------------- AJAX DATA TABLE --------------------- --->
				var oCache = {
					iCacheLower: -1
				};
				function fnSetKey( aoData, sKey, mValue )
				{
					for ( var i=0, iLen=aoData.length ; i<iLen ; i++ )
					{
						if ( aoData[i].name == sKey )
						{
							aoData[i].value = mValue;
						}
					}
				}
				function fnGetKey( aoData, sKey )
				{
					for ( var i=0, iLen=aoData.length ; i<iLen ; i++ )
					{
						if ( aoData[i].name == sKey )
						{
							return aoData[i].value;
						}
					}
					return null;
				}
				function fnDataTablesPipeline ( sSource, aoData, fnCallback , oSettings) {

					<cfset ignorelist="service,includedatatable,pipe,tableclass,ordercolumn,sort">
					<cfloop list="#structkeylist(attributes,',')#" delimiters="," index="item">
						<cfif listfindnocase(ignorelist,item,',') eq 0>
								aoData.push( { "name": "<cfoutput>#LCase(item)#</cfoutput>", "value": "<cfoutput>#attributes[item]#</cfoutput>" } );
						</cfif>
					</cfloop>
					var iPipe = <cfoutput>#attributes.pipe#</cfoutput>; /* Ajust the pipe size */
					var bNeedServer = false;
					var sEcho = fnGetKey(aoData, "sEcho");
					var iRequestStart = fnGetKey(aoData, "iDisplayStart");
					var iRequestLength = fnGetKey(aoData, "iDisplayLength");
					var iRequestEnd = iRequestStart + iRequestLength;
					oCache.iDisplayStart = iRequestStart;
					/* outside pipeline? */
					if ( oCache.iCacheLower < 0 || iRequestStart < oCache.iCacheLower || iRequestEnd > oCache.iCacheUpper )
					{
						bNeedServer = true;
					}
					/* sorting etc changed? */
					if ( oCache.lastRequest && !bNeedServer )
					{
						for( var i=0, iLen=aoData.length ; i<iLen ; i++ )
						{
							if ( aoData[i].name != "iDisplayStart" && aoData[i].name != "iDisplayLength" && aoData[i].name != "sEcho" )
							{
								if ( aoData[i].value != oCache.lastRequest[i].value )
								{
									bNeedServer = true;
									break;
								}
							}
						}
					}
					/* Store the request for checking next time around */
					oCache.lastRequest = aoData.slice();
					if ( bNeedServer )
					{
						if ( iRequestStart < oCache.iCacheLower )
						{
							iRequestStart = iRequestStart - (iRequestLength*(iPipe-1));
							if ( iRequestStart < 0 )
							{
								iRequestStart = 0;
							}
						}
						oCache.iCacheLower = iRequestStart;
						oCache.iCacheUpper = iRequestStart + (iRequestLength * iPipe);
						oCache.iDisplayLength = fnGetKey( aoData, "iDisplayLength" );
						fnSetKey( aoData, "iDisplayStart", iRequestStart );
						fnSetKey( aoData, "iDisplayLength", iRequestLength*iPipe );
						$.getJSON( sSource, aoData, function (json) {
							/* Callback processing */
							oCache.lastJson = jQuery.extend(true, {}, json);

							if ( oCache.iCacheLower != oCache.iDisplayStart )
							{
								json.aaData.splice( 0, oCache.iDisplayStart-oCache.iCacheLower );
							}
							json.aaData.splice( oCache.iDisplayLength, json.aaData.length );
							fnCallback(json)
						} );
					}
					else
					{
						json = jQuery.extend(true, {}, oCache.lastJson);
						json.sEcho = sEcho; /* Update the echo for each response */
						json.aaData.splice( 0, iRequestStart-oCache.iCacheLower );
						json.aaData.splice( iRequestLength, json.aaData.length );
						fnCallback(json);
						return;
					}
				}
				applicationDataTable = $('<cfoutput>.#attributes.tableclass#</cfoutput>').dataTable( {
					"aaSorting": [[ <cfoutput>#attributes.ordercolumn#</cfoutput>, "<cfoutput>#attributes.sort#</cfoutput>" ]],
					"bProcessing": true,
					"bServerSide": true,
					"sPaginationType": "<cfoutput>#attributes.pagination#</cfoutput>",
					"sAjaxSource": "<cfoutput>#attributes.service#</cfoutput>",
					"fnServerData": fnDataTablesPipeline
				});
			});
		<!--- --------------------- AJAX DATA TABLE --------------------- --->
		</script>
	</cfif>

