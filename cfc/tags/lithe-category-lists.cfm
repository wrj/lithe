<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 9/23/13 AD
  Time: 1:46 PM
  To change this template use File | Settings | File Templates.
--->


<cfparam name="attributes.sort" default="desc">
<cfparam name="attributes.url" default="desc">
<cfparam name="attributes.delimiters" type="string" default=">"/>
<cfparam name="attributes.delimitersclass" type="string" default=""/>
<cfoutput>
	<cfif thisTag.executionMode EQ "start">
		<cfhttp url="http://#CGI['HTTP_HOST']#/cfc/services/Category.cfc" method="get" result="objdata">
			<cfhttpparam type="url" name="method" value="categorieslist_custom"/>
			<cfhttpparam type="url" name="sort" value="#attributes.sort#"/>
			<cfhttpparam type="url" name="url" value="#attributes.url#"/>
			<cfhttpparam type="url" name="delimiters" value="#attributes.delimiters#"/>
			<cfhttpparam type="url" name="delimitersclass" value="#attributes.delimitersclass#"/>
		</cfhttp>

<!---					<cfoutput>--->
<!---						#objdata['filecontent']#--->
<!---					</cfoutput>--->
<!---					<cfabort/>--->
		<cfset rawdata = deserializeJSON(objdata['filecontent'])>
<!---					<cfdump var="#rawdata#">--->
		<cfset categorydata = rawdata["contents"]>
		<cfset CALLER["categories"] = StructNew()>
		<cfset CALLER["categories"]["totalrecord"] = rawdata["totalrecord"]>
	</cfif>
</cfoutput>