<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 8/28/13 AD
  Time: 10:04 AM
  To change this template use File | Settings | File Templates.
--->
<cfif thisTag.executionMode EQ "start">
	<cfset basetaglist = getBaseTagList()>
	<cfif listFindNoCase(basetaglist, "cf_lithe-slider") gt 0>
		<cfset data = getBaseTagData("cf_lithe-slider")>
	<cfelseif listFindNoCase(basetaglist, "cf_lithe-html") gt 0>
		<cfset data = getBaseTagData("cf_lithe-html")>
	<cfelseif listFindNoCase(basetaglist, "cf_lithe-gallery-detail") gt 0>
		<cfset data = getBaseTagData("cf_lithe-gallery-detail")>
	<cfelseif listFindNoCase(basetaglist, "cf_lithe-post-detail") gt 0>
		<cfset data = getBaseTagData("cf_lithe-post-detail")>
	<cfelseif listFindNoCase(basetaglist, "cf_lithe-product-detail") gt 0>
		<cfset data = getBaseTagData("cf_lithe-product-detail")>
	</cfif>
	<cfset gallerydata = data['GALLERY']>
	<cfset currentRow = 1>
	<cfset CALLER["image"] = structNew()>
	<cfset caller['image']['totalrecord'] = arraylen(gallerydata)>
	<cfif arraylen(gallerydata) gt 0>
		<cfset CALLER["image"]["currentRow"] = currentRow>
		<cfset CALLER["image"]["title"] = gallerydata[currentRow]["TITLE"]>
		<cfset CALLER["image"]["detail"] = gallerydata[currentRow]["DETAIL"]>
		<cfset CALLER["image"]["link"] = gallerydata[currentRow]["LINK"]>
		<cfset CALLER["image"]["image"] = gallerydata[currentRow]["IMAGE"]>
	</cfif>
</cfif>
<cfif thisTag.executionMode EQ "end">
	<cfset currentRow++>
	<cfif currentRow LE arraylen(gallerydata)>
		<cfset CALLER["image"]["currentRow"] = currentRow>
		<cfset CALLER["image"]["title"] = gallerydata[currentRow]["TITLE"]>
		<cfset CALLER["image"]["detail"] = gallerydata[currentRow]["DETAIL"]>
		<cfset CALLER["image"]["link"] = gallerydata[currentRow]["LINK"]>
		<cfset CALLER["image"]["image"] = gallerydata[currentRow]["IMAGE"]>
		<cfexit method="loop">
	<cfelse>
		<cfexit method="exittag">
	</cfif>
</cfif>
