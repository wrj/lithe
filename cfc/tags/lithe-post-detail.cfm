
<cfparam name="attributes.slug" default="">
<cfparam name="attributes.scope" default="post">
<cfoutput>
	<cfif thisTag.executionMode EQ "start">
		<cfinclude template="/cfc/globalfunction.cfm" runonce="true"/>
		<cfif attributes['slug'] eq "" && isDefined("slug")>
			<cfset attributes.slug = slug>
		</cfif>
		<cfif attributes["slug"] neq "">
			<cfhttp url="http://#CGI['HTTP_HOST']#/cfc/services/post.cfc" method="get" result="objdata">
				<cfhttpparam type="url" name="method" value="postitem"/>
				<cfhttpparam type="url" name="slug" value="#attributes.slug#"/>	
			</cfhttp>
			<cfset rawdata = deserializeJSON(objdata['filecontent'])>
			<cfset CALLER[attributes.scope] = structNew()>
			<cfif rawdata["_id"] neq "">
				<cfset CALLER[attributes.scope] = rawdata>
				<cfset StrEscUtils = createObject("java", "org.apache.commons.lang.StringEscapeUtils") />
				<cfset detailtext = StrEscUtils.unescapeHTML(rawdata['DETAIL'])>
				<cfset detailtext = replaceNoCase(detailtext, "##", "####", 'all')>
				<cfset detailtext = replaceNoCase(detailtext, "####buildlink", "##buildlink", 'all')>
				<cfset detailtext = replaceNoCase(detailtext, ")####", ")##", 'all')>
				<cfset CALLER[attributes.scope]["DETAIL"] = evaluate(DE(detailtext))>
				<cfset CALLER[attributes.scope]["totalrecord"] = 1>
				<cfif isArray(rawdata["TAG"])>
					<cfset tag = rawdata["TAG"]>
				<cfelse>
					<cfset tag = arraynew()>
				</cfif>
				<cfif structKeyExists(rawdata, "GALLERY")>
					<cfset gallery = rawdata["GALLERY"]>
				<cfelse>
					<cfset gallery = arraynew()>
				</cfif>
				<cfif structKeyExists(rawdata, "RELATEITEM")>
					<cfset CALLER[attributes.scope]["relateditemtotalrecord"] = arraylen(rawdata["RELATEITEM"])>
				<cfelse>
					<cfset CALLER[attributes.scope]["relateditemtotalrecord"] = 0>
				</cfif>
			<cfelse>
				<cfset CALLER[attributes.scope]["totalrecord"] = 0>
			</cfif>
		<cfelse>
			<cfset CALLER[attributes.scope] = structNew()>
			<cfset CALLER[attributes.scope]["totalrecord"] = 0>
		</cfif>
	<cfelse>
		<cfset CALLER[attributes.scope] = structNew()>
		<cfset CALLER[attributes.scope]["totalrecord"] = 0>
	</cfif>
</cfoutput>
