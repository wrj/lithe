<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 9/12/13 AD
  Time: 11:51 AM
  To change this template use File | Settings | File Templates.
--->

<cfif thisTag.executionMode EQ "start">
	<cfset data = getBaseTagData("cf_lithe-breadcrumb")>
	<cfset breadcrumbdata = ArrayNew()>
	<cfif structKeyExists(data, "breadcrumb")>
		<cfset breadcrumbdata = data["breadcrumb"]>
	</cfif>
	<cfset currentRow = 1>
	<cfset CALLER["breadcrumb"] = structNew()>
	<cfif arraylen(breadcrumbdata) gt 0>
		<cfset CALLER["breadcrumb"] = breadcrumbdata[currentRow]>
		<cfset CALLER["breadcrumb"]["currentRow"] = currentRow>
	</cfif>
</cfif>
<cfif thisTag.executionMode EQ "end">
	<cfset currentRow++>
	<cfif currentRow LE arraylen(breadcrumbdata)>
		<cfset CALLER["breadcrumb"] = breadcrumbdata[currentRow]>
		<cfset CALLER["breadcrumb"]["currentRow"] = currentRow>
		<cfexit method="loop">
		<cfelse>
		<cfexit method="exittag">
	</cfif>
</cfif>