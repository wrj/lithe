<cfparam name="attributes.pagelink" type="string" default="" />
<cfparam name="attributes.otherparams" type="string" default="" />
<cfparam name="attributes.classdesktop" type="string" default="pagination-left"/>
<cfparam name="attributes.classmobile" type="string" default="pagination-left"/>
<cfparam name="attributes.template" type="string" default=""/>
<cfparam name="attributes.pagetype" type="string" default="short">
<cfparam name="attributes.pagelength" type="numeric" default=3>
<cfparam name="attributes.pattern" type="string" default="">
<cfif thisTag.executionMode EQ "start">
	<cfinclude template="/cfc/globalfunction.cfm" runonce="true"/>
	<cfinclude template="/cfc/config.cfm"/>
	<cfset configdata = readconfig()>
	<cfset data = getBaseTagData("cf_lithe-post-lists")>
	<cfset totalpage = ceiling(data["rawdata"]["totalrecord"] / data["attributes"]["LIMIT"])>
	<cfif attributes.pagelink eq "">
		<cfset linkpage = ListLast(cgi.script_name,'/')>
		<cfelse>
		<cfset linkpage = attributes["pagelink"]>
	</cfif>
	<cfset linkpage="">
	<cfif attributes.pattern neq "">
		<cfset linkpage = configdata["general"]["siteaddress"]&''&attributes.pattern>

		<cfloop array="#structKeyArray(attributes)#" index="item">
			<cfif item neq "page">
				<cfset linkpage = replaceNoCase(linkpage, "{#item#}", attributes[item],"all")>
			</cfif>
		</cfloop>
		<cfloop list="#rewritepatternurl#" delimiters="," index="reurl">
			<cfset linkpage = replaceNoCase(linkpage, reurl, "", 'all')>
		</cfloop>
		<cfelse>
		<cfset urlparamspage = "">
		<cfset StructDelete(url,'PAGE')>
		<cfset StructDelete(url,'page')>
		<cfif StructIsEmpty(url) eq false>
			<cfloop list="#structKeylist(url)#" index="item">
				<cfset urlparamspage = urlparamspage&"#LCase(item)#=#url[item]#&">
			</cfloop>
		</cfif>
		<cfif attributes.otherparams neq "">
			<cfif urlparamspage neq "">
				<cfset urlparamspage = "#urlparamspage##attributes.otherparams#&">
				<cfelse>
				<cfset urlparamspage = "#attributes.otherparams#&">
			</cfif>
		</cfif>
		<cfif urlparamspage neq "">
			<cfset linkpage = "?#urlparamspage#page={page}">
			<cfelse>
			<cfset linkpage = "?page={page}">
		</cfif>
	</cfif>
	<cfset nowpage = data["attributes"]["thispage"]>
	<!--- Render Group BTN --->
	<cfoutput>
		<cfif nowpage neq 1>
			<!--- First Btn --->
			<cfsavecontent variable="desktopfirstgroup">
				<li><a href="#replacenocase(linkpage,'{page}',"1",'all')#">First</a></li>
			</cfsavecontent>
			<!--- previous button group --->
			<cfsavecontent variable="desktopprevgroup">
				<li><a href="#replacenocase(linkpage,'{page}',nowpage-1,'all')#">Prev</a></li>
			</cfsavecontent>
			<cfelse>
			<!--- First Btn --->
			<cfsavecontent variable="desktopfirstgroup">
				<li class="disabled"><a href="javascript:void(0);">First</a></li>
			</cfsavecontent>
			<!--- previous button group --->
			<cfsavecontent variable="desktopprevgroup">
				<li class="disabled"><a href="javascript:void(0);">Prev</a></li>
			</cfsavecontent>
		</cfif>
		<!--- Loop Number of page --->
		<cfif attributes.pagetype eq "short">
			<cfset startpage = 1>
			<cfif attributes.pagelength gt totalpage>
				<cfset stoppage = totalpage>
				<cfelse>
				<cfset stoppage = attributes.pagelength>
			</cfif>
			<!--- <cfset pluspage = ceiling((attributes.pagelength-1)/2)>
			  <cfset minuspage = (attributes.pagelength-1) - pluspage>
			  <cfif nowpage eq 1>
				<cfset calstop = 1 + (attributes.pagelength - 1)>
				<cfif calstop gt totalpage>
				  <cfset stoppage = totalpage>
				<cfelse>
				  <cfset stoppage = calstop>
				</cfif>
			  <cfelse>
				<cfif (nowpage - (attributes.pagelength-1)) lte 1>
				  <cfset calstart = 1>
				<cfelse>
				  <cfset calstart = nowpage - pluspage>
				</cfif>
				<cfif calstart lte 1>
				  <cfset calstop = 1 + (attributes.pagelength - 1)>
				  <cfif calstop gt totalpage>
					<cfset stoppage = totalpage>
				  <cfelse>
					<cfset stoppage = calstop>
				  </cfif>
				<cfelse>
				  <cfset startpage = calstart>
				  <cfset calstop = nowpage + minuspage>
				  <cfif calstop gte totalpage>
					<cfset stoppage = totalpage>
				  <cfelse>
					<cfset stoppage = calstop>
				  </cfif>
				</cfif>
			  </cfif> --->
			<cfif nowpage neq 1>
				<cfif nowpage eq totalpage>
					<cfset startpage = totalpage - (attributes.pagelength - 1)>
					<cfif startpage lte 0>
						<cfset startpage = 1>
					</cfif>
					<cfset stoppage = totalpage>
					<cfelse>
					<cfset startpage = nowpage - 1>
					<cfset stoppage = nowpage + 1>
				</cfif>
			</cfif>
			<cfelse>
			<cfset startpage = 1>
			<cfset stoppage = totalpage>
		</cfif>
		<cfsavecontent variable="desktopnumbergroup">
			<cfloop from="#startpage#" to="#stoppage#" index="i">
				<cfif i eq nowpage>
					<li class="active"><a href="javascript:void(0);">#i#</a></li>
					<cfelse>
					<li><a href="#replacenocase(linkpage,'{page}',i,'all')#">#i#</a></li>
				</cfif>
			</cfloop>
		</cfsavecontent>
		<!--- Next Button group --->
		<cfif totalpage neq 0>
			<cfif nowpage neq totalpage>
				<cfsavecontent variable="desktopnextgroup">
					<li><a href="#replacenocase(linkpage,'{page}',nowpage+1,'all')#">Next</a></li>
				</cfsavecontent>
				<cfsavecontent variable="desktoplastgroup">
					<li><a href="#replacenocase(linkpage,'{page}',totalpage,'all')#">Last</a></li>
				</cfsavecontent>
				<cfelse>
				<cfsavecontent variable="desktopnextgroup">
					<li class="disabled"><a href="javascript:void(0)">Next</a></li>
				</cfsavecontent>
				<cfsavecontent variable="desktoplastgroup">
					<li class="disabled"><a href="javascript:void(0)">Last</a></li>
				</cfsavecontent>
			</cfif>
			<cfelse>
			<cfsavecontent variable="desktopnextgroup">
				<li class="disabled"><a href="javascript:void(0)">Next</a></li>
			</cfsavecontent>
			<cfsavecontent variable="desktoplastgroup">
				<li class="disabled"><a href="javascript:void(0)">Last</a></li>
			</cfsavecontent>
		</cfif>
	</cfoutput>
	<!---End Render Group BTN --->
	<!--- Chose Template --->
	<cfset fileread = "">
	<cfif attributes.template neq "">
		<cfset foundtemplate = FileExists("#expandPath('/')#template/#attributes.template#.cfm")>
		<cfif foundtemplate>
			<cffile action="read" file="#expandPath('/')#template/#attributes.template#.cfm" variable="filedata"/>
			<cfset fileread = filedata>
		</cfif>
	</cfif>
	<cfif fileread eq "">
		<cfoutput>
			<cfsavecontent variable="fileread">
				<div class="pagination hidden-phone #attributes.classdesktop#">
			<ul>
				<!--- previous button group --->
					{{desktop-first-group}}
					{{desktop-prev-group}}
				<!--- Loop Number of page --->
					{{desktop-number-group}}
				<!--- Next Button group --->
					{{desktop-next-group}}
					{{desktop-last-group}}
				</ul>
				</div>
						<div class="pagination pagination-centered hidden-tablet hidden-desktop #attributes.classmobile#">
			<ul>
				<!--- previous button group --->
					{{desktop-prev-group}}
				<!--- Next Button group --->
					{{desktop-next-group}}
				</ul>
				</div>
			</cfsavecontent>
		</cfoutput>
	</cfif>
	<!--- End Chose Template --->
	<cfset fileread = replaceNoCase(fileread, "{{desktop-first-group}}", desktopfirstgroup,"all")>
	<cfset fileread = replaceNoCase(fileread, "{{desktop-prev-group}}", desktopprevgroup,"all")>
	<cfset fileread = replaceNoCase(fileread, "{{desktop-number-group}}", desktopnumbergroup,"all")>
	<cfset fileread = replaceNoCase(fileread, "{{desktop-next-group}}", desktopnextgroup,"all")>
	<cfset fileread = replaceNoCase(fileread, "{{desktop-last-group}}", desktoplastgroup,"all")>
	<cfoutput>
		#fileread#
	</cfoutput>
</cfif>