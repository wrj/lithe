<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 9/16/13 AD
  Time: 3:03 PM
  To change this template use File | Settings | File Templates.
--->
<cfset thistemplate="http://#CGI.HTTP_HOST##CGI.SCRIPT_NAME#">
<cfparam name="attributes.formaction" default="contactsave.cfm">
<cfparam name="attributes.formredirect" default="#thistemplate#">
<cfparam name="attributes.successmessage" default="success">
<cfparam name="attributes.errorrecaptchamessage" default="invalid recaptcha">
<cfparam name="attributes.errormessage" default="error">
<cfparam name="attributes.formclass" default="">
<cfparam name="attributes.method" default="post">
<cfparam name="attributes.enctype" default="multipart/form-data">
<cfparam name="attributes.recaptcha" default="false">
<cfparam name="attributes.email" default="false">
<cfparam name="attributes.emailtemplate" default="contact.txt">
<cfoutput>
	<cfif thisTag.executionMode EQ "start">
		<cfsilent><cfinclude template="readconfig.cfm"></cfsilent>
<!---	RECAPCHA	--->

<!---	END RECAPCHA	--->

		<form action="#attributes.formaction#" class="#attributes.formclass#" method="#attributes.method#" enctype="#attributes.enctype#">
			<input type="hidden" name="option_formredirect" value="#attributes.formredirect#">
			<input type="hidden" name="option_successmessage" value="#attributes.successmessage#">
			<input type="hidden" name="option_errormessage" value="#attributes.errormessage#">
			<input type="hidden" name="option_errorrecaptchamessage" value="#attributes.errorrecaptchamessage#">
		    <input type="hidden" name="option_userecaptcha" value="#attributes.recaptcha#">
			<input type="hidden" name="option_useemail" value="#attributes.email#">
			<input type="hidden" name="option_emailtemplate" value="#attributes.emailtemplate#">
	</cfif>
	<cfif thisTag.executionMode EQ "end">
		</form>
	</cfif>
</cfoutput>