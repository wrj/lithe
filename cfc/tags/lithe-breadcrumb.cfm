<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 9/6/13 AD
  Time: 12:00 PM
  To change this template use File | Settings | File Templates.
--->
<cfinclude template="readconfig.cfm">
<cfset siteaddress=configJSON['general']['siteaddress']>
<cfparam name="attributes.slug" type="string" default="" />
<cfparam name="attributes.category" type="string" default="" />
<cfparam name="attributes.template" type="string" default=""/>
<cfparam name="attributes.delimiters" type="string" default="/"/>
<cfparam name="attributes.delimitersclass" type="string" default="divider"/>
<cfparam name="attributes.hometitle" type="string" default="HOME"/>
<cfparam name="attributes.homeurl" type="string" default=""/>
<cfoutput>
	<cfif thisTag.executionMode EQ "start">
		<cfinclude template="/cfc/customtagsfunction.cfm" runonce="true"/>
		<cfset methodname="">
        <cfif attributes["category"] IS "" AND attributes["slug"] IS "">
			<cfif isDefined("slug")>
				<cfset attributes["slug"] = slug>
			</cfif>
			<cfif isDefined("category")>
				<cfset attributes["category"] = category>
			</cfif>
        </cfif>
		<cfset haveattribute = true>
		<cfif attributes["slug"] IS NOT "" AND attributes["category"] IS "">
			<cfset methodname="breadcrumblistbyslug_custom">
		<cfelseif attributes["category"] IS NOT "" AND attributes["slug"] IS "">
			<cfset methodname="breadcrumblistbycategory_custom">
		<cfelse>
			<cfset haveattribute = false>
		</cfif>
		<cfset homelink="#siteaddress#/#attributes.homeurl#">
		<cfhttp url="http://#CGI['HTTP_HOST']#/cfc/services/tree.cfc?" method="get" result="objdata">
			<cfhttpparam type="url" name="method" value="#methodname#"/>
			<cfhttpparam type="url" name="category" value="#attributes.category#"/>
			<cfhttpparam type="url" name="slug" value="#attributes.slug#"/>
			<cfhttpparam type="url" name="hometitle" value="#attributes.hometitle#"/>
			<cfhttpparam type="url" name="homeurl" value="#homelink#"/>
			<cfhttpparam type="url" name="language" value="#session["language"]#"/>
		</cfhttp>
<!---		<cfoutput>--->
<!---			#objdata['filecontent']#--->
<!---		</cfoutput>--->
		<cfset breadcrumb = arrayNew()>
		<cfif haveattribute>
			<cfset breadcrumb = deserializeJSON(objdata['filecontent'])>
		</cfif>
		<cfif NOT arrayIsEmpty(breadcrumb)>
			<cfset CALLER["breadcrumbs"] = structNew()>
			<cfset CALLER["breadcrumbs"]['totalrecord'] = arraylen(breadcrumb)>
		<cfelse>
			<cfset CALLER["breadcrumbs"] = structNew()>
			<cfset CALLER["breadcrumbs"]['totalrecord'] = 0>
		</cfif>
		<cfif attributes.template neq "" AND NOT arrayIsEmpty(breadcrumb)>
			<cfset foundtemplate = FileExists("#expandPath('/')#template/#attributes.template#.cfm")>
			<cfif foundtemplate>
				<cffile action="read" file="#expandPath('/')#template/#attributes.template#.cfm" variable="fileraw"/>
				<cfset i=1>
				#listFirst(fileraw,"[,]")#
				<cfset filearr = listtoarray(fileraw,"[,]",true)>
				<cfloop array="#breadcrumb#" index="breadcrumbdata">
					<cfset outputdata = "">
					<cfset filedata = "">
					<cfoutput>
						<cfif i LT arraylen(breadcrumb)>
							<cfset filedata = filearr[2]>
							<cfset filedata = cfcondition(filedata,breadcrumbdata,i,arraylen(breadcrumb))>
							<cfset filedata = replaceNoCase(filedata, "{{TITLE}}", breadcrumbdata["TITLE"],"all")>
							<cfset filedata = replaceNoCase(filedata, "{{URL}}", "#breadcrumbdata["URL"]#","all")>
							<cfset filedata = replaceNoCase(filedata, "{{DELIMITERS}}", attributes["DELIMITERS"],"all")>
							<cfset filedata = replaceNoCase(filedata, "{{DELIMITERSCLASS}}", attributes["DELIMITERSCLASS"],"all")>
							<cfset filedata = replaceNoCase(filedata, "{{ACTIVE}}","","all")>
							#filedata#
						<cfelse>
							<cfset filedata = filearr[3]>
							<cfset filedata = cfcondition(filedata,breadcrumbdata,i,arraylen(breadcrumb))>
							<cfset filedata = replaceNoCase(filedata, "{{TITLE}}", breadcrumbdata["TITLE"],"all")>
							<cfset filedata = replaceNoCase(filedata, "{{URL}}", breadcrumbdata["URL"])>
							<cfset filedata = replaceNoCase(filedata, "{{DELIMITERS}}", "","all")>
							<cfset filedata = replaceNoCase(filedata, "{{DELIMITERSCLASS}}","","all")>
							<cfset filedata = replaceNoCase(filedata, "{{ACTIVE}}","active","all")>
							#filedata#
						</cfif>
					</cfoutput>
					<cfset i++>
				</cfloop>
				#listlast(fileraw,"[,]")#
			</cfif>
		</cfif>
	</cfif>
</cfoutput>