<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 8/29/13 AD
  Time: 1:53 PM
  To change this template use File | Settings | File Templates.
--->

<cfparam name="attributes.template" type="string" default=""/>
<cfparam name="attributes.activepage" type="string" default=""/>
<cfparam name="attributes.activeclass" type="string" default="active"/>
<cfparam name="attributes.allchildren" type="boolean" default=true/>
<cfif thisTag.executionMode EQ "start">
	<cfinclude template="/cfc/customtagsfunction.cfm" runonce="true"/>
	<cfset data = getBaseTagData("cf_lithe-menu")>
	<cfset menudata = ArrayNew()>
	<cfif structKeyExists(data, "menudata")>
		<cfset menudata = data["menudata"]>
	</cfif>
	<cfif attributes.template neq "">
		<cfset foundtemplate = FileExists("#expandPath('/')#template/#attributes.template#.cfm")>
		<cfif foundtemplate>
			<cffile action="read" file="#expandPath('/')#template/#attributes.template#.cfm" variable="fileraw" charset="utf-8"/>
			<cfset template = arrayNew()>
			<cfif findNoCase("[[,]]",fileraw) IS 0>
				<cfset template = [fileraw]>
				<cfelse>
				<cfset template = listtoarray(fileraw,"[[,]]",true)>
			</cfif>
			<cffunction name="recursive" output="false" access="public" returntype="any">
				<cfargument name="menudata" type="any" required="true">
				<cfargument name="level" type="numeric" default="1" required="false">
				<cfset var outputdata = "">
				<cfset var filedata = "">
				<cfoutput>
					<cfsavecontent variable="outputdata">
						<cfset var filetemp="">
						<cfif arguments.level LTE arraylen(template)>
							<cfset filetemp = listtoarray(template[arguments.level],"[,]",true)>
							<cfelse>
							<cfif attributes.allchildren>
								<cfset filetemp = listtoarray(template[arraylen(template)],"[,]",true)>
								<cfelse>
								<cfset filetemp = ["","",""]>
							</cfif>
						</cfif>
						#filetemp[1]#
						<cfset var i=1>
						<cfloop array="#arguments.menudata#" index="menuitem">
							<cfset var filedata = "">
							<cfif structKeyExists(menuitem,"children") >
								<cfset filedata = filetemp[arraylen(filetemp)-2]>
								<cfset filedata = cfcondition(filedata,menuitem,i,arraylen(arguments.menudata))>
								<cfset filedata = replaceNoCase(filedata,"{{NAME}}",menuitem["name"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{DESCRIPTION}}",menuitem["description"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{IMAGEFILE}}",menuitem["imagefile"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{IMAGENAME}}",menuitem["imagename"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{LINKNAME}}",menuitem["linkname"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{LINKURL}}",menuitem["linkurl"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{CLASS}}",menuitem["class"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{ID}}",menuitem["id"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{INDEX}}",menuitem["index"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{LINKTYPE}}",menuitem["linktype"],"all")>
								<cfif menuitem["name"] IS attributes.activepage>
									<cfset filedata = replaceNoCase(filedata,"{{ACTIVE}}",attributes.activeclass,"all")>
								<cfelse>
									<cfset filedata = replaceNoCase(filedata,"{{ACTIVE}}","","all")>
								</cfif>
								#filedata#
								#recursive(menuitem["children"],(arguments['level']+1))#
								#cfcondition(filetemp[arraylen(filetemp)-1],menuitem,i,arraylen(arguments.menudata))#
							<cfelse>
								<cfset filedata = filetemp[2]>
								<cfset filedata = cfcondition(filedata,menuitem,i,arraylen(arguments.menudata))>
								<cfset filedata = replaceNoCase(filedata,"{{NAME}}",menuitem["name"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{DESCRIPTION}}",menuitem["description"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{IMAGEFILE}}",menuitem["imagefile"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{IMAGENAME}}",menuitem["imagename"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{LINKNAME}}",menuitem["linkname"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{LINKURL}}",menuitem["linkurl"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{CLASS}}",menuitem["class"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{ID}}",menuitem["id"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{INDEX}}",menuitem["index"],"all")>
								<cfset filedata = replaceNoCase(filedata,"{{LINKTYPE}}",menuitem["linktype"],"all")>
								<cfif menuitem["name"] IS attributes.activepage>
									<cfset filedata = replaceNoCase(filedata,"{{ACTIVE}}",attributes.activeclass,"all")>
								<cfelse>
									<cfset filedata = replaceNoCase(filedata,"{{ACTIVE}}","","all")>
								</cfif>
								#filedata#
								#cfcondition(filetemp[arraylen(filetemp)-1],menuitem,i,arraylen(arguments.menudata))#
							</cfif>
							<cfset i++>
						</cfloop>
						#filetemp[arraylen(filetemp)]#
					</cfsavecontent>
				</cfoutput>
				<cfreturn outputdata>
			</cffunction>
		</cfif>
	</cfif>
	<cfoutput>
		#recursive(menudata)#
	</cfoutput>
</cfif>


