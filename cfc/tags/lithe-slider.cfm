<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 8/28/13 AD
  Time: 9:55 AM
  To change this template use File | Settings | File Templates.
--->

<cfparam name="attributes.slug" default="">
<cfoutput>
	<cfif thisTag.executionMode EQ "start">
		<cfinclude template="/cfc/globalfunction.cfm" runonce="true"/>
		<cfif attributes['slug'] eq "" && isDefined("slug")>
			<cfset attributes.slug = slug>
		</cfif>
		<cfif attributes["slug"] neq "">
			<cfhttp url="http://#CGI['HTTP_HOST']#/cfc/services/slider.cfc" method="get" result="objdata">
				<cfhttpparam type="url" name="method" value="slideshow"/>
				<cfhttpparam type="url" name="slug" value="#attributes.slug#"/>
			</cfhttp>
			<cfset rawdata = deserializeJSON(objdata['filecontent'])>
			<cfset CALLER["slider"] = structNew()>
			<cfif rawdata["_id"] neq "">
				<cfset CALLER["slider"] = rawdata>
				<cfset StrEscUtils = createObject("java", "org.apache.commons.lang.StringEscapeUtils") />
				<cfset detailtext = StrEscUtils.unescapeHTML(rawdata['DETAIL'])>
				<cfset detailtext = replaceNoCase(detailtext, "##", "####", 'all')>
				<cfset detailtext = replaceNoCase(detailtext, "####buildlink", "##buildlink", 'all')>
				<cfset detailtext = replaceNoCase(detailtext, ")####", ")##", 'all')>
				<cfset CALLER["slider"]["DETAIL"] = evaluate(DE(detailtext))>
				<cfset CALLER["slider"]["totalrecord"] = 1>
				<cfif structKeyExists(rawdata, "GALLERY")>
					<cfset gallery = rawdata["GALLERY"]>
				<cfelse>
					<cfset gallery = arrayNew()>
				</cfif>
			<cfelse>
				<cfset CALLER["slider"]["totalrecord"] = 0>
			</cfif>
		<cfelse>
			<cfset CALLER["slider"] = structNew()>
			<cfset CALLER["slider"]["totalrecord"] = 0>
		</cfif>
	<cfelse>
		<cfset CALLER["slider"] = structNew()>
		<cfset CALLER["slider"]["totalrecord"] = 0>
	</cfif>
</cfoutput>