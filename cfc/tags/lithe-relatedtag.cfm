<cfif thisTag.executionMode EQ "start">
    <cfset tagdata = arraynew()>
    <cfset basetaglist = getBaseTagList()>
    <!--- <cfif listFindNoCase(basetaglist, "CF_LITHE-RELATEDITEM") gt 0> --->
    <cfset data = getBaseTagData("CF_LITHE-RELATEDITEM")>    
    <!--- <cfelseif listFindNoCase(basetaglist, "CF_LITHE-POST-RELATEDITEM") gt 0>
        <cfset data = getBaseTagData("CF_LITHE-POST-RELATEDITEM")>
    </cfif> --->
    <cfset tagdata = data["relatedtag"]>
    <cfset currentRow = 1>
    <cfset CALLER["relatedtag"] = structNew()>
    <cfset caller['relatedtag']['totalrecord'] = arraylen(tagdata)>
    <cfif arraylen(tagdata) gt 0>
        <cfset CALLER["relatedtag"]["currentRow"] = currentRow>
        <cfset CALLER["relatedtag"]["name"] = tagdata[currentRow]>
    </cfif>
</cfif>
<cfif thisTag.executionMode EQ "end">
    <cfset currentRow++>
    <cfif currentRow LE arraylen(tagdata)>
        <cfset CALLER["relatedtag"]["name"] = tagdata[currentRow]>
        <cfset CALLER["relatedtag"]["currentRow"] = currentRow>
        <cfexit method="loop">
    <cfelse>
        <cfexit method="exittag">
    </cfif>
</cfif>