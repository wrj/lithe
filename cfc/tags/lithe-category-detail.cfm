<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 9/6/13 AD
  Time: 9:59 AM
  To change this template use File | Settings | File Templates.
--->

<cfparam name="attributes.category" default="">
<cfparam name="attributes.scope" default="category">
<cfoutput>
	<cfif thisTag.executionMode EQ "start">
		<cfinclude template="/cfc/globalfunction.cfm" runonce="true"/>
		<cfif attributes['category'] eq "" && isDefined("category")>
			<cfset attributes.category = category>
		</cfif>
		<cfif attributes["category"] neq "">
			<cfhttp url="http://#CGI['HTTP_HOST']#/cfc/services/Category.cfc" method="get" result="objdata">
				<cfhttpparam type="url" name="method" value="categorydetail"/>
				<cfhttpparam type="url" name="slug" value="#attributes.category#"/>
			</cfhttp>
			<cfset rawdata = deserializeJSON(objdata['filecontent'])>
<!---			<cfdump var="#rawdata#">--->
<!---			<cfoutput>--->
<!---				#objdata['filecontent']#--->
<!---			</cfoutput>--->
<!---			<cfabort/>--->
			<cfset CALLER[attributes.scope] = structNew()>
			<cfif rawdata["id"] neq "">
				<cfset CALLER[attributes.scope] = rawdata>
				<cfset StrEscUtils = createObject("java", "org.apache.commons.lang.StringEscapeUtils") />
				<cfset detailtext = StrEscUtils.unescapeHTML(rawdata['DETAIL'])>
				<cfset detailtext = replaceNoCase(detailtext, "##", "####", 'all')>
				<cfset detailtext = replaceNoCase(detailtext, "####buildlink", "##buildlink", 'all')>
				<cfset detailtext = replaceNoCase(detailtext, ")####", ")##", 'all')>
				<cfset CALLER[attributes.scope]["DETAIL"] = evaluate(DE(detailtext))>
				<cfset CALLER[attributes.scope]["totalrecord"] = 1>
			<cfelse>
				<cfset CALLER[attributes.scope]["totalrecord"] = 0>
			</cfif>
		<cfelse>
			<cfset CALLER[attributes.scope] = structNew()>
			<cfset CALLER[attributes.scope]["totalrecord"] = 0>
		</cfif>
		<cfelse>
		<cfset CALLER[attributes.scope] = structNew()>
		<cfset CALLER[attributes.scope]["totalrecord"] = 0>
	</cfif>
</cfoutput>