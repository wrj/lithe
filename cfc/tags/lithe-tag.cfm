<cfsilent>
<cfif thisTag.executionMode EQ "start">
    <cfset tagdata = arraynew()>
    <cfset basetaglist = getBaseTagList()>
    <cfif listFindNoCase(basetaglist, "CF_LITHE-POST-DETAIL") gt 0>
        <cfset data = getBaseTagData("CF_LITHE-POST-DETAIL")>    
    <cfelseif listFindNoCase(basetaglist, "CF_LITHE-PRODUCT-DETAIL") gt 0>
        <cfset data = getBaseTagData("CF_LITHE-PRODUCT-DETAIL")>
    <cfelseif listFindNoCase(basetaglist, "CF_LITHE-POST-LIST") gt 0>
        <cfset data = getBaseTagData("CF_LITHE-POST-LIST")>
    <cfelseif listFindNoCase(basetaglist, "CF_LITHE-TAG-CLOUDS") gt 0>
	    <cfset data = getBaseTagData("CF_LITHE-TAG-CLOUDS")>
    <cfelseif listFindNoCase(basetaglist, "CF_LITHE-RELATEDITEM") gt 0>
	    <cfset data = getBaseTagData("CF_LITHE-RELATEDITEM")>
    </cfif>
    <cfset tagdata = data["tag"]>
    <cfset currentRow = 1>
    <cfset CALLER["tag"] = structNew()>
    <cfset caller['tag']['totalrecord'] = arraylen(tagdata)>
    <cfif arraylen(tagdata) gt 0>
        <cfset CALLER["tag"]["currentRow"] = currentRow>
        <cfset CALLER["tag"]["name"] = tagdata[currentRow]>
    </cfif>
</cfif>
<cfif thisTag.executionMode EQ "end">
    <cfset currentRow++>
    <cfif currentRow LE arraylen(tagdata)>
        <cfset CALLER["tag"]["name"] = tagdata[currentRow]>
        <cfset CALLER["tag"]["currentRow"] = currentRow>
        <cfexit method="loop">
    <cfelse>
        <cfexit method="exittag">
    </cfif>
</cfif>
</cfsilent>