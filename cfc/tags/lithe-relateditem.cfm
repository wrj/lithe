<cfif thisTag.executionMode EQ "start">
    <cfset basetaglist = getBaseTagList()>
    <cfif listFindNoCase(basetaglist, "CF_LITHE-POST-DETAIL") gt 0>
        <cfset data = getBaseTagData("CF_LITHE-POST-DETAIL")>    
    <cfelseif listFindNoCase(basetaglist, "CF_LITHE-PRODUCT-DETAIL") gt 0>
        <cfset data = getBaseTagData("CF_LITHE-PRODUCT-DETAIL")>
    </cfif>
    <cfset relateddata = ArrayNew()>
    <cfif structKeyExists(data, "rawdata")>
        <cfif structKeyExists(data['rawdata'], "RELATEITEM")>
            <cfset relateddata = data['rawdata']['RELATEITEM']>
        </cfif>
    </cfif>
    <cfset currentRow = 1>
    <cfif arrayLen(relateddata) gt 0>
        <cfset CALLER["relateditem"] = structNew()>
        <cfset CALLER["relateditem"] = relateddata[currentRow]>
        <cfset CALLER["relateditem"]["currentrow"] = currentRow>
        <cfif IsArray(relateddata[currentRow]["TAG"])>
            <cfset relatedtag = relateddata[currentRow]["TAG"]>
        <cfelse>
            <cfset relatedtag = arraynew()>
        </cfif>
    </cfif>
</cfif>
<cfif thisTag.executionMode EQ "end">
    <cfset currentRow++>
    <cfif currentRow LE arraylen(relateddata)>
        <cfset CALLER["relateditem"] = structNew()>
        <cfset CALLER["relateditem"] = relateddata[currentRow]>
        <cfset CALLER["relateditem"]["currentrow"] = currentRow>
        <cfif IsArray(relateddata[currentRow]["TAG"])>
            <cfset relatedtag = relateddata[currentRow]["TAG"]>
        <cfelse>
            <cfset relatedtag = arraynew()>
        </cfif>
        <cfexit method="loop">
    <cfelse>
        <cfexit method="exittag">
    </cfif>
</cfif>