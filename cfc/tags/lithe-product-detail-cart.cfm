<cfparam name="attributes.addbuttonname" type="string" default="Add to Cart">
<cfif thisTag.executionMode EQ "start">
	<cfset data = getBaseTagData("cf_lithe-product-detail")>
	<cfif structKeyExists(data, "rawdata")>
		<cfoutput>
			<p>
			<input type="hidden" class="lithe-product-productkey" value="#data['rawdata']['_id']#">
			<input type="hidden" class="lithe-product-author" value="#data['rawdata']['AUTHOR']#">
			<input type="hidden" class="lithe-product-productsku" value="#data['rawdata']['SKU']#">
			<input type="hidden" class="lithe-product-slug" value="#data['rawdata']['SLUG']#">
			<input type="hidden" class="lithe-product-price" value="#data['rawdata']['PRICE']#">
			<input type="hidden" class="lithe-product-title" value="#data['rawdata']['TITLE']#">
			<input type="hidden" class="lithe-product-pdetail" value="#data['rawdata']['DETAIL']#">
			<input type="hidden" class="lithe-product-intro" value="#data['rawdata']['INTRO']#">
			<input type="hidden" class="lithe-product-sthumb" value="#data['rawdata']['STHUMB']#">
			<input type="hidden" class="lithe-product-lthumb" value="#data['rawdata']['LTHUMB']#">
			<input type="hidden" class="lithe-product-categorytitle" value="#data['rawdata']['CATEGORYTITLE']#">
			<input type="button" value="#attributes.addbuttonname#" class="lithe-product-addbutton btn">
			</p>
		</cfoutput>
	</cfif>
</cfif>



