<cfparam name="attributes.slug" default="">
<cfoutput>
	<cfif thisTag.executionMode EQ "start">
		<cfif attributes['slug'] eq "" && isDefined("slug")>
			<cfset attributes.slug = slug>
		</cfif>
		<cfif attributes["slug"] neq "">
			<cfhttp url="http://#CGI['HTTP_HOST']#/cfc/services/post.cfc" method="get" result="objdata">
				<cfhttpparam type="url" name="method" value="postitem"/>
				<cfhttpparam type="url" name="slug" value="#attributes.slug#"/>	
				<cfhttpparam type="url" name="language" value="#session["language"]#"/>
			</cfhttp>
			<cfset rawdata = deserializeJSON(objdata['filecontent'])>
			<cfset CALLER["product"] = structNew()>
			<cfif rawdata["_id"] neq "">
				<cfset CALLER["product"] = rawdata>
				<cfset StrEscUtils = createObject("java", "org.apache.commons.lang.StringEscapeUtils") />
				<cfset detailtext = StrEscUtils.unescapeHTML(rawdata['DETAIL'])>
				<cfset detailtext = replaceNoCase(detailtext, "##", "####", 'all')>
				<cfset detailtext = replaceNoCase(detailtext, "####buildlink", "##buildlink", 'all')>
				<cfset detailtext = replaceNoCase(detailtext, ")####", ")##", 'all')>
				<cfset CALLER["product"]["DETAIL"] = evaluate(DE(detailtext))>
				<cfset CALLER["product"]["totalrecord"] = 1>
				<cfif isArray(rawdata["TAG"])>
					<cfset tag = rawdata["TAG"]>
				<cfelse>
					<cfset tag = arraynew()>
				</cfif>
				<cfif structKeyExists(rawdata, "GALLERY")>
					<cfset gallery = rawdata["GALLERY"]>
				<cfelse>
					<cfset gallery = arrayNew()>
				</cfif>
				<cfif structKeyExists(rawdata, "RELATEITEM")>
					<cfset CALLER["product"]["relateditemtotalrecord"] = arraylen(rawdata["RELATEITEM"])>
				<cfelse>
					<cfset CALLER["product"]["relateditemtotalrecord"] = 0>
				</cfif>
			<cfelse>
				<cfset CALLER["product"]["totalrecord"] = 0>
			</cfif>
		<cfelse>
			<cfset CALLER["product"] = structNew()>
			<cfset CALLER["product"]["totalrecord"] = 0>
		</cfif>
	<cfelse>

	</cfif>
</cfoutput>
<cffunction name="buildlink" access="private" returntype="String">
	<cfargument name="slug" type="string" required="true">
	<cfargument name="template" type="string" required="true">
	<cfargument name="otherparams" type="string" required="false" default="">
	<cfinclude template="/cfc/config.cfm"/>
	<cfset var htmloutput = replaceNoCase(patternurl, "{template}", "#arguments['template']#", 'all')>
	<cfset var htmloutput = replaceNoCase(htmloutput, "{slug}", "#arguments['slug']#", 'all')>
	<cfset var htmloutput = replaceNoCase(htmloutput, "{otherparams}", "#arguments['otherparams']#", 'all')>
	<cfreturn htmloutput>
</cffunction>