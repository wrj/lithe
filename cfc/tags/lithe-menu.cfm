<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 8/29/13 AD
  Time: 11:19 AM
  To change this template use File | Settings | File Templates.
--->

<cfparam name="attributes.menu" default="">
<cfparam name="attributes.submenu" default="">
<cfparam name="attributes.useparams" type="boolean" default=true/>
<cfoutput>
	<cfif thisTag.executionMode EQ "start">
		<cfif attributes['useparams']>
			<cfif attributes['menu'] eq "" && isDefined("menu")>
				<cfset attributes.menu = menu>
			</cfif>
			<cfif attributes['submenu'] eq "" && isDefined("submenu")>
				<cfset attributes.submenu = submenu>
			</cfif>
		</cfif>
		<cfif attributes["menu"] neq "">
			<cfset filepath="#expandPath("/menu")#/#attributes.menu#.json">
			<cffile action = "read"
					file = "#filepath#"
					variable = "jsondata"
					charset="utf-8">
			<cfset rawdata = deserializeJSON(jsondata)>
			<cfset menudata = arrayNew()>
			<cfset parentdata = structNew()>
			<cfif attributes.submenu neq "">
				<cffunction name="recursive" output="false" access="public" returntype="any">
					<cfargument name="mymenudata" type="any" required="true">
					<cfargument name="level" type="numeric" default="1" required="false">
					<cfloop array="#arguments.mymenudata#" index="menuitem">
						<cfif menuitem.NAME IS attributes.submenu>
							<cfset parentdata = menuitem>
							<cfif structKeyExists(menuitem,"children") >
								<cfset menudata = menuitem["children"]>
							</cfif>
							<cfelse>
							<cfif structKeyExists(menuitem,"children") >
								<cfset recursive(menuitem["children"],(arguments['level']+1))>
							</cfif>
						</cfif>
					</cfloop>
				</cffunction>
				<cfset recursive(rawdata["MENU"]["menu"])>
				<cfelse>
				<cfset menudata=rawdata["MENU"]["menu"]>
			</cfif>
			<cfset totalmenu=1>
			<cffunction name="countmenu" output="false" access="public" returntype="any">
				<cfargument name="mymenudata" type="any" required="true">
				<cfargument name="level" type="numeric" default="1" required="false">
				<cfloop array="#arguments.mymenudata#" index="menuitem">
					<cfset totalmenu++>
					<cfif structKeyExists(menuitem,"children") >
						<cfset recursive(menuitem["children"],(arguments['level']+1))>
					</cfif>
				</cfloop>
			</cffunction>
			<cfset countmenu(rawdata["MENU"]["menu"])>
			<cfset structDelete(rawdata,"menu")>
			<cfset structDelete(parentdata,"children")>
			<cfset CALLER["menu"] = StructNew()>
			<cfset CALLER["menu"] = rawdata>
			<cfset CALLER["menu"]['parent'] = parentdata>
			<cfset CALLER["menu"]["totalrecord"] = arraylen(menudata)>
			<cfset CALLER["menu"]["totalmenu"] = totalmenu>
			<cfelse>
			<cfset CALLER["menu"] = StructNew()>
			<cfset CALLER["menu"]["totalrecord"] = 0>
			<cfset CALLER["menu"]["totalmenu"] = 0>
		</cfif>
		<cfelse>
		<cfset CALLER["menu"] = structNew()>
		<cfset CALLER["menu"]["totalrecord"] = 0>
		<cfset CALLER["menu"]["totalmenu"] = 0>
	</cfif>
</cfoutput>