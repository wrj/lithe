<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 8/28/13 AD
  Time: 10:53 AM
  To change this template use File | Settings | File Templates.
--->

<cfparam name="attributes.orderby" type="string" default="CREATEDAT" />
<cfparam name="attributes.sort" type="string" default="desc" />
<cfparam name="attributes.thispage" type="numeric" default=1 />
<cfparam name="attributes.limit" type="numeric" default=20 />
<cfparam name="attributes.skip" type="numeric" default=0/>
<!---<cfparam name="attributes.random" type="boolean" default=false/>--->
<cfoutput>
	<cfif thisTag.executionMode EQ "start">
		<cfif isDefined("page")>
			<cfset attributes["thispage"] = page>
		</cfif>
		<cfset methodname = "gallerieslist"/>
		<cfhttp url="http://#CGI['HTTP_HOST']#/cfc/services/gallery.cfc" method="get" result="objdata">
			<cfhttpparam type="url" name="method" value="#methodname#"/>
			<cfhttpparam type="url" name="orderby" value="#attributes.orderby#"/>
			<cfhttpparam type="url" name="sort" value="#attributes.sort#"/>
			<cfhttpparam type="url" name="thispage" value="#attributes.thispage#"/>
			<cfhttpparam type="url" name="limit" value="#attributes.limit#"/>
			<cfhttpparam type="url" name="language" value="#session["language"]#"/>
			<cfhttpparam type="url" name="skip" value="#attributes.skip#"/>
		</cfhttp>
		<cfset rawdata = deserializeJSON(objdata['filecontent'])>

		<cfset gallerydata = rawdata["contents"]>
		<cfset CALLER["galleries"] = StructNew()>
		<cfset CALLER["galleries"]["totalrecord"] = rawdata["totalrecord"]>
	</cfif>
</cfoutput>