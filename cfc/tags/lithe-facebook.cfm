<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 9/18/13 AD
  Time: 3:44 PM
  To change this template use File | Settings | File Templates.
--->
<cfsilent>
<cfset QUERY_STRING="">
<cfif CGI.QUERY_STRING NEQ "">
	<cfset QUERY_STRING="?#CGI.QUERY_STRING#">
</cfif>
<cfinclude template="readconfig.cfm">
<cfset servername=configJSON['general']['siteaddress']>
<cfset thistemplate="#servername##CGI.SCRIPT_NAME##QUERY_STRING#">
<cfparam name="attributes.redirect" default="#thistemplate#">
<cfparam name="attributes.facebookpage" default="facebook.cfm">
<cfparam name="attributes.loginname" default="Login Facebook">
<cfparam name="attributes.logoutname" default="Logout Facebook">
<cfparam name="attributes.loginclass" default="faceboolloginbtn">
<cfparam name="attributes.logoutclass" default="faceboollogoutbtn">
<cfparam name="attributes.loginimage" default="">
<cfparam name="attributes.logoutimage" default="">
<cfparam name="attributes.expires" default="10">
<cfset attributes.facebookpage="#servername#/#attributes.facebookpage#">
<cfif NOT isDefined("url.fblogin") AND NOT isDefined("url.fblogout") AND NOT isDefined("url.code")>
	<cfset session.facebookredirect=attributes.redirect>
</cfif>
	<cfif thisTag.executionMode EQ "start">
		<cfsetting enablecfoutputonly="true">
		<cfsilent><cfinclude template="readconfig.cfm"></cfsilent>
		<cfset APP_ID = configJSON['facebook']['appid']>
		<cfset SECRET_KEY = configJSON['facebook']['secretkey']>
		<cfset ADMIN_ID = configJSON['facebook']['adminid']>
		<cfset LANGUAGE = configJSON['facebook']['language']>
		<cfset FACEBOOK_URL = configJSON['facebook']['url']>
		<cfset CALLER["facebook"]["login"]="">
		<cfset CALLER["facebook"]["logout"]="">
		<cfset CALLER["facebook"]["loginstatus"]=false>
		<cffunction name="getFriends" access="public" returnType="array" output="false">
			<cfset var httpResult = "">
			<cfset var initialResult = "">
			<cfhttp url="https://graph.facebook.com/me/friends?fields=name,hometown&access_token=#session.fbaccesstoken#" result="httpResult">
			<cfset initialResult = deserializeJSON(httpResult.filecontent)>
			<!--- For now, skipping pagination. --->
			<cfreturn initialResult.data>
		</cffunction>
		<cffunction name="getMe" access="public" returnType="struct" output="false">
			<cfset var httpResult = "">
			<cfhttp url="https://graph.facebook.com/me?access_token=#session.fbaccesstoken#" result="httpResult">
			<cfreturn deserializeJSON(httpResult.filecontent)>
		</cffunction>
		<!---	SAVE URL	--->
		<!---	Login facebook	--->
		<cfif (NOT  structKeyExists(cookie,"facebookprofile") OR cookie.facebookprofile EQ "") AND isDefined("url.fblogin") AND url.fblogin IS 1>
			<cfset attributes.redirect=replaceNoCase(attributes.redirect,'fblogin=1',"","all")>
			<cfif right(attributes.redirect,1) IS "?" OR right(attributes.redirect,1) IS "&">
				<cfset attributes.redirect=Left(attributes.redirect,len(attributes.redirect)-1)>
			</cfif>
			<cfset session.facebookstate = createUUID()>
			<cfset redirect_uri=urlEncodedFormat(attributes.facebookpage,"utf8")>
			<cflocation url="https://www.facebook.com/dialog/oauth?client_id=#APP_ID#&redirect_uri=#redirect_uri#&state=#session.facebookstate#&scope=email" addtoken="false">
		</cfif>
		<!---	END Login facebook	--->
		<!---	CHECK RETURN FACEBOOK AFTER LOGIN	--->
		<cfif (NOT  structKeyExists(cookie,"facebookprofile") OR cookie.facebookprofile EQ "") AND structKeyExists(session,'facebookstate') AND isDefined("url.code") AND url.state IS session.facebookstate>
			<cfset session.fbcode = url.code>
			<cfset cleanurl = session.facebookredirect>
			<cfset redirect_uri=urlEncodedFormat(attributes.facebookpage,"utf8")>
			<cfhttp url="https://graph.facebook.com/oauth/access_token?client_id=#APP_ID#&redirect_uri=#redirect_uri#&client_secret=#SECRET_KEY#&code=#session.fbcode#">
			<cfif findNoCase("access_token=", cfhttp.filecontent)>
				<cfset parts = listToArray(cfhttp.filecontent, "&")>
				<cfset at = parts[1]>
				<cfset session.fbaccesstoken = listGetAt(at, 2, "=")>
				<!---		CLEAN URL		--->
				<cflocation url="#cleanurl#">
			<cfelseif findNoCase("error", cfhttp.filecontent)>
				<cfset message = deserializeJSON(cfhttp.filecontent).error.message>
				<cfif findNoCase("?", cleanurl) OR findNoCase("&", cleanurl)>
					<cfset cleanurl="#cleanurl#&error=#message#">
				<cfelse>
					<cfset cleanurl="#cleanurl#?error=#message#">
				</cfif>
				<cflocation url="#cleanurl#">
			</cfif>
			<cfelseif isDefined("url.error_reason")>
		</cfif>
		<!---	END CHECK RETURN FACEBOOK AFTER LOGIN	--->
		<!---	Logout facebook	--->
		<cfif isDefined("url.fblogout") AND url.fblogout IS 1>
			<cfset structDelete(session,"fbcode")>
			<cfset structDelete(session,"fbaccesstoken")>
			<cfset structDelete(session,"facebookstate")>
			<cfcookie name="facebookprofile" expires="now">
			<cfset cleanurl = session.facebookredirect>
			<cfset cleanurl=replaceNoCase(cleanurl,'fblogout=1',"","all")>
			<cfif right(cleanurl,1) IS "?" OR right(cleanurl,1) IS "&">
				<cfset cleanurl=Left(cleanurl,len(cleanurl)-1)>
			</cfif>
			<!---		CLEAN URL		--->
			<cflocation url="#cleanurl#">
		</cfif>
		<!---	END Logout facebook	--->
<!---	Check Cookie Facebook Profile	--->
		<cfif NOT  structKeyExists(cookie,"facebookprofile") OR cookie.facebookprofile EQ "">
			<cfif structKeyExists(session,'fbaccesstoken')>
<!---		Add Facebook Profile In Cookie		--->
				<cfset profile =  getMe()>
				<cfset saveprofile = {
					"id"=profile['id'],
					"name"=profile['name'],
					"email"=profile['email'],
					"gender"=profile['gender']
				}>
				<cfcookie name = "facebookprofile"
					value = "#serializeJSON(saveprofile)#"
					expires = attributes.expires>
			<cfelse>
				<cfset logintext="">
				<cfif attributes.loginimage IS "">
					<cfset logintext='#attributes.loginname#'>
					<cfelse>
					<cfset logintext='<img src="#attributes.loginimage#" alt="facebooklogin"/>'>
				</cfif>
				<cfset loginurl ="#attributes.facebookpage#?fblogin=1">
				<cfif findNoCase("?",attributes.facebookpage)>
					<cfset loginurl ="#attributes.facebookpage#&fblogin=1">
				</cfif>
				<cfset CALLER["facebook"]["login"] = '<a class="#attributes.loginclass#" href="#loginurl#">#logintext#</a>'>
			</cfif>
		</cfif>
		<cfif structKeyExists(cookie,"facebookprofile") AND cookie.facebookprofile NEQ "">
<!---		This is a FB user.--->
			<cfset CALLER["facebook"]["loginstatus"]=true>
			<cfset profile =  structNew()>
			<cfset profile =  deserializeJSON(cookie.facebookprofile)>
			<cfset logouttext="">
			<cfif attributes.logoutimage IS "">
				<cfset logouttext='#attributes.logoutname#'>
			<cfelse>
				<cfset logouttext='<img src="#attributes.logoutimage#" alt="facebooklogout"/>'>
			</cfif>
			<cfset logouturl ="#attributes.facebookpage#?fblogout=1">
			<cfif findNoCase("?",attributes.facebookpage)>
				<cfset logouturl ="#attributes.facebookpage#&fblogout=1">
			</cfif>
			<cfset CALLER["facebook"]["logout"] = '<a class="#attributes.logoutclass#" href="#logouturl#">#logouttext#</a>'>
			<cfset CALLER["facebook"]["fbid"] = profile['id']>
			<cfset CALLER["facebook"]["name"] = profile['name']>
			<cfset CALLER["facebook"]["email"] = profile['email']>
			<cfset CALLER["facebook"]["gender"] = profile['gender']>
			<cfset CALLER["facebook"]["image"] = "https://graph.facebook.com/#profile['id']#/picture">
		</cfif>
	</cfif>
</cfsilent>