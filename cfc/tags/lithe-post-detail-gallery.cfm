<cfif thisTag.executionMode EQ "start">
    <cfset data = getBaseTagData("cf_lithe-post-detail")>
    <cfset gallerydata = data['GALLERY']>
    <cfset currentRow = 1>
    <cfset CALLER["gallery"] = structNew()>
    <cfset caller['gallery']['totalrecord'] = arraylen(gallerydata)>
    <cfif arraylen(gallerydata) gt 0>
        <cfset galleryraw = gallerydata[currentRow]>
        <cfset CALLER["gallery"]["currentRow"] = currentRow>
        <cfset CALLER["gallery"]["title"] = galleryraw['title']>
        <cfset CALLER["gallery"]["detail"] = galleryraw['detail']>
        <cfset CALLER["gallery"]["link"] = galleryraw['link']>
        <cfset CALLER["gallery"]["image"] = "/assets/upload/image/"&galleryraw['image']>
    </cfif>
</cfif>
<cfif thisTag.executionMode EQ "end">
    <cfset currentRow++>
    <cfif currentRow LE arraylen(gallerydata)>
        <cfset galleryraw = gallerydata[currentRow]>
        <cfset CALLER["gallery"]["currentRow"] = currentRow>
        <cfset CALLER["gallery"]["title"] = galleryraw['title']>
        <cfset CALLER["gallery"]["detail"] = galleryraw['detail']>
        <cfset CALLER["gallery"]["link"] = galleryraw['link']>
        <cfset CALLER["gallery"]["image"] = "/assets/upload/image/"&galleryraw['image']>
        <cfexit method="loop">
    <cfelse>
        <cfexit method="exittag">
    </cfif>
</cfif>