<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/3/13 AD
  Time: 10:34 AM
  To change this template use File | Settings | File Templates.
--->

<cfset thistemplate="http://#CGI.HTTP_HOST##CGI.SCRIPT_NAME#">
<cfparam name="attributes.collectionname" default="formdb">
<cfparam name="attributes.formaction" default="formsave.cfm">
<cfparam name="attributes.formredirect" default="#thistemplate#">
<cfparam name="attributes.successmessage" default="success">
<cfparam name="attributes.errorrecaptchamessage" default="invalid recaptcha">
<cfparam name="attributes.errormessage" default="error">
<cfparam name="attributes.formclass" default="">
<cfparam name="attributes.method" default="post">
<cfparam name="attributes.isenctype" default=false>
<cfparam name="attributes.recaptcha" default="false">
<cfparam name="attributes.emailtocustomer" default="false">
<cfparam name="attributes.emailtoadmin" default="false">
<cfparam name="attributes.emailtcustomertemplate" default="">
<cfparam name="attributes.emailadmintemplate" default="">
<cfoutput>
	<cfif thisTag.executionMode EQ "start">
		<cfsilent><cfinclude template="readconfig.cfm"></cfsilent>
		<cfset isenctype=''>
		<cfif attributes.isenctype>
			<cfset isenctype='enctype="#attributes.enctype#"'>
		</cfif>
		<form action="#attributes.formaction#" class="#attributes.formclass#" method="#attributes.method#" #isenctype#>
			<input type="hidden" name="option_formredirect" value="#attributes.formredirect#">
			<input type="hidden" name="option_successmessage" value="#attributes.successmessage#">
			<input type="hidden" name="option_errormessage" value="#attributes.errormessage#">
			<input type="hidden" name="option_errorrecaptchamessage" value="#attributes.errorrecaptchamessage#">
			<input type="hidden" name="option_userecaptcha" value="#attributes.recaptcha#">
			<input type="hidden" name="option_emailtocustomer" value="#attributes.emailtocustomer#">
			<input type="hidden" name="option_emailcustomertemplate" value="#attributes.emailcustomertemplate#">
			<input type="hidden" name="option_emailtoadmin" value="#attributes.emailtoadmin#">
			<input type="hidden" name="option_emailadmintemplate" value="#attributes.emailadmintemplate#">
			<input type="hidden" name="option_collectionname" value="#attributes.collectionname#">
	</cfif>
	<cfif thisTag.executionMode EQ "end">
		</form>
	</cfif>
</cfoutput>