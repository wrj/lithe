<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 9/6/13 AD
  Time: 10:33 AM
  To change this template use File | Settings | File Templates.
--->


<cfparam name="attributes.orderby" type="string" default="CREATEDAT" />
<cfparam name="attributes.sort" type="string" default="desc" />
<cfparam name="attributes.thispage" type="numeric" default=1 />
<cfparam name="attributes.limit" type="numeric" default=20 />
<cfparam name="attributes.skip" type="numeric" default=0/>
<cfparam name="attributes.category" type="string" default=""/>
<cfparam name="attributes.useparams" type="boolean" default=true/>
<cfoutput>
	<cfif thisTag.executionMode EQ "start">
		<cfset methodname="">
		<cfif attributes['useparams']>
			<cfif attributes["thispage"] IS 1 AND isDefined("page")>
				<cfset attributes["thispage"] = page>
			</cfif>
			<cfif attributes["category"] IS "" AND isDefined("category")>
				<cfset attributes["category"] = category>
			</cfif>
		</cfif>
		<cfif arraylen(listtoarray(attributes["category"],",")) GT 1>
			<cfset methodname="submulticategories_custom">
		<cfelse>
			<cfset methodname="subcategories_custom">
		</cfif>
		<cfhttp url="http://#CGI['HTTP_HOST']#/cfc/services/Category.cfc" method="get" result="objdata">
			<cfhttpparam type="url" name="method" value="#methodname#"/>
			<cfhttpparam type="url" name="category" value="#attributes.category#"/>
			<cfhttpparam type="url" name="orderby" value="#attributes.orderby#"/>
			<cfhttpparam type="url" name="sort" value="#attributes.sort#"/>
			<cfhttpparam type="url" name="thispage" value="#attributes.thispage#"/>
			<cfhttpparam type="url" name="limit" value="#attributes.limit#"/>
			<cfhttpparam type="url" name="skip" value="#attributes.skip#"/>
		</cfhttp>
<!---					<cfdump var="#rawdata#">--->
<!---					<cfoutput>--->
<!---						#objdata['filecontent']#--->
<!---					</cfoutput>--->
<!---					<cfabort/>--->
		<cfset rawdata = deserializeJSON(objdata['filecontent'])>
		<cfset subcategorydata = rawdata["contents"]>
		<cfset CALLER["subcategories"] = StructNew()>
		<cfset CALLER["subcategories"]["totalrecord"] = rawdata["totalrecord"]>
	</cfif>
</cfoutput>