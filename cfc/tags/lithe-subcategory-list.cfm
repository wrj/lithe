<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 9/6/13 AD
  Time: 10:34 AM
  To change this template use File | Settings | File Templates.
--->

<cfif thisTag.executionMode EQ "start">
	<cfinclude template="/cfc/globalfunction.cfm" runonce="true"/>
	<cfset data = getBaseTagData("cf_lithe-subcategory-lists")>
	<cfset subcategorydata = ArrayNew()>
	<cfif structKeyExists(data, "subcategorydata")>
		<cfset subcategorydata = data["subcategorydata"]>
	</cfif>
	<cfset currentRow = 1>
	<cfset CALLER["subcategory"] = structNew()>
	<cfif arraylen(subcategorydata) gt 0>
		<cfset currentsubcategory = subcategorydata[currentRow]>
		<cfset StrEscUtils = createObject("java", "org.apache.commons.lang.StringEscapeUtils") />
		<cfset detailtext = StrEscUtils.unescapeHTML(currentsubcategory['DETAIL'])>
		<cfset detailtext = replaceNoCase(detailtext, "##", "####", 'all')>
		<cfset detailtext = replaceNoCase(detailtext, "####buildlink", "##buildlink", 'all')>
		<cfset detailtext = replaceNoCase(detailtext, ")####", ")##", 'all')>
		<cfset currentsubcategory["DETAIL"] = evaluate(DE(detailtext))>
		<cfset CALLER["subcategory"] = currentsubcategory>
		<cfset CALLER["subcategory"]["currentRow"] = currentRow>
	</cfif>
</cfif>
<cfif thisTag.executionMode EQ "end">
	<cfset currentRow++>
	<cfif currentRow LE arraylen(subcategorydata)>
		<cfset currentsubcategory = subcategorydata[currentRow]>
		<cfset StrEscUtils = createObject("java", "org.apache.commons.lang.StringEscapeUtils") />
		<cfset detailtext = StrEscUtils.unescapeHTML(currentsubcategory['DETAIL'])>
		<cfset detailtext = replaceNoCase(detailtext, "##", "####", 'all')>
		<cfset detailtext = replaceNoCase(detailtext, "####buildlink", "##buildlink", 'all')>
		<cfset detailtext = replaceNoCase(detailtext, ")####", ")##", 'all')>
		<cfset currentsubcategory["DETAIL"] = evaluate(DE(detailtext))>
		<cfset CALLER["subcategory"] = currentsubcategory>
		<cfset CALLER["subcategory"]["currentRow"] = currentRow>
		<cfexit method="loop">
	<cfelse>
		<cfexit method="exittag">
	</cfif>
</cfif>