<cffunction name="readconfig" access="remote" returntype="struct" returnformat="JSON">
	<cfset configfile = "#expandPath('/')#cfc/config.json">
	<cfset dataFromJSON = deserializeJSON(fileRead( "#configfile#" )) />
	<cfset mydata = dataFromJSON>
	<cfreturn mydata>
</cffunction>

<cffunction name="buildlink" access="private" returntype="String">
	<cfargument name="slug" type="string" required="true">
	<cfargument name="template" type="string" required="true">
	<cfargument name="otherparams" type="string" required="false" default="">
	<cfinclude template="/cfc/config.cfm"/>
	<cfset configdata = readconfig()>
	<cfset var htmloutput = replaceNoCase(patternurl, "{template}", "#arguments['template']#", 'all')>
	<cfset htmloutput = replaceNoCase(htmloutput, "{slug}", "#arguments['slug']#", 'all')>
	<cfset htmloutput = replaceNoCase(htmloutput, "{otherparams}", "#arguments['otherparams']#", 'all')>
	<cfloop list="#rewritepatternurl#" delimiters="," index="reurl">
		<cfset htmloutput = replaceNoCase(htmloutput, reurl, "", 'all')>
	</cfloop>
	<cfset outputdata = "#configdata["general"]["siteaddress"]#/#htmloutput#">
	<cfreturn outputdata>
</cffunction>

<cffunction name="buildotherlink" access="private" returntype="String">
	<cfargument name="template" type="string" required="true">
	<cfargument name="key" type="string" required="false" default="">
	<cfargument name="value" type="string" required="false" default="">
	<cfargument name="otherparams" type="string" required="false" default="">
	<cfinclude template="/cfc/config.cfm"/>
	<cfset configdata = readconfig()>
	<cfparam name="otherpatternurl" default="">
	<cfset var htmloutput = replaceNoCase(otherpatternurl, "{template}", "#arguments['template']#", 'all')>
	<cfset htmloutput = replaceNoCase(htmloutput, "{key}", "#arguments['key']#", 'all')>
	<cfset htmloutput = replaceNoCase(htmloutput, "{value}", "#arguments['value']#", 'all')>
	<cfif arguments['key'] IS "" AND arguments['VALUE'] IS "">
		<cfset htmloutput = replaceNoCase(htmloutput, "?", "", 'one')>
		<cfset htmloutput = replaceNoCase(htmloutput, "=", "", 'one')>
	</cfif>
	<cfset htmloutput = replaceNoCase(htmloutput, "{otherparams}", "#arguments['otherparams']#", 'all')>
	<cfloop list="#rewritepatternurl#" delimiters="," index="reurl">
		<cfset htmloutput = replaceNoCase(htmloutput, reurl, "", 'all')>
	</cfloop>
	<cfset outputdata = "#configdata["general"]["siteaddress"]#/#htmloutput#">
	<cfset outputdata=removeslash(outputdata)>
	<cfreturn outputdata>
</cffunction>

<cffunction name="removeslash" access="public" returntype="String">
	<cfargument name="url" type="string" required="true">
	<cfset var outputdata = arguments.url>
	<cfif right(outputdata,1) EQ "/">
		<cfset outputdata=removeChars(outputdata,len(outputdata),1)>
	</cfif>
	<cfif right(outputdata,1) EQ "/">
		<cfset outputdata=removeslash(outputdata)>
	</cfif>
	<cfreturn outputdata>
</cffunction>

<cffunction name="flashinsert" returntype="void" access="public">
	<cfargument name="typeflash" type="string" required="true">
	<cfargument name="message" type="string" required="true">
	<cfset session['flash'][arguments['typeflash']] = arguments['message']>
</cffunction>

<cffunction name="flashKeyExists" returntype="boolean" access="public">
	<cfargument name="typeflash" type="string" required="true">
	<cfset var result = false>
	<cfif structKeyExists(session,'flash')>
		<cfset result = structKeyExists(session['flash'],arguments['typeflash'])>
	</cfif>
	<cfreturn result>
</cffunction>

<cffunction name="flash" returntype="string" access="public">
	<cfargument name="typeflash" type="string" required="true">
	<cfset var result = session['flash'][arguments['typeflash']]>
	<cfset structDelete(session,'flash')>
	<cfreturn result>
</cffunction>

<cffunction name="storedata" returntype="void" access="public">
	<cfargument name="objdata" type="struct" required="true">
	<cfset session['objdata'] = arguments['objdata']>
</cffunction>

<cffunction name="getstoredata" returntype="struct" access="public">
	<cfset var result = structnew()>
	<cfif structKeyExists(session,'objdata')>
		<cfset result = session['objdata']['value']>
	</cfif>
	<cfset structDelete(session,'objdata')>
	<cfreturn result>
</cffunction>

<cffunction name="getstoredataerror" returntype="string" access="public" output="false">
	<cfif structKeyExists(session,'objdata')>
		<cfsavecontent variable="result">
			<cfoutput>
				<ul>
				<cfloop array="#session['objdata']['allerror']#" index="ierror">
						<li>#ierror#</li>
				</cfloop>
				</ul>
			</cfoutput>
		</cfsavecontent>
		<cfelse>
		<cfset result = ''>
	</cfif>
	<cfreturn result>
</cffunction>