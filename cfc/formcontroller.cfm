<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/15/13 AD
  Time: 5:37 PM
  To change this template use File | Settings | File Templates.
--->

<cfinclude template="dbregist.cfm">

<cffunction name="newid" access="public" returntype="Any">
	<cfargument name="objid" type="string" required="true">
	<cfset var objreturn = MongoObjectid(arguments.objid)>
	<cfreturn objreturn>
</cffunction>

<cffunction name="dbref" access="public" returntype="struct">
	<cfargument name="collection" type="string" required="true">
	<cfargument name="objectid" type="string" required="true">
	<cfset var dbreturn = structNew()>
	<cfset dbreturn['$ref'] = lcase(arguments['collection'])>
	<cfset dbreturn['$id'] =  MongoObjectid(arguments['objectid'])>
	<cfreturn dbreturn>
</cffunction>

<cffunction name="formedit" access="public" returntype="Any">
	<cfargument name="collectionname" type="string" required="true">
	<cfargument name="key" type="string" required="true">
	<cfset realcollectionname = "form_#arguments.collectionname#">
	<cfset onedata = MongoCollectionFindone(databasename,realcollectionname,{'_id'=newid(arguments.key)})>
	<cfset formdata=structNew()>
	<cfif isdefined("onedata")>
		<cfset formdata=onedata>
		<cfset formdata["id"]=formdata["_id"].toString()>
		<cfset formdata["status"]=1>
		<cfset structDelete(formdata,"_id")>
	<cfelse>
		<cfset formdata["status"]=0>
	</cfif>
	<cfreturn formdata>
</cffunction>

<cffunction name="formupdate" access="public" returntype="Any">
	<cfargument name="collectionname" type="string" required="true">
	<cfargument name="key" type="string" required="true">
	<cfargument name="updateform" type="any" required="true">
	<cfset realcollectionname = "form_#arguments.collectionname#">
	<cfset updateform['UPDATEDAT'] = now()>
	<cfset olddata = MongoCollectionFindone(databasename,realcollectionname,{'_id'=newid(arguments.key)})>
	<cfif isdefined("olddata")>
		<cfset updateform['CREATEDAT'] = olddata["CREATEDAT"]>
		<cfset updateform['_id'] = olddata["_id"]>
		<cfset isupdate = MongoCollectionsave(databasename,realcollectionname,arguments.updateform)>
		<cfreturn 1>
		<cfelse>
		<cfreturn 0>
	</cfif>
</cffunction>

<cffunction name="formdelete" access="public" returntype="Any">
	<cfargument name="collectionname" type="string" required="true">
	<cfargument name="key" type="string" required="true">
	<cfset realcollectionname = "form_#arguments.collectionname#">
	<cfset isdel = MongoCollectionremove(databasename,realcollectionname,{'_id'=newid(arguments.key)})>
	<cfreturn 1>
</cffunction>


