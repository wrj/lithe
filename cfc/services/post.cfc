<cfcomponent extends="Utility">

	<cffunction name="getuserid" access="public" returntype="string">
		<cfargument name="secretkey" type="string" required="true">
		<cfinclude template="config.cfm"/>
		<cfset uid = MongoCollectionfindOne(databasename,"user",{"SECRET"=arguments.secretkey})>
		<cfif structKeyExists(uid,"_id")>
			<cfset uid._id = uid._id.toString()>
			<cfreturn uid._id>
		</cfif>
	</cffunction>

	<cffunction name="postslist" access="remote" returntype="array" returnformat="json">
		<cfargument name="orderby" type="string" required="false" default="CREATEDAT" />
		<cfargument name="sort" type="string" required="false" default="desc" />
		<cfargument name="currentpage" type="numeric" required="false" default=1 />
		<cfargument name="limit" type="string" required="false" default="0" />
		<cfargument name="tags" type="string" required="false" />
		<cfargument name="category" type="string" required="false" />
		<cfargument name="language" type="string" required="true" default="thai" />
		<cfargument name="skip" type="numeric" required="false"/>
		<cfargument name="subcategory" type="boolean" default=false required="false"/>
		<cfinclude template="config.cfm"/>


		<cfset var skipnumber = 0>
		<cfset var skippage = 0>
		<cfif structKeyExists(arguments,"skip")>
			<cfset skipnumber = arguments['skip']>
		</cfif>
		<cfif structKeyExists(arguments, "currentpage") and arguments.currentpage neq 1>
			<cfset skippage = (arguments.currentpage*arguments.limit)-arguments.limit>
		</cfif>
		<cfset var skippost = skipnumber + skippage>

		<cfset sortby = 1>
		<cfif arguments.sort eq "desc">
			<cfset sortby = -1>
		</cfif>
		<cfset sortfield = StructNew()>
		<cfset sortfield[arguments.orderby] = sortby>

		<cfset myquerytagstext = structNew()>
		<cfset myquerytagstext['TYPECONTENT'] = 1>
		<cfset myquerytagstext['PUBSTATUS'] = 1>

		<cfset mystrlang = structNew()>
		<cfset mystrlang["$ne"] = ''>
		<cfset myquerytagstext["TITLE.#arguments.language#"] = mystrlang>

		<cfif structKeyExists(arguments, "tags") && arguments["tags"] neq "">
			<cfset mytags = listtoarray(arguments.tags,",")>
			<cfset myquerytags = []>
			<cfloop array="#mytags#" index="t">
				<cfset mystruct = structNew()>
				<cfset mystruct["TAG"] = t>
				<cfset arrayAppend(myquerytags,mystruct)>
			</cfloop>
			<cfset myquerytagstext['$or'] = myquerytags>
		</cfif>
		<cfif structKeyExists(arguments,"category")>
			<cfset mycategory = MongoCollectionfindOne(
				datasource=databasename,
				collection="category",
				query=({"SLUG"=arguments.category})
				)>
			<cfif structKeyExists(mycategory,"_id")>
				<cfset cetegoryArr = arrayNew()>
				<cfset arrayAppend(cetegoryArr,mycategory._id)>
				<cfset categoryIn['$in'] = cetegoryArr>

<!---				MULTI CETEGORY --->

				<cfif arguments['subcategory'] IS true>
					<cfset cateraw = MongoCollectionfind(databasename,'category',{})>
					<cfset newcate = treequeryjson(cateraw)>
					<cfset ceterawArr=arrayNew()>
					<cfloop array="#newcate#" index="item">
						<cfset resultjson = structNew()>
						<cfif  REFind(mycategory['TITLE'],item['TITLE']) GT 0 >
							<cfset resultjson = deserializeJSON(item['TITLE'])>
							<cfset arrayAppend(ceterawArr,resultjson)>
						</cfif>
					</cfloop>
					<cfset maxcete = 0>
					<cfset chooseCetegoryRaw = structNew()>
					<cfloop array="#ceterawArr#" index="i">
						<cfset countChild=$countChild(i)>
						<cfif countChild GT maxcete>
							<cfset maxcete = countChild>
							<cfset chooseCetegoryRaw=i>
						</cfif>
					</cfloop>
					<cfset categoryList = $selectChildCetegory(chooseCetegoryRaw,arguments['category'])>
					<cfset categoryIn['$in'] = categoryList>
				</cfif>
				<cfset myquerytagstext['CATEGORY.$id']=categoryIn>
			</cfif>
		</cfif>
        <cfset contents = MongoCollectionfind(
            datasource=databasename,
            collection="post",
            query=(myquerytagstext),
            skip=skippost,
            size=val(arguments.limit),
            sort=(sortfield))>
		<cfset var datacontent = ArrayNew()>
		<cfloop array="#contents#" index="content">
			<cfset content['_id'] = content['_id'].toString()>
			<cfset content['TITLE'] = i18n(content['TITLE'],arguments.language)>
			<cfset content['DETAIL'] = i18n(content['DETAIL'],arguments.language) />
			<cfset content['AUTHOR'] = content['USER'].fetch()['USERNAME']>
			<cfif isNull(content['CATEGORY'].fetch())>
				<cfset content.CATEGORYTITLE = "">
			<cfelse>
				<cfset content.CATEGORYTITLE = content.CATEGORY.fetch()['TITLE']>
			</cfif>
			<cfif structKeyExists(content,'INTRO')>
				<cfif IsStruct(content['INTRO'])>
					<cfset content['INTRO'] = i18n(content['INTRO'],arguments.language)>
				</cfif>
			<cfelse>
				<cfset content['INTRO'] = ''>
			</cfif>
            <cfset structdelete(content,'CATEGORY')>
            <cfset structdelete(content,'USER')>
			<cfset arrayAppend(datacontent,content)>
		</cfloop>
		<cfreturn datacontent>
	</cffunction>

	<cffunction name="postslistmulticategory" access="remote" returntype="array" returnformat="json">
		<cfargument name="orderby" type="string" required="false" default="CREATEDAT" />
		<cfargument name="sort" type="string" required="false" default="desc" />
		<cfargument name="currentpage" type="numeric" required="false" default=1 />
		<cfargument name="limit" type="string" required="false" />
		<cfargument name="tags" type="string" required="false" />
		<cfargument name="category" type="string" required="false" />
		<cfargument name="language" type="string" required="true" default="thai" />

		<cfinclude template="config.cfm"/>

		<cfset sortby = 1>
		<cfif arguments.sort eq "desc">
			<cfset sortby = -1>
		</cfif>
		<cfset sortfield = StructNew()>
		<cfset sortfield[arguments.orderby] = sortby>

		<cfset myquerytagstext = structNew()>
		<cfset myquerytagstext['TYPECONTENT'] = 1>
		<cfset myquerytagstext['PUBSTATUS'] = 1>

		<cfset mystrlang = structNew()>
		<cfset mystrlang["$ne"] = ''>
		<cfset myquerytagstext["TITLE.#arguments.language#"] = mystrlang>

		<cfif structKeyExists(arguments, "tags")>
			<cfset mytags = listtoarray(arguments.tags,",")>
			<cfset myquerytags = []>
			<cfloop array="#mytags#" index="t">
				<cfset mystruct = structNew()>
				<cfset mystruct["TAG"] = t>
				<cfset arrayAppend(myquerytags,mystruct)>
			</cfloop>
			<cfset myquerytagstext['$or'] = myquerytags>
		</cfif>

        <cfset var skippost = 0>
        <cfif structKeyExists(arguments, "currentpage") and arguments.currentpage neq 1>
            <cfset skippost = (arguments.currentpage*arguments.limit)-arguments.limit>
        </cfif>

		<cfif structKeyExists(arguments,"category")>
			<cfset mycates = listtoarray(arguments.category,",")>
			<cfset myquerycates = []>
			<cfloop array="#mycates#" index="c">
				<cfset mystructcates = structNew()>
				<cfset mystructcates["SLUG"] = c>
				<cfset arrayAppend(myquerycates,mystructcates)>
			</cfloop>
			<cfset myquerycatestext['$or'] = myquerycates>

			<cfset mycategory = MongoCollectionfind(
				datasource=databasename,
				collection="category",
				query=(myquerycatestext),
				field={"_id"=true}
					)>

			<cfif isarray(mycategory) and arraylen(mycategory) gte 1>
				<cfif not isDefined("myquerytags")>
					<cfset myquerytags = []>
				</cfif>
				<cfloop array="#mycategory#" index="mca">
					<cfset mystrcat = structNew()>
					<cfset mca._id = mca._id.toString()>
					<cfset mycateid = MongoObjectid(mca._id)>
					<cfset mystrcat["CATEGORY.$id"] = mycateid>
					<cfset arrayAppend(myquerytags,mystrcat)>
				</cfloop>
				<cfset myquerytagstext['$or'] = myquerytags>
			</cfif>
		</cfif>
        <cfset contents = MongoCollectionfind(
                datasource=databasename,
                collection="post",
                query=(myquerytagstext),
                skip=skippost,
                size=arguments.limit,
                sort=(sortfield))>
		<cfloop array="#contents#" index="i">
			<cfset i._id = i._id.toString()>
			<cfset i['TITLE'] = i18n(i.TITLE,arguments.language)>
			<cfset i['AUTHOR'] = i['USER'].fetch()['USERNAME']>
			<cfif structKeyExists(i,"INTRO")>
				<cfset i.INTRO = i18n(i.INTRO,arguments.language)>
			</cfif>
			<cfset i.DETAIL = i18n(i.DETAIL,arguments.language) />
			<cfif isNull(i['CATEGORY'].fetch())>
				<cfset i.CATEGORYTITLE = "">
			<cfelse>
				<cfset i.CATEGORYTITLE = i.CATEGORY.fetch()['TITLE']>
			</cfif>
		</cfloop>
		<cfreturn contents>
	</cffunction>

	<cffunction name="poststitlelistmulticategory" access="remote" returntype="array" returnformat="json">
		<cfargument name="orderby" type="string" required="false" default="CREATEDAT" />
		<cfargument name="sort" type="string" required="false" default="desc" />
		<cfargument name="currentpage" type="numeric" required="false" default=1 />
		<cfargument name="limit" type="string" required="false" />
		<cfargument name="tags" type="string" required="false" />
		<cfargument name="category" type="string" required="false" />
		<cfargument name="language" type="string" required="true" default="thai" />

		<cfinclude template="config.cfm"/>

		<cfset sortby = 1>
		<cfif arguments.sort eq "desc">
			<cfset sortby = -1>
		</cfif>
		<cfset sortfield = StructNew()>
		<cfset sortfield[arguments.orderby] = sortby>

		<cfset myquerytagstext = structNew()>
		<cfset myquerytagstext['TYPECONTENT'] = 1>
		<cfset myquerytagstext['PUBSTATUS'] = 1>

		<cfset mystrlang = structNew()>
		<cfset mystrlang["$ne"] = ''>
		<cfset myquerytagstext["TITLE.#arguments.language#"] = mystrlang>

		<cfif structKeyExists(arguments, "tags")>
			<cfset mytags = listtoarray(arguments.tags,",")>
			<cfset myquerytags = []>
			<cfloop array="#mytags#" index="t">
				<cfset mystruct = structNew()>
				<cfset mystruct["TAG"] = t>
				<cfset arrayAppend(myquerytags,mystruct)>
			</cfloop>
			<cfset myquerytagstext['$or'] = myquerytags>
		</cfif>

		<cfif structKeyExists(arguments,"category")>
			<cfset mycates = listtoarray(arguments.category,",")>
			<cfset myquerycates = []>
			<cfloop array="#mycates#" index="c">
				<cfset mystructcates = structNew()>
				<cfset mystructcates["SLUG"] = c>
				<cfset arrayAppend(myquerycates,mystructcates)>
			</cfloop>
			<cfset myquerycatestext['$or'] = myquerycates>

			<cfset mycategory = MongoCollectionfind(
				datasource=databasename,
				collection="category",
				query=(myquerycatestext),
				field={"_id"=true}
					)>

			<cfif isarray(mycategory) and arraylen(mycategory) gte 1>
				<cfif not isDefined("myquerytags")>
					<cfset myquerytags = []>
				</cfif>
				<cfloop array="#mycategory#" index="mca">
					<cfset mystrcat = structNew()>
					<cfset mca._id = mca._id.toString()>
					<cfset mycateid = MongoObjectid(mca._id)>
					<cfset mystrcat["CATEGORY.$id"] = mycateid>
					<cfset arrayAppend(myquerytags,mystrcat)>
				</cfloop>
				<cfset myquerytagstext['$or'] = myquerytags>
			</cfif>
		</cfif>

        <cfset var skippost = 0/>
        <cfif structKeyExists(arguments, "currentpage") and arguments.currentpage neq 1>
            <cfset skippost = (arguments.currentpage*arguments.limit)-arguments.limit/>
        </cfif>
        <cfset contents = MongoCollectionfind(
            datasource=databasename,
            collection="post",
            query=(myquerytagstext),
            skip=skippost,
            size=val(arguments.limit),
            sort=(sortfield),
            fields={"INTRO"=true,"SLUG"=true,"_id"=true,"TITLE"=true,"CREATEDAT"=true,"UPDATEDAT"=true,"PUBSTATUS"=true,"PUBDATE"=true,"USER"=true,"TAG"=true,"TYPECONTENT"=true,"CATEGORY"=true})>
		<cfloop array="#contents#" index="i">
			<cfset i['_id'] = i['_id'].toString()>
			<cfset i['TITLE'] = i18n(i['TITLE'],arguments.language)>
			<cfset i['AUTHOR'] = i['USER'].fetch()['USERNAME']>
			<cfif structKeyExists(i,"INTRO")>
				<cfset i['INTRO'] = i18n(i['INTRO'],arguments.language)>
			</cfif>
			<cfif isNull(i['CATEGORY'].fetch())>
				<cfset i['CATEGORYTITLE'] = "">
			<cfelse>
				<cfset i['CATEGORYTITLE'] = i['CATEGORY'].fetch()['TITLE']>
			</cfif>
		</cfloop>
		<cfreturn contents>
	</cffunction>

	<cffunction name="poststitlelist" access="remote" returntype="array" returnformat="json">
		<cfargument name="orderby" type="string" required="false" default="CREATEDAT" />
		<cfargument name="sort" type="string" required="false" default="desc" />
		<cfargument name="currentpage" type="numeric" required="false" default=1 />
		<cfargument name="limit" type="string" required="false" />
		<cfargument name="tags" type="string" required="false" />
		<cfargument name="category" type="string" required="false" />
		<cfargument name="language" type="string" required="true" default="thai" />

		<cfinclude template="config.cfm"/>

		<cfset sortby = 1>
		<cfif arguments.sort eq "desc">
			<cfset sortby = -1>
		</cfif>
		<cfset sortfield = StructNew()>
		<cfset sortfield[arguments.orderby] = sortby>

		<cfset myquerytagstext = structNew()>
		<cfset myquerytagstext['TYPECONTENT'] = 1>
		<cfset myquerytagstext['PUBSTATUS'] = 1>

		<cfset mystrlang = structNew()>
		<cfset mystrlang["$ne"] = ''>
		<cfset myquerytagstext["TITLE.#arguments.language#"] = mystrlang>

		<cfif structKeyExists(arguments, "tags")>
			<cfset mytags = listtoarray(arguments.tags,",")>
			<cfset myquerytags = []>
			<cfloop array="#mytags#" index="t">
				<cfset mystruct = structNew()>
				<cfset mystruct["TAG"] = t>
				<cfset arrayAppend(myquerytags,mystruct)>
			</cfloop>
			<cfset myquerytagstext['$or'] = myquerytags>
		</cfif>
        <cfset var skippost = 0/>
        <cfif structKeyExists(arguments, "currentpage") and arguments.currentpage neq 1>
            <cfset skippost = (arguments.currentpage*arguments.limit)-arguments.limit/>
        </cfif>
		<cfif structKeyExists(arguments,"category")>
			<cfset mycategory = MongoCollectionfindOne(
				datasource=databasename,
				collection="category",
				query=({"SLUG"=arguments.category})
				)>
			<cfif structKeyExists(mycategory,"_id")>
				<cfset mycategory._id = mycategory._id.toString()>
				<cfset mycateid = MongoObjectid(mycategory._id)>
				<cfset myquerytagstext['CATEGORY.$id'] = mycateid>
			</cfif>
		</cfif>
        <cfset contents = MongoCollectionfind(
            datasource=databasename,
            collection="post",
            query=(myquerytagstext),
            skip=skippost,
            size=val(arguments.limit),
            sort=(sortfield),
            fields={"INTRO"=true,"SLUG"=true,"_id"=true,"TITLE"=true,"CREATEDAT"=true,"UPDATEDAT"=true,"PUBSTATUS"=true,"PUBDATE"=true,"USER"=true,"TAG"=true,"TYPECONTENT"=true,"CATEGORY"=true})>
		<cfloop from="1" to="#arrayLen(contents)#" index="i">
			<cfif arrayIndexExists(contents, i)>
				<cfset contents[i]._id = contents[i]._id.toString()>
				<cfset contents[i].TITLE = removehtmltags(i18n(contents[i].TITLE,arguments.language))>
				<cfset contents[i]['AUTHOR'] = contents[i]['USER'].fetch()['USERNAME']>
				<cfif structkeyexists(contents[i],'INTRO')>
					<cfset contents[i].INTRO = i18n(contents[i].INTRO,arguments.language)>
				</cfif>
				<cfset contents[i].CREATEDATMONTHDATE = DATEFORMAT(contents[i].CREATEDAT,"mmm dd")>
				<cfset contents[i].CREATEDATYEAR = YEAR(contents[i].CREATEDAT)>
				<cfif isNull(contents[i]['CATEGORY'].fetch())>
					<cfset contents[i].CATEGORYTITLE = "">
				<cfelse>
					<cfset contents[i].CATEGORYTITLE = contents[i].CATEGORY.fetch()['TITLE']>
				</cfif>
			</cfif>
		</cfloop>
		<cfreturn contents>
	</cffunction>

	<cffunction name="postitem" access="remote" returntype="struct" returnformat="json">
		<cfargument name="key" type="string" required="false" default="" />
		<cfargument name="slug" type="string" required="true" default="" />
		<cfargument name="language" type="string" required="true" default="thai" />

		<cfinclude template="config.cfm"/>
		<cfif environment eq "product">
	        <cfset var querystring = structNew()/>
	        <cfset querystring['TYPECONTENT'] = 1>
	        <cfset querystring['PUBSTATUS'] = 1>
			<cfif (structKeyExists(arguments,"key")) and (arguments.key neq "") and (arguments.key neq "undefined")>
				<cfset querystring['_id'] = MongoObjectId(arguments.key)>
			<cfelse>
	            <cfset querystring['SLUG'] = arguments.slug>
			</cfif>
	        <cfset item = MongoCollectionfindone(
	            datasource=databasename,
	            collection="post",
	            query=querystring)>
		        <cfif IsNull(item) eq "NO">
					<cfset item._id = item._id.toString()>
					<cfset item.TITLE = removehtmltags(i18n(item.TITLE,arguments.language)) />
					<cfset item['AUTHOR'] = item['USER'].fetch()['USERNAME']>
					<cfif structKeyExists(item,"INTRO")>
						<cfset item.INTRO = removehtmltags(i18n(item.INTRO,arguments.language)) />
					</cfif>
					<cfset item.CREATEDATMONTHDATE = DATEFORMAT(item.CREATEDAT,"mmm dd")>
					<cfset item.CREATEDATYEAR = YEAR(item.CREATEDAT)>
					<cfset item.DETAIL = i18n(item.DETAIL,arguments.language) />
			        <cfset categoryfetch = item['CATEGORY'].fetch()>
					<cfif isNull(categoryfetch)>
						<cfset item['CATEGORYTITLE'] = "">
						<cfset item['CATEGORYSLUG'] = "">
						<cfset item['CATEGORYTEMPLATE'] = "">
					<cfelse>
						<cfset item['CATEGORYTITLE'] = categoryfetch['TITLE']>
						<cfset item['CATEGORYSLUG'] = categoryfetch['SLUG']>
						<cfset item['CATEGORYTEMPLATE'] = categoryfetch['TEMPLATE']>
			            <cfset item['CATEGORYID'] = categoryfetch['_id'].toString()>
					</cfif>
					<cfset item['RELATEITEM'] = postsrelate(item['SLUG'],item['CATEGORYID'],arguments['language'])>
			        <cfset structdelete(item,'CATEGORY')>
			        <cfset structdelete(item,'USER')>
			    <cfelse>
			    	<cfset item = structnew()>
			    	<cfset item["_id"] = "">
		    	</cfif>
	    	<cfelse>
	    		<cfset var mockdata = getmockdata(1,8)>
	    		<cfset item = mockdata['detail']>
    		</cfif>
		<cfreturn item>
	</cffunction>
	

	<cffunction name="postsrelate" access="remote" returntype="array">
        <cfargument name="slug" type="string" required="true" />
        <cfargument name="category" type="string" required="true" />
        <cfargument name="language" type="string" required="false" default="thai" />
        <cfargument name="subcategory" type="boolean" default=false required="false"/>

        <cfinclude template="config.cfm"/>

        <cfset sortfield = StructNew()>
        <cfset sortfield['CREATEDAT'] = 1>
        <cfset querystring = structNew()>
        <cfset querystring['TYPECONTENT'] = 1>
        <cfset querystring['PUBSTATUS'] = 1>
        <cfset ignorestruct = structnew()>
        <cfset ignorestruct["$ne"] = arguments['slug']>
        <cfset querystring['SLUG'] = ignorestruct>
        <cfset querystring['CATEGORY'] = dbref('category',arguments['category'])>
        <cfset contents = MongoCollectionfind(
            datasource=databasename,
            collection="post",
            query=querystring,
            size=10,
            sort=sortfield)>
        <cfset var ranlist = ""/>
        <cfloop from="1" to="20" index="i">
            <cfset rannum = randRange(1,arraylen(contents))/>
            <cfif listfind(ranlist,rannum) eq 0>
                <cfset ranlist = listappend(ranlist,rannum)/>
                <cfif listlen(ranlist) eq 5>
                    <cfbreak/>
                </cfif>
            </cfif>
        </cfloop>
        <cfset var datacontent = ArrayNew()>
        <cfset var i = 1/>
        <cfloop array="#contents#" index="content">
            <cfif listfind(ranlist,i) gt 0>
                <cfset content['_id'] = content['_id'].toString()>
                <cfset content['TITLE'] = i18n(content['TITLE'],arguments.language)>
                <cfset content['DETAIL'] = i18n(content['DETAIL'],arguments.language) />
                <cfset content['AUTHOR'] = content['USER'].fetch()['USERNAME']>
	            <cfset categoryfetch = content['CATEGORY'].fetch()>
                <cfif isNull(categoryfetch)>
                    <cfset content['CATEGORYTITLE'] = "">
	                <cfset content['CATEGORYSLUG'] = "">
	                <cfset content['CATEGORYTEMPLATE'] = "">
                <cfelse>
                    <cfset content['CATEGORYTITLE'] = categoryfetch['TITLE']>
	                <cfset content['CATEGORYSLUG'] = categoryfetch['SLUG']>
	                <cfset content['CATEGORYTEMPLATE'] = categoryfetch['TEMPLATE']>
                </cfif>
                <cfif structKeyExists(content,'INTRO')>
                    <cfif IsStruct(content['INTRO'])>
                        <cfset content['INTRO'] = i18n(content['INTRO'],arguments.language)>
                    </cfif>
                    <cfelse>
                    <cfset content['INTRO'] = ''>
                </cfif>
                <cfset structdelete(content,'CATEGORY')>
                <cfset structdelete(content,'USER')>
                <cfset arrayAppend(datacontent,content)>
            </cfif>
            <cfset i++>
        </cfloop>
        <cfreturn datacontent>
    </cffunction>

	<cffunction name="postslistrssfeed" access="remote" returntype="any" returnformat="plain">
		<cfargument name="orderby" type="string" required="false" default="CREATEDAT" />
		<cfargument name="sort" type="string" required="false" default="desc" />
		<cfargument name="currentpage" type="numeric" required="false" default=1 />
		<cfargument name="limit" type="string" required="false" />
		<cfargument name="tags" type="string" required="false" />
		<cfargument name="category" type="string" required="false" />
		<cfargument name="language" type="string" required="true" default="thai" />

		<cfinclude template="config.cfm"/>
		<cfset mycategory=structNew()>
		<cfset sortby = -1>
		<cfset rsstitle="post list">
		<cfset rssdetail="RSS Posts">
		<cfset rssimage="">
		<cfset rsslink="">
		<cfif arguments.sort eq "asc">
			<cfset sortby = 1>
		</cfif>
		<cfset sortfield = StructNew()>
		<cfset sortfield[arguments.orderby] = sortby>

		<cfset myquerytagstext = structNew()>
		<cfset myquerytagstext['TYPECONTENT'] = 1>
		<cfset myquerytagstext['PUBSTATUS'] = 1>

		<cfset mystrlang = structNew()>
		<cfset mystrlang["$ne"] = ''>
		<cfset myquerytagstext["TITLE.#arguments.language#"] = mystrlang>

		<cfif structKeyExists(arguments, "tags")>
			<cfset mytags = listtoarray(arguments.tags,",")>
			<cfset rsstitle=arguments.tags>
			<cfset myquerytags = []>
			<cfloop array="#mytags#" index="t">
				<cfset mystruct = structNew()>
				<cfset mystruct["TAG"] = t>
				<cfset arrayAppend(myquerytags,mystruct)>
			</cfloop>
			<cfset myquerytagstext['$or'] = myquerytags>
		</cfif>

        <cfset var skippost = 0>
        <cfif structKeyExists(arguments, "currentpage") and arguments.currentpage neq 1>
            <cfset skippost = (arguments.currentpage*arguments.limit)-arguments.limit>
        </cfif>

		<cfif structKeyExists(arguments,"category")>
			<cfset mycategory = MongoCollectionfindOne(
				datasource=databasename,
				collection="category",
				query=({"SLUG"=arguments.category})
				)>
			<cfif isdefined("mycategory")>
				<cfset rsstitle = mycategory.TITLE>
				<cfset rssdetail = mycategory.DETAIL>
				<cfif mycategory.IMAGEREF IS NOT "">
					<cfset rssimage = "#domainname##mycategory.IMAGEREF#">
				</cfif>
				<cfset rsslink = mycategory.URL>
				<cfif structKeyExists(mycategory,"_id")>
					<cfset myquerytagstext['CATEGORY.$id'] = mycategory._id>
				</cfif>
			<cfelse>
				<cfset myquerytagstext['CATEGORY'] = false>
			</cfif>
		</cfif>

        <cfset contents = MongoCollectionfind(
            datasource=databasename,
            collection="post",
            query=(myquerytagstext),
            skip=skippost,
            size=val(arguments.limit),
            sort=(sortfield))>

		<cfset myStruct = StructNew() />
		<cfset mystruct.link = "#domainname#"/>
		<cfset myStruct.title = rsstitle />
		<cfset mystruct.description = rssdetail />
		<cfif rssimage IS NOT "">
			<cfset mystruct.image.url = rssimage />
			<cfset mystruct.image.title = rsstitle />
			<cfset mystruct.image.link = rsslink />
		</cfif>
		<cfset mystruct.pubDate = Now() />
		<cfset mystruct.version = "rss_2.0" />
		<cfset myStruct.item = ArrayNew(1) />
		<cfset currentRow = 1>
		<cfloop array="#contents#" index="c">
			<cfset categoryfetch = c.CATEGORY.fetch()>
			<cfset template = categoryfetch.TEMPLATE>
			<cfset linkurl = buildlink(categoryfetch.PATTERN,c.SLUG,template)>
			<cfset myStruct.item[currentRow] = StructNew() />
			<cfset myStruct.item[currentRow].guid = structNew() />
			<cfset myStruct.item[currentRow].guid.isPermaLink="YES" />
			<cfset myStruct.item[currentRow].guid.value = linkurl />
			<cfset myStruct.item[currentRow].pubDate = c.PUBDATE />
			<cfset myStruct.item[currentRow].title = i18n(c['TITLE'],arguments.language) />
			<cfif IsNull(c['USER'].fetch()) eq false>
				<cfset myStruct.item[currentRow].author = c['USER'].fetch()['EMAIL'] />
			</cfif>
			<cfif structKeyExists(c,"INTRO")>
				<cfset myStruct.item[currentRow].description = structNew() />
				<cfset introimage="">
				<cfif c.STHUMB IS NOT "">
					<cfset introimage= "<img width='150' height='150' src='#domainname##c['STHUMB']#' alt='#i18n(c['TITLE'],arguments.language)#' title='#i18n(c['TITLE'],arguments.language)#' style='float:left; margin:0 15px 15px 0;' />">
				</cfif>
				<cfset myStruct.item[currentRow].description.value = "#introimage##i18n(c['INTRO'],arguments.language)#" />
			</cfif>
			<cfset myStruct.item[currentRow].link = linkurl />
			<cfset currentRow++>
		</cfloop>

		<cffeed action="create" name="#myStruct#" overwrite="true" xmlVar="myXML" />

		<cfreturn myXML>

	</cffunction>

	<cffunction name="postslisttest" access="remote" returntype="struct" returnformat="json">
		<cfargument name="orderby" type="string" required="false" default="CREATEDAT" />
		<cfargument name="sort" type="string" required="false" default="desc" />
		<cfargument name="thispage" type="numeric" required="false" default=1 />
		<cfargument name="limit" type="string" required="false" default="0" />
		<cfargument name="tags" type="string" required="false" />
		<cfargument name="category" type="string" required="false" />
		<cfargument name="language" type="string" required="true" default="thai" />
		<cfargument name="skip" type="numeric" required="false" default="0"/>
		<cfargument name="subcategory" type="boolean" default=false required="false"/>

		<cfinclude template="config.cfm"/>
		<cfif environment eq "product">

			<cfset var skipnumber = 0>
			<cfset var skippage = 0>
			<cfif structKeyExists(arguments,"skip")>
				<cfset skipnumber = arguments['skip']>
			</cfif>
			<cfif structKeyExists(arguments, "thispage") and arguments.thispage neq 1>
				<cfset skippage = (arguments.thispage*arguments.limit)-arguments.limit>
			</cfif>
			<cfset var skippost = skipnumber + skippage>

			<cfset sortby = 1>
			<cfif arguments.sort eq "desc">
				<cfset sortby = -1>
			</cfif>
			<cfset sortfield = StructNew()>
			<cfset sortfield[arguments.orderby] = sortby>

			<cfset querytext = structNew()>
			<cfset querytext['TYPECONTENT'] = 1>
			<cfset querytext['PUBSTATUS'] = 1>

			<cfset mystrlang = structNew()>
			<cfset categoryIn = structNew()>
			<cfset mystrlang["$ne"] = ''>
			<cfif structKeyExists(arguments, "tags") && arguments["tags"] neq "">
				<cfset mytags = listtoarray(arguments.tags,",")>
				<cfset myquerytags = []>
				<cfloop array="#mytags#" index="t">
					<cfset arrayAppend(myquerytags,t)>
				</cfloop>
				<cfset querytext['$or'] = [{"TAG":{"$in":myquerytags}}]>
			</cfif>
	<!--- 		Category --->
			<cfif structKeyExists(arguments,"category") && arguments['category'] neq "">
				<cfset category = MongoCollectionfindOne(
					datasource=databasename,
					collection="category",
					query=({"SLUG"=arguments.category})
					)>
				<cfif IsNull(category) eq "No">
					<cfif structKeyExists(category,"_id")>
						<cfset cetegoryArr = arrayNew()>
						<cfset arrayAppend(cetegoryArr,dbref('category',category['_id'].toString()))>
						<cfset categoryIn['$in'] = cetegoryArr>
						<!---				MULTI CETEGORY --->
						<cfif arguments['subcategory'] IS true>
							<cfset cateraw = MongoCollectionfind(databasename,'category',{})>
							<cfset var newcatequery = arrayNew()>
							<cfloop array="#cateraw#" index="item">
								<cfset cateparent = "">
								<cfif structKeyExists(item,'PARENT')>
									<cfset cateparent = item['PARENT'].fetch()['_id'].toString()>
								</cfif>
								<cfset arrayAppend(newcatequery,{"id":item['_id'].toString(),"title":item['title'],"parent":cateparent})>
							</cfloop>
							<cfset catequery = getChildCategory(newcatequery,category['_id'].toString())>
							<cfset newcate = treequeryjson(cateraw)>
							<cfset categoryIn['$in'] = catequery>
						</cfif>
					</cfif>
				</cfif>
				<cfset querytext['CATEGORY']=categoryIn>
			</cfif>
			<!--- end category --->
	        <cfset contents = MongoCollectionfind(
	            datasource=databasename,
	            collection="post",
	            query=(querytext),
	            skip=skippost,
	            size=val(arguments.limit),
	            sort=(sortfield))>
			<cfset var result = structnew()>
			<cfset result["totalrecord"] = MongoCollectioncount(
	            datasource=databasename,
	            collection="post",
	            query=(querytext)) - arguments['skip']>

			<cfloop array="#contents#" index="content">
				<cfset content['_id'] = content['_id'].toString()>
				<cfset content['TITLE'] = i18n(content['TITLE'],arguments.language)>
				<cfset content['DETAIL'] = i18n(content['DETAIL'],arguments.language) />
				<cfset content['AUTHOR'] = content['USER'].fetch()['USERNAME']>
				<cfset categoryfetch = content['CATEGORY'].fetch()>
				<cfif isNull(categoryfetch)>
					<cfset content['CATEGORYTITLE'] = "">
					<cfset content['CATEGORYSLUG'] = "">
					<cfset content['CATEGORYTEMPLATE'] = "">
				<cfelse>
					<cfset content['CATEGORYTITLE'] = categoryfetch['TITLE']>
					<cfset content['CATEGORYSLUG'] =  categoryfetch['SLUG']>
					<cfset content['CATEGORYTEMPLATE'] =  categoryfetch['TEMPLATE']>
				</cfif>
				<cfif structKeyExists(content,'INTRO')>
					<cfif IsStruct(content['INTRO'])>
						<cfset content['INTRO'] = i18n(content['INTRO'],arguments.language)>
					</cfif>
				<cfelse>
					<cfset content['INTRO'] = ''>
				</cfif>
	            <cfset structdelete(content,'CATEGORY')>
	            <cfset structdelete(content,'USER')>
			</cfloop>
			<cfset result["contents"] = contents>
		<cfelse>
			<cfset var mockdata = getmockdata(val(arguments['thispage']),val(arguments['limit']))>
			<cfset result['contents'] = mockdata['contents']>
			<cfset result['totalrecord'] = mockdata['totalrecord']>
		</cfif>
		<cfreturn result>
	</cffunction>

	<cffunction name="postsrandom" access="remote" returntype="struct" returnformat="json">
		<cfargument name="limit" type="numeric" required="false" default="20" />
		<cfargument name="tags" type="string" required="false" />
		<cfargument name="category" type="string" required="false" />
		<cfargument name="language" type="string" required="true" default="thai" />
		<cfargument name="subcategory" type="boolean" default=false required="false"/>
		<cfinclude template="config.cfm"/>
		<cfset var result = structNew()>
		<cfif environment eq "product">
			<cfset result["totalrecord"] = 0>
			<cfset result["contents"] = arrayNew()>

			<!--- Search Struct --->
			<cfset querytext = structNew()>
			<cfset querytext['TYPECONTENT'] = 1>
			<cfset querytext['PUBSTATUS'] = 1>

			<cfset strlang = structNew()>
			<cfset strlang["$ne"] = ''>

			<!--- Tags --->
			<cfif structKeyExists(arguments, "tags") && arguments["tags"] neq "">
				<cfset mytags = listtoarray(arguments.tags,",")>
				<cfset querytags = []>
				<cfloop array="#mytags#" index="t">
					<cfset mystruct = structNew()>
					<cfset mystruct["TAG"] = t>
					<cfset arrayAppend(querytags,mystruct)>
				</cfloop>
				<cfset querytext['$or'] = querytags>
			</cfif>
			<!--- End Tags --->
			<!--- Category --->
			<cfif structKeyExists(arguments,"category") && arguments['category'] neq "">
				<cfset category = MongoCollectionfindOne(
					datasource=databasename,
					collection="category",
					query=({"SLUG"=arguments.category})
					)>
				<cfif IsNull(category) eq "No">
					<cfif structKeyExists(category,"_id")>
						<cfset cetegoryArr = arrayNew()>
						<cfset arrayAppend(cetegoryArr,dbref('category',category['_id'].toString()))>
						<cfset categoryIn['$in'] = cetegoryArr>
						<!---				MULTI CETEGORY --->
						<cfif arguments['subcategory'] IS true>
							<cfset cateraw = MongoCollectionfind(databasename,'category',{})>
							<cfset var newcatequery = arrayNew()>
							<cfloop array="#cateraw#" index="item">
								<cfset cateparent = "">
								<cfif structKeyExists(item,'PARENT')>
									<cfset cateparent = item['PARENT'].fetch()['_id'].toString()>
								</cfif>
								<cfset arrayAppend(newcatequery,{"id":item['_id'].toString(),"title":item['title'],"parent":cateparent})>
							</cfloop>
							<cfset catequery = getChildCategory(newcatequery,category['_id'].toString())>
							<cfset newcate = treequeryjson(cateraw)>
							<cfset categoryIn['$in'] = catequery>
						</cfif>
						<cfset querytext['CATEGORY']=categoryIn>
					</cfif>
				</cfif>
			</cfif>
			<!--- End Category --->
			<!--- Query --->
				<cfset var contents = MongoCollectionfind(
	            	datasource=databasename,
	            	collection="post",
	            	query=(querytext))>
			<!--- End Query --->
			<!--- Random number --->
			<cfset var totalrecord = arraylen(contents)>
			<cfset var arraydata = arraynew()>
			<cfif totalrecord gt 0>
				<cfset var listrandom = getrandom(totalrecord,arguments['limit'])>
				<!--- End random number --->
				<!--- Get data --->
				<cfloop list="#listrandom#" index="i">
					<cfset content = contents[i]>
					<cfset content['_id'] = content['_id'].toString()>
					<cfset content['TITLE'] = i18n(content['TITLE'],arguments.language)>
					<cfset content['DETAIL'] = i18n(content['DETAIL'],arguments.language) />
					<cfset content['AUTHOR'] = content['USER'].fetch()['USERNAME']>
					<cfset categoryfetch = content['CATEGORY'].fetch()>
					<cfif isNull(categoryfetch)>
						<cfset content['CATEGORYTITLE'] = "">
						<cfset content['CATEGORYSLUG'] = "">
						<cfset content['CATEGORYTEMPLATE'] = "">
					<cfelse>
						<cfset content['CATEGORYTITLE'] = categoryfetch['TITLE']>
						<cfset content['CATEGORYSLUG'] = categoryfetch['SLUG']>
						<cfset content['CATEGORYTEMPLATE'] = categoryfetch['TEMPLATE']>
					</cfif>
					<cfif structKeyExists(content,'INTRO')>
						<cfif IsStruct(content['INTRO'])>
							<cfset content['INTRO'] = i18n(content['INTRO'],arguments.language)>
						</cfif>
					<cfelse>
						<cfset content['INTRO'] = ''>
					</cfif>
		            <cfset structdelete(content,'CATEGORY')>
		            <cfset structdelete(content,'USER')>
		            <cfset arrayAppend(arraydata, content)>
				</cfloop>
			<!--- End get data --->
			</cfif>
			<cfset result["totalrecord"] = totalrecord>
			<cfset result["contents"] = arraydata>
		<cfelse>
			<cfset var mockdata = getmockdata(1,val(arguments['limit']))>
			<cfset result['contents'] = mockdata['contents']>
			<cfset result['totalrecord'] = mockdata['totalrecord']>
		</cfif>
		<cfreturn result>
		<!--- <cfdump var="#result#"/>
		<cfabort/> --->
	</cffunction>

</cfcomponent>