<cfcomponent extends="Utility">

	<cffunction name="getuserid" access="private" returntype="string">
		<cfargument name="secretkey" type="string" required="true">
		<cfinclude template="config.cfm"/>
		<cfset uid = MongoCollectionfindOne(databasename,"user",{SECRET:arguments.secretkey})>
		<cfset uid._id = uid._id.toString()>
		<cfreturn uid._id>
	</cffunction>

	<cffunction name="breadcrumblistbyuserid" access="remote" returntype="array" returnformat="json">
		<cfargument name="userid" type="string" required="true" default="">
		<cfinclude template="config.cfm"/>
		<cfset uid = getuserid(arguments.userid)>
		<cfset uid = MongoObjectId(uid)>
		<cfset cateraw = MongoCollectionfind(databasename,'category',{'USER.$id':uid})>
		<cfset caterawquery = treequery(cateraw) />
		<cfreturn caterawquery>
	</cffunction>

	<cffunction name="breadcrumblistbycategory_custom" access="remote" returntype="array" returnformat="json">
		<cfargument name="category" type="string" required="true" default="">
		<cfargument name="id" type="string" default="_id">
		<cfargument name="name" type="string" default="title">
		<cfargument name="parent" type="string" default="PARENT">
		<cfargument name="urluse" type="string" default="">
		<cfargument name="hometitle" type="string" default="HOME">
		<cfargument name="homeurl" type="string" default="http://#CGI['HTTP_HOST']#">
		<cfinclude template="config.cfm"/>
		<cfset categorylistsort = arrayNew()>
		<cfif environment eq "product">
			<cfset categorydata = MongoCollectionfindOne(databasename,'category',{'SLUG'=arguments['category']})>
			<cfset categorylist = $categoryparent(categorydata) >
			<cfset categorylist=deserializeJSON("[#categorylist#]")>
			<cfloop array="#categorylist#" index="i">
				<cfset catelink=structNew()>
				<cfset catelink["SLUG"]=i.SLUG>
				<cfset catelink["TITLE"]=i.TITLE>
				<cfset catelink["URL"]=buildotherlink(i.URL,"category",i.SLUG)>
				<cfset arrayprepend(categorylistsort,catelink)>
			</cfloop>
		<cfelse>
			<cfset catelink=structNew()>
			<cfset catelink["SLUG"]=arguments.category>
			<cfset catelink["TITLE"]=arguments.category>
			<cfset catelink["URL"]="categorydetail.cfm?category=#arguments.category#">
			<cfset arrayprepend(categorylistsort,catelink)>
		</cfif>
		<cfset home["SLUG"]="">
		<cfset home["TITLE"]=arguments.hometitle>
		<cfset home["URL"]=arguments.homeurl>
		<cfset arrayprepend(categorylistsort,home)>
		<cfreturn categorylistsort>
	</cffunction>

	<cffunction name="breadcrumblistbyslug_custom" access="remote" returntype="array" returnformat="json">
		<cfargument name="slug" type="string" required="true" default="">
		<cfargument name="id" type="string" default="_id">
		<cfargument name="name" type="string" default="title">
		<cfargument name="parent" type="string" default="PARENT">
		<cfargument name="urluse" type="string" default="">
		<cfargument name="hometitle" type="string" default="HOME">
		<cfargument name="homeurl" type="string" default="http://#CGI['HTTP_HOST']#">
		<cfargument name="language" type="string" required="true" default="thai" />
		<cfinclude template="config.cfm"/>
		<cfset var categorylistsort=arrayNew()>
		<cfset var template="">
		<cfset var pattern="">
		<cfif environment eq "product">
			<cfset postdata = MongoCollectionfindOne(databasename,'post',{'SLUG'=arguments['slug']})>
			<cfif isdefined("postdata")>
				<cfset thiscategory = postdata.CATEGORY.fetch()>
				<cfset categorylist = $categoryparent(thiscategory) >
				<cfset template=thiscategory.TEMPLATE>
				<cfset pattern=thiscategory.PATTERN>
				<cfset categorylist=deserializeJSON("[#categorylist#]")>
				<cfset posturl="">
				<cfset j=0>
				<cfloop array="#categorylist#" index="i">
					<cfif j IS 0>
						<cfset posturl=i.TEMPLATE>
					</cfif>
					<cfset j++>
					<cfset catelink=structNew()>
					<cfset catelink["SLUG"]=i.SLUG>
					<cfset catelink["TITLE"]=i.TITLE>
					<cfset catelink["URL"]=buildotherlink(i.URL,"category",i.SLUG)>
					<cfset arrayprepend(categorylistsort,catelink)>
				</cfloop>
				<cfset postlink= structNew()>
				<cfset postlink["SLUG"]=postdata["SLUG"]>
				<cfset postlink["TITLE"]=removehtmltags(i18n(postdata.TITLE,arguments.language))>
				<cfset postlink["URL"]=buildlink(pattern,postdata["SLUG"],template)>
				<cfset arrayappend(categorylistsort,postlink)>
				<cfset resultjson = structNew()>
			</cfif>
		<cfelse>
			<cfset catelink=structNew()>
			<cfset catelink["SLUG"]=arguments.slug>
			<cfset catelink["TITLE"]=arguments.slug>
			<cfset catelink["URL"]="postdetail.cfm?slug=#arguments.slug#">
			<cfset arrayprepend(categorylistsort,catelink)>
		</cfif>
		<cfset home["SLUG"]="">
		<cfset home["TITLE"]=arguments.hometitle>
		<cfset home["URL"]=arguments.homeurl>
		<cfset arrayprepend(categorylistsort,home)>
		<cfreturn categorylistsort>
	</cffunction>

	<cffunction name="$categoryparent"  returntype="any" >
		<cfargument name="category" type="struct" required="true" default="">

		<cfoutput>
			<cfsavecontent variable="str">
				#serializeJSON(category)#
				<cfif structKeyExists(arguments.category,"PARENT")>
					,#$categoryparent(arguments.category.PARENT.fetch())#
				</cfif>
			</cfsavecontent>
		</cfoutput>

		<cfreturn str>
	</cffunction>

</cfcomponent>
