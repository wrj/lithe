<cfset callfunction = createObject("component","cfc.services.contactus")>
<cfset systemconfig = callfunction.readconfig()>
<cfset databasename = systemconfig.database.databasename>
<cfset domainname= systemconfig.general.siteaddress>
<cfset databasehost= systemconfig.database.databasehost>
<cfset databaseport= systemconfig.database.databaseport>
<cfset databaseusername = systemconfig.database.databaseusername>
<cfset databasepassword = decrypt(systemconfig.database.DATABASEPASSWORD,systemconfig.database.databaseusername)>
<cfset frontendurl="#domainname##systemconfig.general.pathfrontend#">
<cfset mail = {
	from: systemconfig.email.username,
	server: systemconfig.email.server,
	useSSL: systemconfig.email.ssl,
	port: systemconfig.email.port,
	username: systemconfig.email.username,
	password: decrypt(systemconfig.email.password,systemconfig.email.username)
}>
<cfif structKeyExists(systemconfig, "environment")>
	<cfset environment = systemconfig["environment"]>
<cfelse>
	<cfset environment = "product">
</cfif>
