<cfcomponent extends="Utility">

	<cffunction name="tagcloudlist" access="remote" returntype="array" returnformat="json">
		<cfinclude template="config.cfm">
		<cfif environment eq "product">
			<cfset taglist = MongoCollectionfind(databasename,"tag",{},{'NAME':true})>
			<cfset mytag = "">
			<cfset tagArray = arrayNew() />
			<cfloop array="#taglist#" index="p">
				<cfset arrayAppend(tagArray, p["NAME"]) />
			</cfloop>
		<cfelse>
			<cfset var mockdata = getmockdata()>
			<cfset tagArray = mockdata['tagcloud']>
		</cfif>
		<cfreturn tagArray>
	</cffunction>
</cfcomponent>