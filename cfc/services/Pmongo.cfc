<cfcomponent>
    <cffunction name="dbref" access="public" returntype="struct">
        <cfargument name="collection" type="string" required="true">
        <cfargument name="objectid" type="string" required="true">
        <cfset var dbreturn = structNew()>
        <cfset dbreturn['$ref'] = lcase(arguments['collection'])>
        <cfset dbreturn['$id'] =  MongoObjectid(arguments['objectid'])>
        <cfreturn dbreturn>
    </cffunction>
</cfcomponent>
