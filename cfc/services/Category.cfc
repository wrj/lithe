<cfcomponent extends="Utility">

	<cffunction name="categoryquery" returntype="array" access="public" >
		<cfargument name="obj" type="any" required="true">
		<cfargument name="url" type="string" required="false" default="">
		<cfargument name="delimiters" type="string" required="false" default=">" />
		<cfargument name="delimitersclass" type="string" required="false" default="" />
		<cfset var resultarr = arraynew()>
		<cfloop array="#arguments.obj#" index="item">
			<cfset depfound = false>
			<cfset var str=''>
			<cfif structKeyExists(item,"PARENT")>
				<cfset str = parentquery(objnode=item['PARENT'].fetch(),arguments.url,arguments.delimiters,arguments.delimitersclass)>
			</cfif>
			<cfif depfound eq false>
				<cfset template="">
				<cfif arguments.url IS "">
					<cfset template=item["URL"]>
				<cfelse>
					<cfset template=arguments.url>
				</cfif>
				<cfset urlcate = buildotherlink(template,"category",item['SLUG'])>
				<cfset temp = structNew()>
				<cfset temp['_id']=item['_id'].toString()>
				<cfset temp['LINK'] = "#str# <a href='#urlcate#'>#item['TITLE']#</a>">
				<cfset temp['IMAGEREF'] = checkdata(item,'IMAGEREF') >
				<cfset temp['TITLE'] = item["TITLE"] >
				<cfset temp['SLUG'] = item["SLUG"] >
				<cfset arrayAppend(resultarr,temp)>
			</cfif>
		</cfloop>
		<cfreturn resultarr>
	</cffunction>

	<cffunction name="parentquery" returntype="string" access="public" >
		<cfargument name="objnode" type="any">
		<cfargument name="url" type="string" required="false" default="">
		<cfargument name="delimiters" type="string" required="false" default=">" />
		<cfargument name="delimitersclass" type="string" required="false" default="" />
		<cfset str="">
		<cfif structKeyExists(objnode,'PARENT')>
			<cfset str = parentquery(objnode=objnode['PARENT'].fetch(),arguments.url,arguments.delimiters,arguments.delimitersclass)>
		</cfif>
		<cfset template="">
		<cfif arguments.url IS "">
			<cfset template=item["URL"]>
		<cfelse>
			<cfset template=arguments.url>
		</cfif>
		<cfset urlcate = buildotherlink(template,"category",item['SLUG'])>
		<cfreturn "#str#<a href='#urlcate#'>#objnode["TITLE"]#</a><span class='#arguments.delimitersclass#'>#arguments.delimiters#</span>">
	</cffunction>

	<cffunction name="categorieslist_custom" access="remote" returntype="struct" returnformat="json">
		<cfargument name="sort" type="string" required="false" default="desc" />
		<cfargument name="url" type="string" required="false" default="" />
		<cfargument name="delimiters" type="string" required="false" default=">" />
		<cfargument name="delimitersclass" type="string" required="false" default="" />
		<cfinclude template="config.cfm"/>
		<cfset returndata=structNew()>
		<cfif environment eq "product">
			<cfset sortby = 1>
			<cfif arguments.sort eq "desc">
				<cfset sortby = -1>
			</cfif>
			<cfset sortfield = StructNew()>
			<cfset sortfield["TITLE"] = sortby>
			<cfset cateraw = MongoCollectionfind(
				datasource=databasename,
				collection="category",
				query={},
				skip=0,
				size=0,
				sort=(sortfield))>
			<cfset caterawquery = categoryquery(cateraw,arguments.url,arguments.delimiters,arguments.delimitersclass) />
			<cfset returndata['contents'] = caterawquery>
			<cfset returndata['totalrecord'] = arraylen(caterawquery)>
		<cfelse>
			<cfset var mockdata = getmockdata()>
			<cfset caterawquery = mockdata['category']>
			<cfset returndata['contents'] = caterawquery>
			<cfset returndata['totalrecord'] = arraylen(caterawquery)>
		</cfif>
		<cfreturn returndata>
	</cffunction>

	<cffunction name="subcategories_custom" access="remote" returntype="struct" returnformat="json">
		<cfargument name="orderby" type="string" required="false" default="CREATEDAT" />
		<cfargument name="sort" type="string" required="false" default="asc" />
		<cfargument name="category" type="string" required="false" />
		<cfargument name="thispage" type="numeric" required="false" default=1 />
		<cfargument name="limit" type="string" required="false" default="0" />
		<cfargument name="skip" type="numeric" required="false"/>
		<cfinclude template="config.cfm"/>
		<cfset returndata = structNew()>
		<cfif environment eq "product">
			<cfset var skipnumber = 0>
			<cfset var skippage = 0>
			<cfif structKeyExists(arguments,"skip")>
				<cfset skipnumber = arguments['skip']>
			</cfif>
			<cfif structKeyExists(arguments, "thispage") and arguments.thispage neq 1>
				<cfset skippage = (arguments.thispage*arguments.limit)-arguments.limit>
			</cfif>
			<cfset var skippost = skipnumber + skippage>
			<cfset sortby = 1>
			<cfif arguments.sort eq "desc">
				<cfset sortby = -1>
			</cfif>
			<cfset sortfield = StructNew()>
			<cfset sortfield[arguments.orderby] = sortby>
			<cfset output = arrayNew()>
			<cfset var categorysearch=structNew()>
			<cfif arguments.category NEQ "">
				<cfset parent_category = MongoCollectionfindOne(databasename,'category',{'SLUG'=arguments.category})>
				<cfif isDefined("parent_category")>
					<cfset categorysearch['PARENT.$id']=parent_category._id>
				<cfelse>
					<cfset categorysearch['PARENT.$id']=false>
				</cfif>
			</cfif>
			<cfset sub_category = MongoCollectionfind(
				datasource=databasename,
				collection="category",
				query=(categorysearch),
				skip=skippost,
				size=val(arguments.limit),
				sort=(sortfield))>
			<cfloop index="i" array="#sub_category#">
				<cfset i.ID=i._id.tostring()>
				<cfset structDelete(i,"_id")>
				<cfset structDelete(i,"USER")>
				<cfset arrayAppend(output,i)>
			</cfloop>
			<cfset count_sub_category = MongoCollectioncount(
				datasource=databasename,
				collection="category",
				query=categorysearch)- arguments['skip']>
			<cfset returndata['contents'] = output>
			<cfset returndata['totalrecord'] = count_sub_category>
		<cfelse>
			<cfset var mockdata = getmockdata(val(arguments['thispage']),val(arguments['limit']))>
			<cfset returndata['contents'] = mockdata['subcategory']['contents']>
			<cfset returndata['totalrecord'] = mockdata['subcategory']['totalrecord']>
		</cfif>
		<cfreturn returndata>
	</cffunction>

	<cffunction name="submulticategories_custom" access="remote" returntype="struct" returnformat="json">
		<cfargument name="orderby" type="string" required="false" default="CREATEDAT" />
		<cfargument name="sort" type="string" required="false" default="desc" />
		<cfargument name="category" type="string" required="false" />
		<cfargument name="thispage" type="numeric" required="false" default=1 />
		<cfargument name="limit" type="string" required="false" default="0" />
		<cfargument name="skip" type="numeric" required="false"/>
		<cfinclude template="config.cfm"/>
		<cfif environment eq "product">
			<cfset var skipnumber = 0>
			<cfset var skippage = 0>
			<cfif structKeyExists(arguments,"skip")>
				<cfset skipnumber = arguments['skip']>
			</cfif>
			<cfif structKeyExists(arguments, "thispage") and arguments.thispage neq 1>
				<cfset skippage = (arguments.thispage*arguments.limit)-arguments.limit>
			</cfif>
			<cfset var skippost = skipnumber + skippage>
			<cfset sortby = 1>
			<cfif arguments.sort eq "desc">
				<cfset sortby = -1>
			</cfif>
			<cfset sortfield = StructNew()>
			<cfset sortfield[arguments.orderby] = sortby>
			<cfset mycates = listtoarray(arguments.category,",")>
			<cfset myquerycatestext = structNew()>
			<cfset myquerycates = []>
			<cfloop array="#mycates#" index="c">
				<cfset mystructcates = structNew()>
				<cfset mystructcates["SLUG"] = c>
				<cfset arrayAppend(myquerycates,mystructcates)>
			</cfloop>
			<cfset myquerycatestext['$or'] = myquerycates>
			<cfset parent_category = MongoCollectionfind(databasename,'category',myquerycatestext)>
			<cfset output = arrayNew()>
			<cfset returndata = structNew()>
			<cfif isDefined("parent_category")>
				<cfset myquerycatestext2 = structNew()>
				<cfset myquerycates2 = []>
				<cfloop array="#parent_category#" index="q">
					<cfset mystructcates = structNew()>
					<cfset mystructcates['PARENT.$id'] = q._id>
					<cfset arrayAppend(myquerycates2,mystructcates)>
				</cfloop>
				<cfset myquerycatestext2['$or'] = myquerycates2>
				<cfset sub_category = MongoCollectionfind(
					datasource=databasename,
					collection="category",
					query=myquerycatestext2,
					skip=skippost,
					size=val(arguments.limit),
					sort=(sortfield))>
				<cfloop index="i" array="#sub_category#">
					<cfset i.ID=i._id.tostring()>
					<cfset structDelete(i,"_id")>
					<cfset structDelete(i,"USER")>
					<cfset arrayAppend(output,i)>
				</cfloop>
			</cfif>
			<cfset count_sub_category = MongoCollectioncount(
				datasource=databasename,
				collection="category",
				query=myquerycatestext2)- arguments['skip']>
			<cfset returndata['contents'] = output>
			<cfset returndata['totalrecord'] = count_sub_category>
		<cfelse>
			<cfset var mockdata = getmockdata(val(arguments['thispage']),val(arguments['limit']))>
			<cfset returndata['contents'] = mockdata['subcategory']['contents']>
			<cfset returndata['totalrecord'] = mockdata['subcategory']['totalrecord']>
		</cfif>
		<cfreturn returndata>
	</cffunction>

	<cffunction name="categorydetail" access="remote" returntype="struct" returnformat="json">
		<cfargument name="slug" type="string" required="true" />
		<cfinclude template="config.cfm"/>
		<cfset searchst = structNew()>
		<cfset cateraw=structNew()>
		<cfif environment eq "product">
			<cfset searchst['SLUG'] = arguments['slug']>
			<cfset cateraw = MongoCollectionfindOne(databasename,'category',searchst)>
			<cfset newid = cateraw['_id'].toString()>
			<cfset cateraw['ID'] = newid>
			<cfset structDelete(cateraw,'_id')>
		<cfelse>
			<cfset var mockdata = getmockdata()>
			<cfset cateraw = mockdata['subcategory']['detail']>
		</cfif>
		<cfreturn cateraw>
	</cffunction>
	
</cfcomponent>