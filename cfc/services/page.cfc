<cfcomponent extends="Utility">

	<cffunction name="pageslist" access="remote" returntype="array" returnformat="json">
		<cfargument name="orderby" type="string" required="false" default="CREATEDAT" />
		<cfargument name="sort" type="string" required="false" default="asc" />
		<cfargument name="currentpage" type="numeric" required="false" default=1 />
		<cfargument name="limit" type="string" required="false" />
		<cfargument name="category" type="string" required="false" />
		<cfargument name="language" type="string" required="true" default="thai" />

		<cfinclude template="config.cfm">

		<cfset sortby = 1>
		<cfif arguments.sort eq "desc">
			<cfset sortby = -1>
		</cfif>
		<cfset sortfield = StructNew()>
		<cfset sortfield[arguments.orderby] = sortby>

		<cfset myquerytagstext = structNew()>
		<cfset myquerytagstext['TYPECONTENT'] = 2>
		<cfset myquerytagstext['PUBSTATUS'] = 1>

		<cfset mystrlang = structNew()>
		<cfset mystrlang["$ne"] = ''>
		<cfset myquerytagstext["TITLE.#arguments.language#"] = mystrlang>
        <cfset var skippage = 0/>
        <cfif structKeyExists(arguments, "currentpage") and arguments.currentpage neq 1>
            <cfset skippage = (arguments.currentpage*arguments.limit)-arguments.limit/>
        </cfif>

		<cfif structKeyExists(arguments,"category")>
			<cfset mycategory = MongoCollectionfindOne(
				datasource=databasename,
				collection="category",
				query=({"SLUG"=arguments.category})
				)>
			<cfif isDefined('mycategory') AND structKeyExists(mycategory,"_id")>
				<cfset myquerytagstext['CATEGORY.$id'] = mycategory._id>
			</cfif>
		</cfif>
        <cfset contents = MongoCollectionfind(
            datasource=databasename,
            collection="post",
            query=(myquerytagstext),
            skip=skippage,
            size=val(arguments.limit),
            sort=(sortfield))>

		<cfloop from="1" to="#arrayLen(contents)#" index="i">
			<cfif arrayIndexExists(contents, i)>
				<cfset contents[i]._id = contents[i]._id.toString()>
				<cfset contents[i]['TITLE'] = i18n(contents[i]['TITLE'],arguments.language)>
				<cfset contents[i]['AUTHOR'] = contents[i]['USER'].fetch()['USERNAME']>
				<cfset contents[i].DETAIL = i18n(contents[i].DETAIL,arguments.language) />
				<cfif isNull(contents[i].CATEGORY.fetch())>
					<cfset contents[i].CATEGORYTITLE = "">
				<cfelse>
					<cfset contents[i].CATEGORYTITLE = contents[i].CATEGORY.fetch()['TITLE']>
				</cfif>
                <cfset contents[i]['INTRO'] = ''>
				<cfif structKeyExists(contents[i],'INTRO')>
					<cfif IsStruct(contents[i]['INTRO'])>
						<cfset contents[i]['INTRO'] = i18n(contents[i]['INTRO'],arguments.language)>
					</cfif>
				</cfif>

			</cfif>
		</cfloop>
		<cfreturn contents>
	</cffunction>

	<cffunction name="pageslistmulticategory" access="remote" returntype="array" returnformat="json">
		<cfargument name="orderby" type="string" required="false" default="CREATEDAT" />
		<cfargument name="sort" type="string" required="false" default="asc" />
		<cfargument name="currentpage" type="numeric" required="false" default=1 />
		<cfargument name="limit" type="string" required="false" />
		<cfargument name="category" type="string" required="false" />
		<cfargument name="language" type="string" required="true" default="thai" />

		<cfinclude template="config.cfm"/>

		<cfset sortby = 1>
		<cfif arguments.sort eq "desc">
			<cfset sortby = -1>
		</cfif>
		<cfset sortfield = StructNew()>
		<cfset sortfield[arguments.orderby] = sortby>

		<cfset myquerytagstext = structNew()>
		<cfset myquerytagstext['TYPECONTENT'] = 2>
		<cfset myquerytagstext['PUBSTATUS'] = 1>

		<cfset mystrlang = structNew()>
		<cfset mystrlang["$ne"] = ''>
		<cfset myquerytagstext["TITLE.#arguments.language#"] = mystrlang>

        <cfset var skippage = 0>
        <cfif structKeyExists(arguments, "currentpage") and arguments.currentpage neq 1>
            <cfset skippage = (arguments.currentpage*arguments.limit)-arguments.limit>
        </cfif>
		<cfif structKeyExists(arguments,"category")>
			<cfset mycates = listtoarray(arguments.category,",")>
			<cfset myquerycates = []>
			<cfloop array="#mycates#" index="c">
				<cfset mystructcates = structNew()>
				<cfset mystructcates["SLUG"] = c>
				<cfset arrayAppend(myquerycates,mystructcates)>
			</cfloop>
			<cfset myquerycatestext['$or'] = myquerycates>

			<cfset mycategory = MongoCollectionfind(
				datasource=databasename,
				collection="category",
				query=(myquerycatestext),
				field={"_id"=true}
					)>

			<cfif isarray(mycategory) and arraylen(mycategory) gte 1>
				<cfif not isDefined("myquerytags")>
					<cfset myquerytags = []>
				</cfif>
				<cfloop array="#mycategory#" index="mca">
					<cfset mystrcat = structNew()>
					<cfset mystrcat["CATEGORY.$id"] = mca._id>
					<cfset arrayAppend(myquerytags,mystrcat)>
				</cfloop>
				<cfset myquerytagstext['$or'] = myquerytags>
			</cfif>
		</cfif>
        <cfset contents = MongoCollectionfind(
            datasource=databasename,
            collection="post",
            query=(myquerytagstext),
            skip=skippage,
            size=val(arguments.limit),
            sort=(sortfield))>
		<cfloop array="#contents#" index="i">
			<cfset i._id = i._id.toString()>
			<cfset i.TITLE = i18n(i.TITLE,arguments.language)>
			<cfset i['AUTHOR'] = i['USER'].fetch()['USERNAME']>
			<cfif structKeyExists(i,"INTRO")>
				<cfset i.INTRO = i18n(i.INTRO,arguments.language)>
			</cfif>
			<cfset i.DETAIL = i18n(i.DETAIL,arguments.language) />
			<cfif isNull(i.CATEGORY.fetch())>
				<cfset i.CATEGORYTITLE = "">
			<cfelse>
				<cfset i.CATEGORYTITLE = i.CATEGORY.fetch()['TITLE']>
			</cfif>
		</cfloop>
		<cfreturn contents>
	</cffunction>

	<cffunction name="pagestitlelistmulticategory" access="remote" returntype="array" returnformat="json">
		<cfargument name="orderby" type="string" required="false" default="CREATEDAT" />
		<cfargument name="sort" type="string" required="false" default="asc" />
		<cfargument name="currentpage" type="numeric" required="false" default=1 />
		<cfargument name="limit" type="string" required="false" />
		<cfargument name="category" type="string" required="false" />
		<cfargument name="language" type="string" required="true" default="thai" />

		<cfinclude template="config.cfm"/>

		<cfset sortby = 1>
		<cfif arguments.sort eq "desc">
			<cfset sortby = -1>
		</cfif>
		<cfset sortfield = StructNew()>
		<cfset sortfield[arguments.orderby] = sortby>

		<cfset myquerytagstext = structNew()>
		<cfset myquerytagstext['TYPECONTENT'] = 2>
		<cfset myquerytagstext['PUBSTATUS'] = 1>

		<cfset mystrlang = structNew()>
		<cfset mystrlang["$ne"] = ''>
		<cfset myquerytagstext["TITLE.#arguments.language#"] = mystrlang>

		<cfif structKeyExists(arguments,"category")>
			<cfset mycates = listtoarray(arguments.category,",")>
			<cfset myquerycates = []>
			<cfloop array="#mycates#" index="c">
				<cfset mystructcates = structNew()>
				<cfset mystructcates["SLUG"] = c>
				<cfset arrayAppend(myquerycates,mystructcates)>
			</cfloop>
			<cfset myquerycatestext['$or'] = myquerycates>

			<cfset mycategory = MongoCollectionfind(
				datasource=databasename,
				collection="category",
				query=(myquerycatestext),
				field={"_id"=true})>

			<cfif isarray(mycategory) and arraylen(mycategory) gte 1>
				<cfif not isDefined("myquerytags")>
					<cfset myquerytags = []>
				</cfif>
				<cfloop array="#mycategory#" index="mca">
					<cfset mystrcat = structNew()>
					<cfset mystrcat["CATEGORY.$id"] = mca._id>
					<cfset arrayAppend(myquerytags,mystrcat)>
				</cfloop>
				<cfset myquerytagstext['$or'] = myquerytags>
			</cfif>
		</cfif>
        <cfset var skippage = 0>
        <cfif structKeyExists(arguments, "currentpage") and arguments.currentpage neq 1>
            <cfset skippage = (arguments.currentpage*arguments.limit)-arguments.limit>
        </cfif>
        <cfset contents = MongoCollectionfind(
            datasource=databasename,
            collection="post",
            query=(myquerytagstext),
            skip=skippage,
            size=val(arguments.limit),
            sort=(sortfield),
            fields={"INTRO"=true,"SLUG"=true,"_id"=true,"TITLE"=true,"CREATEDAT"=true,"UPDATEDAT"=true,"PUBSTATUS"=true,"PUBDATE"=true,"USER"=true,"TAG"=true,"TYPECONTENT"=true,"CATEGORY"=true})>
		<cfloop array="#contents#" index="i">
			<cfset i._id = i._id.toString()>
			<cfset i.TITLE = i18n(i.TITLE,arguments.language)>
			<cfset i['AUTHOR'] = i['USER'].fetch()['USERNAME']>
			<cfif structKeyExists(i,"INTRO")>
				<cfset i.INTRO = i18n(i.INTRO,arguments.language)>
			</cfif>
			<cfif isNull(i.CATEGORY.fetch())>
				<cfset i.CATEGORYTITLE = "">
			<cfelse>
				<cfset i.CATEGORYTITLE = i.CATEGORY.fetch()['TITLE']>
			</cfif>
		</cfloop>
		<cfreturn contents>
	</cffunction>

	<cffunction name="pagestitlelist" access="remote" returntype="array" returnformat="json">
		<cfargument name="orderby" type="string" required="false" default="UPDATEDAT" />
		<cfargument name="sort" type="string" required="false" default="asc" />
		<cfargument name="currentpage" type="numeric" required="false" default=1 />
		<cfargument name="limit" type="string" required="false" />
		<cfargument name="category" type="string" required="false" />
		<cfargument name="language" type="string" required="true" default="thai" />

		<cfinclude template="config.cfm">

		<cfset sortby = 1>
		<cfif arguments.sort eq "desc">
			<cfset sortby = -1>
		</cfif>
		<cfset sortfield = StructNew()>
		<cfset sortfield[arguments.orderby] = sortby>

		<cfset myquerytagstext = structNew()>
		<cfset myquerytagstext['TYPECONTENT'] = 2>
		<cfset myquerytagstext['PUBSTATUS'] = 1>

		<cfset mystrlang = structNew()>
		<cfset mystrlang["$ne"] = ''>
		<cfset myquerytagstext["TITLE.#arguments.language#"] = mystrlang>

        <cfset var skippage = 0>
        <cfif structKeyExists(arguments, "currentpage") and arguments.currentpage neq 1>
            <cfset skippage = (arguments.currentpage*arguments.limit)-arguments.limit>
        </cfif>

		<cfif structKeyExists(arguments,"category")>
			<cfset mycategory = MongoCollectionfindOne(
				datasource=databasename,
				collection="category",
				query=({"SLUG"=arguments.category})
				)>
			<cfif structKeyExists(mycategory,"_id")>
				<cfset myquerytagstext['CATEGORY.$id'] = mycategory._id>
			</cfif>
		</cfif>
        <cfset contents = MongoCollectionfind(
            datasource=databasename,
            collection="post",
            query=(myquerytagstext),
            skip=skippage,
            size=val(arguments.limit),
            sort=(sortfield))>

		<cfloop from="1" to="#arrayLen(contents)#" index="i">
			<cfif arrayIndexExists(contents, i)>
				<cfset contents[i]._id = contents[i]._id.toString()>
				<cfset contents[i]['AUTHOR'] = contents[i]['USER'].fetch()['USERNAME']>
				<cfif structKeyExists(contents[i],"INTRO")>
					<cfset contents[i].INTRO = i18n(contents[i].INTRO,arguments.language)>
				</cfif>
				<cfset contents[i].TITLE = i18n(contents[i].TITLE,arguments.language)>
				<cfif isNull(contents[i].CATEGORY.fetch())>
					<cfset contents[i].CATEGORYTITLE = "">
				<cfelse>
					<cfset contents[i].CATEGORYTITLE = contents[i].CATEGORY.fetch()['TITLE']>
				</cfif>
			</cfif>
		</cfloop>
		<cfreturn contents>
	</cffunction>

	<cffunction name="pageitem" access="remote" returntype="struct" returnformat="json">
		<cfargument name="key" type="string" required="false" default="" />
		<cfargument name="slug" type="string" required="true" default="" />
		<cfargument name="language" type="string" required="true" default="thai" />
		
		<cfinclude template="config.cfm"/>
		<cfif environment eq "product">
	        <cfset var querystring = structNew()/>
	        <cfset querystring['TYPECONTENT'] = 2>
	        <cfset querystring['PUBSTATUS'] = 1>
			<cfif (structKeyExists(arguments,"key")) and (arguments.key neq "") and (arguments.key neq "undefined")>
				<cfset querystring['_id'] = MongoObjectId(arguments.key)>
			<cfelse>
	            <cfset querystring['SLUG'] = arguments.slug>
			</cfif>
	        <cfset item = MongoCollectionfindone(
	            datasource=databasename,
	            collection="post",
	            query=querystring)>
	        <cfif IsNull(item) eq "NO">
				<cfset item._id = item._id.toString()>
				<cfset item.TITLE = removehtmltags(i18n(item.TITLE,arguments.language)) />
				<cfset item['AUTHOR'] = item['USER'].fetch()['USERNAME']>
				<cfif structKeyExists(item,"INTRO")>
					<cfset item.INTRO = removehtmltags(i18n(item.INTRO,arguments.language)) />
				</cfif>
				<cfset item.CREATEDATMONTHDATE = DATEFORMAT(item.CREATEDAT,"mmm dd")>
				<cfset item.CREATEDATYEAR = YEAR(item.CREATEDAT)>
				<cfset item.DETAIL = i18n(item.DETAIL,arguments.language) />
<!---				<cfif isNull(item['CATEGORY'].fetch())>--->
<!---					<cfset item.CATEGORYTITLE = "">--->
<!---				<cfelse>--->
<!---					<cfset item.CATEGORYTITLE = item.CATEGORY.fetch()['TITLE']>--->
<!---		            <cfset newid = item.CATEGORY.fetch()['_id'].toString()>--->
<!---		            <cfset item['CATEGORYID'] = newid>--->
<!---				</cfif>--->
				<cfset item['RELATEITEM'] = arraynew()>
		        <cfset structdelete(item,'CATEGORY')>
		        <cfset structdelete(item,'USER')>
		    <cfelse>
		    	<cfset item = structnew()>
		    	<cfset item["_id"] = "">
	    	</cfif>
    	<cfelse>
			<cfset var mockdata = getmockdata(1,8)>
    		<cfset item = mockdata['detail']>
		</cfif>
		<cfreturn item>
	</cffunction>

</cfcomponent>