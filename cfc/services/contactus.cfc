<cfcomponent>

	<cffunction name="dbref" access="public" returntype="struct">
		<cfargument name="collection" type="string" required="true">
		<cfargument name="objectid" type="string" required="true">
		<cfset var dbreturn = structNew()>
		<cfset dbreturn['$ref'] = lcase(arguments['collection'])>
		<cfset dbreturn['$id'] =  MongoObjectid(arguments['objectid'])>
		<cfreturn dbreturn>
	</cffunction>

	<cffunction name="savecontactus" access="remote" returntype="struct" returnformat="JSON">
		<cfargument name="title" required="true" type="string">
		<cfargument name="email" required="true" type="string">
		<cfargument name="detail" required="true" type="string">
		<cfargument name="filepath" required="false">
		<cfinclude template="config.cfm"/>
			<cfset doc = structNew()>
			<cfset doc.title = arguments.title>
			<cfset doc.email = arguments.email>
			<cfset doc.detail = arguments.detail>
			<cfset doc.createdat = now()>
			<cfset doc.updatedat = now()>
			<cfif structKeyExists(arguments,"filepath") and arguments.filepath neq "">
				<cfset doc.filepath = arguments.filepath>
			</cfif>
			<cfset save = MongoCollectionInsert(databasename,"contactus",doc)>
			<cfset output = structNew()>
			<cfset output["status"] = 1>
		<cfreturn output>
	</cffunction>
	
	<cffunction name="uploadFile" access="remote" returntype="struct" returnformat="JSON">
		<cfset pathupload = "#expandPath('.')#/../../assets/upload/contactus">
		<cfif NOT DirectoryExists("#pathupload#")>
			<cfdirectory action="create" mode="777"  directory="#pathupload#">
		</cfif>
 		<cffile action="upload"
 			destination="#pathupload#"
 			nameconflict="overwrite" result="fileResult"/>
		<cfset filename ="#CreateUUID()#">
 		<cfset file="#filename#.#fileResult.clientfileext#">
 		<cffile action="rename" source = "#fileResult.serverfileuri#"
 			destination="#pathupload#/#file#" attributes="normal">
		<cfreturn {
			'file': file,
			'filename': filename,
			'old_file': fileResult.clientfile,
			'old_filename': fileResult.clientfilename,
			'extention': fileResult.clientfileext,
			'contenttype': fileResult.contenttype,
			'filesize': fileResult.filesize
		}>
 	</cffunction>
 	
 	<cffunction name="readconfig" access="remote" returntype="struct" returnformat="JSON">
 		<cfset configfile = "#expandPath('.')#/../config.json">
		<cfset dataFromJSON = deserializeJSON(fileRead( "#configfile#" )) />
 		<cfset mydata = dataFromJSON>
 		<cfreturn mydata>	
 	</cffunction>
 	
 	<cffunction name="verifyrecaptcha" access="remote" returntype="struct" returnformat="JSON">
 		<cfargument name="challenge" type="string" required="true">
 		<cfargument name="response" type="string" required="true">
 		
 		<cfset output=structNew()>
 		
 		<cfset mydata = readconfig()>
 		<cfset arguments["remoteip"] = CGI.remote_addr>
 		<cfset arguments["privatekey"] = mydata.reCaptcha.privateKey>
 		
 		<cfhttp method="POST" url="http://www.google.com/recaptcha/api/verify" result="myresult" charset="utf-8">
			<cfhttpparam name="challenge" value="#arguments.challenge#" type="FormField">
			<cfhttpparam name="response" value="#arguments.response#" type="FormField">
			<cfhttpparam name="remoteip" value="#arguments.remoteip#" type="FormField">
			<cfhttpparam name="privatekey" value="#arguments.privatekey#" type="FormField">
		</cfhttp>
		<cfset var ans = replacenocase(jsstringformat(myresult.filecontent),'\n','/','all')>
		<cfif listfirst(ans,'/') eq "true">
			<cfset output["verify"]=1>
		<cfelse>
			<cfset output["verify"]=0>
		</cfif>
		<cfreturn output>
	</cffunction>

	<cffunction name="sendemail" access="remote" returntype="struct" returnformat="JSON">
		<cfargument name="title" required="true" type="string">
		<cfargument name="email" required="true" type="string">
		<cfargument name="detail" required="true" type="string">
		<cfargument name="filepath" required="false">
		<cfargument name="template" required="false" type="string">
		<cfinclude template="config.cfm"/>
		<cfset mydata = readconfig()>
		<cfset mailobj = createObject('mail.mailservice')>
		<cfset showreturn['status'] = mailobj.sendmail(
			from=arguments.email,
			to=mydata.email.contactaddress,
			subject= arguments.title,
			typemail = 'html',
			server=mydata.email.server,
			username=mydata.email.username,
			password=decrypt(mydata.email.password,mydata.email.username),
			ssl = mydata.email.ssl,
			port = mydata.email.port,
			template = arguments.template,
			email = arguments.email,
			title = arguments.title,
			detail = arguments.detail,
			file = arguments.filepath
				)>
		<cfset output["status"]=1>
		<cfreturn output>
	</cffunction>
 	
</cfcomponent>