<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/3/13 AD
  Time: 10:01 AM
  To change this template use File | Settings | File Templates.
--->
<cfcomponent>
	<cffunction name="newid" access="public" returntype="Any">
		<cfargument name="objid" type="string" required="true">
		<cfset var objreturn = MongoObjectid(arguments.objid)>
		<cfreturn objreturn>
	</cffunction>
	<cffunction name="dbref" access="public" returntype="struct">
		<cfargument name="collection" type="string" required="true">
		<cfargument name="objectid" type="string" required="true">
		<cfset var dbreturn = structNew()>
		<cfset dbreturn['$ref'] = lcase(arguments['collection'])>
		<cfset dbreturn['$id'] =  MongoObjectid(arguments['objectid'])>
		<cfreturn dbreturn>
	</cffunction>

	<cffunction name="insertform" access="remote" returntype="struct" returnformat="JSON">
		<cfargument name="collectionname" required="true" type="string">
		<cfargument name="insertdata" required="true" type="any">
		<cfinclude template="config.cfm"/>
		<cfset doc = deserializeJSON(arguments.insertdata)>
		<cfset doc.createdat = now()>
		<cfset doc.updatedat = now()>
		<cfset collectionname = "form_#arguments.collectionname#">
		<cfset save = MongoCollectionInsert(databasename,collectionname,doc)>
		<cfset output = structNew()>
		<cfset output["status"] = 1>
		<cfreturn output>
	</cffunction>

	<cffunction name="uploadFile" access="remote" returntype="struct" returnformat="JSON">
		<cfset pathupload = "#expandPath('.')#/../../assets/upload/contactus">
		<cfif NOT DirectoryExists("#pathupload#")>
			<cfdirectory action="create" mode="777"  directory="#pathupload#">
		</cfif>
		<cffile action="upload"
				destination="#pathupload#"
				nameconflict="overwrite" result="fileResult"/>
		<cfset filename ="#CreateUUID()#">
		<cfset file="#filename#.#fileResult.clientfileext#">
		<cffile action="rename" source = "#fileResult.serverfileuri#"
				destination="#pathupload#/#file#" attributes="normal">
		<cfreturn {
				'file'= file,
			'filename'= filename,
			'old_file'= fileResult.clientfile,
			'old_filename'= fileResult.clientfilename,
			'extention'= fileResult.clientfileext,
			'contenttype'= fileResult.contenttype,
			'filesize'= fileResult.filesize
			}>
	</cffunction>

	<cffunction name="readconfig" access="remote" returntype="struct" returnformat="JSON">
		<cfset configfile = "#expandPath('.')#/../config.json">
		<cfset dataFromJSON = deserializeJSON(fileRead( "#configfile#" )) />
		<cfset mydata = dataFromJSON>
		<cfreturn mydata>
	</cffunction>

	<cffunction name="sendemailtocustomer" access="remote" returntype="struct" returnformat="JSON">
		<cfargument name="subject" required="true" type="string">
		<cfargument name="email" required="true" type="string">
		<cfargument name="emaildata" required="true" type="any">
		<cfargument name="template" required="false" type="string">
		<cfinclude template="config.cfm"/>
		<cfset mydata = readconfig()>
		<cfset mailobj = createObject('mail.mailservice')>
		<cfset showreturn['status'] = mailobj.sendmailmoredata(
			from=mydata.email.contactaddress,
			to=arguments.email,
			subject= arguments.subject,
			typemail = 'html',
			server=mydata.email.server,
			username=mydata.email.username,
			password=decrypt(mydata.email.password,mydata.email.username),
			ssl = mydata.email.ssl,
			port = mydata.email.port,
			template = arguments.template,
			emaildata = serializeJSON(arguments.emaildata)
				)>
		<cfset output["status"]=1>
		<cfreturn output>
	</cffunction>

	<cffunction name="sendmultiemailtoadmin" access="remote" returntype="struct" returnformat="JSON">
		<cfargument name="subject" required="true" type="string">
		<cfargument name="email" required="true" type="string">
		<cfargument name="emaildata" required="true" type="any">
		<cfargument name="template" required="false" type="string">
		<cfinclude template="config.cfm"/>
		<cfset mydata = readconfig()>
		<cfset mailobj = createObject('mail.mailservice')>
		<cfset showreturn['status'] = mailobj.mailinglistmoredata(
			from=arguments.email,
			to=mydata.email.contactaddress,
			subject= arguments.subject,
			typemail = 'html',
			server=mydata.email.server,
			username=mydata.email.username,
			password=decrypt(mydata.email.password,mydata.email.username),
			ssl = mydata.email.ssl,
			port = mydata.email.port,
			template = arguments.template,
			emaildata = serializeJSON(arguments.emaildata)
				)>
		<cfset output["status"]=1>
		<cfreturn output>
	</cffunction>
</cfcomponent>
