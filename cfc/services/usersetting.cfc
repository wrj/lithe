<cfcomponent>

	<cffunction name="menupath" access="remote" returntype="struct" returnformat="json">
		<cfinclude template="config.cfm" />
		<cfset myusersetting = MongoCollectionfindone(
				datasource=databasename,
				collection="usersetting",
				query=({})
				)>
		<cfset urlpart = "#myusersetting['SITEADDRESS']#/menu">
		<cfset filepath="#expandPath("/#urlpart#/menu")#">
		
		<cfset output = structNew()>
		<cfset output["siteaddress"] = urlpart>
		<cfreturn output>
	</cffunction>
	
	<cffunction name="codepath" access="remote" returntype="struct" returnformat="json">
		<cfinclude template="config.cfm" />
		<cfset myusersetting = MongoCollectionfindone(
				datasource=databasename,
				collection="usersetting",
				query=({})
				)>
		<cfset urlpart = myusersetting['PATHFRONTEND']>
		<cfset filepath="#expandPath("/#urlpart#")#">
		
		<cfset output = structNew()>
		<cfset output["siteaddress"] = filepath>
		<cfreturn output>
	</cffunction>
	
</cfcomponent>