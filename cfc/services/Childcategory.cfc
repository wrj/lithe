<cfcomponent>
    <cffunction name="$countChild" returntype="string" access="public" >
        <cfargument name="structdata" type="any">
        <cfargument name="count" type="numeric" default=0 required="false">
        <cfset returnCount=int(arguments['count'])+1>
        <cfif structKeyExists(arguments['structdata'],'child')>
            <cfset returnCount = $countChild(arguments['structdata']['child'],returnCount)>
        </cfif>
        <cfreturn returnCount>
    </cffunction>

    <cffunction name="$selectChildCetegory" returntype="array" access="public" >
        <cfargument name="structdata" type="struct">
        <cfargument name="cetegorytitle" type="string">
        <cfargument name="arraydata" type="array" required="false">
        <cfset savearraydata=arrayNew()>
        <cfif isDefined("arguments['arraydata']")>
            <cfset savearraydata=arguments['arraydata']>
        </cfif>
        <cfif arguments['structdata']['TITLE'] IS arguments['cetegorytitle'] OR arguments['cetegorytitle'] IS "true">
            <cfset arrayAppend(savearraydata,MongoObjectid(arguments['structdata']['_id']))>
            <cfif structKeyExists(arguments['structdata'],'child')>
                <cfset cetegoryStatus = "true">
                <cfset returnCount = $selectChildCetegory(arguments['structdata']['child'],cetegoryStatus,savearraydata)>
            </cfif>
            <cfelse>
            <cfset returnCount = $selectChildCetegory(arguments['structdata']['child'],arguments['cetegorytitle'],savearraydata)>
        </cfif>
        <cfreturn savearraydata>
    </cffunction>
    
    <cffunction name="getChildCategory" returnType="array" access="public">
    	<cfargument name="arraydata" type="array">
    	<cfargument name="categoryid" type="string">
    	<cfset var arrayreturn = arrayNew()>
    	<cfloop array="#arguments['arraydata']#" index="item">
    		<cfif item['id'] eq arguments['categoryid']>
	    		<cfset arrayAppend(arrayreturn,dbref('category',item['id']))>
				<!--- get node --->
				<cfset nodequery = getNodeCategory(arguments['arraydata'],arguments['categoryid'])>
				<cfloop array=#nodequery# index="nodeitem">
					<cfset arrayAppend(arrayreturn,nodeitem)>	
				</cfloop>
				<!--- end node --->
	    		<cfbreak>
    		</cfif>
    	</cfloop>
    	<cfreturn arrayreturn>
    </cffunction>
    
    <cffunction name="getNodeCategory" returnType="array" access="public">
    	<cfargument name="arraydata" type="array">
    	<cfargument name="parentid" type="string">
    	<cfset var arrayreturn = arrayNew()>
    	<cfloop array="#arguments['arraydata']#" index="item">
    		<cfif item['parent'] eq arguments.parentid>
    			<cfset arrayAppend(arrayreturn,dbref('category',item['id']))>
    			<!--- get node --->
    				<cfset var nodequery = getNodeCategory(arguments['arraydata'],item['id'])>
    				<cfloop array=#nodequery# index="nodeitem">
    					<cfset arrayAppend(arrayreturn,nodeitem)>	
    				</cfloop>
    			<!--- end node --->
    		</cfif>
    	</cfloop>
    	<cfreturn arrayreturn>
    </cffunction>    

    <cffunction name="getrandom" access="public" returntype="string" output="true">
        <cfargument name="totalrecord" type="numeric" required="true">
        <cfargument name="limit" type="numeric" required="true">
        <cfset var randomlist = "">
        <cfset var stopnum = 50>
        <cfif arguments['totalrecord'] gt stopnum>
            <cfset stopnum = (arguments['totalrecord'] * 5)>
        </cfif>
        <cfif arguments['limit'] gt arguments['totalrecord']>
            <cfset arguments['limit'] = arguments['totalrecord']>
        </cfif>
        <cfloop from="1" to="#stopnum#" index="i">
            <cfset var numrandom = RandRange(1,arguments['totalrecord'])>       
            <cfif listFindNoCase(randomlist, numrandom, ',') eq 0>
                <!--- not found number in list --->
                <cfset randomlist = listAppend(randomlist, numrandom, ',')>
                <cfif listlen(randomlist) eq arguments['limit']>
                    <cfbreak/>
                </cfif>
            </cfif>
        </cfloop>
    <cfreturn randomlist>
</cffunction>
</cfcomponent>
