<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 8/28/13 AD
  Time: 11:03 AM
  To change this template use File | Settings | File Templates.
--->
<cfcomponent extends="Utility">


	<cffunction name="gallerieslist" access="remote" returntype="struct" returnformat="json">
		<cfargument name="orderby" type="string" required="false" default="CREATEDAT" />
		<cfargument name="sort" type="string" required="false" default="desc" />
		<cfargument name="thispage" type="numeric" required="false" default=1 />
		<cfargument name="currentpage" type="numeric" required="false" default=1 />
		<cfargument name="limit" type="string" required="false" default="0" />
		<cfargument name="tags" type="string" required="false" />
		<cfargument name="language" type="string" required="true" default="thai" />
		<cfargument name="skip" type="numeric" required="false"/>
		<cfinclude template="config.cfm"/>
		<cfif environment eq "product">
			<cfset var skipnumber = 0>
			<cfset var skippage = 0>
			<cfif structKeyExists(arguments,"skip")>
				<cfset skipnumber = arguments['skip']>
			</cfif>
			<cfif structKeyExists(arguments, "thispage") and arguments.thispage neq 1>
				<cfset skippage = (arguments.thispage*arguments.limit)-arguments.limit>
			</cfif>
			<cfset var skippost = skipnumber + skippage>
			<cfset sortby = 1>
			<cfif arguments.sort eq "desc">
				<cfset sortby = -1>
			</cfif>
			<cfset sortfield = StructNew()>
			<cfset sortfield[arguments.orderby] = sortby>
			<cfset contents = MongoCollectionfind(
				datasource=databasename,
				collection="gallery",
				query=({}),
				skip=skippost,
				size=val(arguments.limit),
				sort=(sortfield))>
			<cfset var datacontent = ArrayNew()>
			<cfloop array="#contents#" index="content">
				<cfset content['_id'] = content['_id'].toString()>
				<cfset content['TITLE'] = i18n(content['TITLE'],arguments.language)>
				<cfset content['DETAIL'] = i18n(content['DETAIL'],arguments.language) />
				<cfset content['AUTHOR'] = content['USER'].fetch()['USERNAME']>
				<cfif structKeyExists(content,'INTRO')>
					<cfif IsStruct(content['INTRO'])>
						<cfset content['INTRO'] = i18n(content['INTRO'],arguments.language)>
					</cfif>
					<cfelse>
					<cfset content['INTRO'] = ''>
				</cfif>
				<cfset arrayAppend(datacontent,content)>
			</cfloop>
			<cfset contentsdata = structNew()>
			<cfset contentsdata["contents"]=datacontent>
			<cfset contentsdata["totalrecord"] = MongoCollectioncount(
				datasource=databasename,
				collection="gallery",
				query={}) - arguments['skip']>
		<cfelse>
			<cfset var mockdata = getmockdata(val(arguments['thispage']),val(arguments['limit']))>
			<cfset contentsdata['contents'] = mockdata['contents']>
			<cfset contentsdata['totalrecord'] = mockdata['totalrecord']>
		</cfif>
		<cfreturn contentsdata>
	</cffunction>



	<cffunction name="gallerydetail" access="remote" returntype="struct" returnformat="json">
		<cfargument name="key" type="string" required="false" default="" />
		<cfargument name="slug" type="string" required="true" default="" />
		<cfargument name="language" type="string" required="true" default="thai" />
		<cfinclude template="config.cfm"/>
		<cfif environment eq "product">
			<cfset var querystring = structNew()/>
			<cfif (structKeyExists(arguments,"key")) and (arguments.key neq "") and (arguments.key neq "undefined")>
				<cfset querystring['_id'] = MongoObjectId(arguments.key)>
				<cfelse>
				<cfset querystring['SLUG'] = arguments.slug>
			</cfif>
			<cfset item = MongoCollectionfindone(
				datasource=databasename,
				collection="gallery",
				query=querystring)>
			<cfif IsNull(item) eq "NO">
				<cfset item._id = item._id.toString()>
				<cfset item.TITLE = removehtmltags(i18n(item.TITLE,arguments.language)) />
				<cfset item['AUTHOR'] = item['USER'].fetch()['USERNAME']>
				<cfif structKeyExists(item,"INTRO")>
					<cfset item.INTRO = removehtmltags(i18n(item.INTRO,arguments.language)) />
				</cfif>
				<cfset item.CREATEDATMONTHDATE = DATEFORMAT(item.CREATEDAT,"mmm dd")>
				<cfset item.CREATEDATYEAR = YEAR(item.CREATEDAT)>
				<cfset item.DETAIL = i18n(item.DETAIL,arguments.language) />
				<cfelse>
				<cfset item = structnew()>
				<cfset item["_id"] = "">
			</cfif>
		<cfelse>
			<cfset var mockdata = getmockdata(1,8)>
    		<cfset item = mockdata['detail']>
		</cfif>
		<cfreturn item>
	</cffunction>



</cfcomponent>
