<cfcomponent>
    <cffunction name="getmockdata" access="public" returntype="struct">
    	<cfargument name="thispage" type="any" required="false" default=1 />
		<cfargument name="limit" type="any" required="false" default=8 />
		<cfif val(arguments['thispage']) lt 1>
			<cfset arguments['thispage'] = 1>
		</cfif>
		<cfif val(arguments['limit']) lt 1>
			<cfset arguments['limit'] = 8>
		</cfif>
		<cfset var startnum = (val(arguments['thispage']) - 1) * val(arguments['limit'])>
		<cfset var finalnum = val(arguments['thispage']) * val(arguments['limit'])>
		<cfif startnum lt 1>
			<cfset startnum = 1>
		</cfif>
		<cfif finalnum gt 16>
			<cfset finalnum = 16>
		</cfif>
    	<cfset var mockdata = structNew()>
    	<cfset mockdata["totalrecord"] = 16>
    	<cfinclude template="mockdata.cfm"/>
    	<cfset contentarray = arrayNew()>
    	<cfloop from="#startnum#" to="#finalnum#" index="i">
    		<cfset content = structNew()>
    		<cfset content['AUTHOR'] = "Mock Author">
    		<cfset content['CATEGORYTITLE'] = "Mock Category">
    		<cfset content['CREATEDAT'] = DateFormat(now(),'dd/mm/yyyy')>
    		<cfset content['DETAIL'] = lipsumtext[i]>
    		<cfset content['INTRO'] = lipsumintro[i]>
    		<cfset content['LTHUMB'] = setimage[i]>
    		<cfset content['PRICE'] = RandRange(100,600)>
    		<cfset content['PRIVATE'] = false>
    		<cfset content['PUBDATE'] = DateFormat(now(),'dd/mm/yyyy')>
    		<cfset content['PUBSTATUS'] = 1>
    		<cfset content['SKU'] = "SKUMOCK#i#">
    		<cfset content['SLUG'] = replaceNoCase("#lipsumtitle[i]#-#i#", " ", "-","all")>
    		<cfset content['STHUMB'] = setimage[i]>
    		<cfset content['TAG'] = ['tag1','tag2','tag3']>
    		<cfset content['TITLE'] = lipsumtitle[i]>
    		<cfset content['TYPECONTENT'] = 1>
    		<cfset content['UPDATEDAT'] = DateFormat(now(),'dd/mm/yyyy')>
    		<cfset content['_id'] = "mockid#i#">
    		<cfset garray = arrayNew()>
    		<cfloop from="1" to="6" index="i">
    			<cfset gstruct = structNew()>
    			<cfset imagefile = listLast(setimage[i], '/')>
    			<cfset imagename = listFirst(imagefile,'.')>
    			<cfset gstruct['title'] = imagename>
    			<cfset gstruct['detail'] = imagename>
    			<cfset gstruct['link'] = ''>
    			<cfset gstruct['image'] = setimage[i]>
    			<cfset arrayAppend(garray, gstruct)>
    		</cfloop>
    		<cfset content['GALLERY'] = garray>
    		<cfset arrayAppend(contentarray, content)>
    	</cfloop>
    	<!--- RELATEITEM --->
		<cfset contentrelatedarray = arrayNew()>
		<cfloop from="1" to="3" index="i">
			<cfset content = structNew()>
			<cfset content['AUTHOR'] = "Mock Author">
			<cfset content['CATEGORYTITLE'] = "Mock Category">
			<cfset content['CREATEDAT'] = DateFormat(now(),'dd/mm/yyyy')>
			<cfset content['DETAIL'] = lipsumtext[i]>
			<cfset content['INTRO'] = lipsumintro[i]>
			<cfset content['LTHUMB'] = setimage[i]>
			<cfset content['PRICE'] = RandRange(100,600)>
			<cfset content['PRIVATE'] = false>
			<cfset content['PUBDATE'] = DateFormat(now(),'dd/mm/yyyy')>
			<cfset content['PUBSTATUS'] = 1>
			<cfset content['SKU'] = "SKUMOCK#i#">
			<cfset content['SLUG'] = replaceNoCase("#lipsumtitle[i]#-#i#", " ", "-","all")>
			<cfset content['STHUMB'] = setimage[i]>
			<cfset content['TAG'] = ['tag1','tag2','tag3']>
			<cfset content['TITLE'] = lipsumtitle[i]>
			<cfset content['TYPECONTENT'] = 1>
			<cfset content['UPDATEDAT'] = DateFormat(now(),'dd/mm/yyyy')>
			<cfset content['_id'] = "mockid#i#">
			<cfset garray = arrayNew()>
			<cfloop from="1" to="6" index="i">
				<cfset gstruct = structNew()>
				<cfset imagefile = listLast(setimage[i], '/')>
				<cfset imagename = listFirst(imagefile,'.')>
				<cfset gstruct['title'] = imagename>
				<cfset gstruct['detail'] = imagename>
				<cfset gstruct['link'] = ''>
				<cfset gstruct['image'] = setimage[i]>
				<cfset arrayAppend(garray, gstruct)>
			</cfloop>
			<cfset content['GALLERY'] = garray>
			<cfset arrayAppend(contentrelatedarray, content)>
		</cfloop>
    	<!--- End relateitem --->
    	<!--- Slide --->
			<cfset var slidedata = structNew()>
			<cfset slidedata['AUTHOR'] = "Mock Slide Author">
			<cfset slidedata['CREATEDAT'] = DateFormat(now(),'dd/mm/yyyy')>
			<cfset slidedata['DETAIL'] = "Slide detail">
			<cfset slidedata['INTRO'] = "Slide intro">
			<cfset slidedata['LTHUMB'] = "">
			<cfset slidedata['SLUG'] = "Slide">
			<cfset slidedata['STHUMB'] = "">
			<cfset slidedata['TAG'] = ['tag1','tag2','tag3']>
			<cfset slidedata['TITLE'] = "Slide Title">
			<cfset slidedata['UPDATEDAT'] = DateFormat(now(),'dd/mm/yyyy')>
			<cfset slidedata['_id'] = "slideid#i#">
			<cfset garray = arrayNew()>
			<cfloop array="#slideimage#" index="item">
				<cfset gstruct = structNew()>
				<cfset imagefile = listLast(item, '/')>
				<cfset imagename = listFirst(imagefile,'.')>
				<cfset gstruct['title'] = imagename>
				<cfset gstruct['detail'] = imagename>
				<cfset gstruct['link'] = ''>
				<cfset gstruct['image'] = item>
				<cfset arrayAppend(garray, gstruct)>
			</cfloop>
			<cfset slidedata['GALLERY'] = garray>
    	<!--- End Slide --->
    	<cfset mockdata['contents'] = contentarray>
    	<cfset var rawdetail = contentarray[1]>
    	<cfset rawdetail.CATEGORYID = "CID1">
    	<cfset rawdetail.CREATEDATMONTHDATE = DATEFORMAT(rawdetail.CREATEDAT,"mmm dd")>
		<cfset rawdetail.CREATEDATYEAR = YEAR(rawdetail.CREATEDAT)>
		<cfset rawdetail.RELATEITEM = contentrelatedarray>
    	<cfset mockdata['detail'] = rawdetail>
    	<cfset mockdata['slide'] = slidedata>
		<!---	 CATEGORY   --->
	    <cfset subcategorycontent=arrayNew()>
	    <cfset j=0>
	    <cfloop from="#startnum#" to="#finalnum#" index="i">
		    <cfset content = structNew()>
		    <cfset content['ID']=i>
		    <cfset content['TITLE']=lipsumtitle[i]>
		    <cfset content['SLUG']=replaceNoCase("#lipsumtitle[i]#-#i#", " ", "-","all")>
		    <cfset content['DETAIL']=lipsumtext[i]>
		    <cfset content['CREATEDAT']=DateFormat(now(),'dd/mm/yyyy')>
		    <cfset content['UPDATEDAT']=DateFormat(now(),'dd/mm/yyyy')>
		    <cfset content['IMAGEREF'] = setimage[i]>
		    <cfset content['PARENT'] = "">
		    <cfset content['TEMPLATE'] = "post.cfm">
		    <cfset content['URL'] = "postdetail.cfm">
		    <cfset arrayAppend(subcategorycontent,content)>
		    <cfset j++>
	    </cfloop>
	    <cfset categorylist=arrayNew()>
	    <cfloop from="#startnum#" to="#finalnum#" index="i">
		    <cfset content = structNew()>
		    <cfset content['ID']=i>
		    <cfset content['TITLE']=lipsumtitle[i]>
		    <cfset content['SLUG']=replaceNoCase("#lipsumtitle[i]#-#i#", " ", "-","all")>
		    <cfset content['LINK']='<a href="category.cfm=#content['SLUG']#">#lipsumtitle[i]#</a>'>
		    <cfset content['IMAGEREF'] = setimage[i]>
		    <cfset arrayAppend(categorylist,content)>
	    </cfloop>
	    <cfset subcategory=structNew()>
	    <cfset subcategory['contents']=subcategorycontent>
	    <cfset subcategory['detail']=subcategorycontent[RandRange(1,j)]>
	    <cfset subcategory['totalrecord']=16>
	    <cfset mockdata['subcategory'] = subcategory>
		<cfset mockdata['category']=categorylist>
	    <!---	 END CATEGORY   --->

		<!--- TAG CLOUNDS --->
	    <cfset tagclounddata=lipsumtext[RandRange(1,16)]>
	    <cfset tagclounddata=listToArray(tagclounddata," ",true)>
	    <cfset mockdata['tagcloud']=tagclounddata>
		<!--- END TAG CLOUNDS --->
    	<cfreturn mockdata>
	</cffunction>
</cfcomponent>