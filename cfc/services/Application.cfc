<!---
  Created with IntelliJ IDEA.
  User: kwangkungzaa
  Date: 1/22/13 AD
  Time: 4:32 PM
  To change this template use File | Settings | File Templates.
--->
<cfcomponent>
	<cfset This.name = "SERVICE">
	<cfset This.applicationtimeout="#createtimespan(5,0,0,0)#">
	
	<cffunction name="onApplicationStart" returntype="boolean" output="false">
		<cfinclude template="config.cfm"/>
		<cfif MongoIsvalid(databasename) eq 'NO'>
			<cfif databaseusername neq '' AND databasepassword neq ''>
				<cfif databaseport neq ''>
					<cfset MongoRegister( name=databasename, server=databasehost,db=databasename,port=databaseport,username=databaseusername,password=databasepassword)>
				<cfelse>
					<cfset MongoRegister( name=databasename, server=databasehost,db=databasename,username=databaseusername,password=databasepassword)>
				</cfif>
			<cfelse>
				<cfif databaseport neq ''>
					<cfset MongoRegister( name=databasename, server=databasehost, db=databasename,port=databaseport)>
				<cfelse>
					<cfset MongoRegister( name=databasename, server=databasehost, db=databasename)>
				</cfif>
			</cfif>
		</cfif>
		<cfreturn true>
	</cffunction>

</cfcomponent>