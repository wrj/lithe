<cfcomponent>

	<cffunction name="savemember" access="remote" returntype="struct" returnformat="json">
		<cfargument name="fbid" required="true" type="string">
		<cfargument name="name" required="true" type="string">
		<cfargument name="image" required="true" type="string">
		<cfargument name="email" required="true" type="string">
		<cfargument name="shippingaddress" required="false">
		<cfargument name="billingaddress" required="false">

		<cfinclude template="config.cfm"/>
		
		<cfset doc = structNew()>
		<cfset doc.fbid = arguments.fbid>
		<cfset doc.name = arguments.name>
		<cfset doc.email = arguments.email>
		<cfset doc.image = arguments.image>
		<cfif structKeyExists(arguments,"shipping_address")>
			<cfset doc.shippingaddress = deserializejson(arguments.shippingaddress)>
		</cfif>
		<cfif structKeyExists(arguments,"billing_address")>
			<cfset doc.billingaddress = deserializejson(arguments.billingaddress)>
		</cfif>
		<cfset doc.createdat = now()>
		<cfset doc.updatedat = now()>
		<cfset save = MongoCollectionInsert(databasename,"member",doc)>
		
		<cfset output = structNew()>
		<cfset output["status"] = "success">
		
		<cfreturn output>
	</cffunction>
	
</cfcomponent>