<cfcomponent>

	<cffunction name="token" access="remote" returntype="struct" returnformat="json">
		
		<cfinclude template="config.cfm" />

		
		
		<cfset callfunction = createObject("component","cfc.services.usersetting")>
		<cfset filepath = callfunction.codepath()>

		<cfif NOT DirectoryExists("#filepath.siteaddress#")>
			<cfdirectory action="create" mode="777"  directory="#filepath#">
		</cfif>
		<cfset pathfile = '#filepath.siteaddress#/token.json'>
		<cfif not fileExists(pathfile)>
			<cfset mytoken = LSParseNumber(mid(REReplace(hash(domainname),"[A-Z]","","ALL"),4,4))>
			<cffile action="write" mode="777" file="#pathfile#" output="#mytoken#" charset="utf-8">
		<cfelse>
			<cffile action="read" file="#pathfile#" variable="mytoken" charset="utf-8">
		</cfif>
		<cfset token = trim(mytoken)>
		<cfset output = structNew()>
		<cfset output["token"] = token>
		<cfreturn output>

	</cffunction>
	
</cfcomponent>