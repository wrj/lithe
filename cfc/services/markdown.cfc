<cfcomponent>

	<cffunction name="markdowntext" access="public" returnType="string">
		<cfargument name="data" type="string" required="true">
		<!--- <cfset var markdowntext = arguments['data']>
		<cfscript language="java" jarlist="markdownj.jar" import="com.petebevin.markdown.MarkdownProcessor">
		    MarkdownProcessor m = new MarkdownProcessor();
		    String html = m.markdown((String)cf.get("markdowntext"));
		    cf.set( "html", html );
		</cfscript> --->
		<cfreturn arguments['data']>
	</cffunction>
	
</cfcomponent>