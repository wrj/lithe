<cfcomponent>
	<cffunction name="getrequest" access="remote" returnType="struct" returnformat="json">
		<cfargument name="id" required="true">
		<cfinclude template="connectmysqldb.cfm"/>
		<cfquery name="results" datasource="#attributes.DSN#">
			SELECT * FROM request WHERE id=<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.id#">;
		</cfquery>
		<cfif QueryIsempty(results)>
			<cfset returndata = {}>
		<cfelse>
			<cfset returndata = QueryRowstruct(results,1)>
		</cfif>
		<cfreturn returndata>
	</cffunction>
	
	<cffunction name="confirmrequest" access="remote" returnType="struct" returnformat="json">
		<cfargument name="id" required="true">
		<cfinclude template="config.cfm"/>	
		<cfinclude template="connectmysqldb.cfm"/>	
		<cfquery name="update" datasource="#attributes.DSN#">
			UPDATE request SET status='request' WHERE id=<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.id#">;
		</cfquery>
		<cfquery name="results" datasource="#attributes.DSN#">
			SELECT * FROM request WHERE id=<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.id#">;
		</cfquery>
		<cfset resultsStruct = QueryRowstruct(results,1)>
		<cfmail to="#resultsStruct.email#"
    	    from="#mail.from#"
	        subject="ใบคำขอบริการทดสอบ"
	        server="#mail.server#" useSSL="#mail.useSSL#" port="#mail.port#"
	        username="#mail.username#" password="#mail.password#">
			<cfoutput>
				#domainname#/w/requestformpreview.html?id=#arguments.id#
			</cfoutput>
		</cfmail>
		<cfset returndata = {
			"success": true,
			"resultsStruct":resultsStruct.email
		}>
		<cfreturn returndata>
	</cffunction>
	
	<cffunction name="cancelrequest" access="remote" returnType="struct" returnformat="json">
		<cfargument name="id" required="true">
		<cfinclude template="connectmysqldb.cfm"/>	
		<cfquery name="update" datasource="#attributes.DSN#">
			DELETE FROM request WHERE id=<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.id#">;
		</cfquery>
		<cfset returndata = {
			"success": true
		}>
		<cfreturn returndata>
	</cffunction>

	<cffunction name="saverequest" access="remote" returntype="struct" returnformat="json">
		<cfargument name="requestno" type="string" required="false" default="-1">
		<cfargument name="requestdate" type="string" required="true">
		<cfargument name="requestname" type="string" required="true">
		<cfargument name="requestorg" type="string" required="false">
		<cfargument name="orgtype" type="string" required="false">
		<cfargument name="addressno" type="string" required="true">
		<cfargument name="addressmoo" type="string" required="false">
		<cfargument name="addresssoi" type="string" required="false">
		<cfargument name="addressroad" type="string" required="false">
		<cfargument name="addresssection" type="string" required="true">
		<cfargument name="addressdistrict" type="string" required="true">
		<cfargument name="addressprovince" type="string" required="true">
		<cfargument name="addresszipcode" type="string" required="true">
		<cfargument name="telephone" type="string" required="true">
		<cfargument name="fax" type="string" required="false">
		<cfargument name="email" type="string" required="true">
		<cfargument name="requestfor" type="string" required="true">
		<cfargument name="membertype" type="string" required="false">
		<cfargument name="memberno" type="string" required="false">
		<cfargument name="examplemethod" type="string" required="true">
		<cfargument name="examplemethod2" type="string" required="false">
		<cfargument name="reportlanguage" type="string" required="true">
		<cfargument name="reportreceive" type="string" required="true">
		<cfargument name="reporthowfast" type="string" required="true">
<!--- 		<cfargument name="datadessampletest" type="string" required="true"> --->
		<cfargument name="destestno1" type="string" required="true">
		<cfargument name="destestsamplecode1" type="string" required="true">
		<cfargument name="destestdescription1" type="string" required="true">
		<cfargument name="destestamount1" type="string" required="true">
		<cfargument name="destestunit1" type="string" required="true">
		<cfargument name="destesttestcode1" type="string" required="false">
		<cfargument name="destestno2" type="string" required="false">
		<cfargument name="destestsamplecode2" type="string" required="false">
		<cfargument name="destestdescription2" type="string" required="false">
		<cfargument name="destestamount2" type="string" required="false">
		<cfargument name="destestunit2" type="string" required="false">
		<cfargument name="destesttestcode2" type="string" required="false">
		<cfargument name="destestno3" type="string" required="false">
		<cfargument name="destestsamplecode3" type="string" required="false">
		<cfargument name="destestdescription3" type="string" required="false">
		<cfargument name="destestamount3" type="string" required="false">
		<cfargument name="destestunit3" type="string" required="false">
		<cfargument name="destesttestcode3" type="string" required="false">
		<cfargument name="status" type="string" required="false" default="preview">
		
		

		<cfinclude template="connectmysqldb.cfm"/>

		<cfif not structKeyExists(arguments,"requestorg")>
			<cfset arguments.requestorg = "">
		</cfif>
		<cfif not structKeyExists(arguments,"addresssoi")>
			<cfset arguments.addresssoi = "">
		</cfif>
		<cfif structKeyExists(arguments,"fax")>
			<cfset arguments.fax = "">
		</cfif>
		<cfif structKeyExists(arguments,"memberno")>
			<cfset arguments.memberno = "">
		</cfif>
		<cfif structKeyExists(arguments,"examplemethod2")>
			<cfset arguments.examplemethod2 = "">
		</cfif>

		<cfdbinfo dbname="#attributes.DSN#" type="tables" datasource="#attributes.DSN#" name="dbdata">

		<cfquery name="tableExists"  dbtype="query">
			SELECT * FROM dbdata WHERE TABLE_TYPE='TABLE';
		</cfquery>

		<cfset tables=valueList(tableExists.TABLE_NAME)>
		<cfif not ListFindNoCase(tables,'request')>
			<cfquery name="createtblrequest" datasource="#attributes.DSN#">
				CREATE TABLE IF NOT EXISTS request(
					id int(11) NOT NULL AUTO_INCREMENT,
					secretkey varchar(255) DEFAULT NULL,
					requestno varchar(255) NOT NULL,
					requestdate date NOT NULL,
					requestname varchar(255) NOT NULL,
					requestorg varchar(255) DEFAULT NULL,
					orgtype varchar(255) DEFAULT NULL,
					addressno varchar(255) NOT NULL,
					addressmoo varchar(128) DEFAULT NULL,
					addresssoi varchar(255) DEFAULT NULL,
					addressroad varchar(255) DEFAULT NULL,
					addresssection varchar(255) NOT NULL,
					addressdistrict varchar(255) NOT NULL,
					addressprovince varchar(255) NOT NULL,
					addresszipcode varchar(128) NOT NULL,
					telephone varchar(255) NOT NULL,
					fax varchar(255) DEFAULT NULL,
					email varchar(255) NOT NULL,
					requestfor text NOT NULL,
					membertype varchar(255) DEFAULT NULL,
					memberno varchar(255) DEFAULT NULL,
					examplemethod varchar(255) NOT NULL,
					examplemethod2 varchar(255) DEFAULT NULL,
					reportlanguage varchar(128) NOT NULL,
					reportreceive varchar(255) NOT NULL,
					reporthowfast varchar(255) NOT NULL,
					createdat datetime NULL DEFAULT NULL,
					updatedat datetime NULL DEFAULT NULL,
					destestno1 varchar(128) NOT NULL,
					destestsamplecode1 varchar(255) NOT NULL,
					destestdescription1 varchar(255) NOT NULL,
					destestamount1 varchar(255) NOT NULL,
					destestunit1 varchar(255) NOT NULL,
					destesttestcode1 varchar(255) DEFAULT NULL,
					destestno2 varchar(128) DEFAULT NULL,
					destestsamplecode2 varchar(255) DEFAULT NULL,
					destestdescription2 varchar(255) DEFAULT NULL,
					destestamount2 varchar(255) DEFAULT NULL,
					destestunit2 varchar(255) DEFAULT NULL,
					destesttestcode2 varchar(255) DEFAULT NULL,
					destestno3 varchar(128) DEFAULT NULL,
					destestsamplecode3 varchar(255) DEFAULT NULL,
					destestdescription3 varchar(255) DEFAULT NULL,
					destestamount3 varchar(255) DEFAULT NULL,
					destestunit3 varchar(255) DEFAULT NULL,
					destesttestcode3 varchar(255) DEFAULT NULL,
					status varchar(255) NOT NULL,
					
					PRIMARY KEY (id)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;
			</cfquery>
		</cfif>
		<cfquery name="saverequest" datasource="#attributes.DSN#">
			insert into request (
				id,
				secretkey,
				requestno,
				requestdate,
				requestname,
				requestorg,
				orgtype,
				addressno,
				addressmoo,
				addresssoi,
				addressroad,
				addresssection,
				addressdistrict,
				addressprovince,
				addresszipcode,
				telephone,
				fax,
				email,
				requestfor,
				membertype,
				memberno,
				examplemethod,
				examplemethod2,
				reportlanguage,
				reportreceive,
				reporthowfast,
				destestno1,
				destestsamplecode1,
				destestdescription1,
				destestamount1,
				destestunit1,
				destesttestcode1,
				destestno2,
				destestsamplecode2,
				destestdescription2,
				destestamount2,
				destestunit2,
				destesttestcode2,
				destestno3,
				destestsamplecode3,
				destestdescription3,
				destestamount3,
				destestunit3,
				destesttestcode3,
				status,
				createdat,
				updatedat
				
			) values (
				DEFAULT,
				'',
				'#arguments.requestno#',
				'#arguments.requestdate#',
				'#arguments.requestname#',
				'#arguments.requestorg#',
				'#arguments.orgtype#',
				'#arguments.addressno#',
				'#arguments.addressmoo#',
				'#arguments.addresssoi#',
				'#arguments.addressroad#',
				'#arguments.addresssection#',
				'#arguments.addressdistrict#',
				'#arguments.addressprovince#',
				'#arguments.addresszipcode#',
				'#arguments.telephone#',
				'#arguments.fax#',
				'#arguments.email#',
				'#arguments.requestfor#',
				'#arguments.membertype#',
				'#arguments.memberno#',
				'#arguments.examplemethod#',
				'#arguments.examplemethod2#',
				'#arguments.reportlanguage#',
				'#arguments.reportreceive#',
				'#arguments.reporthowfast#',
				'#arguments.destestno1#',
				'#arguments.destestsamplecode1#',
				'#arguments.destestdescription1#',
				'#arguments.destestamount1#',
				'#arguments.destestunit1#',
				'#arguments.destesttestcode1#',
				'#arguments.destestno2#',
				'#arguments.destestsamplecode2#',
				'#arguments.destestdescription2#',
				'#arguments.destestamount2#',
				'#arguments.destestunit2#',
				'#arguments.destesttestcode2#',
				'#arguments.destestno3#',
				'#arguments.destestsamplecode3#',
				'#arguments.destestdescription3#',
				'#arguments.destestamount3#',
				'#arguments.destestunit3#',
				'#arguments.destesttestcode3#',
				'#arguments.status#',
				'#dateformat(now(),"yyyy-mm-dd")# #timeFormat(now(),"HH:mm:ss")#',
				'#dateformat(now(),"yyyy-mm-dd")# #timeFormat(now(),"HH:mm:ss")#'
			);
		</cfquery>
		<cfquery name="qNewContact" datasource="#attributes.DSN#">
			SELECT LAST_INSERT_ID() AS NEW_ID
		</cfquery>
		<cfquery name="update" datasource="#attributes.DSN#">
			UPDATE request SET requestno='#NumberFormat(qNewContact.NEW_ID,"0000000")#' WHERE id=<cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#qNewContact.NEW_ID#">;
		</cfquery>
		<cfset output=structNew()>
		<cfset output["status"]=1>
		<cfset output["id"]=qNewContact.NEW_ID>
		<cfreturn output>
	</cffunction>
</cfcomponent>