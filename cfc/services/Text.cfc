<cfcomponent>
    <cffunction name="removehtmltags" access="public" returntype="string">
        <cfargument name="str" required="YES" type="string">
        <cfscript>
            str = arguments.str;
            str = reReplaceNoCase(str, "<*style.*?>(.*?)</style>","","all");
            str = reReplaceNoCase(str, "<*script.*?>(.*?)</script>","","all");
            str = reReplaceNoCase(str, "<.*?>","","all");

            //get partial html in front
            str = reReplaceNoCase(str, "^.*?>","");

            //get partial html at end
            str = reReplaceNoCase(str, "<.*$","");

            return trim(str);
        </cfscript>
    </cffunction>

    <cffunction name="i18n" access="public" returntype="String" output="false">
        <cfargument name="textdata" type="any" required="true">
        <cfargument name="getlang" type="string" required="false" default="">
        <cfset var textreturn = ''>
        <cfset var lang = arguments["getlang"]>
        <cfset var systemlang = "">
        <cfset var settingfrontfile = "#expandPath('/')#cfc/config.json">
        <cfif lang eq ''>
            <cfif fileExists(settingfrontfile)>
                <cfset configfront = deserializeJSON(fileRead( "#settingfrontfile#",'utf-8')) />   
                <cfset lang = listFirst(configfront["general"]["language"])>
                <cfset systemlang = configfront["general"]["language"]>
            </cfif>
        </cfif>
        <cfif IsStruct(arguments['textdata'])>
            <cfif structKeyExists(arguments['textdata'],lang) && arguments['textdata'][lang] neq "">
                <cfset textreturn = arguments['textdata'][lang]>
            <cfelse>
                <cfif fileExists(settingfrontfile)>
                    <cfset configfront = deserializeJSON(fileRead( "#settingfrontfile#",'utf-8')) />   
                    <cfset lang = listFirst(configfront["general"]["language"])>
                </cfif>
                <cfif structKeyExists(arguments['textdata'],lang) && arguments['textdata'][lang] neq "">
                    <cfset textreturn = arguments['textdata'][lang]>
                </cfif>
            </cfif>
        <cfelse>
            <cfset textreturn = arguments['textdata']>
        </cfif>
        <cfreturn textreturn>
    </cffunction>
    
    <cffunction name="checkdata" access="public" returntype="any" output="false">
        <cfargument name="rawdata" type="struct" required="true">
        <cfargument name="fieldcheck" type="string" required="true">
        <cfif structKeyExists(arguments['rawdata'], arguments['fieldcheck'])>
            <cfreturn arguments['rawdata'][arguments['fieldcheck']]>
        <cfelse>
            <cfreturn ''/>
        </cfif>
    </cffunction>

	<cffunction name="readconfig" access="remote" returntype="struct" returnformat="JSON">
		<cfset configfile = "#expandPath('/')#cfc/config.json">
		<cfset dataFromJSON = deserializeJSON(fileRead( "#configfile#" )) />
		<cfset mydata = dataFromJSON>
		<cfreturn mydata>
	</cffunction>

	<cffunction name="removeslash" access="public" returntype="String">
		<cfargument name="url" type="string" required="true">
		<cfset var outputdata = arguments.url>
		<cfif right(outputdata,1) EQ "/">
			<cfset outputdata=removeChars(outputdata,len(outputdata),1)>
		</cfif>
		<cfif right(outputdata,1) EQ "/">
			<cfset outputdata=removeslash(outputdata)>
		</cfif>
		<cfreturn outputdata>
	</cffunction>

	<cffunction name="buildlink" access="private" returntype="String">
		<cfargument name="pattern" type="string" required="false" default="">
		<cfargument name="slug" type="string" required="true">
		<cfargument name="template" type="string" required="true">
		<cfargument name="otherparams" type="string" required="false" default="">
		<cfinclude template="/cfc/config.cfm"/>
		<cfset configdata = readconfig()>
		<cfif arguments.pattern is not "">
			<cfset patternurl=arguments.pattern>
		</cfif>
		<cfset var htmloutput = replaceNoCase(patternurl, "{template}", "#arguments['template']#", 'all')>
		<cfset htmloutput = replaceNoCase(htmloutput, "{slug}", "#arguments['slug']#", 'all')>
		<cfset htmloutput = replaceNoCase(htmloutput, "{otherparams}", "#arguments['otherparams']#", 'all')>
		<cfloop list="#rewritepatternurl#" delimiters="," index="reurl">
			<cfset htmloutput = replaceNoCase(htmloutput, reurl, "", 'all')>
		</cfloop>
		<cfset outputdata = "#configdata["general"]["siteaddress"]#/#htmloutput#">
		<cfreturn outputdata>
	</cffunction>


	<cffunction name="buildotherlink" access="private" returntype="String">
		<cfargument name="template" type="string" required="true">
		<cfargument name="key" type="string" required="false" default="">
		<cfargument name="value" type="string" required="false" default="">
		<cfargument name="otherparams" type="string" required="false" default="">
		<cfinclude template="/cfc/config.cfm"/>
		<cfset configdata = readconfig()>
		<cfparam name="otherpatternurl" default="">
		<cfset var htmloutput = replaceNoCase(otherpatternurl, "{template}", "#arguments['template']#", 'all')>
		<cfset htmloutput = replaceNoCase(htmloutput, "{key}", "#arguments['key']#", 'all')>
		<cfset htmloutput = replaceNoCase(htmloutput, "{value}", "#arguments['value']#", 'all')>
		<cfif arguments['key'] IS "" AND arguments['VALUE'] IS "">
			<cfset htmloutput = replaceNoCase(htmloutput, "?", "", 'one')>
			<cfset htmloutput = replaceNoCase(htmloutput, "=", "", 'one')>
		</cfif>
		<cfset htmloutput = replaceNoCase(htmloutput, "{otherparams}", "#arguments['otherparams']#", 'all')>
		<cfloop list="#rewritepatternurl#" delimiters="," index="reurl">
			<cfset htmloutput = replaceNoCase(htmloutput, reurl, "", 'all')>
		</cfloop>
		<cfset outputdata = "#configdata["general"]["siteaddress"]#/#htmloutput#">
		<cfset outputdata=removeslash(outputdata)>
		<cfreturn outputdata>
	</cffunction>

</cfcomponent>
