<cfcomponent>

	<cffunction name="$insertSlugInCategory" returntype="string" access="public" >
		<cfargument name="structdata" type="struct">
		<cfargument name="jsondata" type="string">
		<cfargument name="title" type="string">
		<cfoutput>
			<cfsavecontent variable="inputdata" >
			,"child":{
	            "TITLE":"#structdata.TITLE#",
	            "SLUG":"#structdata.SLUG#",
	            "TEMPLATE":"#structdata.TEMPLATE#",
				"_id":"#structdata._id#"[more]
			}
			</cfsavecontent>
		</cfoutput>
		<cfif structKeyExists(structdata, "child")>
			<cfset inputdata = $insertSlugInCategory(structdata['child'],inputdata,arguments['title'])>
			<cfset jsondata = ReplaceNoCase(jsondata, "[more]", "#inputdata#", "All")>
		<cfelse>
			<cfoutput>
				<cfsavecontent variable="inputslug" >
				,"child":{
					"TITLE":"#structdata.TITLE#",
                    "SLUG":"#structdata.SLUG#",
                    "TEMPLATE":"#structdata.TEMPLATE#",
					"_id":"#structdata._id#"
					,"child":{
						"TITLE":"#arguments['title']#",
						"type":"slug"
					}
				}
				</cfsavecontent>
			</cfoutput>
			<cfset jsondata = ReplaceNoCase(jsondata, "[more]", "#inputslug#", "All")>
		</cfif>
		<cfreturn jsondata>
	</cffunction>
	
	<cffunction name="treequery" returntype="array" access="public" >
        <cfargument name="obj" type="any" required="true">
        <cfargument name="currentid" type="string" default="" required="false">
        <cfargument name="id" type="string" default="_id">
        <cfargument name="name" type="string" default="title">
        <cfargument name="parent" type="string" default="PARENT">
		<cfargument name="urluse" type="string" default="">
    	<cfinclude template="config.cfm"/>
<!---    	<cfset arguments['urluse'] = "#domainname##arguments['urluse']#">--->
        <cfset qnew = QueryNew("_id,title,thumbnail","varchar,varchar,varchar") />
        <cfset nameroot = ''>
        <cfloop array="#arguments.obj#" index="item">
            <cfset depfound = false>
            <cfset str="">
            <cfif item['_id'].toString() eq arguments['currentid']>
                <cfset nameroot = item['title']>
            <cfelse>
                <cfif structKeyExists(item,arguments.parent)>
                    <cfset str = treenodequery(objnode=item[arguments.parent].fetch(),name=arguments.name,parent=arguments.parent,urluse=arguments.urluse)>
                </cfif>
                <cfloop list="#str#" index="depnamelist" delimiters=">">
                    <cfif (Trim(depnamelist) eq nameroot) && (Trim(depnamelist) neq '')>
                        <cfset depfound = true>
                        <cfbreak>
                    </cfif>
                </cfloop>
                <cfif depfound eq false>
                    <cfset temp = QueryAddRow(qnew)>
                    <cfset temp = QuerySetCell(qnew,'_id',#item['_id'].toString()#)>
                    <cfset temp = QuerySetCell(qnew,arguments['name'],#str#&#item['TITLE']#) >
                    <cfset temp = QuerySetCell(qnew,'thumbnail',#checkdata(item,'IMAGEREF')#) >
                </cfif>
            </cfif>
        </cfloop>
        <cfset var resultarr = arraynew()>
        <cfloop query="qnew">
            <cfset cst = structNew()>
            <cfset cst['_id'] = _id>
            <cfset cst['TITLE'] = title>
            <cfset cst['THUMBNAIL'] = thumbnail>
            <cfset arrayAppend(resultarr,cst)>
        </cfloop>
        <cfreturn resultarr>
    </cffunction>

    <cffunction name="treenodequery" returntype="string" access="public" >
        <cfargument name="objnode" type="any">
        <cfargument name="name" type="string">
        <cfargument name="parent" type="string">
		<cfargument name="urluse" type="string">
        <cfset str="">
        <cfif structKeyExists(objnode,'PARENT')>
            <cfset str = treenodequery(objnode=objnode[arguments.parent].fetch(),name=arguments.name,parent=arguments.parent)>
        </cfif>
        <cfoutput>
	        <cfsavecontent variable="urlcate"><cfif arguments.urluse IS "">#objnode["URL"]#?category=<cfelse>#arguments.urluse#</cfif>#objnode['_id'].toString()#</cfsavecontent>
        </cfoutput>
        <cfreturn "#str#<a href='#urlcate#'>#objnode[arguments.name]#</a> > ">
    </cffunction>

    <cffunction name="genRecursiveList" access="public" returntype="string" output="false">
        <cfargument name="Data" type="Query" required="true" />
        <cfargument name="parentID" type="string" default="" />
        <cfargument name="Level" type="numeric" default="0" required="false"/>
        <cfset var RELOCAL = structNew() />
        <cfset var strreturn = "" />
        <cfset var parentstr = ""/>
        <cfset var itemstr = ""/>
        <cfset var treeselect = 'false'/>
        <cfset var visitefirst = true/>
        <cfquery name="RELOCAL.Children" dbtype="query" >
	        select id,name,pid
	        from arguments.Data
	        where pid = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments['parentID']#"/>
            order by id
        </cfquery>
        <cfif RELOCAL.Children.RecordCount>
            <cfloop query="RELOCAL.Children">
                <cfset parentstr = genRecursiveList(arguments.Data,RELOCAL.Children.id,(arguments['Level']+1))>
                <cfif arguments['Level'] eq 0>
                    <cfif visitefirst eq true>
                        <cfset itemstr = '"title": "#RELOCAL.Children.name#", "key": "#RELOCAL.Children.id#","activate":true'>
                        <cfset visitefirst = false>
                        <cfelse>
                        <cfset itemstr = '"title": "#RELOCAL.Children.name#", "key": "#RELOCAL.Children.id#"'>
                    </cfif>
                    <cfelse>
                    <cfset itemstr = '"title": "#RELOCAL.Children.name#", "key": "#RELOCAL.Children.id#"'>
                </cfif>


                <cfif parentstr neq ''>
                    <cfset itemstr = itemstr & ',"isFolder": true,"children": ['&parentstr&']'>
                </cfif>
                <cfif (strreturn neq '')>
                    <cfset strreturn = strreturn&','>
                </cfif>
                <cfset strreturn = strreturn&"{#itemstr#}">
            </cfloop>
        </cfif>
        <cfreturn strreturn />
    </cffunction>

    <cffunction name="treecategoryoutput" access="public" returnType="string">
		<cfinclude template="config.cfm"/>
        <cfset var catetreedata = MongoCollectionfind(databasename,'category',{"USER"=dbref('user',session['userid'])})>
        <cfoutput>
            #stylesheetinclude("ui.dynatree")#
            #javaScriptInclude("jquery.dynatree.min,createtree")#
            <cfsavecontent variable="htmlstr">
                <div id="tree">

                </div>
            </cfsavecontent>
        </cfoutput>
        <cfreturn htmlstr>
    </cffunction>

    <cffunction name="treequeryjson" returntype="array" access="public" >
        <cfargument name="obj" type="any" required="true">
        <cfargument name="currentid" type="string" default="" required="false">
        <cfargument name="id" type="string" default="_id">
        <cfargument name="name" type="string" default="TITLE">
        <cfargument name="parent" type="string" default="PARENT">
        <cfset qnew = QueryNew("_id,title","varchar,varchar") />
        <cfset nameroot = ''>
        <cfloop array="#arguments.obj#" index="item">
            <cfset depfound = false>
            <cfset str="">
            <cfif item['_id'].toString() eq arguments['currentid']>
                <cfset nameroot = item['title']>
                <cfelse>
                <cfif structKeyExists(item,arguments.parent)>
                    <cfset str = treenodequeryjson(objnode=item[arguments.parent].fetch(),name=arguments.name,parent=arguments.parent)>
                </cfif>
                <cfloop list="#str#" index="depnamelist" delimiters=">">
                    <cfif (Trim(depnamelist) eq nameroot) && (Trim(depnamelist) neq '')>
                        <cfset depfound = true>
                        <cfbreak>
                    </cfif>
                </cfloop>
                <cfif depfound eq false>
                    <cfset temp = QueryAddRow(qnew)>
                    <cfset temp = QuerySetCell(qnew,'_id',#item['SLUG']#)>
                    <cfif str neq "">
                        <cfset childtext = ',"child":{"_id":"#item["_id"].toString()#","SLUG":"#item['SLUG']#","TEMPLATE":"#item['TEMPLATE']#","TITLE":"#item[arguments.name]#"}'>
                        <cfset str = replacenocase(str,'rechild',childtext,'all')>
                        <cfelse>
                        <cfset str = '#str#{"_id":"#item['_id'].toString()#","SLUG":"#item['SLUG']#","TEMPLATE":"#item['TEMPLATE']#","TITLE":"#item[arguments.name]#"}'>
                    </cfif>
                    <cfset temp = QuerySetCell(qnew,arguments['name'],str) >
                </cfif>
            </cfif>
        </cfloop>
        <cfset var resultarr = arraynew()>
        <cfloop query="qnew">
            <cfset cst = structNew()>
            <cfset cst['_id'] = _id>
            <cfset cst['TITLE'] = title>
            <cfset arrayAppend(resultarr,cst)>
        </cfloop>
        <cfreturn resultarr>
    </cffunction>

    <cffunction name="treenodequeryjson" returntype="string" access="public" >
        <cfargument name="objnode" type="any">
        <cfargument name="name" type="string">
        <cfargument name="parent" type="string">
        <cfset str="">
        <cfif structKeyExists(objnode,'PARENT')>
            <cfset str = treenodequeryjson(objnode=objnode[arguments.parent].fetch(),name=arguments.name,parent=arguments.parent)>
        </cfif>
        <cfif str eq "">
            <cfset str = '{"_id":"#objnode["_id"].toString()#","SLUG":"#objnode['SLUG']#","TEMPLATE":"#objnode['TEMPLATE']#","TITLE":"#objnode[arguments.name]#"rechild}'>
            <cfelse>
            <cfset childtext = ',"child":{"_id":"#objnode["_id"].toString()#","SLUG":"#objnode['SLUG']#","TEMPLATE":"#objnode['TEMPLATE']#","TITLE":"#objnode[arguments.name]#"rechild}'>
            <cfset str = replacenocase(str,'rechild',childtext,'all')>
        </cfif>
        <cfreturn str>
    </cffunction>
</cfcomponent>
