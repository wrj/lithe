<cfcomponent extends="Utility">

	<cffunction name="countpostslist" access="remote" returntype="numeric" returnformat="json">
		<cfargument name="tags" type="string" required="false" />
		<cfargument name="category" type="string" required="false" />
		<cfargument name="skip" type="string" required="false" default=0 />
		<cfargument name="language" type="string" required="true" default="thai" />
		<cfargument name="subcategory" type="boolean" default=false required="false"/>

		<cfinclude template="config.cfm"/>

		<cfset myquerytagstext = structNew()>
		<cfset myquerytagstext['TYPECONTENT'] = 1>
		<cfset myquerytagstext['PUBSTATUS'] = 1>
		
		<cfset mystrlang = structNew()>
		<cfset mystrlang["$ne"] = ''>
		<cfset myquerytagstext["TITLE.#arguments.language#"] = mystrlang>

		<cfif structKeyExists(arguments, "tags")>
			<cfset mytags = listtoarray(arguments.tags,",")>
			<cfset myquerytags = []>
			<cfloop array="#mytags#" index="t">
				<cfset mystruct = structNew()>
				<cfset mystruct["TAG"] = t>
				<cfset arrayAppend(myquerytags,mystruct)>
			</cfloop>
			<cfset myquerytagstext['$or'] = myquerytags>
		</cfif>

		<cfif structKeyExists(arguments,"category")>
			<cfset mycategory = MongoCollectionfindOne(
				datasource=databasename,
				collection="category",
				query=({"SLUG":arguments.category})
				)>
			<cfif structKeyExists(mycategory,"_id")>
				<cfset mycategory._id = mycategory._id.toString()>
				<cfset mycateid = MongoObjectid(mycategory._id)>

				<cfset cetegoryArr = arrayNew()>
				<cfset arrayAppend(cetegoryArr,mycateid)>
				<cfset categoryIn['$in'] = cetegoryArr>

<!---				<cfset myquerytagstext['CATEGORY.$id'] = mycateid>--->
<!---				MULTI CETEGORY --->
				<cfif arguments['subcategory'] IS true>
					<cfset cateraw = MongoCollectionfind(databasename,'category',{})>
					<cfset newcate = treequeryjson(cateraw)>
					<cfset ceterawArr=arrayNew()>
					<cfloop array="#newcate#" index="item">
						<cfset resultjson = structNew()>
						<cfif  REFind(mycategory['SLUG'],item['TITLE']) GT 0 >
							<cfset resultjson = deserializeJSON(item['TITLE'])>
							<cfset arrayAppend(ceterawArr,resultjson)>
						</cfif>
					</cfloop>
					<cfset maxcete = 0>
					<cfset chooseCetegoryRaw = structNew()>
					<cfloop array="#ceterawArr#" index="i">
						<cfset countChild=$countChild(i)>
						<cfif countChild GT maxcete>
							<cfset maxcete = countChild>
							<cfset chooseCetegoryRaw=i>
						</cfif>
					</cfloop>
					<cfset categoryList = $selectChildCetegory(chooseCetegoryRaw,arguments['category'])>
					<cfset categoryIn['$in'] = categoryList>
				</cfif>

				<cfset myquerytagstext['CATEGORY.$id']=categoryIn>

			</cfif>

			<cfset contents = MongoCollectioncount(
				datasource=databasename,
				collection="post",
				query=(myquerytagstext)
					)>
		<cfelse>
			<cfset contents = MongoCollectioncount(
				datasource=databasename,
				collection="post",
				query=(myquerytagstext)
					)>
		</cfif>
		<cfset contents = contents-arguments.skip>
		<cfif contents LT 0>
			<cfset contents=0>
		</cfif>
		<cfreturn contents>
	</cffunction>

	<cffunction name="countpostslistmulticategory" access="remote" returntype="numeric" returnformat="json">
		<cfargument name="tags" type="string" required="false" />
		<cfargument name="category" type="string" required="false" />
		<cfargument name="skip" type="string" required="false" default=0 />
		<cfargument name="language" type="string" required="true" default="thai" />

		<cfinclude template="config.cfm"/>

		<cfset myquerytagstext = structNew()>
		<cfset myquerytagstext['TYPECONTENT'] = 1>
		<cfset myquerytagstext['PUBSTATUS'] = 1>
		
		<cfset mystrlang = structNew()>
		<cfset mystrlang["$ne"] = ''>
		<cfset myquerytagstext["TITLE.#arguments.language#"] = mystrlang>
		
		<cfset myquerytags = []>
		
		<cfif structKeyExists(arguments, "tags")>
			<cfset mytags = listtoarray(arguments.tags,",")>	
			<cfloop array="#mytags#" index="t">
				<cfset mystruct = structNew()>
				<cfset mystruct["TAG"] = t>
				<cfset arrayAppend(myquerytags,mystruct)>
			</cfloop>
			<cfset myquerytagstext['$or'] = myquerytags>
		</cfif>
		

		<cfif structKeyExists(arguments,"category")>
			<cfset mycates = listtoarray(arguments.category,",")>
			
			<cfset myquerycates = []>
			<cfloop array="#mycates#" index="c">
				<cfset mystructcates = structNew()>
				<cfset mystructcates["SLUG"] = c>
				<cfset arrayAppend(myquerycates,mystructcates)>
			</cfloop>
			<cfset myquerycatestext['$or'] = myquerycates>

			<cfset mycategory = MongoCollectionfind(
				datasource=databasename,
				collection="category",
				query=(myquerycatestext),
				field={"_id":true}
					)>
					
			<cfif isarray(mycategory) and arraylen(mycategory) gte 1>
				<cfif not isDefined("myquerytags")>
					<cfset myquerytags = []>
				</cfif>
				<cfloop array="#mycategory#" index="mca">
					<cfset mystrcat = structNew()>
					<cfset mca._id = mca._id.toString()>
					<cfset mycateid = MongoObjectid(mca._id)>
					<cfset mystrcat["CATEGORY.$id"] = mycateid>
					<cfset arrayAppend(myquerytags,mystrcat)>
				</cfloop>
				<cfset myquerytagstext['$or'] = myquerytags>
			</cfif>
		</cfif>
		<cfset contents = MongoCollectioncount(
					datasource=databasename,
					collection="post",
					query=(myquerytagstext)
						)>
		<cfset contents = contents-arguments.skip>
		<cfif contents LT 0>
			<cfset contents=0>
		</cfif>
		<cfreturn contents>
	</cffunction>
	
	<cffunction name="countpageslistmulticategory" access="remote" returntype="numeric" returnformat="json">
		<cfargument name="tags" type="string" required="false" />
		<cfargument name="category" type="string" required="false" />
		<cfargument name="skip" type="string" required="false" default=0 />
		<cfargument name="language" type="string" required="true" default="THAI" />

		<cfinclude template="config.cfm"/>

		<cfset myquerytagstext = structNew()>
		<cfset myquerytagstext['TYPECONTENT'] = 2>
		<cfset myquerytagstext['PUBSTATUS'] = 1>
		
		<cfset mystrlang = structNew()>
		<cfset mystrlang["$ne"] = ''>
		<cfset myquerytagstext["TITLE.#arguments.language#"] = mystrlang>
		
		<cfset myquerytags = []>
		
		<cfif structKeyExists(arguments, "tags")>
			<cfset mytags = listtoarray(arguments.tags,",")>	
			<cfloop array="#mytags#" index="t">
				<cfset mystruct = structNew()>
				<cfset mystruct["TAG"] = t>
				<cfset arrayAppend(myquerytags,mystruct)>
			</cfloop>
			<cfset myquerytagstext['$or'] = myquerytags>
		</cfif>
		

		<cfif structKeyExists(arguments,"category")>
			<cfset mycates = listtoarray(arguments.category,",")>
			
			<cfset myquerycates = []>
			<cfloop array="#mycates#" index="c">
				<cfset mystructcates = structNew()>
				<cfset mystructcates["SLUG"] = c>
				<cfset arrayAppend(myquerycates,mystructcates)>
			</cfloop>
			<cfset myquerycatestext['$or'] = myquerycates>

			<cfset mycategory = MongoCollectionfind(
				datasource=databasename,
				collection="category",
				query=(myquerycatestext),
				field={"_id":true}
					)>
					
			<cfif isarray(mycategory) and arraylen(mycategory) gte 1>
				<cfif not isDefined("myquerytags")>
					<cfset myquerytags = []>
				</cfif>
				<cfloop array="#mycategory#" index="mca">
					<cfset mystrcat = structNew()>
					<cfset mca._id = mca._id.toString()>
					<cfset mycateid = MongoObjectid(mca._id)>
					<cfset mystrcat["CATEGORY.$id"] = mycateid>
					<cfset arrayAppend(myquerytags,mystrcat)>
				</cfloop>
				<cfset myquerytagstext['$or'] = myquerytags>
			</cfif>
		</cfif>
		<cfset contents = MongoCollectioncount(
					datasource=databasename,
					collection="post",
					query=(myquerytagstext)
						)>
		<cfset contents = contents-arguments.skip>
		<cfif contents LT 0>
			<cfset contents=0>
		</cfif>
		<cfreturn contents>
	</cffunction>

	<cffunction name="countpageslist" access="remote" returntype="numeric" returnformat="json">
		<cfargument name="category" type="string" required="false" />
		<cfargument name="skip" type="string" required="false" default=0 />
		<cfargument name="language" type="string" required="true" default="THAI" />

		<cfinclude template="config.cfm"/>

		<cfset myquerytagstext = structNew()>
		<cfset myquerytagstext['TYPECONTENT'] = 2>
		<cfset myquerytagstext['PUBSTATUS'] = 1>
		
		<cfset mystrlang = structNew()>
		<cfset mystrlang["$ne"] = ''>
		<cfset myquerytagstext["TITLE.#arguments.language#"] = mystrlang>

		<cfif structKeyExists(arguments,"category")>
			<cfset mycategory = MongoCollectionfindOne(
				datasource=databasename,
				collection="category",
				query=({"SLUG":arguments.category})
				)>
			<cfif structKeyExists(mycategory,"_id")>
				<cfset mycategory._id = mycategory._id.toString()>
				<cfset mycateid = MongoObjectid(mycategory._id)>
				<cfset myquerytagstext['CATEGORY.$id'] = mycateid>
			</cfif>

			<cfset contents = MongoCollectioncount(
				datasource=databasename,
				collection="post",
				query=(myquerytagstext)
					)>
			<cfelse>
			<cfset contents = MongoCollectioncount(
				datasource=databasename,
				collection="post",
				query=(myquerytagstext)
					)>
		</cfif>
		<cfset contents = contents-arguments.skip>
		<cfif contents LT 0>
			<cfset contents=0>
		</cfif>
		<cfreturn contents>
	</cffunction>


	<cffunction name="countsubcategories" access="remote" returntype="numeric" returnformat="json">
		<cfargument name="category" type="string" required="false" />
		<cfinclude template="config.cfm"/>
		<cfset output = 0>
		<cfset parent_category = MongoCollectionfindOne(databasename,'category',{'SLUG'=arguments.category})>
		<cfif isDefined("parent_category")>
			<cfset output = MongoCollectioncount(
				datasource=databasename,
				collection="category",
				query=({'PARENT.$id'=parent_category._id}))>
		</cfif>
		<cfreturn output>
	</cffunction>

	<cffunction name="countsubmulticategories" access="remote" returntype="numeric" returnformat="json">
		<cfargument name="category" type="string" required="false" />
		<cfinclude template="config.cfm"/>
		<cfset mycates = listtoarray(arguments.category,",")>
		<cfset myquerycatestext = structNew()>
		<cfset myquerycates = []>
		<cfloop array="#mycates#" index="c">
			<cfset mystructcates = structNew()>
			<cfset mystructcates["SLUG"] = c>
			<cfset arrayAppend(myquerycates,mystructcates)>
		</cfloop>
		<cfset myquerycatestext['$or'] = myquerycates>
		<cfset parent_category = MongoCollectionfind(databasename,'category',myquerycatestext)>
		<cfset output = 0>
		<cfif isDefined("parent_category")>
			<cfset myquerycatestext2 = structNew()>
			<cfset myquerycates2 = []>
			<cfloop array="#parent_category#" index="q">
				<cfset mystructcates = structNew()>
				<cfset mystructcates['PARENT.$id'] = q._id>
				<cfset arrayAppend(myquerycates2,mystructcates)>
			</cfloop>
			<cfset myquerycatestext2['$or'] = myquerycates2>
			<cfset output = MongoCollectioncount(databasename,'category',myquerycatestext2)>
		</cfif>
		<cfreturn output>
	</cffunction>



</cfcomponent>