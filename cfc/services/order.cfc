<cfcomponent>
	<cfprocessingdirective pageencoding="utf-8" />
	<cffunction name="order" access="remote" returntype="struct" returnformat="JSON">
		<cfargument name="order" type="string" required="true">
		<cfinclude template="config.cfm"/>
		<cfset doc = deserializeJSON(arguments.order)>
		<cfset doc.CREATEDAT = now()>
		<cfset doc.STATUS = "order">
		<cfset doc.NOTE = "">
		<cfset save = MongoCollectionInsert(databasename,"order",doc)>
		<cfset output = {"_id":save}>
		<cfset order = MongoCollectionfindone(databasename,'order',{_id=MongoObjectId(save)})>
		<cfif not structKeyExists(order, "DISCOUNT")>
			<cfset order['DISCOUNT'] = 0>
		</cfif>
		<cfif not structKeyExists(order, "SHIPPINGPRICE")>
			<cfset order['SHIPPINGPRICE'] = 0>
		</cfif>
<cfoutput>
    <cfsavecontent variable="mailbody">
        <table class="tableorder" style="background-color: transparent;border-spacing: 0;font-size: 14px;margin-bottom: 20px;width: 100%;border-collapse: separate;border-color: ##DDDDDD;border-radius: 4px 4px 4px 4px;border-style: solid solid solid none;border-width: 1px 1px 1px 0;border: 1px solid ##DDDDDD;">
            <thead>
                <tr>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: center;padding: 4px 5px;">Name</td>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: center;">Qty</td>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: center;">Unit price</td>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: center;">Amount</td>
                </tr>
            </thead>
        <tbody>
            <cfloop array="#order['products']#" index="item">
                <tr>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;padding: 4px 5px;">#item['name']#</td>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: right;">#item['qty']#</td>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: right;">#item['price']#</td>
                    <cfset amount = item['qty'] * item['price']>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: right;">#amount#</td>
                </tr>
            </cfloop>
        </tbody>
        <tfoot>
        	<tr>
                <td colspan="3" style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;padding: 4px 5px;">Discount</td>
                <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: right;">
                    #order['DISCOUNT']#
                </td>
            </tr>
            <tr>
                <td colspan="3" style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;padding: 4px 5px;">Shipping</td>
                <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: right;">
                    #order['SHIPPINGPRICE']#
                </td>
            </tr>
            <tr>
                <td colspan="3" style="border-left: 1px solid ##DDDDDD;padding: 4px 5px;">Total</td>
                <td style="border-left: 1px solid ##DDDDDD;text-align: right;">
                    #order['TOTALPRICE']#
                </td>
            </tr>
        </tfoot>
        </table>
    </cfsavecontent>
    #mailbody#
</cfoutput>
		<cfset mailobj = createObject('mail.mailservice')>
	    <cfset mailobj.sendmail(
	        from='order@skoode.com',
	        to=order['CUSTOMER']['EMAIL'],
	        subject = 'Order',
	        typemail = 'html',
	        server = mail.server,
	        ssl = mail.useSSL,
	        port = mail.port,
	        username = mail.username,
	        password = mail.password,
	        template = 'orderfirst.txt',
	        tableorder=mailbody
	            )>
		<cfreturn output>
	</cffunction>

	<cffunction name="orderlist" access="remote" returntype="array" returnformat="JSON">
		<cfargument name="orderby" type="string" required="false" default="CREATEDAT" />
		<cfargument name="sort" type="string" required="false" default="desc" />
		<cfargument name="fbid" type="string" required="true">
		<cfargument name="currentpage" type="numeric" required="false" default=1 />
		<cfargument name="limit" type="string" required="false" />
		<cfinclude template="config.cfm"/>
		<cfset doc = {
				"CUSTOMER.FBID": arguments.fbid
		}>
		<cfset sortby = 1>
		<cfif arguments.sort eq "desc">
			<cfset sortby = -1>
		</cfif>
		<cfset sortfield = StructNew()>
		<cfset sortfield[arguments.orderby] = sortby>

		<cfif structKeyExists(arguments, "limit")>
			<cfif structKeyExists(arguments, "currentpage") and arguments.currentpage neq 1>
				<cfset result = MongoCollectionfind(
					datasource=databasename,
					collection="order",
					query=(doc),
					skip=(arguments.currentpage*arguments.limit)-arguments.limit,
					size=arguments.limit,
					sort=(sortfield)
						)>
				<cfelse>
				<cfset result = MongoCollectionfind(
					datasource=databasename,
					collection="order",
					query=(doc),
					size=arguments.limit,
					sort=(sortfield)
						)>
			</cfif>
			<cfelse>
			<cfset result = MongoCollectionfind(
				datasource=databasename,
				collection="order",
				query=(doc),
				sort=(sortfield)
					)>
		</cfif>
		<cfset results = DeserializeJSON(SerializeJSON(result))>
		<cfreturn results>
	</cffunction>

	<cffunction name="orderdetail" access="remote" returntype="struct" returnformat="JSON">
		<cfargument name="fbid" type="string" required="true">
		<cfargument name="id" type="string" required="true">
		<cfinclude template="config.cfm"/>
		<cfset doc = {
				"CUSTOMER.FBID": arguments.fbid,
			"_id": MongoObjectId(arguments.id)
		}>
		<cfset result = MongoCollectionfindone(
			datasource=databasename,
			collection="order",
			query=(doc)
				)>
		<cfset results = DeserializeJSON(SerializeJSON(result))>
		<cfreturn results>
	</cffunction>

	<cffunction name="uploadFile" access="remote" returntype="struct" returnformat="JSON">
		<cfset pathupload = "#expandPath('../../assets/upload/order')#">
		<cfif NOT DirectoryExists("#pathupload#")>
			<cfdirectory action="create" mode="777"  directory="#pathupload#">
		</cfif>
		<cffile action="upload"
				destination="#pathupload#"
				nameconflict="overwrite" result="fileResult"/>
		<cfset filename ="#CreateUUID()#">
		<cfset file="#filename#.#fileResult.clientfileext#">
		<cffile action="rename" source = "#fileResult.serverfileuri#"
				destination="#pathupload#/#file#" attributes="normal">
		<cfreturn {
			'file': file,
			'filename': filename,
			'old_file': fileResult.clientfile,
			'old_filename': fileResult.clientfilename,
			'extention': fileResult.clientfileext,
			'contenttype': fileResult.contenttype,
			'filesize': fileResult.filesize
	}>
	</cffunction>

	<cffunction name="savemoneytranfer" access="remote" returntype="struct" returnformat="JSON">
		<cfargument name="note" type="string" required="true">
		<cfargument name="filedata" type="string" required="true">
		<cfargument name="email" type="string" required="true">
		<cfargument name="id" type="string" required="true">
		<cfargument name="subject" type="string" required="false" default="คุณได้แจ้งโอนเงินเรียบร้อย">
		<cfinclude template="config.cfm"/>
		<cfset findorder = MongoCollectionfindone(databasename,"order",{_id=MongoObjectId(arguments.id)})>
		<cfset findorder.FILE = deserializeJSON(arguments.filedata).file>
		<cfset findorder.NOTE = arguments.note>
		<cfset save = MongoCollectionSave(databasename,"order",findorder)>
		<cfset output = {"_id":save}>

<!--- 		<cfset configfile = "#expandPath('.')#/../config.json">
		<cfset dataFromJSON = deserializeJSON(fileRead( "#configfile#" )) />
		<cfset mail = dataFromJSON.email>
		<cfmail to="#arguments.email#"
				from="#mail.username#"
				subject="#arguments.subject#"
				server="#mail.server#" useSSL="#mail.ssl#" port="#mail.port#"
				username="#mail.username#" password="#mail.password#">
			<cfoutput>
				#arguments.subject#
			</cfoutput>
		</cfmail> --->
		<cfset order = MongoCollectionfindone(databasename,'order',{_id=MongoObjectId(arguments.id)})>
		<cfset order['MAILNOTE'] = arguments.subject>
		<cfif not structKeyExists(order, "DISCOUNT")>
			<cfset order['DISCOUNT'] = 0>
		</cfif>
		<cfif not structKeyExists(order, "SHIPPINGPRICE")>
			<cfset order['SHIPPINGPRICE'] = 0>
		</cfif>
<cfoutput>
    <cfsavecontent variable="mailbody">
        <table class="tableorder" style="background-color: transparent;border-spacing: 0;font-size: 14px;margin-bottom: 20px;width: 100%;border-collapse: separate;border-color: ##DDDDDD;border-radius: 4px 4px 4px 4px;border-style: solid solid solid none;border-width: 1px 1px 1px 0;border: 1px solid ##DDDDDD;">
            <thead>
                <tr>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: center;padding: 4px 5px;">Name</td>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: center;">Qty</td>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: center;">Unit price</td>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: center;">Amount</td>
                </tr>
            </thead>
        <tbody>
            <cfloop array="#order['products']#" index="item">
                <tr>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;padding: 4px 5px;">#item['name']#</td>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: right;">#item['qty']#</td>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: right;">#item['price']#</td>
                    <cfset amount = item['qty'] * item['price']>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: right;">#amount#</td>
                </tr>
            </cfloop>
        </tbody>
        <tfoot>
        	<tr>
                <td colspan="3" style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;padding: 4px 5px;">Discount</td>
                <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: right;">
                    #order['DISCOUNT']#
                </td>
            </tr>
            <tr>
                <td colspan="3" style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;padding: 4px 5px;">Shipping</td>
                <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: right;">
                    #order['SHIPPINGPRICE']#
                </td>
            </tr>
            <tr>
                <td colspan="3" style="border-left: 1px solid ##DDDDDD;padding: 4px 5px;">Total</td>
                <td style="border-left: 1px solid ##DDDDDD;text-align: right;">
                    #order['TOTALPRICE']#
                </td>
            </tr>
        </tfoot>
        </table>
    </cfsavecontent>
    #mailbody#
</cfoutput>
		<cfset mailobj = createObject('mail.mailservice')>
	    <cfset mailobj.sendmail(
	        from='order@skoode.com',
	        to=arguments.email,
	        subject = 'Order',
	        typemail = 'html',
	        server = mail.server,
	        ssl = mail.useSSL,
	        port = mail.port,
	        username = mail.username,
	        password = mail.password,
	        template = 'ordermoneytranfer.txt',
	        tableorder=mailbody,
	        ordernote=order['MAILNOTE']
	            )>
		<cfreturn output>
	</cffunction>

	<cffunction name="countorderlist" access="remote" returntype="numeric" returnformat="json">
		<cfargument name="fbid" type="string" required="true">
		<cfinclude template="config.cfm"/>
		<cfset doc = {
				"CUSTOMER.FBID": arguments.fbid
	}>
		<cfset contents = MongoCollectioncount(
			datasource=databasename,
			collection="order",
			query=(doc)
				)>
		<cfreturn contents>
	</cffunction>

</cfcomponent>