<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 8/28/13 AD
  Time: 10:28 AM
  To change this template use File | Settings | File Templates.
--->
<cfcomponent extends="Utility">

	<cffunction name="slideshow" access="remote" returntype="struct" returnformat="json">
		<cfargument name="key" type="string" required="false" default="" />
		<cfargument name="slug" type="string" required="true" default="" />
		<cfargument name="language" type="string" required="true" default="thai" />
		<cfinclude template="config.cfm"/>
		<cfif environment eq "product">
			<cfset var querystring = structNew()/>
			<cfif (structKeyExists(arguments,"key")) and (arguments.key neq "") and (arguments.key neq "undefined")>
				<cfset querystring['_id'] = MongoObjectId(arguments.key)>
			<cfelse>
				<cfset querystring['SLUG'] = arguments.slug>
			</cfif>
			<cfset item = MongoCollectionfindone(
				datasource=databasename,
				collection="slide",
				query=querystring)>
			<cfif IsNull(item) eq "NO">
				<cfset item._id = item._id.toString()>
				<cfset item.TITLE = removehtmltags(i18n(item.TITLE,arguments.language)) />
				<cfset item['AUTHOR'] = item['USER'].fetch()['USERNAME']>
				<cfif structKeyExists(item,"INTRO")>
					<cfset item.INTRO = removehtmltags(i18n(item.INTRO,arguments.language)) />
				</cfif>
				<cfset item.CREATEDATMONTHDATE = DATEFORMAT(item.CREATEDAT,"mmm dd")>
				<cfset item.CREATEDATYEAR = YEAR(item.CREATEDAT)>
				<cfset item.DETAIL = i18n(item.DETAIL,arguments.language) />
			<cfelse>
				<cfset item = structnew()>
				<cfset item["_id"] = "">
			</cfif>
		<cfelse>
			<cfset var mockdata = getmockdata(1,8)>
    		<cfset item = mockdata['slide']>
		</cfif>
		<cfreturn item>
	</cffunction>

   <!--- Place your content here --->
</cfcomponent>
