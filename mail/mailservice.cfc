<cfcomponent output="false">
    <cffunction name="init" output="false">
        <cfreturn this>
    </cffunction>

    <cffunction name="compiletemplate" access="public" returntype="string">
        <cfargument name="template" type="string" required="true">
        <cfargument name="mailargument" type="any" required="true">
        <cfargument name="recipient" type="string" required="false" default="">
        <cfset var templatebody = arguments['template']>
        <cfset var mailargument = arguments['mailargument']>
        <cfif arguments['recipient'] neq "">
            <cfset templatebody = replaceNoCase(templatebody,'[recipientname]',arguments['recipient'],'all')>
        </cfif>
        <cfloop list="#structkeylist(mailargument,',')#" delimiters="," index="item">
            <cfset templatebody = replaceNoCase(templatebody,'[#item#]',mailargument[item],'all')>
        </cfloop>
        <cfreturn templatebody>
    </cffunction>

    <cffunction name="sendmail" access="public" returntype="string">
        <cfargument name="from" type="string" required="true" hint="E-mail message sender">
        <cfargument name="to" type="string" required="true" hint="Message recipient e-mail addresses">
        <cfargument name="subject" type="string" required="true" hint="Message subject.">
        <cfargument name="server" type="string" required="true" hint="SMTP server address">
        <cfargument name="port" type="numeric" required="true" hint="TCP/IP port on which SMTP server listens for requests (normally 25).">
        <cfargument name="ssl" type="string" required="true" hint="Whether to use Secure Sockets Layer.">
        <cfargument name="typemail" type="string" default="html" hint="MIME type of the message. Can be a valid MIME media type or one of the following:
    text: specifies text/plain type.
    plain: specifies text/plain type.
    html: specifies text/html type.
">
        <cfargument name="username" type="string" required="true" hint="A user name to send to SMTP servers that require authentication.">
        <cfargument name="password" type="string" required="true" hint="A password to send to SMTP servers that require authentication.">
        <cfargument name="template" type="string" required="true" hint="Template to use">
        <cfset var ignorelist = "from,to,subject,server,port,ssl,typemail,username,password,template">
        <cfset var mailargument = structNew()>
        <cfloop list="#structkeylist(arguments,',')#" delimiters="," index="item">
            <cfif listfindnocase(ignorelist,item,',') eq 0>
                <cfset mailargument[item] = arguments[item]>
            </cfif>
        </cfloop>
        <cfset var templatefolder = expandPath('/mail')>
        <cffile action="read" variable="templateraw" file="#templatefolder#/mailtemplate/#arguments['template']#" charset="utf-8">
        <cfset var mailbody = compiletemplate(template=templateraw,mailargument=mailargument)>
        <cfmail from = "#arguments['from']#"
                To = "#arguments['to']#"
                Subject = "#arguments['subject']#"
                charset="utf-8"
                server="#arguments['server']#"
                port=#arguments['port']#
                username="#arguments['username']#"
                password="#arguments['password']#"
                USESSL="#arguments['ssl']#"
                TYPE="#arguments['typemail']#">
            #mailbody#
        </cfmail>
        <cfreturn 'Send Complete'>
    </cffunction>

    <cffunction name="mailinglist" access="public" returntype="string">
        <cfargument name="from" type="string" required="true" hint="E-mail message sender">
        <cfargument name="to" type="string" required="true" hint="Message recipient e-mail addresses">
        <cfargument name="subject" type="string" required="true" hint="Message subject.">
        <cfargument name="server" type="string" required="true" hint="SMTP server address">
        <cfargument name="port" type="numeric" required="true" hint="TCP/IP port on which SMTP server listens for requests (normally 25).">
        <cfargument name="ssl" type="string" required="true" hint="Whether to use Secure Sockets Layer.">
        <cfargument name="typemail" type="string" default="html" hint="MIME type of the message. Can be a valid MIME media type or one of the following:
    text: specifies text/plain type.
    plain: specifies text/plain type.
    html: specifies text/html type.
">
        <cfargument name="username" type="string" required="true" hint="A user name to send to SMTP servers that require authentication.">
        <cfargument name="password" type="string" required="true" hint="A password to send to SMTP servers that require authentication.">
        <cfargument name="template" type="string" required="true" hint="Template to use">
        <cfset var ignorelist = "from,to,subject,server,port,ssl,typemail,username,password,template">
        <cfset var mailargument = structNew()>
        <cfloop list="#structkeylist(arguments,',')#" delimiters="," index="item">
            <cfif listfindnocase(ignorelist,item,',') eq 0>
                <cfset mailargument[item] = arguments[item]>
            </cfif>
        </cfloop>
        <cfset var templatefolder = expandPath('/mail')>
        <cffile action="read" variable="templateraw" file="#templatefolder#/mailtemplate/#arguments['template']#" charset="utf-8">
        <cfloop list="#arguments['to']#" delimiters="," index="recipient">
            <cfset var mailbody = compiletemplate(template=templateraw,mailargument=mailargument,recipient=recipient)>
            <cfmail from = "#arguments['from']#"
                    To = "#recipient#"
                    Subject = "#arguments['subject']#"
                    charset="utf-8"
                    server="#arguments['server']#"
                    port=#arguments['port']#
                    username="#arguments['username']#"
                    password="#arguments['password']#"
                    USESSL="#arguments['ssl']#"
                    TYPE="#arguments['typemail']#">
                #mailbody#
            </cfmail>
        </cfloop>
        <cfreturn 'Send Complete'>
    </cffunction>

	<cffunction name="sendmailmoredata" access="public" returntype="string">
		<cfargument name="from" type="string" required="true" hint="E-mail message sender">
		<cfargument name="to" type="string" required="true" hint="Message recipient e-mail addresses">
		<cfargument name="subject" type="string" required="true" hint="Message subject.">
		<cfargument name="server" type="string" required="true" hint="SMTP server address">
		<cfargument name="port" type="numeric" required="true" hint="TCP/IP port on which SMTP server listens for requests (normally 25).">
		<cfargument name="ssl" type="string" required="true" hint="Whether to use Secure Sockets Layer.">
		<cfargument name="typemail" type="string" default="html" hint="MIME type of the message. Can be a valid MIME media type or one of the following:
    text: specifies text/plain type.
    plain: specifies text/plain type.
    html: specifies text/html type.
">
		<cfargument name="username" type="string" required="true" hint="A user name to send to SMTP servers that require authentication.">
		<cfargument name="password" type="string" required="true" hint="A password to send to SMTP servers that require authentication.">
		<cfargument name="template" type="string" required="true" hint="Template to use">
		<cfargument name="emaildata" type="string" required="true" hint="Email data with JSON">
		<cfset var ignorelist = "from,to,subject,server,port,ssl,typemail,username,password,template">
		<!---		<cfset var mailargument = structNew()>--->
		<cfset var mailargument = deserializeJSON(deserializeJSON(emaildata))>
		<cfset var templatefolder = expandPath('/mail')>

		<cffile action="read" variable="templateraw" file="#templatefolder#/mailtemplate/#arguments['template']#" charset="utf-8">
		<cfset var mailbody = compiletemplate(template=templateraw,mailargument=mailargument)>

		<cfmail from = "#arguments['from']#"
				To = "#arguments['to']#"
				Subject = "#arguments['subject']#"
				charset="utf-8"
				server="#arguments['server']#"
				port=#arguments['port']#
				username="#arguments['username']#"
				password="#arguments['password']#"
				USESSL="#arguments['ssl']#"
				TYPE="#arguments['typemail']#">
			#mailbody#
		</cfmail>
		<cfreturn 'Send Complete'>
	</cffunction>

	<cffunction name="mailinglistmoredata" access="public" returntype="string">
		<cfargument name="from" type="string" required="true" hint="E-mail message sender">
		<cfargument name="to" type="string" required="true" hint="Message recipient e-mail addresses">
		<cfargument name="subject" type="string" required="true" hint="Message subject.">
		<cfargument name="server" type="string" required="true" hint="SMTP server address">
		<cfargument name="port" type="numeric" required="true" hint="TCP/IP port on which SMTP server listens for requests (normally 25).">
		<cfargument name="ssl" type="string" required="true" hint="Whether to use Secure Sockets Layer.">
		<cfargument name="typemail" type="string" default="html" hint="MIME type of the message. Can be a valid MIME media type or one of the following:
    text: specifies text/plain type.
    plain: specifies text/plain type.
    html: specifies text/html type.
">
		<cfargument name="username" type="string" required="true" hint="A user name to send to SMTP servers that require authentication.">
		<cfargument name="password" type="string" required="true" hint="A password to send to SMTP servers that require authentication.">
		<cfargument name="template" type="string" required="true" hint="Template to use">
		<cfset var ignorelist = "from,to,subject,server,port,ssl,typemail,username,password,template">
		<cfset var mailargument = deserializeJSON(deserializeJSON(emaildata))>
		<cfset var templatefolder = expandPath('/mail')>
		<cffile action="read" variable="templateraw" file="#templatefolder#/mailtemplate/#arguments['template']#" charset="utf-8">
		<cfloop list="#arguments['to']#" delimiters="," index="recipient">
			<cfset var mailbody = compiletemplate(template=templateraw,mailargument=mailargument,recipient=recipient)>
			<cfmail from = "#arguments['from']#"
					To = "#recipient#"
					Subject = "#arguments['subject']#"
					charset="utf-8"
					server="#arguments['server']#"
					port=#arguments['port']#
					username="#arguments['username']#"
					password="#arguments['password']#"
					USESSL="#arguments['ssl']#"
					TYPE="#arguments['typemail']#">
				#mailbody#
			</cfmail>
		</cfloop>
		<cfreturn 'Send Complete'>
	</cffunction>

</cfcomponent>
