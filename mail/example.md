1. sendmail
```
<cfset mailobj = createObject('mail.mailservice')>
<cfset mailobj.sendmail(
    from='sender email',
    to='recipient email',
    subject = 'supporttest',
    typemail = 'html,text,plain',
    server = 'SMTP server address',
    ssl = 'true',
    port = 465,
    username = 'mailusername',
    password = 'mailpassword',
    template = 'completeregister.txt',
    customermail='argument [customermail] in template',
    loginpage='argument [loginpage] in template'
        )>
```
2. maillinglist
```
<cfset mailobj = createObject('mail.mailservice')>
<cfset mailobj.mailinglist(
    from='sender email',
    to='"recipient name" <recipient email>,"recipient name" <recipient email>',
    subject = 'supporttest',
    typemail = 'html,text,plain',
    server = 'SMTP server address',
    ssl = 'true',
    port = 465,
    username = 'mailusername',
    password = 'mailpassword',
    template = 'newsletter.txt',
    loginpage='argument [loginpage] in template'
        )>
```