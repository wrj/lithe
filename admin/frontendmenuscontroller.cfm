<!---
  Created with IntelliJ IDEA.
  User: peerabumrung
  Date: 1/21/13
  Time: 2:49 PM
  To change this template use File | Settings | File Templates.
--->
<cfinclude template="system/loadplugins.cfm">

<cfset structDelete(form,'FIELDNAMES')>
<cfswitch expression="#mode#">
	<cfcase value="create">
		<cfset issuccess = form['ISSUCCESS']>
		<cfset structDelete(form,'MODE')>
		<cfset structDelete(form,'ISSUCCESS')>
		<cfset structDelete(form,'MENUMODE')>
		<cfset result = mongovalidate('frontendmenu','create',form)>
		<cfif result['result'] eq true>
			<cfif issuccess IS "yes">
				<cfset name = result['value']['NAME']>
				<cfset isSameName = MongoCollectionfindone(application.applicationname,'frontendmenu',{'NAME'=result['value']['NAME']})>
				<cfif NOT isStruct(isSameName)>
					<cfset postid = MongoCollectioninsert(application.applicationname,'frontendmenu',result['value'])>
					<cfset result['value']['MENU']=deserializeJSON(result['value']['MENU'])>
					<cfset result['value']['ID']=postid>
					<cfset jsondata = serializeJSON(result['value'])>
<!---					<cfset settingdata = MongoCollectionfindone(application.applicationname,'usersetting', {'USER'=dbref('user',session['userid'])})>--->
					<cfset urlpart = "menu">
					<cfset filepath="#expandPath("/#urlpart#")#">
					<cfif NOT DirectoryExists(filepath)>
						<cfdirectory action="create" mode="777"  directory="#filepath#">
					</cfif>
<!---		NEW FILE MENU DATA  --->
					<cffile
							action = "write"
							file = "#filepath#/#name#.json"
							output = "#jsondata#"
							addNewLine = "yes"
							charset = "utf-8"
							fixnewline = "no"
							mode = "777">
					<cfif fileExists("#filepath#/#name#.json")>
						<cfset flashinsert('success','Create menu complete')>
						<cflocation url="frontendmenus.cfm" addtoken="false">
					<cfelse>
						<cfset flashinsert('error','Error create file <br> <ul><li> not found file: #filepath#/#name#.json</li></ul>')>
						<cfset storedata(result)>
						<cflocation url="frontendmenusnew.cfm" addtoken="false">
					</cfif>
				<cfelse>
					<cfset flashinsert('error','NAME value have been use.')>
					<cfset storedata(result)>
					<cflocation url="frontendmenusnew.cfm" addtoken="false">
				</cfif>
			<cfelseif issuccess IS "no">
				<cfset flashinsert('error','Please input required in menu item field')>
				<cfset storedata(result)>
				<cflocation url="frontendmenusnew.cfm" addtoken="false">
			</cfif>
		<cfelse>
			<cfset flashinsert('error','There was an error creating the menu.')>
			<cfset storedata(result)>
			<cflocation url="frontendmenusnew.cfm" addtoken="false">
		</cfif>
	</cfcase>
	<cfcase value="update">
		<cfset keyid = key>
		<cfset issuccess = form['ISSUCCESS']>
		<cfset displaystart = form['DISPLAYSTART']>
		<cfset structDelete(form,'KEY')>
		<cfset structDelete(form,'MODE')>
		<cfset structDelete(form,'MENUMODE')>
		<cfset structDelete(form,'ISSUCCESS')>
		<cfset structDelete(form,'DISPLAYSTART')>
		<cfset result = mongovalidate('frontendmenu','update',form)>

		<cfif result['result'] eq true>
			<cfif issuccess IS "yes">
				<cfset olddata = MongoCollectionfindone(application.applicationname,'frontendmenu',{'_id'=newid(keyid)})>
				<cfset dataupdate = mongomapvalue('frontendmenu',olddata,result['value'],'default')>
				<cfset result['value']['MENU']= deserializeJSON(result['value']['MENU'])>
				<cfset result['value']['ID']=keyid>
				<cfset jsondata = serializeJSON(result['value'])>
				<cfset urlpart = "menu">
				<cfset filepath="#expandPath("/#urlpart#")#">
				<cfif NOT DirectoryExists(filepath)>
					<cfdirectory action="create" mode="777"  directory="#filepath#">
				</cfif>
				<cfset name = result['value']['NAME']>
<!---		APPEND FILE MENU DATA  --->
				<cffile
						action = "write"
						file = "#filepath#/#name#.json"
						output = "#jsondata#"
						addNewLine = "yes"
						charset = "utf-8"
						fixnewline = "no"
						mode = "777">
				<cfif fileExists("#filepath#/#name#.json")>
					<cfset MongoCollectionsave(application.applicationname,'frontendmenu',dataupdate)>
					<cfset flashinsert('success','Update menu complete')>
					<cflocation url="frontendmenus.cfm?start=#displaystart#" addtoken="false">
				<cfelse>
					<cfset flashinsert('error','Error create file <br> <ul><li> not found file: #filepath#/#name#.json</li></ul>')>
					<cfset storedata(result)>
					<cflocation url="frontendmenusupdate.cfm?key=#keyid#" addtoken="false">
				</cfif>
			<cfelseif issuccess IS "no">
				<cfset flashinsert('error','Please input required in sub menu field')>
				<cfset storedata(result)>
				<cflocation url="frontendmenusupdate.cfm?key=#keyid#" addtoken="false">
			</cfif>
		<cfelse>
			<cfset flashinsert('error','There was an error update the menu.')>
			<cfset storedata(result)>
			<cflocation url="frontendmenusupdate.cfm?key=#keyid#" addtoken="false">
		</cfif>
	</cfcase>

	<cfcase value="delete">
		<cfset deltag(key)>
		<cfset MongoCollectionremove(application.applicationname,'frontendmenu',{'_id'=newid(key)})>
		<cfset urlpart = "menu">
		<cfset filepath="#expandPath("/#urlpart#")#">
		<cfif NOT DirectoryExists(filepath)>
			<cfdirectory action="create" mode="777"  directory="#filepath#">
		</cfif>
		<cfif fileexists("#filepath#/#key#.json")>
			<cffile
				action = "delete"
				file = "#filepath#/#key#.json">
		</cfif>
        <cfset result = structNew()>
        <cfset result['result'] = 'true'>
        <cfset result['message'] = "Delete menu complete">
        <cfoutput>
            #serializeJSON(result)#
        </cfoutput>
	</cfcase>

</cfswitch>

