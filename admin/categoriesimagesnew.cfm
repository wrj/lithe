<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/23/13 AD
  Time: 11:49 AM
  To change this template use File | Settings | File Templates.
--->

<cfinclude template="include/header.cfm">
<cfset cateraw = MongoCollectionfind(application.applicationname,'categoryimage',{})>
<cfset caterawquery = treequery(cateraw)/>
<cfset catequery = arraynew()>
<cfset rowquery = structNew()>
<cfset rowquery["_id"]="0">
<cfset rowquery["TITLE"]="No Parent">
<cfset arrayAppend(catequery,rowquery)>
<cfloop array="#caterawquery#" index="item">
	<cfset rowquery = structNew()>
	<cfset rowquery["_id"]=item['_id']>
	<cfset rowquery["TITLE"]=item['TITLE']>
	<cfset arrayAppend(catequery,rowquery)>
</cfloop>
<cfif isdefined('ERROR')>
	<cfset category = getstoredata()>
	<cfif structIsEmpty(category)>
		<cfset category = mongonew('categoryimage')>
	</cfif>
	<cfset parentid = ''>
	<cfif structKeyExists(category,'PARENT')>
		<cfset parentid = category['PARENT']>
	</cfif>
<cfelse>
	<cfset category = mongonew('categoryimage')>
	<cfset parentid = ''>
</cfif>
<cfoutput>
	<div class="row-fluid">
		<legend class="titlepage">New Category Image</legend>
	#startform(action='categoriesimagescontroller')#
	#hiddenfield(name='mode',value='create')#
	<cfinclude template="categoriesimagesform.cfm"/>
	#submit(label=get('labelbtnadd'))#
	#endform()#
	</div>
</cfoutput>
<cfinclude template="include/footer.cfm">