<cfinclude template="include/header.cfm">
<cfset cateraw = MongoCollectionfind(application.applicationname,'categoryimage',{})>
<cfset caterawquery = treequery(cateraw)/>
<cfset catequery = arraynew()>
<cfloop array="#caterawquery#" index="item">
	<cfset rowquery = structNew()>
	<cfset rowquery["_id"]=item['_id']>
	<cfset rowquery["TITLE"]=item['TITLE']>
	<cfset arrayAppend(catequery,rowquery)>
</cfloop>
<cfif isdefined('ERROR')>
    <cfset image = getstoredata()>
    <cfif structIsEmpty(image)>
        <cfset image = mongonew('image')>
    </cfif>
<cfelse>
    <cfset objdata = MongoCollectionfindone(application.applicationname,'image',{"_id"= newid(key)})>
    <cfset image = mongonew("image",objdata)>
	<cfif isdefined(image['category'])>
		<cfset image['category'] = image['category'].getId().toString()>
	<cfelse>
		<cfset image['category']="">
	</cfif>
</cfif>

<cfoutput>
<div class="row-fluid">
    <legend class="titlepage">Edit image - #image['filename']#</legend>
    #startform(action='imagescontroller')#
    #hiddenfield(name='mode',value='update')#
    #hiddenfield(name='key',value=key)#
	#hiddenfield(name='displaystart',value=start)#
    #textfield(name='filename',label='filename',value=image['filename'],readonly=true)#
	#select(name='category',label='category',select=image['category'],option=catequery,field='title',key='_id',require=true)#
    #hiddenfield(name='oldfilename',value=image['NEWFILENAME'])#
    #hiddenfield(name='newfilename')#
    <div class="control-group">
        <label class="control-label">Thumbnail</label>
        <div class="controls">
            <ul class="thumbnails">
                <li class="span3">
                    <div class="thumbnail">
                        <a class="icon-zoom-in pull-right" href="javascript:previewimage('#image['newfilename']#')"></a>
                        <div class="image-thumbnail">
							<a href="javascript:previewimage('#image['newfilename']#')">
                            <img src="#get('pathuploadfolder')#image/#image['newfilename']#" class="img-thumb img-rounded"/>
                            </a>
						</div>
                        <p class="thumbnail-text">Current Thumbnail</p>
                    </div>
                </li>
			    <li class="">
					<cfinclude template="imagedefaultsize.cfm">
					<!---   slideshow   largeimage  thumbnail   categoryimage   menuimage   otherimage	--->
					#$showimagedefaultsize("slideshow,thumbnail,largeimage,categoryimage,menuimage,otherimage")#
		        </li>
                <li class="span3" id="previewthumb"></li>
            </ul>
        </div>
    </div>
    #genUpload('#get('filesizeimage')#','uploadprocess','image')#
    #button(label='Upload File',onclick='doupload()')#
	#submit(label="#get('labelbtnedit')# Category Image")#
    #endform()#
</div>
</cfoutput>
<!---    Preview Thumb--->
<div class="modal hide fade" id="pModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Preview Image</h3>
    </div>
    <div class="modal-body" id="previewimage">

    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>
<script type="text/javascript">
    function upfilecomplete(serverfile,clientfile)
    {
        $('#newfilename').val(serverfile);
        $('form').submit();
    }

    function doupload()
    {
	    if(window.validateform($("form"))){
		    $('#file_upload').uploadifive('upload')
	    }
    }

    function previewimage(imagename)
    {
    <cfoutput>
        var pathpreview = '<div class="thumbnail"><img src="#get('pathuploadfolder')#image/'+imagename+'"/></div>';
    </cfoutput>
        $('#previewimage').html(pathpreview);
        $('#pModal').modal('show');
    }
    $('#pModal').on('hidden', function () {
        $("#previewimage").html('');
    });
	$(function(){

    $(".image-thumbnail").css("width",$(".image-thumbnail").find("img").width());
})
</script>
<cfinclude template="include/footer.cfm">
<!--- BACK BUTTON DETECT --->
<script type="text/javascript">
history.pushState({page: 123}, "update", "");
var _firstload = true;
var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
var isIE = /*@cc_on!@*/false;                            // At least IE6
if (isFirefox){_firstload = false;}
		window.onpopstate = function(e){
		if(_firstload){_firstload = false;}
else{_firstload = false;
	<cfoutput>window.location.href = 'images.cfm?category=#image['category']#&start=#start#';</cfoutput>}
};
</script>