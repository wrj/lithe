<cfinclude template="system/loadplugins.cfm">
<cfset structDelete(form,'FIELDNAMES')>
<cfset structDelete(form,'FILE_UPLOAD')>
<cfswitch expression="#mode#">
    <cfcase value="create">
        <cfset structDelete(form,'mode')>
        <cfset result = mongovalidate('file','create',form)>
        <cfif result['result'] eq true>
            <cfset form['USER'] = dbref('user',session['userid'])>
            <cfset result['value']['USER'] = dbref('user',session['userid'])>
            <cfset filestruct = movefile(form['NEWFILENAME'],'file/')>
            <cfset result['value']['NEWFILENAME'] = filestruct['newserverfile']>
            <cfset MongoCollectioninsert(application.applicationname,'file',result['value'])>
            <cfset flashinsert('success','Create file complete')>
            <cflocation url="files.cfm" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error creating the file.')>
            <cfset storedata(result)>
            <cflocation url="filesnew.cfm" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="update">
        <cfset keyid = key>
	    <cfset displaystart = form['DISPLAYSTART']>
        <cfset structDelete(form,'KEY')>
        <cfset structDelete(form,'MODE')>
	    <cfset structDelete(form,'DISPLAYSTART')>
        <cfset oldfilename = form['oldfilename']>
        <cfset structDelete(form,'oldfilename')>
        <cfset result = mongovalidate('file','update',form)>
        <cfif result['result'] eq true>
            <cfset updateobj = result['value']>
            <cfset olddata = MongoCollectionfindone(application.applicationname,'file',{'_id'=newid(keyid)})>
            <cfset dataupdate = mongomapvalue('file',olddata,updateobj,'default')>
            <cfset dataupdate['USER'] = dbref('user',session['userid'])>
<!---            File on server--->
            <cfif form['NEWFILENAME'] neq ''>
                <cfset temp = deletefile(oldfilename,'file/')>
                <cfset filestruct = movefile(form['NEWFILENAME'],'file/')>
                <cfset dataupdate['NEWFILENAME'] = filestruct['newserverfile']>
            <cfelse>
                <cfset dataupdate['NEWFILENAME'] = oldfilename>
            </cfif>
            <cfset MongoCollectionsave(application.applicationname,'file',dataupdate)>
            <cfset flashinsert('success','Update file complete')>
            <cflocation url="files.cfm?start=#displaystart#" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error update the file.')>
            <cfset storedata(result)>
            <cflocation url="filesupdate.cfm?key=#keyid#" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="delete">
        <cfset file = MongoCollectionfindone(application.applicationname,'file',{'_id'=newid(key)})>
        <cfset temp = deletefile(file['NEWFILENAME'],'file/')>
        <cfset MongoCollectionremove(application.applicationname,'file',{'_id'=newid(key)})>
        <cfset result = structNew()>
        <cfset result['result'] = 'true'>
        <cfset result['message'] = "Delete file complete">
        <cfoutput>
            #serializeJSON(result)#
        </cfoutput>
    </cfcase>

</cfswitch>