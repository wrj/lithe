<cfinclude template="include/header.cfm">
<cfoutput>
    #startForm(action="loginprocess")#
    <div class="row-fluid">
        <div class="span6 offset2">
            <legend>Sign in to : #get('sitetitle')#</legend>
            #textField(name='username',label='username')#
            #passwordField(name='password',label='password')#
            #submit(label='Login')#
        </div>
    </div>
    #endForm()#
</cfoutput>
    <div id="footer">
        <div class="offset2">
            <p>Copyright &copy; Skoode Skill Co., Ltd.</p>
            <p>Lithe CMS is a LGPL license software by Skoode Skill Co.,Ltd.</p>
            <p>Find out more detail at http://www.openlithe.org</p>
        </div>
    </div>
</div>
</body>
</html>
<script>
    $(document).ready(function() {
        $('#password').keypress(function(event) {
            if (event.which == '13') {
                $('form').submit();
            }
        });
    });
</script>