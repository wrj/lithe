<cfoutput>
    #stylesheetinclude('jquery-ui-1.9.0.custom,customui')#
    <script src="ckeditor/ckeditor.js"></script>
    <script src="ckeditor/adapters/jquery.js"></script>
    <script src="ckeditor/toolbargroupconfig.js"></script>
    #textfield(name='title',label='title',value=category['title'],require=true)#
    #textfield(name='slug',label='slug',value=category['slug'],require=true)#
    #textfield(name='template',label='template',value=category['template'])#
    #textfield(name='url',label='url',value=category['url'],require=true)#
    #textfield(name='pattern',label='pattern',value=category['pattern'],placeholder="{template}?slug={slug}{otherparams}")#
    #select(name='category',label='Parent Category',select=parentid,option=catequery,field='title',key='_id')#
	<cfinclude template="imagedefaultsize.cfm">
	<div class="control-group">
        <label class="control-label">Thumbnail</label>
        <div class="controls">
            <ul class="thumbnails">
                <li class="span2">
                    <a href="javascript:selectthumb('image')" class="btn">Select</a>
                    #hiddenfield(name='imageref',value=category['IMAGEREF'])#
                </li>
                <li class="span3" id="showimage">
                    <div class="thumbnail thumbnail-empty">
                        <div class="thumbnail-empty-text">
                            New Image
                        </div>
                        <div class="thumbnail-empty-size">
							<!---   slideshow   largeimage  thumbnail   categoryimage   menuimage   otherimage	--->
							#$showimagedefaultsize("largeimage","text")#
                        </div>
                    </div>
                </li>
                <cfif category['IMAGEREF'] neq ''>
                    <li class="span3">
                        <div class="thumbnail">
                            <a class="icon-remove pull-left" href="javascript:void(0)" onclick="removeimage('thumbnail',this)"></a>
                            <a class="icon-zoom-in pull-right" href="javascript:previewimage('#category["IMAGEREF"]#')"></a>
		                    <div class="image-thumbnail">
	                            <a href="javascript:previewimage('#category["IMAGEREF"]#')">
	                            <img src="#category['IMAGEREF']#" class="img-thumb img-rounded"/>
	                            </a>
			                </div>
                            <p class="thumbnail-text">Current Thumbnail</p>
                        </div>
                    </li>
                </cfif>
            </ul>
        </div>
    </div>
    #textarea(name='detail',label='detail',value=category['detail'],class='span12')#
</cfoutput>
<!--- Modal Choose Image --->
<div class="modal hide fade bigModal" id="bModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Browse</h3>
    </div>
    <div class="modal-body-bModal">

    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <a onclick="selectobject()" class="btn btn-primary">OK</a>
    </div>
</div>
<!---    Preview Thumb--->
<div class="modal hide fade" id="pModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Preview Image</h3>
    </div>
    <div class="modal-body" id="previewimage">

    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>
<cfinclude template="postsjsconfig.cfm">
<script type="text/javascript">
$(function(){
    $( '#detail' ).ckeditor({
                extraPlugins : 'video,downloadfile,iframe,addlink',
                toolbar: toolbarfullconfig,
                filebrowserImageBrowseUrl:seturlbase()+"/browseck.cfm?type=image",
                filebrowserBrowseUrl:seturlbase()+"/browseck.cfm",
                filebrowserWindowWidth : '770',
                filebrowserWindowHeight : '750',
                height : '400'               
            }); 
    $('#title').focusout(function(){
        if ($('#slug').val() == '')
        {
            $('#slug').val(replacetext($('#title').val()));
        }
    });
    $('#slug').focusout(function(){
        $('#slug').val(replacetext($('#slug').val()));
    });
    $("form").submit(function() {
        var slugtext = $('#slug').val();
        if (slugtext == '')
        {
            slugtext = $('#title').val();
        }
        $('#slug').val(replacetext(slugtext));
    });
    $(".thumbnail-empty").height($(".thumbnail-empty").parent().next().find(".thumbnail").height());
    if ($(".thumbnail-empty").parent().next().find(".thumbnail").length > 0){
        $(".thumbnail-empty-text").css("padding-top",$(".thumbnail-empty").parent().next().find(".thumbnail").height()*0.5-10);
    }else{
        $(".thumbnail-empty-text").css("padding-top",$(".thumbnail-empty").height()*0.5-10);
    }
	$(".image-thumbnail").css("width",$(".image-thumbnail").find("img").width());
})

function selectthumb(type){
    typeimage = type;
    browsewindow('image');
}

function browsewindow(type){
    typeopen = type;
    $(".modal-body-bModal").html('<iframe id="modalIframeId" width="720" height="620" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" />');
    $("#modalIframeId").attr("src",seturlbase()+"/browse.cfm?type="+type);
    $('#bModal').modal('show');
}

$('#bModal').on('hidden', function () {
    $(".modal-body-bModal").html('');
});

function selectedcomplete(fileraw)
{
    if (typeopen == 'image')
    {
        var filearr = fileraw[0].split("|");
        var linkinsert = '';
        if(typeimage == 'markdown')
        {
            <cfoutput>
            linkinsert = '![img](#get('pathuploadfolder')#image/'+filearr[0]+' "'+filearr[1]+'")'
            </cfoutput>
            $.markItUp({ target:'#detail',replaceWith: linkinsert });
        }else{
            <cfoutput>
            linkinsert = '#get('pathuploadfolder')#image/'+filearr[0];
            </cfoutput>
            imgstr = '<div class="thumbnail"><img src="'+linkinsert+'" class="img-thumb img-rounded"/><p>New Thumbnail</p></div>'
            $('#showimage').html(imgstr);
            $('#imageref').val(linkinsert);
        }
    }
    if (typeopen == 'video')
    {
        $.markItUp({ target:'#detail',replaceWith: fileraw[0] });
    }
    if (typeopen == 'file')
    {
        var filearr = fileraw[0].split("|");
        var linkinsert = '';
        <cfoutput>
        linkinsert = '['+filearr[0]+'](#get('pathuploadfolder')#file/'+filearr[1]+' "'+filearr[0]+'")'
        </cfoutput>
        $.markItUp({ target:'#detail',replaceWith: linkinsert });
    }
    if (typeopen == 'link')
    {
        var filearr = fileraw[0].split("|");
        var linkinsert = '';
        linkinsert = '['+filearr[1]+']('+filearr[0]+' "'+filearr[1]+'")'
        $.markItUp({ target:'#detail',replaceWith: linkinsert });
    }
}

function selectobject()
{
    document.getElementById('modalIframeId').contentWindow.selectfile();
}

function previewimage(imagename)
{
    var pathpreview = '<div class="thumbnail"><img src="'+imagename+'"/></div>';
    $('#previewimage').html(pathpreview);
    $('#pModal').modal('show');
}

$('#pModal').on('hidden', function () {
    $("#previewimage").html('');
});

    function removeimage(imageremove,obj)
    {
        $(obj).parent().remove();
        $('#imageref').val('remove');
    }
</script>