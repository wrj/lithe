<cfinclude template="system/loadplugins.cfm">
<cfset logout()>
<cfset passencode = encrypt(form['PASSWORD'],application.applicationname)>
<cfset userencode = encrypt(form['USERNAME'],application.applicationname)>
<cfif get('adminuser') eq userencode && get('adminpass') eq passencode>
    <cfset session['userid'] = ''>
    <cfset session['username'] = form['USERNAME']>
    <cfset session['secret'] = ''>
    <cfset session['cart'] = ''>
    <cfset session['rule'] = 'superadmin'>
    <cfset session['language'] = listFirst(get('weblanguage'),',')>
    <cflocation url="users.cfm" addtoken="false">
<cfelse>
    <cfset flashinsert('error','Username or Password miss match')>
    <cflocation url="adminlogin.cfm" addtoken="false">
</cfif>