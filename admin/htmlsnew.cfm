<cfinclude template="include/header.cfm">
<cfif isdefined('ERROR')>
    <cfset post = getstoredata()>
    <cfif structIsEmpty(post)>
        <cfset post = mongonew('post')>
    </cfif>
    <cfset post['tag'] = ''>
<cfelse>
    <cfset post = mongonew('post')>
    <cfset post['tag'] = ''>
</cfif>
<cfset modework = 'new'>
<cfoutput>
    <div class="row-fluid">
        <legend class="titlepage">New html</legend>
        #startform(action='htmlscontroller')#
            #hiddenfield(name='mode',value='create')#
            <cfinclude template="htmlsform.cfm">
            #submit(label=get('labelbtnadd'))#
        #endform()#
    </div>
</cfoutput>
<cfinclude template="include/footer.cfm">