<cfinclude template="include/header.cfm">
<cfimport taglib="lib" prefix="ui">
<cfoutput>
<div class="row-fluid">
    <p class="headpage">Gallery</p>
    <div class="row-fluid">
        <ui:datatable field="#capitalize('thumbnail')#,#capitalize('title')#,#capitalize('date modified')#" size="150,0,150"/>
    </div>
</div>
<div class="modal hide fade bigModal" id="delModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Delete Gallery</h3>
    </div>
    <div class="modal-body">
        <p id="shownamedelete"></p>
        <h2>Please note</h2>
        <p>After you delete an gallery.It'll not collect usage data anymore.</p>
        <input type="hidden" id="iddelete" name="iddelete">
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <a onclick="confirmdelete('galleries');" class="btn btn-danger">OK</a>
    </div>
</div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<cfoutput>#javaScriptInclude("jquery.dataTables,DT_bootstrap,createdatatable,deletedialog")#</cfoutput>
<script type="text/javascript">
    var visitfirst = true;

    $(function(){
        inittable();
    })

    function inittable()
    {
        if(visitfirst == true)
        {
            visitfirst = false;
        }else{
            startpage = 0;
            clearall();
        }
        createdatatable('usetable','galleriesgrid','','3/desc');
    }

    function clearall()
    {
        oTable.fnDestroy();
    }

    function gonew()
    {
        window.location = "galleriesnew.cfm";
    }

    function goedit(id)
    {
        window.location = "galleriesupdate.cfm?key="+id+"&start="+oTable.fnSettings()._iDisplayStart;
    }
</script>
