<!---
  Created with IntelliJ IDEA.
  User: peerabumrung
  Date: 1/21/13
  Time: 2:50 PM
  To change this template use File | Settings | File Templates.
--->
<cfinclude template="system/loadplugins.cfm">
<cfset table = mongomodel("frontendmenu")/>
<cfset sortlabel = ""/>
<cfset sortvalue = 1/>
<cfset searchtxt = ""/>
<cfset listColumns = "NAME,MENUID,MENUCLASS" />
<cfif not isDefined('iSortCol_0')>
	<cfset iSortCol_0 = 0>
	<cfset sSortDir_0 = 1>
	<cfset sSearch = "">
	<cfset sEcho = 1>
	<cfset iDisplayStart =0>
	<cfset iDisplayLength = 10>
</cfif>
<cfset sortlabel = listGetAt(listColumns,(Val(iSortCol_0) + 1))/>
<cfif sSortDir_0 eq "desc">
	<cfset sortvalue = -1/>
</cfif>
<cfset sortst = structNew()>
<cfset sortst[sortlabel] = sortvalue>
<cfoutput>
	<cfif sSearch neq "">
		<cfset pattern = createObject('java', 'java.util.regex.Pattern')/>
		<cfset searchtxt = pattern.compile('\(.*|'&sSearch,pattern.CASE_INSENSITIVE)/>
	</cfif>
	<cfset defaultsearch = structnew()>
<!---	<cfset defaultsearch["CATEGORY"] = dbref('category',category)>--->
<!---	<cfset defaultsearch["USER"] = dbref('user',session['userid'])>--->
<!---	<cfset defaultsearch["TYPECONTENT"] = 1>--->
	<cfif sSearch neq "">
		<cfset searchst = structNew()>
		<cfset fieldarray = arraynew()>
		<cfset fieldsearch = structNew()>
		<cfset fieldsearch["NAME"] = searchtxt>
		<cfset fieldsearch["DESCRIPTION"] = searchtxt>
		<cfset fieldsearch["MENUCLASS"] = searchtxt>
		<cfset fieldsearch["MENUID"] = searchtxt>
		<cfset arrayappend(fieldarray,fieldsearch)>
		<cfset defaultsearch["$or"] = fieldarray>
<!---Filter text--->
		<cfset mydatatotal = MongoCollectioncount(datasource=application.applicationname,collection='frontendmenu',query=defaultsearch)>
		<cfset mydata = MongoCollectionFind(datasource=application.applicationname,collection='frontendmenu',query=defaultsearch,skip=iDisplayStart,size=iDisplayLength,sort=sortst)>
	<cfelse>
<!---No text filter--->

		<cfset mydatatotal = MongoCollectioncount(datasource=application.applicationname,collection="frontendmenu",query=defaultsearch)>
		<cfset mydata = MongoCollectionfind(datasource=application.applicationname,collection="frontendmenu",query=defaultsearch,skip=iDisplayStart,size=iDisplayLength,sort=sortst)>
	</cfif>
<!---    <cfset mydata = mydatatotal.skip(#val(params.iDisplayStart)#).limit(#val(params.iDisplayLength)#).sort({'#sortlabel#'=#sortvalue#})/>--->
	<cfsavecontent variable="sourcedatatable">
		{"sEcho": #val(sEcho)#,
	"iTotalRecords": #mydatatotal#,
	"iTotalDisplayRecords": #arraylen(mydata)#,
	"aaData": [
		<cfset i = 1/>
		<cfloop array="#mydata#" index="rowitem">
			[
			<cfloop list="#listColumns#" index="thisColumn">
				<cfif thisColumn neq listFirst(listColumns)>,</cfif>
				<cfif ListFind("createdat,updatedat",thisColumn,',') gt 0>
					"#jsStringFormat(DateFormat(rowitem[thisColumn],'dd/mm/yy'))#"
					<cfelse>
					"#jsStringFormat(checkdata(rowitem,thisColumn))#"
				</cfif>
			</cfloop>
			,"<a onclick=\"javascript:goedit('#rowitem['_id'].toString()#')\" href=\"javascript:void(0)\" class=\"btn\">#get('labeledit')#</a> <a onclick=\"javascript:deletedata('#rowitem['_id'].toString()#','#checkdata(rowitem,ListFirst(listColumns))#')\" href=\"javascript:void(0)\" class=\"btn\">#get('labeldelete')#</a></a>"]
			<cfif i neq arraylen(mydata)>,</cfif>
			<cfset i++/>
		</cfloop>]}
	</cfsavecontent>
	<cfoutput>
		#sourcedatatable#
	</cfoutput>
</cfoutput>

