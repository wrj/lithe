<cfinclude template="system/loadplugins.cfm" runonce="true">
<cfset structdelete(form,'FIELDNAMES')>
<cfif form['USERNAME'] neq '' && form['PASSWORD'] neq ''>
    <cfset settingrawfile = "#expandPath('./')#config/settingsraw.cfm">
    <cffile action="read" variable="settingraw" file="#settingrawfile#" charset="utf-8">
    <cfset settingraw = replaceNoCase(settingraw,'replaceapplicationfolder',form['APPLICATIONFOLDER'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replaceimageformat',form['IMAGEFORMAT'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replacevideoformat',form['VIDEOFORMAT'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replacefileformat',form['FILEFORMAT'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replacefilesizeimage',form['FILESIZEIMAGE'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replacefilesizevideo',form['FILESIZEVIDEO'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replacefilesizefile',form['FILESIZEFILE'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replacelabelbtnadd',form['LABELBTNADD'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replacelabelbtnedit',form['LABELBTNEDIT'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replacelabeledit',form['LABELEDIT'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replacelabeldelete',form['LABELDELETE'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replacelogaccess',form['LOGACCESS'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replaceweblanguage',form['LANGUAGE'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replaceshoppingcart',form['SHOPPINGCART'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replacetemplate',form['TEMPLATE'],'all')>
<!---    Site--->
    <cfset settingraw = replaceNoCase(settingraw,'replacesitetitle',form['SITETITLE'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replacesiteaddress',form['SITEADDRESS'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replacepathfrontend',form['PATHFRONTEND'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replacedateformat',form['DATEFORMAT'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replacetimeformat',form['TIMEFORMAT'],'all')>
<!---Email--->
	<cfset settingraw = replaceNoCase(settingraw,'replaceemailcontactaddress',form['EMAILCONTACTADDRESS'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replaceemailhost',form['SERVER'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replaceemailssl',form['EMAILSSL'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replaceemailport',form['EMAILPORT'],'all')>
    <cfset passmail = ''>
    <cfif form['EMAILPASSWORD'] neq ''>
        <cfset passmail = encrypt(form['EMAILPASSWORD'],form['EMAILUSERNAME'])>
        <cfset form['EMAILPASSWORD'] = passmail>
    </cfif>
    <cfset settingraw = replaceNoCase(settingraw,'replaceemailuser',form['EMAILUSERNAME'],'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replaceemailpass',passmail,'all')>
<!---Admin--->
    <cfset adminuser = encrypt(form['USERNAME'],form['APPLICATIONNAME'])>
    <cfset adminpass = encrypt(form['PASSWORD'],form['APPLICATIONNAME'])>
    <cfset form['USERNAME'] = adminuser>
    <cfset form['PASSWORD'] = adminpass>
    <cfset settingraw = replaceNoCase(settingraw,'replaceadminuser',adminuser,'all')>
    <cfset settingraw = replaceNoCase(settingraw,'replaceadminpass',adminpass,'all')>
<!---ImageDefaultSize--->
	<cfset settingraw = replaceNoCase(settingraw,'replaceslideshowwidth',form['SLIDESHOWWIDTH'],'all')>
	<cfset settingraw = replaceNoCase(settingraw,'replaceslideshowheight',form['SLIDESHOWHEIGHT'],'all')>
	<cfset settingraw = replaceNoCase(settingraw,'replacelargeimagewidth',form['LARGEIMAGEWIDTH'],'all')>
	<cfset settingraw = replaceNoCase(settingraw,'replacelargeimageheight',form['LARGEIMAGEHEIGHT'],'all')>
	<cfset settingraw = replaceNoCase(settingraw,'replacethumbnailwidth',form['THUMBNAILWIDTH'],'all')>
	<cfset settingraw = replaceNoCase(settingraw,'replacethumbnailheight',form['THUMBNAILHEIGHT'],'all')>
	<cfset settingraw = replaceNoCase(settingraw,'replacecategoryimagewidth',form['CATEGORYIMAGEWIDTH'],'all')>
	<cfset settingraw = replaceNoCase(settingraw,'replacecategoryimageheight',form['CATEGORYIMAGEHEIGHT'],'all')>
	<cfset settingraw = replaceNoCase(settingraw,'replacemenuimagewidth',form['MENUIMAGEWIDTH'],'all')>
	<cfset settingraw = replaceNoCase(settingraw,'replacemenuimageheight',form['MENUIMAGEHEIGHT'],'all')>
	<cfset settingraw = replaceNoCase(settingraw,'replaceotherimage',form['OTHERIMAGE'],'all')>
<!---Write File--->
    <cfset filesettingwrite = "#expandPath('./')#config/settings.cfm">
    <cffile action="write" nameconflict="overwrite" output="#settingraw#" file="#filesettingwrite#" charset="utf-8" mode="777">
<!---    Database--->
    <cfset databaserawfile = "#expandPath('./')#config/databaseraw.cfm">
    <cffile action="read" variable="databaseraw" file="#databaserawfile#" charset="utf-8">
    <cfset databaseraw = replaceNoCase(databaseraw,'replacedatabasename',form['DATABASENAME'],'all')>
    <cfset databaseraw = replaceNoCase(databaseraw,'replacedatabasehost',form['DATABASEHOST'],'all')>
    <cfset databaseraw = replaceNoCase(databaseraw,'replacedatabaseport',form['DATABASEPORT'],'all')>
    <cfset databaseraw = replaceNoCase(databaseraw,'replacedatabaseusername',form['DATABASEUSERNAME'],'all')>
    <cfset completereplacepass = ''>
    <cfif form['DATABASEPASSWORD'] neq ''>
        <cfset databasepasswordencrypt = encrypt(form['DATABASEPASSWORD'],form['APPLICATIONNAME'])>
        <cfset form['DATABASEPASSWORD'] = databasepasswordencrypt>
        <cfset completereplacepass = "##encrypt('#databasepasswordencrypt#',form['APPLICATIONNAME'])##">
    </cfif>
    <cfset databaseraw = replaceNoCase(databaseraw,'replacedatabasepassword',completereplacepass,'all')>
    <cfset filedatabasewrite = "#expandPath('./')#config/database.cfm">
    <cffile action="write" nameconflict="overwrite" output="#databaseraw#" file="#filedatabasewrite#" charset="utf-8" mode="777">
<!---Application--->
<cfoutput>
<cfsavecontent variable="app">
<replacecfset This.name = "#form.APPLICATIONNAME#">
<replacecfset This.Sessionmanagement="True">
<replacecfset This.Sessiontimeout="replacesharpcreatetimespan(0,1,0,0)replacesharp">
<replacecfset This.applicationtimeout="replacesharpcreatetimespan(5,0,0,0)replacesharp">
</cfsavecontent>
</cfoutput>
    <cfset app = replaceNoCase(app,'replacesharp','##','all')>
    <cfset app = replaceNoCase(app,'replacecfset','cfset','all')>
    <cfset fileappwrite = "#expandPath('./')#config/app.cfm">
    <cffile action="write" nameconflict="overwrite" output="#app#" file="#fileappwrite#" charset="utf-8" mode="777">
<!---Create settingjs--->
<cfoutput>
<cfsavecontent variable="settingjstext">
function seturlbase(){
    return '/#form['applicationfolder']#'
}
</cfsavecontent>
</cfoutput>
    <cfset settingjswrite = "#expandPath('./')#js/settingjs.js">
    <cffile action="write" nameconflict="overwrite" output="#settingjstext#" file="#settingjswrite#" charset="utf-8" mode="777">
<!---Create install.log--->
    <cfset filelogwrite = "#expandPath('./')#config/install.log">
    <cffile action="append" nameconflict="overwrite" output="update at #now()#" file="#filelogwrite#" charset="utf-8" mode="777">
    <cfif MongoIsvalid(application.applicationname)>
        <cfset MongoDeregister(application.applicationname)>
		<cftry>
	        <cfif form['DATABASEUSERNAME'] neq '' AND form['DATABASEPASSWORD'] neq ''>
	            <cfif form['DATABASEPORT'] neq ''>
	                <cfset MongoRegister( name=form['APPLICATIONNAME'], server=form['DATABASEHOST'], db=form['DATABASENAME'],port=form['DATABASEPORT'],username=form['DATABASEUSERNAME'],password=form['DATABASEPASSWORD'])>
                <cfelse>
	                <cfset MongoRegister( name=form['APPLICATIONNAME'], server=form['DATABASEHOST'], db=form['DATABASENAME'],username=form['DATABASEUSERNAME'],password=form['DATABASEPASSWORD'])>
	            </cfif>
            <cfelse>
	            <cfif form['DATABASEPORT'] neq ''>
	                <cfset MongoRegister( name=form['APPLICATIONNAME'], server=form['DATABASEHOST'], db=form['DATABASENAME'],port=form['DATABASEPORT'])>
                <cfelse>
	                <cfset MongoRegister( name=form['APPLICATIONNAME'], server=form['DATABASEHOST'], db=form['DATABASENAME'])>
	            </cfif>
	        </cfif>
	        <cfcatch type="Any">
	
	        </cfcatch>
	    </cftry>
    </cfif>
    <cfset application.applicationname = form['APPLICATIONNAME']>
    <cfset MongoCollectiondrop(application.applicationname,'setting')>
    <cfset MongoCollectioninsert(application.applicationname,'setting',form)>
<!---Genconfig file--->
    <cfset configfile = "#expandPath('/')#cfc/config.json">
    <cfset sitesettingdata = MongoCollectionfindone(application.applicationname,'setting',{})>
<!--- Pattern File --->
    <cfset pfile = "#expandPath('/')#cfc/config.cfm">
    <cfsavecontent variable="patternfiledata">
        <cfoutput>
        cfreplaceset patternurl = "#sitesettingdata['linkpattern']#">
        cfreplaceset otherpatternurl = "#sitesettingdata['otherlinkpattern']#">
        cfreplaceset rewritepatternurl = "#sitesettingdata['rewritepattern']#">
        </cfoutput>
    </cfsavecontent>
    <cfset patternfiledata = replaceNoCase(patternfiledata, "cfreplaceset", "<cfset", 'all')>
    <cffile action="write" charset="utf-8" mode="777" nameconflict="overwrite" output="#patternfiledata#" file="#pfile#">
    <!--- End pattern file --->
    <cfset configdata = structNew()>
<!---General--->
    <cfset configdata["environment"] = sitesettingdata['environment']>
    <cfset generaldata = structNew()>
    <cfset generaldata["dateformat"] = sitesettingdata['DATEFORMAT']>
    <cfset generaldata["timeformat"] = sitesettingdata['TIMEFORMAT']>
    <cfset generaldata["title"] = sitesettingdata['SITETITLE']>
    <cfset generaldata["siteaddress"] = sitesettingdata['SITEADDRESS']>
    <cfset generaldata["pathfrontend"] = sitesettingdata['PATHFRONTEND']>
    <cfset generaldata["metadescription"] = sitesettingdata['METADESCRIPTION']>
    <cfset generaldata["metakeyword"] = sitesettingdata['METAKEYWORD']>
    <cfset generaldata["language"] = sitesettingdata["LANGUAGE"]>
    <cfset configdata["general"] = generaldata>
<!---Database--->
    <cfset dbdata = structNew()>
    <cfset dbdata['databasename'] = sitesettingdata['DATABASENAME']>
    <cfset dbdata['databasehost'] = sitesettingdata['DATABASEHOST']>
    <cfset dbdata['databaseport'] = sitesettingdata['DATABASEPORT']>
    <cfset dbdata['databaseusername'] = sitesettingdata['DATABASEUSERNAME']>
    <cfset databasepass = ''>
    <cfif sitesettingdata['DATABASEPASSWORD'] neq ''>
        <cfset dbpassde = decrypt(sitesettingdata['DATABASEPASSWORD'],application.applicationname)>
        <cfset databasepass = encrypt(dbpassde,sitesettingdata['DATABASENAME'])>
    </cfif>
    <cfset dbdata['DATABASEPASSWORD'] = databasepass>
    <cfset configdata["database"] = dbdata>
<!---Email--->
    <cfset emaildata = structNew()>
    <cfset emaildata["contactaddress"] = sitesettingdata['EMAILCONTACTADDRESS']>
    <cfset emaildata["server"] = sitesettingdata['SERVER']>
    <cfset emaildata["port"] = val(sitesettingdata['EMAILPORT'])>
    <cfset emaildata["ssl"] = sitesettingdata['EMAILSSL']>
    <cfset emaildata["username"] = sitesettingdata['EMAILUSERNAME']>
    <cfset emaildata["password"] = sitesettingdata['EMAILPASSWORD']>
    <cfset configdata["email"] = emaildata>
<!---Facebook--->
    <cfset fbdata = structNew()>
    <cfset fbdata["appid"] = sitesettingdata['FACEBOOKAPPID']>
    <cfset fbdata["secretkey"] = sitesettingdata['FACEBOOKSECRET']>
    <cfset fbdata["adminid"] = sitesettingdata['FACEBOOKADMINID']>
    <cfset fbdata["language"] = sitesettingdata['FACEBOOKLANGUAGE']>
    <cfset fbdata["secretkey"] = sitesettingdata['FACEBOOKSECRET']>
    <cfset fbdata["url"] = sitesettingdata['FACEBOOK']>
    <cfset configdata["facebook"] = fbdata>
<!---Google--->
    <cfset gdata = structNew()>
    <cfset gdata["apikey"] = sitesettingdata['GOOGLEAPIKEY']>
    <cfset gdata["plus"] = sitesettingdata['GOOGLEPLUS']>
    <cfset gdata["youtube"] = sitesettingdata['YOUTUBE']>
    <cfset gdata["analytics"] = sitesettingdata['GOOGLEANALYTICS']>
    <cfset configdata["google"] = gdata>
<!---Recaptcha--->
    <cfset recapdata = structNew()>
    <cfset recapdata["publickey"] = sitesettingdata['PUBLICKEY']>
    <cfset recapdata["privatekey"] = sitesettingdata['PRIVATEKEY']>
    <cfset configdata["recaptcha"] = recapdata>
<!---Integration--->
    <cfset integrationdata = structNew()>
    <cfset integrationdata["twitter"] = sitesettingdata['TWITTER']>
    <cfset integrationdata["linkedin"] = sitesettingdata['LINKEDIN']>
    <cfset integrationdata["flickr"] = sitesettingdata['FLICKR']>
    <cfset configdata["integration"] = integrationdata>
    <cffile action="write" charset="utf-8" mode="777" nameconflict="overwrite" output="#serializeJSON(configdata)#" file="#configfile#">
<!---End Genconfig file--->
    <cfset flashinsert('success','Update setting complete')>
    <cfif form["oldapplicationname"] eq form["applicationname"]>
        <cflocation url="setting.cfm?reload=true" addtoken="false">    
    <cfelse>
        <cflocation url="setting.cfm" addtoken="false">
    </cfif>
    
<cfelse>
    <cfset flashinsert('error','There was an error create the setting.')>
    <cflocation url="setting.cfm" addtoken="false">
</cfif>