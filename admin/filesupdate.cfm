<cfinclude template="include/header.cfm">
<cfif isdefined('ERROR')>
    <cfset file = getstoredata()>
    <cfif structIsEmpty(file)>
        <cfset file = mongonew('file')>
    </cfif>
<cfelse>
    <cfset objdata = MongoCollectionfindone(application.applicationname,'file',{"_id"= newid(key)})>
    <cfset file = mongonew("file",objdata)>
</cfif>
<cfoutput>
<div class="row-fluid">
    <legend>Edit file - #file['filename']#</legend>
    #startform(action='filescontroller')#
    #hiddenfield(name='mode',value='update')#
    #hiddenfield(name='key',value=key)#
	#hiddenfield(name='displaystart',value=start)#
    #textfield(name='filename',label='filename',value=file['filename'],readonly=true)#
    #hiddenfield(name='oldfilename',value=file['NEWFILENAME'])#
    #hiddenfield(name='newfilename')#
    #genUpload('#get('filesizefile')#','uploadprocess','file')#
    #button(label='Upload',onclick='doupload()')#
    #endform()#
</div>
</cfoutput>
<script type="text/javascript">
    function upfilecomplete(serverfile,clientfile)
    {
        $('#newfilename').val(serverfile);
        $('form').submit();
    }
    function doupload()
    {
        $('#file_upload').uploadifive('upload')
    }
</script>
<cfinclude template="include/footer.cfm">
<!--- BACK BUTTON DETECT --->
<script type="text/javascript">
history.pushState({page: 123}, "update", "");
var _firstload = true;
var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
var isIE = /*@cc_on!@*/false;                            // At least IE6
if (isFirefox){_firstload = false;}
		window.onpopstate = function(e){
		if(_firstload){_firstload = false;}
else{_firstload = false;
	<cfoutput>window.location.href = 'files.cfm?start=#start#';</cfoutput>}
};
</script>