<cfoutput>
<!--- error, warning, notice --->
    <cfif flashKeyExists("success")>
        <div class="alert alert-success">
        #flash("success")#
        </div>
    </cfif>
    <cfif flashKeyExists("error")>
        <cfset ERROR = true>
        <div class="alert alert-error">
        #flash("error")#
        #getstoredataerror()#
        </div>
    </cfif>
</cfoutput>

