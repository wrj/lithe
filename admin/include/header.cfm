<cfinclude template="../system/loadplugins.cfm">
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <cfinclude template="stylesheet.cfm">
    <title><cfoutput>#application.applicationname#</cfoutput></title>
    <cfinclude template="javascript.cfm">
</head>
<body>
<cfinclude template="headermenu.cfm"/>
<div class="container">
    <div class="row-fluid" id="flash-session">
        <cfinclude template="showFlash.cfm">
    </div>