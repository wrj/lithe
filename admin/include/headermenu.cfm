<div class="navbar navbar-inverse navbar-fixed-top">
<div class="navbar-inner">
<div class="container">
    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>
<!---            <a class="brand" href="#">SKOODECMS</a>--->
<cfoutput>
    <cfset routename = 'login'>
</cfoutput>
<div class="nav-collapse collapse">
<cfif structKeyExists(session,'username')>
    <cfinclude template="#session['rule']#menu.cfm">
</cfif>
</div>
<cfif structKeyExists(session,'username') && session['username'] neq ''>
    <ul class="nav pull-right">
        <li class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="icon-user icon-white"></i>
            <cfoutput>
                #session['username']#
            </cfoutput>
            <b class="caret"></b>
        </a>
            <ul class="dropdown-menu">
                <cfif session['rule'] neq 'superadmin'>
                    <cfoutput>
                    <li>#linkto(text='profile',action='profile')#</li>
                    </cfoutput>
                </cfif>
                <li><a href="javascript:void(0)" onclick="dologout()">Logout</a></li>
            </ul>
        </li>
    </ul>
    <ul class="nav pull-right">
        <li class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="icon-flag icon-white"></i>
        <cfoutput>
            #session['language']#
        </cfoutput>
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
        <cfloop list="#get('weblanguage')#" delimiters="," index="lang">
            <cfoutput>
                    <li><a href="javascript:void(0)" onclick="changelang('#lang#')">#lang#</a></li>
            </cfoutput>
        </cfloop>
        </ul>
        </li>
    </ul>
</cfif>
</div>
</div>
</div>
<script type="text/javascript">
    function changelang(lang)
    {
        $.ajax({
            url:seturlbase()+"/changelang.cfm",
            data: { language: lang,format:'json'},
            success: function(data) {
                location.reload();
            }
        })
    }
</script>
