<ul class="nav" xmlns="http://www.w3.org/1999/html">
    <li class="dropdown">
        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Post<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a href="postsnew.cfm">Add Post</a></li>
            <li><a href="posts.cfm">List Post</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">HTML<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a href="htmlsnew.cfm">Add HTML</a></li>
            <li><a href="htmls.cfm">List HTML</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Gallery<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a href="galleriesnew.cfm">Add Gallery</a></li>
            <li><a href="galleries.cfm">List Gallery</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Slide<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a href="slidesnew.cfm">Add Slide</a></li>
            <li><a href="slides.cfm">List Slide</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Category<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a href="categoriesnew.cfm">Add Category</a></li>
            <li><a href="categories.cfm">List Category</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Assets<b class="caret"></b></a>
        <ul class="dropdown-menu">
	        <li class="dropdown-submenu">
		        <a tabindex="-1" href="#">Category Image</a>
		        <ul class="dropdown-menu">
			        <li><a href="categoriesimagesnew.cfm">Add Category Image</a></li>
			        <li><a href="categoriesimages.cfm">List Category Image</a></li>
		        </ul>
	        </li>
            <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Image</a>
                <ul class="dropdown-menu">
                    <li><a href="imagesnew.cfm">Add Image</a></li>
                    <li><a href="images.cfm">List Image</a></li>
                </ul>
            </li>
            <li class="dropdown-submenu">
                <a tabindex="-1" href="#">Video</a>
                <ul class="dropdown-menu">
                    <li><a href="videosnew.cfm">Add Video</a></li>
                    <li><a href="videos.cfm">List Video</a></li>
                </ul>
            </li>
            <li class="dropdown-submenu">
                <a tabindex="-1" href="#">File</a>
                <ul class="dropdown-menu">
                    <li><a href="filesnew.cfm">Add File</a></li>
                    <li><a href="files.cfm">List File</a></li>
                </ul>
            </li>
        </ul>
    </li>
    <li>
        <a href="contactus.cfm">Contact</a>
    </li>
    <cfif session['cart'] eq 'on'>
    <li>
        <a href="orders.cfm">Order</a>
    </li>
    </cfif>
    <cfif fileExists("#expandPath('./')#include/usermenucustom.cfm")>
        <cfinclude template="usermenucustom.cfm">
    </cfif>
    <li>
        <a href="userssetting.cfm">Setting</a>
    </li>
</ul>