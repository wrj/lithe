<!---
  Created with IntelliJ IDEA.
  User: arunwatd
  Date: 1/14/13
  Time: 3:27 PM
  To change this template use File | Settings | File Templates.
--->
<cfinclude template="system/loadplugins.cfm">
<cfset structDelete(form,'FIELDNAMES')>
<cfswitch expression="#mode#">
    <cfcase value="create">
        <cfset result = mongovalidate('contactus','create',form)>
        <cfif result['result'] eq true>
            <cfset MongoCollectioninsert(application.applicationname,'contactus',result['value'])>
            <cfset flashinsert('success','Create contactus complete')>
            <cflocation url="contactus.cfm" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error creating the contactus.')>
            <cfset storedata(result)>
            <cflocation url="contactusnew.cfm" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="update">
        <cfset keyid = key>
        <cfset structDelete(form,'KEY')>
        <cfset structDelete(form,'MODE')>
        <cfset result = mongovalidate('contactus','update',form)>
        <cfif result['result'] eq true>
            <cfset olddata = MongoCollectionfindone(application.applicationname,'contactus',{'_id'=newid(keyid)})>
            <cfset dataupdate = mongomapvalue('contactus',olddata,result['value'],'default')>
            <cfset MongoCollectionsave(application.applicationname,'contactus',dataupdate)>
            <cfset flashinsert('success','Update contactus complete')>
            <cflocation url="contactus.cfm" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error update the contactus.')>
            <cfset storedata(result)>
            <cflocation url="contactusupdate.cfm?key=#keyid#" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="delete">
        <cfset MongoCollectionremove(application.applicationname,'contactus',{'_id'=newid(key)})>
<!---        <cfset flashinsert('success','Delete contactus complete')>--->
        <cfset result = structNew()>
        <cfset result['result'] = 'true'>
        <cfset result['message'] = "Delete contactus complete">
        <cfoutput>
            #serializeJSON(result)#
        </cfoutput>
    </cfcase>

</cfswitch>