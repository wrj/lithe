<cfinclude template="system/loadplugins.cfm">
<cfset structDelete(form,'FIELDNAMES')>
<cfswitch expression="#mode#">
    <cfcase value="create">
        <cfset result = mongovalidate('user','create',form)>
        <cfif result['result'] eq true>
            <cfset result['value']['SECRET'] = "S#replaceNoCase(createuuid(),'-','','all')#">
            <cfif session['rule'] eq 'admin'>
                <cfset result['value']['OWNER'] = dbref('user',session['userid'])>
                <cfset result['value']['RULE'] = 'user'>
            </cfif>
            <cfset userid = MongoCollectioninsert(application.applicationname,'user',result['value'])>
            <cfset categorydata = mongonew('category')>
            <cfset categorydata['TITLE'] = result['value']['username']>
            <cfset categorydata['TEMPLATE'] = get('template')>
            <cfset categorydata['USER'] = dbref('user',userid.toString())>
            <cfset MongoCollectioninsert(application.applicationname,'category',categorydata)>
            <cfset usersetting = mongonew('usersetting')>
            <cfset usersetting['FACEBOOK'] = ''>
            <cfset usersetting['TWITTER'] = ''>
            <cfset usersetting['GOOGLEPLUS'] = ''>
            <cfset usersetting['LINKEDIN'] = ''>
            <cfset usersetting['USER'] = dbref('user',userid.toString())>
            <cfset MongoCollectioninsert(application.applicationname,'usersetting',usersetting)>
            <cfset flashinsert('success','Create user complete')>
            <cflocation url="users.cfm" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error creating the user.')>
            <cfset storedata(result)>
            <cflocation url="usersnew.cfm" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="update">
        <cfset keyid = key>
	    <cfset displaystart = form['DISPLAYSTART']>
        <cfset structDelete(form,'KEY')>
        <cfset structDelete(form,'MODE')>
	    <cfset structDelete(form,'DISPLAYSTART')>
        <cfset result = mongovalidate('user','update',form)>
        <cfif result['result'] eq true>
            <cfset olddata = MongoCollectionfindone(application.applicationname,'user',{'_id'=newid(keyid)})>
            <cfset oldpassword = olddata['password']/>
            <cfset dataupdate = mongomapvalue('user',olddata,result['value'],'default')>
            <cfif dataupdate['PASSWORD'] eq "">
                <cfset dataupdate['PASSWORD'] = oldpassword>
            </cfif>
            <cfset dataupdate['SECRET'] = "S#replaceNoCase(createuuid(),'-','','all')#">
	        <cfset dataupdate["OWNER"] = dbref('user',dataupdate["OWNER"].fetch()["_id"].toString())>
	        <cfset MongoCollectionsave(application.applicationname,'user',dataupdate)>
            <cfset flashinsert('success','Update user complete')>
            <cflocation url="users.cfm?start=#displaystart#" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error update the user.')>
            <cfset storedata(result)>
            <cflocation url="usersupdate.cfm?key=#keyid#" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="delete">
        <cfset userref = structNew()>
        <cfset userref['USER'] = dbref('user',key)>
<!---        <cfset ownerref = structNew()>--->
<!---        <cfset ownerref['OWNER'] = dbref('user',key)>--->
<!---        <cfset userdata = MongoCollectionfindone(application.applicationname,'user',{"_id"= newid(key)})>--->
<!---        <cfset MongoCollectionremove(application.applicationname,'category',userref)>--->
        <cfset MongoCollectionremove(application.applicationname,'usersetting',userref)>
<!---        <cfset MongoCollectionremove(application.applicationname,'post',userref)>--->
<!---        <cfset MongoCollectionremove(application.applicationname,'video',userref)>--->
<!---        <cfset MongoCollectionremove(application.applicationname,'file',userref)>--->
<!---        <cfset MongoCollectionremove(application.applicationname,'image',userref)>--->
<!---        <cfset MongoCollectionremove(application.applicationname,'contactus',userref)>--->
<!---        <cfset MongoCollectionremove(application.applicationname,'order',{"SECRETKEY"=userdata['SECRET']})>--->
<!---        <cfset MongoCollectionremove(application.applicationname,'user',ownerref)>--->
<!---        <cfset filedel = get('pathupload')&userdata['USERNAME']>--->
<!---        <cfdirectory action="delete" directory="#filedel#" type="all" recurse="true">--->
        <cfset MongoCollectionremove(application.applicationname,'user',{'_id'=newid(key)})>
        <cfset result = structNew()>
        <cfset result['result'] = 'true'>
        <cfset result['message'] = "Delete user complete">
        <cfoutput>
            #serializeJSON(result)#
        </cfoutput>
    </cfcase>

</cfswitch>