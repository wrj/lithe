<!---
  Created with IntelliJ IDEA.
  User: peerabumrung
  Date: 1/21/13
  Time: 5:22 PM
  To change this template use File | Settings | File Templates.
--->
<cfinclude template="include/header.cfm">
<cfoutput>
	<link type="text/css" rel="stylesheet" media="all" href="stylesheets/frontendmenu/style.css">
	#javaScriptInclude("jquery.mjs.nestedSortable,math.uuid,frontendmenu/coffee/manage-menu,frontendmenu/coffee/add-menu,frontendmenu/coffee/save-menu,frontendmenu/coffee/clear-menu,frontendmenu/coffee/remove-menu,frontendmenu/coffee/remove-menu,frontendmenu/coffee/update-menu")#
</cfoutput>
<cfoutput>
<cfset parentid = '0'>
<cfif isdefined('ERROR')>
	<cfset frontendmenu = getstoredata()>
	<cfif structIsEmpty(frontendmenu)>
		<cfset frontendmenu = mongonew('frontendmenu')>
	</cfif>
<cfelse>
	<cfset objdata = MongoCollectionfindone(application.applicationname,'frontendmenu',{"_id"= newid(key)})>
	<cfset frontendmenu = mongonew("frontendmenu",objdata)>
<!---	<cfset frontendmenu=objdata>--->
	<cfset frontendmenu['id']= objdata['_id'].toString()>
	<cfset menudata = DeserializeJSON(frontendmenu['menu'])>
</cfif>
<!---All Data--->
</cfoutput>
<cfoutput>
	<div class="row-fluid">
	<legend class="titlepage">Edit Menu - #frontendmenu['NAME']#</legend>
	#startform(action='frontendmenuscontroller',class="form-horizontal")#
	#hiddenfield(name='mode',value='update')#
	#hiddenfield(name='key',value=key)#
	#hiddenfield(name='displaystart',value=start)#
	<input type="hidden" class="menu-mode" value="update" name="MENUMODE">
<!--- --------------------------------------- form --------------------------------- --->
	<div id="mainForm">
		<div id="main-title">
			<div class="head-name">
				<span class="bold">Menu </span>: #frontendmenu['NAME']#
			</div>
			<i class="form-up-down icon-chevron-up main-icon-up-down"></i>
			<div class="clear"></div>
		</div>
		<div class="main-form-hide-show">
			<div class="control-group">
				<label class="control-label">Name</label>
				<div class="controls">
					<input type="text" class="span11 menuName" readonly="readonly" value="#frontendmenu['NAME']#" name="NAME" id="mainName">
				</div>
			</div>
<!---			#textField(name='NAME',value=frontendmenu['NAME'], label='Name' ,id='mainName')#--->
			#textArea(rows=3,class="menuDescription span11",name='DESCRIPTION',value=frontendmenu['DESCRIPTION'], label='Description')#
			#textField(name='MENUID',class="menuId span11",value=frontendmenu['MENUID'], label='Menu CSS ID')#
			#textField(name='MENUCLASS',class="menuClass span11",value=frontendmenu['MENUCLASS'], label='Menu CSS Class')#
			<input id="Menu" type="hidden" value='#frontendmenu['MENU']#' name="MENU" class='menuItem'>
			<input id="issuccess" type="hidden" value='no' name="ISSUCCESS" class='issuccess'>
		</div>
		<cfinclude template="imagedefaultsize.cfm">
        <div class="control-group">
	        <div class="controls">
		        <div class="span11">
					<!---   slideshow   largeimage  thumbnail   categoryimage   menuimage   otherimage	--->
                   #$showimagedefaultsize("menuimage")#
		        </div>
	        </div>
        </div>
		<div class="control-group">
			<div class="controls">
				<div id="mainAction">
					<div class="actionBlock">
						<form action="savemenu.cfm" method="post" id="saveform">
							<input type="submit" value="Save Menu" id="saveMenu" class="btn btn-primary">
						</form>
					</div>

					<div class="actionBlock">
							<input type="button" value="Update Menu" id="updateMenu" class="btn">
					</div>

					<div class="actionBlock">
						<input type="button" value="Clear Menu" id="clearMenu" class="btn">
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="menuitem-lebel">Menu Item</label>
			<div class="controls">
				<div class="actionBlock-right">
					<input type="button" value="Add Menu Item" id="addMenu" class="btn">
				</div>
			</div>
		</div>
	</div>
<!---   ------------------------------------- /form -------------------------------- --->
	#endform()#
	</div>
<!--- แสดงเมนูทั้งหมด --->
	<div class="mainMenu">
		<cfset data = DeserializeJSON(frontendmenu['MENU'])>
		<ol class="sortable">
		<cfinclude template="frontendmenusgenerate.cfm">
			#generate(data)#
		</ol>
	</div>
<!--- /แสดงเมนูทั้งหมด --->
    <input type="hidden" class="imagepath" value="#get('pathuploadfolder')#image">
</cfoutput>
<cfinclude template="include/footer.cfm">
<!--- ---------------------------------------- ส่วน มอดอล -------------------------------------- --->
<div class="modal hide fade bigModal" id="bModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Browse</h3>
	</div>
	<div class="modal-body-bModal"></div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<a onclick="selectobject()" class="btn btn-primary selectobject">OK</a>
	</div>
</div>

<!---    Preview Thumb--->
<div class="modal hide fade" id="pModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Preview Image</h3>
    </div>
    <div class="modal-body" id="previewimage">
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>
<cfinclude template="postsjsconfig.cfm">
<script type="text/javascript">
	var typeopen = '';
	$(function(){
		$('.add-link').live('click', function(e) {
			thisclass=$(this);
            typeopen = 'link';
            linkname = $(this).parent().parent().children().first().find('.linkname').val();
            linkurl = $(this).parent().parent().children().first().find('.linkurl').val();
			categoryid= $(this).parent().parent().children().first().find('.categoryid').val();
            var linktype = $(this).parent().parent().children().first().find('.linktype').val();
			if (linktype == ""){
				linktype = 1
			}
            browsewindow("Custom",linkname,linkurl,linktype,categoryid);
			$("#modalstatus").val("content");
		});
        $('.attach-image').live('click', function(e) {
            thisclass=$(this);
            typeopen = 'image';
            browsewindow('image');
            $("#modalstatus").val("image");
        });
        $(".menuitem-img").live('click',function(e){
            imagepath = $('.imagepath').val()
            imgesurl=$(this).parent().parent().parent().find(".imagefile").val();
            previewimage(imagepath+'/'+imgesurl);
        })
        $(".remove-image").live('click',function(e){
            $(this).parent().parent().find(".imagefile").val("")
            $(this).parent().parent().find(".imagename").val("")
            $(this).parent().parent().parent().parent().parent().parent().find(".menu-thumbnail").html('<div class="menu-thumbnail-noimage">No Image</div>')
        })
	});
	function browsewindow(type,linkname,linkurl,pagevalue,category){
		$(".modal-body-bModal").html('<iframe id="modalIframeId" width="720" height="620" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" />');
		$("#modalIframeId").attr("src",seturlbase()+"/browse.cfm?type="+type+"&linkname="+linkname+"&linkurl="+linkurl+"&typelink="+pagevalue+"&category="+category);
		$('#bModal').modal('show');
	}
	$('#bModal').on('hidden', function () {
		$(".modal-body-bModal").html('');
	});
	function selectedcomplete(fileraw)
	{
		var filearr = fileraw[0].split("|");
		var linkinsert = '';
		if (typeopen == 'image')
		{
            var filearr = fileraw[0].split("|");
            var imagepath = $('.imagepath').val()+'/'+filearr[0];
            var imagehtml = '<a class="icon-zoom-in pull-right" href="javascript:previewimage(\''+imagepath+'\')"></a><img src="'+imagepath+'" alt="'+filearr[1]+'" class="menuitem-img img-thumb img-rounded">'
            $(thisclass).parent().parent().find(".imagefile").val(filearr[0]);
            $(thisclass).parent().parent().find(".imagename").val(filearr[1]);
            $(thisclass).parent().parent().parent().parent().parent().parent().find(".menu-thumbnail").html(imagehtml);
        }else if (typeopen == 'link'){
            $(thisclass).parent().parent().find(".linkurl").val(filearr[0]);
            $(thisclass).parent().parent().find(".linkname").val(filearr[1]);
            if(filearr.length>3){
                $(thisclass).parent().parent().find(".categoryid").val(filearr[2]);
                $(thisclass).parent().parent().find(".linktype").val(filearr[3]);
            }else{
                $(thisclass).parent().parent().find(".categoryid").val("");
                $(thisclass).parent().parent().find(".linktype").val(filearr[2]);
            }
		}
	}
	function selectobject()
	{
		document.getElementById('modalIframeId').contentWindow.selectfile();
	}
    function previewimage(imagename)
    {
        var pathpreview = '<div class="thumbnail"><img src="'+imagename+'"/></div>';
        $('#previewimage').html(pathpreview);
        $('#pModal').modal('show');
    }
</script>
<!--- BACK BUTTON DETECT --->
<script type="text/javascript">
history.pushState({page: 123}, "update", "");
var _firstload = true;
var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
var isIE = /*@cc_on!@*/false;                            // At least IE6
if (isFirefox){_firstload = false;}
		window.onpopstate = function(e){
		if(_firstload){_firstload = false;}
else{_firstload = false;
	<cfoutput>window.location.href = 'frontendmenus.cfm?start=#start#';</cfoutput>}
};
</script>