<cfinclude template="include/header.cfm" runonce="true">

<link rel="stylesheet" type="text/css" href="markitup/skins/markitup/style.css" />
<link rel="stylesheet" type="text/css" href="markitup/sets/htmlmark/style.css" />
<script type="text/javascript" src="markitup/jquery.markitup.js"></script>
<script type="text/javascript" src="markitup/sets/htmlmark/set.js"></script>

<cfset setting = MongoCollectionfindone(application.applicationname,'setting',{})>
<cfif structKeyExists(setting, "environment") eq false>
    <cfset setting['environment'] = "mock">
</cfif>
<!---Get File xml--->
<cfset pathfile = expandPath('/')>
<cfif setting['PATHFRONTEND'] neq '/' OR setting['PATHFRONTEND'] neq ''>
    <cfset pathfile = "#pathfile##setting['PATHFRONTEND']#">
</cfif>
<cfset xmlfile = "#pathfile#/sitemap.xml">
<cfset lastmodified = ''>
<cfif fileExists(xmlfile)>
    <cfset datemo = GetFileInfo(xmlfile)['lastmodified']>
    <cfset lastmodified = "#dateformat(datemo,setting['DATEFORMAT'])# #timeFormat(datemo,setting['TIMEFORMAT'])#">
</cfif>
<!---environment--->
<cfset environmentq = arraynew()>
<cfset environmentst = structNew()>
<cfset environmentst['id'] = 'mock'>
<cfset environmentst['text'] = 'mock'>
<cfset arrayappend(environmentq,environmentst)>
<cfset environmentst = structNew()>
<cfset environmentst['id'] = 'product'>
<cfset environmentst['text'] = 'product'>
<cfset arrayappend(environmentq,environmentst)>
<!---Log--->
<cfset logq = arraynew()>
<cfset logst = structNew()>
<cfset logst['id'] = 'off'>
<cfset logst['text'] = 'disable'>
<cfset arrayappend(logq,logst)>
<cfset logst = structNew()>
<cfset logst['id'] = 'on'>
<cfset logst['text'] = 'enable'>
<cfset arrayappend(logq,logst)>
<!---Shopping Cart--->
<cfset shoppingq = arraynew()>
<cfset shoppingst = structNew()>
<cfset shoppingst['id'] = 'off'>
<cfset shoppingst['text'] = 'disable'>
<cfset arrayappend(shoppingq,shoppingst)>
<cfset shoppingst = structNew()>
<cfset shoppingst['id'] = 'on'>
<cfset shoppingst['text'] = 'enable'>
<cfset arrayappend(shoppingq,shoppingst)>
<!---ssl Array--->
<cfset sslq = arraynew()>
<cfset ssls = structNew()>
<cfset ssls['id'] = 'true'>
<cfset ssls['title'] = 'true'>
<cfset arrayAppend(sslq,ssls)>
<cfset ssls = structNew()>
<cfset ssls['id'] = 'false'>
<cfset ssls['title'] = 'false'>
<cfset arrayAppend(sslq,ssls)>
<!---Date Array--->
<cfset dformat = arraynew()>
<cfset dformatst = structNew()>
<cfset dformatst['id'] = 'mmmm dd,yyyy'>
<cfset dformatst['title'] = dateFormat(now(),'mmmm dd,yyyy')>
<cfset arrayAppend(dformat,dformatst)>
<cfset dformatst = structNew()>
<cfset dformatst['id'] = 'yyyy/mm/dd'>
<cfset dformatst['title'] = dateFormat(now(),'yyyy/mm/dd')>
<cfset arrayAppend(dformat,dformatst)>
<cfset dformatst = structNew()>
<cfset dformatst['id'] = 'dd/mm/yyyy'>
<cfset dformatst['title'] = dateFormat(now(),'dd/mm/yyyy')>
<cfset arrayAppend(dformat,dformatst)>
<!---Time Array--->
<cfset tformat = arraynew()>
<cfset tformatst = structNew()>
<cfset tformatst['id'] = 'H:mm'>
<cfset tformatst['title'] = timeFormat(now(),'H:mm')>
<cfset arrayAppend(tformat,tformatst)>
<cfset tformatst = structNew()>
<cfset tformatst['id'] = 'h:mm tt'>
<cfset tformatst['title'] = timeFormat(now(),'h:mm tt')>
<cfset arrayAppend(tformat,tformatst)>
<cfoutput>
    <div class="row-fluid">
        <legend>Setting</legend>
        #startform(action='settingprocess')#
        <ul class="nav nav-tabs" id="tabs">
            <li class="active"><a href="##tabs-general" data-toggle="tab">General</a></li>
            <li><a href="##tabs-site" data-toggle="tab">Site</a></li>
            <li><a href="##tabs-email" data-toggle="tab">Email</a></li>
            <li><a href="##tabs-asset" data-toggle="tab">Asset</a></li>
            <li><a href="##tabs-label" data-toggle="tab">Label</a></li>
            <li><a href="##tabs-database" data-toggle="tab">Database</a></li>
            <li><a href="##tabs-seo" data-toggle="tab">Seo</a></li>
            <li><a href="##tabs-facebook" data-toggle="tab">Facebook</a></li>
            <li><a href="##tabs-google" data-toggle="tab">Google</a></li>
            <li><a href="##tabs-recaptcha" data-toggle="tab">reCaptcha</a></li>
            <li><a href="##tabs-integration" data-toggle="tab">Integration</a></li>
            <li><a href="##tabs-sitemap" data-toggle="tab">Site map</a></li>
            <li><a href="##tabs-sampledata" data-toggle="tab">Sample data</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tabs-general">
                #textfield(name='applicationfolder',label='application folder',value=setting['APPLICATIONFOLDER'])#
                #textfield(name='applicationname',label='application name',value=setting['applicationname'])#
                #hiddenfield(name='oldapplicationname',value=setting['applicationname'])#
                #textfield(name='username',label='username',value=decrypt(setting['username'],application.applicationname))#
                #passwordfield(name='password',label='password',value=decrypt(setting['password'],application.applicationname))#
                #textfield(name='template',label='global template',value=setting['TEMPLATE'])#
                <cfif NOT structKeyExists(setting,'linkpattern')>
                    <cfset setting['linkpattern'] = "{template}?slug={slug}{otherparams}">
                </cfif>
                #textfield(name='linkpattern',label='Link Pattern',value=setting['linkpattern'],placeholder="{template}?slug={slug}{otherparams}")#
                <cfif NOT structKeyExists(setting,'otherlinkpattern')>
					<cfset setting['otherlinkpattern'] = "{template}?{key}={value}{otherparams}">
				</cfif>
                #textfield(name='otherlinkpattern',label='Other Link Pattern',value=setting['otherlinkpattern'],placeholder="{template}?{key}={value}{otherparams}")#
                <cfif NOT structKeyExists(setting,'rewritepattern')>
                    <cfset setting['rewritepattern'] = "">
                </cfif>
                #textfield(name='rewritepattern',label='Rewrite Pattern',value=setting['rewritepattern'],helper='Example .cfm,.html')#
                #select(name='shoppingcart',label='shopping cart',select=setting['shoppingcart'],option=shoppingq,field='text',key='id')#
                #select(name='logaccess',label='log access',select=setting['logaccess'],option=logq,field='text',key='id')#
                #textfield(name='language',label='language',value=setting['LANGUAGE'])#
                #select(name='environment',label='environment',select=setting['environment'],option=environmentq,field='text',key='id')#
            </div>
            <div class="tab-pane" id="tabs-site">
                #textfield(name='sitetitle',label="site title",value=setting['SITETITLE'])#
                #textfield(name='siteaddress',label="site address",value=setting['SITEADDRESS'])#
                #textfield(name='pathfrontend',label="site folder",value=setting['PATHFRONTEND'])#
                #radio(name='dateformat',label='date format',check=setting['DATEFORMAT'],option=dformat,key='id',field='title')#
                #radio(name='timeformat',label='time format',check=setting['TIMEFORMAT'],option=tformat,key='id',field='title')#
            </div>
            <div class="tab-pane" id="tabs-email">
            	#textfield(name='emailcontactaddress',label='contact address',value=setting['emailcontactaddress'])#
                #textfield(name='server',label='server',value=setting['server'])#
                #select(name='emailssl',label="ssl",option=sslq,field='title',key='id',select=setting['EMAILSSL'])#
                #textfield(name='emailport',label='port',value=setting['emailport'])#
                #textfield(name='emailusername',label='username',value=setting['EMAILUSERNAME'])#
                <cfif setting['EMAILPASSWORD'] neq ''>
                    <cfset mailpass = decrypt(setting['EMAILPASSWORD'],setting['EMAILUSERNAME'])>
                    <cfset setting['EMAILPASSWORD'] = mailpass>
                </cfif>
                #passwordfield(name='emailpassword', label='password',value=setting['EMAILPASSWORD'])#
            </div>
            <div class="tab-pane" id="tabs-asset">
                #textfield(name='imageformat',label='image format',value=setting['imageformat'])#
                #textfield(name='filesizeimage',label='image size (k)',value=setting['filesizeimage'])#
                #hiddenfield(name='videoformat',label='video format',value=setting['videoformat'])#
                #hiddenfield(name='filesizevideo',label='video size (k)',value=setting['filesizevideo'])#
                #textfield(name='fileformat',label='file format',value=setting['fileformat'])#
                #textfield(name='filesizefile',label='file size (k)',value=setting['filesizefile'])#

				<div class="control-group">
                    <label class="control-label">Slide show (px)</label>
                    <div class="controls">
	                    <div class="row-fluid">
		                    <div class="span6">
                                <div class="row-fluid">
                                    <label class="twoinputlabel span2">width</label><input class="twoinputitext span8" type="text" name="slideshowwidth" value="#setting['slideshowwidth']#">
	                            </div>
                            </div>
		                    <div class="span6">
			                    <div class="row-fluid">
			                        <label class="twoinputlabel span2">height</label><input class="twoinputitext span8" type="text" name="slideshowheight" value="#setting['slideshowheight']#">
				                </div>
			                </div>
		                </div>
                    </div>
                </div>

		        <div class="control-group">
		            <label class="control-label">Large image (px)</label>
		            <div class="controls">
		                <div class="row-fluid">
		                    <div class="span6">
		                        <div class="row-fluid">
		                            <label class="twoinputlabel span2">width</label><input class="twoinputitext span8" type="text" name="largeimagewidth" value="#setting['largeimagewidth']#">
		                        </div>
		                    </div>
		                    <div class="span6">
		                        <div class="row-fluid">
		                            <label class="twoinputlabel span2">height</label><input class="twoinputitext span8" type="text" name="largeimageheight" value="#setting['largeimageheight']#">
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>

		        <div class="control-group">
		            <label class="control-label">Thumbnail (px)</label>
		            <div class="controls">
		                <div class="row-fluid">
		                    <div class="span6">
		                        <div class="row-fluid">
		                            <label class="twoinputlabel span2">width</label><input class="twoinputitext span8" type="text" name="thumbnailwidth" value="#setting['thumbnailwidth']#">
		                        </div>
		                    </div>
		                    <div class="span6">
		                        <div class="row-fluid">
		                            <label class="twoinputlabel span2">height</label><input class="twoinputitext span8" type="text" name="thumbnailheight" value="#setting['thumbnailheight']#">
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>

		        <div class="control-group">
		            <label class="control-label">Category image (px)</label>
		            <div class="controls">
		                <div class="row-fluid">
		                    <div class="span6">
		                        <div class="row-fluid">
		                            <label class="twoinputlabel span2">width</label><input class="twoinputitext span8" type="text" name="categoryimagewidth" value="#setting['categoryimagewidth']#">
		                        </div>
		                    </div>
		                    <div class="span6">
		                        <div class="row-fluid">
		                            <label class="twoinputlabel span2">height</label><input class="twoinputitext span8" type="text" name="categoryimageheight" value="#setting['categoryimageheight']#">
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>

		        <div class="control-group">
		            <label class="control-label">Menu image (px)</label>
		            <div class="controls">
		                <div class="row-fluid">
		                    <div class="span6">
		                        <div class="row-fluid">
		                            <label class="twoinputlabel span2">width</label><input class="twoinputitext span8" type="text" name="menuimagewidth" value="#setting['menuimagewidth']#">
		                        </div>
		                    </div>
		                    <div class="span6">
		                        <div class="row-fluid">
		                            <label class="twoinputlabel span2">height</label><input class="twoinputitext span8" type="text" name="menuimageheight" value="#setting['menuimageheight']#">
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>

				#textarea(name='otherimage',label='Other image (px)',value="#setting['otherimage']#",class='span12')#

            </div>
            <div class="tab-pane" id="tabs-label">
                #textfield(name='labelbtnadd',value=setting['labelbtnadd'], label='label add button')#
                #textfield(name='labelbtnedit',value=setting['labelbtnedit'], label='label edit button')#
                #textfield(name='labeledit',value=setting['labeledit'], label='label edit')#
                #textfield(name='labeldelete',value=setting['labeldelete'], label='label delete')#
            </div>
            <div class="tab-pane" id="tabs-database">
                #textfield(name='databasename',value=setting['databasename'],label='database name')#
                #textfield(name='databasehost',value=setting['databasehost'],label='database host')#
                #textfield(name='databaseport',value=setting['databaseport'],label='database port')#
                #textfield(name='databaseusername',value=setting['databaseusername'],label='database username')#
                <cfset databasepass = ''>
                <cfif setting['databasepassword'] neq ''>
                    <cfset databasepass = decrypt(setting['databasepassword'],application.applicationname)>
                </cfif>
                #textfield(name='databasepassword',label='database password',value=databasepass)#
            </div>
            <div class="tab-pane" id="tabs-seo">
                #textarea(name='metadescription',label="meta description",value=setting['METADESCRIPTION'])#
                #textarea(name='metakeyword',label="meta keyword",value=setting['METAKEYWORD'])#
            </div>
            <div class="tab-pane" id="tabs-facebook">
                <div class="control-group">
                    <div class="controls">
                        <span class="help-block">
                        Create Your <a href="https://developers.facebook.com/apps" target="_blank">Facebook APP</a>
                        </span>
                    </div>
                </div>
                #textfield(name='facebook',label='facebook',value=setting['FACEBOOK'])#
                #textfield(name='facebookappid',label='app id',value=setting['FACEBOOKAPPID'])#
                #textfield(name='facebooksecret',label='app secret',value=setting['FACEBOOKSECRET'])#
                #textfield(name='facebookadminid',label='admin id',value=setting['FACEBOOKADMINID'],helper="The ID is important for moderation. <a href='http://developers.facebook.com/tools/explorer/' target='_blank'>Get your Facebook User ID</a>")#
                #textfield(name='facebooklanguage',label='language',value=setting['FACEBOOKLANGUAGE'],helper="For brazilian portuguese the code above would be pt_BR")#
            </div>
            <div class="tab-pane" id="tabs-google">
                #textfield(name='googleapikey',label='api key',value=setting['GOOGLEAPIKEY'])#
                #textfield(name='youtube',label='youtube',value=setting['YOUTUBE'])#
                #textfield(name='googleplus',label='plus',value=setting['GOOGLEPLUS'])#
                #textarea(name='googleanalytics',label='analytics',value=setting['GOOGLEANALYTICS'])#
            </div>
            <div class="tab-pane" id="tabs-recaptcha">
                #textfield(name='privatekey',label='private key',value=setting['PRIVATEKEY'])#
                #textfield(name='publickey',label='public key',value=setting['PUBLICKEY'])#
            </div>
            <div class="tab-pane" id="tabs-integration">
                #textfield(name='twitter',label='twitter',value=setting['TWITTER'])#
                #textfield(name='linkedin',label='linkedin',value=setting['LINKEDIN'])#
                #textfield(name='flickr',label='flickr',value=setting['FLICKR'])#
            </div>
            <div class="tab-pane" id="tabs-sitemap">
<!---                #textfield(name='serviceurl',label='service url',value=setting['SERVICEURL'])#--->
				 <cfinclude template="sitemapcreate.cfm">
<!---                #button(label='Generate xml',onclick='generatexml()',class="btn pull-right geratexmlBtn")#--->
            </div>
            <div class="tab-pane" id="tabs-sampledata">
                <div class="control-group" id="showgenflashsampledata">
                </div>
                <div id="progress" class="progress progress-striped active hidden">
                    <div class="bar" style="width: 100%;"></div>
                </div>
                <div id="genbtn">
                    #button(label='Generate sample data',onclick='generatesampledata()')#
                </div>
            </div>
        </div>
        #submit(label=get('labelbtnedit'))#
        #endform()#
    </div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<script type="text/javascript">
    function generatexml()
    {
        $.ajax({
            url:'generatexml.cfm',
            success:function(e){
                var showtext = '<div class="alert alert-success">generate success</div>';
                $('#showgenflash').html(showtext);
                $('.uneditable-input').html(e.date);
            }
        })
    }

    function generatesampledata()
    {
        $.ajax({
            url:'sampledata.cfm',
            beforeSend:function(){
                $('#progress').removeClass('hidden');
                $('#genbtn').addClass('hidden');
            },
            success:function(e){
                $('#progress').addClass('hidden');
                var showtext = '<div class="alert alert-success">generate success</div>';
                $('#showgenflashsampledata').html(showtext);
            }
        })
    }

    $(function(){
        $("#otherimage").markItUp(myHtmlSettings);
    })
</script>