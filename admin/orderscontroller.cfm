<cfinclude template="system/loadplugins.cfm">
<cfset order = MongoCollectionfindone(application.applicationname,'order',{"_id"=newid(form['key'])})>
<cfset order['TOTALPRICE'] = form['NEWTOTAL']>
<cfset order['MAILNOTE'] = form['MAILNOTE']>
<cfset order['DISCOUNT'] = form['DISCOUNT']>
<cfset order['SHIPPINGPRICE'] = form['SHIPPINGPRICE']>
<cfset order['STATUS'] = form['status']>
<cfoutput>
    <cfsavecontent variable="mailbody">
        <table class="tableorder" style="background-color: transparent;border-spacing: 0;font-size: 14px;margin-bottom: 20px;width: 100%;border-collapse: separate;border-color: ##DDDDDD;border-radius: 4px 4px 4px 4px;border-style: solid solid solid none;border-width: 1px 1px 1px 0;border: 1px solid ##DDDDDD;">
            <thead>
                <tr>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: center;padding: 4px 5px;">Name</td>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: center;">Qty</td>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: center;">Unit price</td>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: center;">Amount</td>
                </tr>
            </thead>
        <tbody>
            <cfloop array="#order['products']#" index="item">
                <tr>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;padding: 4px 5px;">#item['name']#</td>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: right;">#item['qty']#</td>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: right;">#item['price']#</td>
                    <cfset amount = item['qty'] * item['price']>
                    <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: right;">#amount#</td>
                </tr>
            </cfloop>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;padding: 4px 5px;">Discount</td>
                <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: right;">
                    #order['DISCOUNT']#
                </td>
            </tr>
            <tr>
                <td colspan="3" style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;padding: 4px 5px;">Shipping</td>
                <td style="border-left: 1px solid ##DDDDDD;border-bottom: 1px solid ##DDDDDD;text-align: right;">
                    #order['SHIPPINGPRICE']#
                </td>
            </tr>
            <tr>
                <td colspan="3" style="border-left: 1px solid ##DDDDDD;padding: 4px 5px;">Total</td>
                <td style="border-left: 1px solid ##DDDDDD;text-align: right;">
                    #order['TOTALPRICE']#
                </td>
            </tr>
        </tfoot>
        </table>
    </cfsavecontent>
    #mailbody#
</cfoutput>
<cfset MongoCollectionsave(application.applicationname,'order',order)>
<cfset flashmessage = "Update order complete">
<cfif get('emailuser') neq ''>
    <cfset mailobj = createObject('mail.mailservice')>
    <cfset mailobj.sendmail(
        from='order@skoode.com',
        to=order['CUSTOMER']['EMAIL'],
        subject = 'Order',
        typemail = 'html',
        server = get('emailhost'),
        ssl = get('emailssl'),
        port = get('emailport'),
        username = get('emailuser'),
        password = decrypt(get('emailpass'),get('emailuser')),
        template = 'order.txt',
        tableorder=mailbody,
        ordernote=order['MAILNOTE']
            )>
<cfelse>
    <cfset flashmessage = "Update order complete.!Email can't send please setting email">
</cfif>
<cfset flashinsert('success',flashmessage)>
<cflocation url="orders.cfm" addtoken="false">