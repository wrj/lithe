<!---
  Created with IntelliJ IDEA.
  User: peerabumrung
  Date: 1/21/13
  Time: 2:51 PM
  To change this template use File | Settings | File Templates.
--->

<cfinclude template="include/header.cfm">
<cfoutput>
	#javaScriptInclude("jquery.mjs.nestedSortable,math.uuid,frontendmenu/coffee/manage-menu,frontendmenu/coffee/add-menu,frontendmenu/coffee/save-menu,frontendmenu/coffee/clear-menu,frontendmenu/coffee/remove-menu")#
	<link type="text/css" rel="stylesheet" media="all" href="stylesheets/frontendmenu/style.css">
</cfoutput>

<cfoutput>
	<cfif isdefined('ERROR')>
		<cfset frontendmenu = getstoredata()>
		<cfif structIsEmpty(frontendmenu)>
			<cfset frontendmenu = mongonew('frontendmenu')>
		</cfif>
	<cfelse>
		<cfset frontendmenu = mongonew('frontendmenu')>
	</cfif>

	<div class="row-fluid">
		<legend class="titlepage">New Menu</legend>
	#startform(action='frontendmenuscontroller',class="form-horizontal")#
	#hiddenfield(name='mode',value='create',class="menu-mode")#
		<input type="hidden" class="menu-mode" value="create" name="MENUMODE">

<!---	form--->

		<div id="mainForm">
			<div id="main-title">
				<div class="head-name">
					Menu :
				</div>
				<i class="form-up-down icon-chevron-up main-icon-up-down"></i>
				<div class="clear"></div>
			</div>
			<div class="main-form-hide-show">
				#textField(name='NAME',value=frontendmenu['NAME'], label='name',id='MainName',require=true)#
				#textArea(rows=3,name='Description',value=frontendmenu['DESCRIPTION'], label='Description')#
				#textField(name='MenuId',value=frontendmenu['MENUID'], label='Menu CSS ID')#
				#textField(name='MenuClass',value=frontendmenu['MENUCLASS'], label='Menu CSS Class')#
				<input id="Menu" type="hidden" value='#frontendmenu['MENU']#' name="Menu" class='menudata'>
				<input id="issuccess" type="hidden" value='no' name="ISSUCCESS" class='issuccess'>
			</div>

			<cfinclude template="imagedefaultsize.cfm">
		    <div class="control-group">
			    <div class="controls">
				    <div class="span11">
						<!---   slideshow   largeimage  thumbnail   categoryimage   menuimage   otherimage	--->
	                   #$showimagedefaultsize("menuimage")#
						#$showimagedefaultsize("menuimage","hidden")#
		            </div>
		        </div>
	        </div>


			<div class="control-group">
				<div class="controls">
					<div id="mainAction">
						<div class="actionBlock">
							<form action="savemenu.cfm" method="post" id="saveform">
								<input type="submit" value="Save Menu" id="saveMenu" class="btn btn-primary">
							</form>
						</div>

						<div class="actionBlock">
							<input type="button" value="Clear Menu" id="clearMenu" class="btn">
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</div>

			<div class="control-group">
				<label class="menuitem-lebel">Menu Item</label>
				<div class="controls">
					<div class="actionBlock-right">
						<input type="button" value="Add Menu Item" id="addMenu" class="btn">
					</div>
				</div>
			</div>
		</div>




<!---	/form--->
	#endform()#
		<div class="mainMenu">



			<ol class="sortable">

		<cfif frontendmenu.menu IS NOT "">
			<cfset frontendmenu = DeserializeJSON(frontendmenu['menu'])>
			<cfinclude template="frontendmenusgenerate.cfm">
			#generate(frontendmenu)#
		<cfelse>
			<li id='menu-1'>
				<div class='submenu'>
					<div class='submenu-name'>
						<div class='name-text'></div>
						<i class='remove-menu icon-remove'>
							<input type='hidden' name='countSub' class='removeIndex' value='1'>
						</i>
						<i class='form-up-down icon-chevron-up'>
							<input type='hidden' name='countSub' class='countSub' value='1'>
						</i>
					</div>
					<div class='submenu-form submenu-form-hide-show'>
						<div class="row-fluid">
							<div class="span2">
								<div class="menu-thumbnail">
									<div class="menu-thumbnail-noimage">No Image</div>
								</div>
							</div>
                            <div class="span10">
								<div class='submenu-formLine'>
									<div class='row-fluid'>
										<div class='span12'>
											<div class='submenu-Block'>
												<div class='submenu-Label'>Name <span class="required">*</span></div>
												<input type='text' name='submenuName' class='submenuName requirefield' value=''>
											</div>
										</div>
									</div>
								</div>

								<div class='submenu-formLine'>
									<div class='row-fluid'>
										<div class='span12'>
											<div class='submenu-Block'>
												<div class='submenu-Label'>Description</div>
												<textarea name="submenuDescription" class="submenuDescription" id="" cols="10" rows="5"></textarea>
											</div>
										</div>
									</div>
								</div>

								<div class='submenu-Line'>

									<div class='row-fluid'>
										<div class='span6'>
											<div class='submenu-Block'>
												<div class='submenu-Label'>CSS ID</div>
												<input type='text' name='submenuId' class='submenuId'>
											</div>
										</div>
										<div class='span6'>
											<div class='submenu-Block'>
												<div class='submenu-Label'>CSS Class</div>
												<input type='text' name='submenuClass' class='submenuClass'>
											</div>
										</div>
									</div>

								</div>

								<div class='submenu-Line'>
									<div class='row-fluid'>
										<div class='span6'>
											<div class='subgallery-Label'>Link <span class="required">*</span></div>
										</div>
									</div>
								</div>
								<div class='submenu-Line'>
									<div class='row-fluid'>
										<div class='span6'>
                                            <input type='text' name='linkname'  class='linkname requirefield' readonly='readonly'>
											<input type='hidden' name='linkurl'  class='linkurl'>
                                            <input type='hidden' name='categoryid'  class='categoryid'>
                                            <input type='hidden' name='linktype'  class='linktype'>
										</div>
										<div class='span6'>
											<input type='button' value='Browse Link' class='btn add-link'/>
										</div>
									</div>
                                    <div class='submenu-Line'>
                                        <div class='row-fluid'>
                                            <div class='span6'>
                                                <div class='subgallery-Label'>Image</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='submenu-Line'>
	                                    <div class='row-fluid'>
		                                    <div class='span6'>
		                                        <input type='text' name='imagename'  class='imagename' readonly='readonly'>
		                                        <input type='hidden' name='imagefile'  class='imagefile'>
		                                    </div>
		                                    <div class='span6'>
		                                        <input type='button' value='Attach Image' class='btn attach-image'/>
		                                        <input type='button' value='Remove Image' class='btn remove-image'/>
		                                    </div>
	                                    </div>
                                    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</li>
		</cfif>
			</ol>



		</div>

	</div>

<input type="hidden" class="imagepath" value="#get('pathuploadfolder')#image">

</cfoutput>

<cfinclude template="include/footer.cfm">
<!--- ETC --->
<!--- ส่วน มอดอล --->
<div class="modal hide fade bigModal" id="bModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Browse</h3>
	</div>
	<div class="modal-body-bModal">

	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		<a onclick="selectobject()" class="btn btn-primary">OK</a>
	</div>
</div>
<!---    Preview Thumb--->
<div class="modal hide fade" id="pModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Preview Image</h3>
    </div>
    <div class="modal-body" id="previewimage">

    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>

<cfinclude template="postsjsconfig.cfm">


<script type="text/javascript">

var typeopen = '';
$(function(){
	$('#MainName').focusout(function(){
		$('#MainName').val(replacetext($('#MainName').val()));
		$('.head-name').text($('#MainName').val())
	});
	$("form").submit(function() {
		gallerytojson();
		$('#MainName').val(replacetext($('#MainName').val()));
	});
	$('.add-link').live('click', function(e) {
		thisclass=$(this);
        typeopen = 'link';
        linkname = $(this).parent().parent().children().first().find('.linkname').val();
        linkurl = $(this).parent().parent().children().first().find('.linkurl').val();
		categoryid = $(this).parent().parent().children().first().find('.categoryid').val();
        var linktype = $(this).parent().parent().children().first().find('.linktype').val();
		if (linktype == ""){
			linktype = 1
		}
        browsewindow("Custom",linkname,linkurl,linktype,categoryid);
		$("#modalstatus").val("content");
	});
    $('.attach-image').live('click', function(e) {
        thisclass=$(this);
        typeopen = 'image';
        browsewindow('image');
        $("#modalstatus").val("image");
    });
	$(".menuitem-img").live('click',function(e){
        imagepath = $('.imagepath').val()
        imgesurl=$(this).parent().parent().parent().find(".imagefile").val();
        previewimage(imagepath+'/'+imgesurl);
	})

    $(".remove-image").live('click',function(e){
        $(this).parent().parent().find(".imagefile").val("")
        $(this).parent().parent().find(".imagename").val("")
        $(this).parent().parent().parent().parent().parent().parent().find(".menu-thumbnail").html('<div class="menu-thumbnail-noimage">No Image</div>')
    })

});


function browsewindow(type,linkname,linkurl,pagevalue,category){
	$(".modal-body-bModal").html('<iframe id="modalIframeId" width="720" height="620" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" />');
    $("#modalIframeId").attr("src",seturlbase()+"/browse.cfm?type="+type+"&linkname="+linkname+"&linkurl="+linkurl+"&typelink="+pagevalue+"&category="+category);
	$('#bModal').modal('show');
}

$('#bModal').on('hidden', function () {
	$(".modal-body-bModal").html('');
});

function selectedcomplete(fileraw)
{
	var filearr = fileraw[0].split("|");
	var linkinsert = '';
		if (typeopen == 'image')
	{
        var filearr = fileraw[0].split("|");
        var imagepath = $('.imagepath').val()+'/'+filearr[0];
		var imagehtml = '<a class="icon-zoom-in pull-right" href="javascript:previewimage(\''+imagepath+'\')"></a><img src="'+imagepath+'" alt="'+filearr[1]+'" class="menuitem-img img-thumb img-rounded">'
        $(thisclass).parent().parent().find(".imagefile").val(filearr[0]);
        $(thisclass).parent().parent().find(".imagename").val(filearr[1]);
        $(thisclass).parent().parent().parent().parent().parent().parent().find(".menu-thumbnail").html(imagehtml);

	}else if (typeopen == 'link'){
		$(thisclass).parent().parent().find(".linkurl").val(filearr[0]);
		$(thisclass).parent().parent().find(".linkname").val(filearr[1]);
		if(filearr.length>3){
            $(thisclass).parent().parent().find(".categoryid").val(filearr[2]);
            $(thisclass).parent().parent().find(".linktype").val(filearr[3]);
        }else{
            $(thisclass).parent().parent().find(".categoryid").val("");
            $(thisclass).parent().parent().find(".linktype").val(filearr[2]);
		}
}
//	$.markItUp({ target:'#detail',replaceWith: linkinsert });
}

function selectobject()
{
	document.getElementById('modalIframeId').contentWindow.selectfile();
}

function previewimage(imagename)
{
    var pathpreview = '<div class="thumbnail"><img src="'+imagename+'"/></div>';
    $('#previewimage').html(pathpreview);
    $('#pModal').modal('show');
}
</script>



