<cfimport taglib="lib" prefix="ui">
<cfoutput>
    <div class="row-fluid selectfile">
        <div class="pull-left">
            <h4>Link</h4>
        </div>
        <div class="pull-right selectbtn">
	        <input type="button" class="btn" value="Close" onClick="javascript:window.close();">
            <input type="button" value="Select" class="btn btn-primary" onClick="selectfile()">
        </div>
    </div>
    <div class="row-fluid tableclass">
        <div class="span2" style="width: 175px; float: left;">
            #treecategoryoutput()#
        </div>
        <div class="span5" style="float: left; margin-left: 10px; width: 494px;">
            <ui:datatable field="id,Title,update" size="0,0,100" tool=false />
        </div>
    </div>
</cfoutput>
<script type="text/javascript">
    var visitfirst = true;
    var category = '';

    $(function(){
        createtree('tree');
    })

    function reportkey(key){
        category = key;
        inittable();
    }

    function inittable()
    {
        if(visitfirst == true)
        {
            visitfirst = false;
        }else{
            clearall();
        }
        var linkvalue = '?category='+category+'&typecontent=1';
        createdatatablebrowse('usetable','linksbrowsegridck',linkvalue,'3/desc');
    }

    function clearall()
    {
        oTable.fnDestroy();
        gaiSelected = [];
    }

    $('#usetable tbody tr').live('click', function () {
        var aData = oTable.fnGetData( this );
        var iId = aData[0];
        gaiSelected = [];
        if ( $(this).hasClass('row_selected') ) {
            $(this).removeClass('row_selected');
        }else{
            oTable.$('tr.row_selected').removeClass('row_selected');
            $(this).addClass('row_selected');
            gaiSelected.push(iId);
        }
    } );

    function selectfile() {
        if (gaiSelected != "") {
            <cfoutput>
            window.top.opener.CKEDITOR.tools.callFunction(#CKEditorFuncNum#, gaiSelected[0]);
            </cfoutput>
            closewindow();
        }else{
            alert("Please select link");
        }
    }

    function closewindow()
    {
        window.top.close();
    }
</script>