<cfinclude template="../config/database.cfm">
<cfif MongoIsvalid(application.applicationname) neq 'YES'>
    <cftry>
        <cfif databaseusername neq '' AND databasepassword neq ''>
        	<cfset passdecrypt = decrypt(databasepassword,application.applicationname)>
            <cfif databaseport neq ''>
                <cfset MongoRegister( name=application.applicationname, server=databasehost, db=databasename,port=databaseport,username=databaseusername,password=passdecrypt)>
                <cfelse>
                <cfset MongoRegister( name=application.applicationname, server=databasehost, db=databasename,username=databaseusername,password=passdecrypt)>
            </cfif>
            <cfelse>
            <cfif databaseport neq ''>
                <cfset MongoRegister( name=application.applicationname, server=databasehost, db=databasename,port=databaseport)>
                <cfelse>
                <cfset MongoRegister( name=application.applicationname, server=databasehost, db=databasename)>
            </cfif>
        </cfif>
        <cfcatch type="Any">

        </cfcatch>
    </cftry>
</cfif>