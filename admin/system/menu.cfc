<!---
  Created with IntelliJ IDEA.
  User: arunwatd
  Date: 1/14/13
  Time: 2:39 PM
  To change this template use File | Settings | File Templates.
--->
<cfcomponent output="false">
    <cffunction name="genbreadcrumb" access="public" returntype="String">
        <cfargument name="prepend" type="string" default="" required="false">
        <cfargument name="menulist" type="string" default="" required="false">
        <cfset var newmenulist = ''>
        <cfif arguments['menulist'] eq "">
            <cfset newmenulist = listdeleteat(CGI['SCRIPT_NAME'],1,'/')>
            <cfset newmenulist = replaceNoCase(newmenulist,'/',',','all')>
            <cfset newmenulist = replaceNoCase(newmenulist,'.cfm','','all')>
            <cfset arguments['menulist'] = newmenulist>
        </cfif>
        <cfset i = 1>
        <cfsavecontent variable="menuliststring">
            <cfoutput>
                <ul class="breadcrumb">
                <cfloop list="#arguments.prepend#" index="preitem" delimiters=",">
                    <li>#capitalize(preitem)# <span class="divider">/</span>
                </cfloop>
                <cfloop list="#arguments.menulist#" index="menuitem" delimiters=",">
                    <li>
                        <cfif menuitem eq 'update'>
                            <cfset menuitem = 'edit'>
                        </cfif>
                        <cfif menuitem eq 'create'>
                            <cfset menuitem = 'new'>
                        </cfif>
                        #capitalize(singularize(menuitem))#
                        <cfif listFind('edit,new,update,create',menuitem) eq 0>
                            <cfif listLen(arguments.menulist,',') neq i>
                                <span class="divider">/</span>
                            </cfif>
                            </li>
                        <cfelse>
                            </li>
                        <cfbreak>
                    </cfif>
                    <cfset i++>
                </cfloop>
                </ul>
            </cfoutput>
        </cfsavecontent>
        <cfreturn menuliststring>
    </cffunction>
</cfcomponent>
