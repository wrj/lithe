<!---
  Created with IntelliJ IDEA.
  User: arunwatd
  Date: 1/16/13
  Time: 1:39 PM
  To change this template use File | Settings | File Templates.
--->
<cfcomponent>

    <cffunction name="keywordTag" output="Yes">
        <cfargument name="name" type="string" default="keyword">
        <cfargument name="label" type="string" default="Keyword">
        <cfargument name="url" type="string" default="/tagprocess.cfm">
        <cfargument name="value" type="any" default="">
        <cfargument name="id" type="string" default="T#Replace(createUUID(), '-','', 'ALL')#">
        <cfset var valuelist = ''>
        <cfif IsArray(arguments['value'])>
            <cfset valuelist = ArrayToList(arguments['value'],',')>
            <cfelse>
            <cfset valuelist = arguments['value']>
        </cfif>
        <cfoutput>
            #stylesheetinclude("tagify-style")#
            #javaScriptInclude("jquery.tagify")#
            <style>
                .tagify-container{width: 90%;}
            </style>
            #textField(name=arguments.name,value=valuelist,id=arguments.id,class='text_field',label=arguments['label'])#
            <script type="text/javascript">
                var myTextArea = $("###arguments['id']#").tagify();
                $(function(){
                    myTextArea.tagify('inputField').autocomplete({
                        source: function(request, response) {
                            return $.ajax(seturlbase()+"#arguments['url']#", {
                                data: "term=" + (extractLast(request.term)),
                                success: response
                            });
                        },
                        position: { of: myTextArea.tagify('containerDiv') },
                        close: function(event, ui) { myTextArea.tagify('add'); }
                    });
                    $("form").submit(function(){
                        myTextArea.tagify('serialize')
                    })
                });
                function split( val ) {
                    return val.split( /,\s*/ );
                }
                function extractLast( term ) {
                    return split( term ).pop();
                }
            </script>
        </cfoutput>
    </cffunction>

    <cffunction name="checktag" access="public" returntype="void">
        <cfargument name="taglist" type="string" required="true">
        <cfset var ownerid = dbref('user',session['userid'])>
        <cfloop list="#arguments['taglist']#" index="i">
            <cfset findtag = MongoCollectioncount(application.applicationname,'tag',{"NAME"=i,"USER"=ownerid})>
            <cfif findtag eq 0>
                <!---New Tag--->
                <cfset newtag = structNew()>
                <cfset newtag['NAME'] = i>
                <cfset resulttag = mongovalidate('tag','create',newtag)>
                <cfset resulttag['value']['USER'] = ownerid>
                <cfset MongoCollectioninsert(application.applicationname,'tag',resulttag['value'])>
            </cfif>
        </cfloop>
    </cffunction>

    <cffunction name="addtag" access="public" returntype="void">
        <cfargument name="tagadd" type="string" required="true">
        <cfargument name="id" type="string" required="true">
        <cfset var tagadd = arguments['tagadd']>
        <cfset var ownerid = dbref('user',session['userid'])>
        <cfloop list="#tagadd#" index="i">
            <cfset worktag = MongoCollectionfindone(application.applicationname,'tag',{"NAME"=i,"USER"=ownerid})>
            <cfif structKeyExists(worktag,'POST') && isArray(worktag['POST'])>
                <cfset var tagnewarray = worktag['POST']>
            <cfelse>
                <cfset var tagnewarray = arrayNew()>
            </cfif>
            <cfset var tagstnew = structNew()>
            <cfset tagstnew['REFID'] = arguments['id']>
            <cfset arrayAppend(tagnewarray,tagstnew)>
            <cfset worktag['POST'] = tagnewarray>
            <cfset worktag['USER'] = ownerid>
            <cfset MongoCollectionsave(application.applicationname,'tag',worktag)>
        </cfloop>
<!--- End Tag--->
    </cffunction>
<!------>
    <cffunction name="updatetag" access="public" returntype="void">
        <cfargument name="tagadd" type="string" required="true">
        <cfargument name="id" type="string" required="true">
        <cfset var id = arguments['id']>
        <cfset var ownerid = dbref('user',session['userid'])>
        <cfset var fromlist = ''>
        <cfset var deletelist = ''>
        <cfset var newlist = ''>
        <cfset var olditem = ''>
        <cfset var tagdata = Mongocollectionfind(application.applicationname,'tag',{"POST.REFID"=id,"USER"=ownerid})>
        <cfloop array="#tagdata#" index="item">
            <cfset olditem = listappend(olditem,item['NAME'],',')>
        </cfloop>
        <cfset fromlist = arguments['tagadd']>
        <cfloop list="#olditem#" index="item">
            <cfif listfind(fromlist,item) eq 0>
                <cfset deletelist = listappend(deletelist,item,',')>
            </cfif>
        </cfloop>
        <cfloop list="#fromlist#" index="item">
            <cfif listfind(olditem,item) eq 0>
                <cfset newlist = listappend(newlist,item)>
            </cfif>
        </cfloop>
<!--- Delete Tag--->
        <cfloop list="#deletelist#" index="i">
            <cfset tagdel = MongoCollectionfindone(application.applicationname,'tag',{"NAME"=i,"USER"=ownerid})>
            <cfset tagnewarray = arrayNew()>
            <cfset tagstnew = structNew()>
            <cfloop array="#tagdel['POST']#" index="tdel">
                <cfif tdel['REFID'] neq id>
                    <cfset tagstnew = structNew()>
                    <cfset tagstnew['REFID'] = tdel['REFID']>
                    <cfset arrayAppend(tagnewarray,tagstnew)>
                </cfif>
            </cfloop>
            <cfset tagdel['POST'] = tagnewarray>
            <cfset tagdel['USER'] = ownerid>
            <cfset MongoCollectionsave(application.applicationname,'tag',tagdel)>
        </cfloop>
        <!--- New tag--->
        <cfloop list="#newlist#" index="i">
            <cfset tagnew = MongoCollectionfindone(application.applicationname,'tag',{"NAME"=i,"USER"=ownerid})>
            <cfset tagstnew = structNew()>
            <cfif structKeyExists(tagnew,'POST') && isArray(tagnew['POST'])>
                <cfset var tagnewarray = tagnew['POST']>
            <cfelse>
                <cfset var tagnewarray = arrayNew()>
            </cfif>
            <!--- Add new content ref--->
            <cfset tagstnew['REFID'] = id>
            <!--- End add new content ref--->
            <cfset arrayAppend(tagnewarray,tagstnew)>
            <cfset tagnew['POST'] = tagnewarray>
            <cfset tagnew['USER'] = ownerid>
            <cfset MongoCollectionsave(application.applicationname,'tag',tagnew)>
        </cfloop>
    </cffunction>

    <cffunction name="deltag" access="public" returntype="void">
        <cfargument name="id" type="string" required="true">
        <cfset var id = arguments['id']>
        <cfset var ownerid = dbref('user',session['userid'])>
        <cfset tagdata = Mongocollectionfind(application.applicationname,'tag',{"POST.REFID"=id,"USER"=ownerid})>
        <cfloop array="#tagdata#" index="item">
            <cfset refarr = arrayNew()>
            <cfloop array="#item['POST']#" index="subitem">
                <cfif subitem['REFID'] neq id>
                    <cfset arrayappend(refarr,subitem)>
                </cfif>
            </cfloop>
            <cfset item['POST'] = refarr>
            <cfset item['USER'] = ownerid>
            <cfset MongoCollectionsave(application.applicationname,'tag',item)>
        </cfloop>
    </cffunction>
</cfcomponent>
