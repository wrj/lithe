<cfinclude template="systemlog.cfc">
<cfset writesystemlog()>
<cfset pagename = listlast(arguments.targetPage,'/')>
<cfset pagename = listfirst(pagename,'.')>
<cfset nosession = 'login,loginprocess,install,installgenerate,adminlogin,adminloginprocess'>
<cfset usernoaccess = 'users,usersnew,usersupdate,frontendmenus,frontendmenusnew,frontendmenusupdate,setting'>
<cfset adminusernoaccess = 'setting'>
<cfif listfind(nosession,pagename,',') eq 0>
    <cfif structKeyExists(Session,'username')>
        <cfswitch expression="#session['rule']#">
            <cfcase value="user">
                <cfif listFindNoCase(usernoaccess, pagename, ',') gt 0>
                    <cfset session['flash']['error'] = "You don't have permission access #pagename# page">
                    <cfset Session['error'] = "permission denine">
                    <cfset logst = structnew()>
                    <cfset logst['detail'] = "You don't have permission access #pagename# page">
                    <cfset writesystemlog(datainput=logst,filename='accesserror')>
                    <cflocation url="posts.cfm" addtoken="false">
                </cfif>
            </cfcase>
            <cfcase value="admin">
                <cfif listFindNoCase(adminusernoaccess, pagename, ',') gt 0>
                    <cfset session['flash']['error'] = "You don't have permission access #pagename# page">
                    <cfset Session['error'] = "permission denine">
                    <cfset logst = structnew()>
                    <cfset logst['detail'] = "You don't have permission access #pagename# page">
                    <cfset writesystemlog(datainput=logst,filename='accesserror')>
                    <cflocation url="posts.cfm" addtoken="false">
                </cfif>
            </cfcase>
            <cfdefaultcase>
                
            </cfdefaultcase>
        </cfswitch>
    <cfelse>
        <cfif pagename neq "index">
            <cfset Session['error'] = "Can't login">
            <cfset session['flash']['error'] = "Can't login">    
        </cfif>
        <cfset logst = structnew()>
        <cfset logst['detail'] = 'Not login'>
        <cfset writesystemlog(datainput=logst,filename='accesserror')>
        <cflocation url="login.cfm" addtoken="false">
    </cfif>
</cfif>