<cfdirectory directory = "#expandPath('./')#plugins" action = "list" filter = "*.cfc" listInfo = "all" name = "pluginlist" type = "file">
<cfoutput query="pluginlist">
    <cfinclude template="../plugins/#name#" runonce="true">
</cfoutput>
<!---Load System Plugin--->
<cfinclude template="formhelper.cfc" runonce="true">
<cfinclude template="menu.cfc" runonce="true">
<cfinclude template="pmongo.cfc" runonce="true">
<cfinclude template="tag.cfc" runonce="true">
<cfinclude template="tree.cfc" runonce="true">
<cfinclude template="upload.cfc" runonce="true">
<cfinclude template="systemfunction.cfc" runonce="true">
<cfinclude template="systemlanguage.cfc" runonce="true">
<cfinclude template="systemlog.cfc" runonce="true">