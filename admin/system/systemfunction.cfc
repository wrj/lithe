<!---
  Created with IntelliJ IDEA.
  User: arunwatd
  Date: 1/11/13
  Time: 1:27 PM
  To change this template use File | Settings | File Templates.
--->
<cfcomponent output="false">
   <!--- Place your content here --->
    <cffunction name="javascriptinclude" returntype="string" access="public" output="false">
        <cfargument name="filelist" type="string" required="true">
        <cfoutput>
        <cfsavecontent variable="jslist">
            <cfloop list="#arguments['filelist']#" delimiters="," index="item">
                <script src="js/#item#.js"></script>
            </cfloop>
        </cfsavecontent>
        </cfoutput>
        <cfreturn jslist>
    </cffunction>

    <cffunction name="stylesheetinclude" returntype="string" access="public" output="false">
        <cfargument name="filelist" type="string" required="true">
        <cfoutput>
            <cfsavecontent variable="returnlist">
                <cfloop list="#arguments['filelist']#" delimiters="," index="item">
                    <link href="stylesheets/#item#.css" rel="stylesheet">
                </cfloop>
            </cfsavecontent>
        </cfoutput>
        <cfreturn returnlist>
    </cffunction>

    <cffunction name="set" returntype="void" access="public">
        <cfargument name="key" type="string" required="true">
        <cfargument name="value" type="string" required="true">
        <cfset application['blog'][arguments['key']] = arguments['value']>
    </cffunction>

    <cffunction name="get" returntype="string" access="public">
        <cfargument name="key" type="string" required="true">
        <cfset var textreturn = ''>
        <cfif structKeyExists(application['blog'],arguments['key'])>
            <cfset textreturn = application['blog'][arguments['key']]>
        </cfif>
        <cfreturn textreturn>
    </cffunction>

    <cffunction name="flashinsert" returntype="void" access="public">
        <cfargument name="typeflash" type="string" required="true">
        <cfargument name="message" type="string" required="true">
        <cfset session['flash'][arguments['typeflash']] = arguments['message']>
    </cffunction>

    <cffunction name="flashKeyExists" returntype="boolean" access="public">
        <cfargument name="typeflash" type="string" required="true">
        <cfset var result = false>
        <cfif structKeyExists(session,'flash')>
            <cfset result = structKeyExists(session['flash'],arguments['typeflash'])>
        </cfif>
        <cfreturn result>
    </cffunction>

    <cffunction name="flash" returntype="string" access="public">
        <cfargument name="typeflash" type="string" required="true">
        <cfset var result = session['flash'][arguments['typeflash']]>
        <cfset structDelete(session,'flash')>
        <cfreturn result>
    </cffunction>

    <cffunction name="storedata" returntype="void" access="public">
        <cfargument name="objdata" type="struct" required="true">
        <cfset session['objdata'] = arguments['objdata']>
    </cffunction>

    <cffunction name="getstoredata" returntype="struct" access="public">
        <cfset var result = structnew()>
        <cfif structKeyExists(session,'objdata')>
            <cfset result = session['objdata']['value']>
        </cfif>
        <cfset structDelete(session,'objdata')>
        <cfreturn result>
    </cffunction>

    <cffunction name="getstoredataerror" returntype="string" access="public" output="false">
        <cfif structKeyExists(session,'objdata')>
            <cfsavecontent variable="result">
                <cfoutput>
            <ul>
            <cfloop array="#session['objdata']['allerror']#" index="ierror">
                    <li>#ierror#</li>
            </cfloop>
            </ul>
                </cfoutput>
            </cfsavecontent>
        <cfelse>
            <cfset result = ''>
        </cfif>
        <cfreturn result>
    </cffunction>

    <cffunction name="checkdata" access="public" returntype="any" output="false">
        <cfargument name="rawdata" type="struct" required="true">
        <cfargument name="fieldcheck" type="string" required="true">
        <cfif structKeyExists(arguments['rawdata'], arguments['fieldcheck'])>
            <cfreturn arguments['rawdata'][arguments['fieldcheck']]>
        <cfelse>
            <cfreturn ''/>
        </cfif>
    </cffunction>

    <cffunction name="capitalize" returntype="string" access="public" output="false" hint="Returns the text with the first character converted to uppercase."
            examples=
                    '
		<!--- Capitalize a sentence, will result in "Wheels is a framework" --->
		##capitalize("wheels is a framework")##
	'
            categories="global,string" chapters="miscellaneous-helpers" functions="humanize,pluralize,singularize">
        <cfargument name="text" type="string" required="true" hint="Text to capitalize.">
        <cfif !Len(arguments.text)>
            <cfreturn arguments.text />
        </cfif>
        <cfreturn UCase(Left(arguments.text, 1)) & Mid(arguments.text, 2, Len(arguments.text)-1)>
    </cffunction>

    <cffunction name="humanize" returntype="string" access="public" output="false" hint="Returns readable text by capitalizing and converting camel casing to multiple words."
            examples=
                    '
		<!--- Humanize a string, will result in "Wheels Is A Framework" --->
		##humanize("wheelsIsAFramework")##

		<!--- Humanize a string, force wheels to replace "Cfml" with "CFML" --->
		##humanize("wheelsIsACFMLFramework", "CFML")##
	'
            categories="global,string" chapters="miscellaneous-helpers" functions="capitalize,pluralize,singularize">
        <cfargument name="text" type="string" required="true" hint="Text to humanize.">
        <cfargument name="except" type="string" required="false" default="" hint="a list of strings (space separated) to replace within the output.">
        <cfscript>
            var loc = {};
            loc.returnValue = REReplace(arguments.text, "([[:upper:]])", " \1", "all"); // adds a space before every capitalized word
            loc.returnValue = REReplace(loc.returnValue, "([[:upper:]]) ([[:upper:]])(?:\s|\b)", "\1\2", "all"); // fixes abbreviations so they form a word again (example: aURLVariable)
            if (Len(arguments.except))
            {
                loc.iEnd = ListLen(arguments.except, " ");
                for (loc.i = 1; loc.i lte loc.iEnd; loc.i++)
                {
                    loc.a = ListGetAt(arguments.except, loc.i);
                    loc.returnValue = ReReplaceNoCase(loc.returnValue, "#loc.a#(?:\b)", "#loc.a#", "all");
                }
            }
            loc.returnValue = Trim(capitalize(loc.returnValue)); // capitalize the first letter and trim final result (which removes the leading space that happens if the string starts with an upper case character)
        </cfscript>
        <cfreturn loc.returnValue>
    </cffunction>

    <cffunction name="pluralize" returntype="string" access="public" output="false" hint="Returns the plural form of the passed in word. Can also pluralize a word based on a value passed to the `count` argument."
            examples=
                    '
		<!--- Pluralize a word, will result in "people" --->
		##pluralize("person")##

		<!--- Pluralize based on the count passed in --->
		Your search returned ##pluralize(word="person", count=users.RecordCount)##
	'
            categories="global,string" chapters="miscellaneous-helpers" functions="capitalize,humanize,singularize">
        <cfargument name="word" type="string" required="true" hint="The word to pluralize.">
        <cfargument name="count" type="numeric" required="false" default="-1" hint="Pluralization will occur when this value is not `1`.">
        <cfargument name="returnCount" type="boolean" required="false" default="true" hint="Will return `count` prepended to the pluralization when `true` and `count` is not `-1`.">
        <cfreturn $singularizeOrPluralize(text=arguments.word, which="pluralize", count=arguments.count, returnCount=arguments.returnCount)>
    </cffunction>

    <cffunction name="singularize" returntype="string" access="public" output="false" hint="Returns the singular form of the passed in word."
            examples=
                    '
		<!--- Singularize a word, will result in "language" --->
		##singularize("languages")##
	'
            categories="global,string" chapters="miscellaneous-helpers" functions="capitalize,humanize,pluralize">
        <cfargument name="word" type="string" required="true" hint="String to singularize.">
        <cfreturn $singularizeOrPluralize(text=arguments.word, which="singularize")>
    </cffunction>

    <cffunction name="$singularizeOrPluralize" returntype="string" access="public" output="false" hint="Called by singularize and pluralize to perform the conversion.">
        <cfargument name="text" type="string" required="true">
        <cfargument name="which" type="string" required="true">
        <cfargument name="count" type="numeric" required="false" default="-1">
        <cfargument name="returnCount" type="boolean" required="false" default="true">
        <cfscript>
            var loc = {};

// by default we pluralize/singularize the entire string
            loc.text = arguments.text;

// when count is 1 we don't need to pluralize at all so just set the return value to the input string
            loc.returnValue = loc.text;

            if (arguments.count != 1)
            {

                if (REFind("[A-Z]", loc.text))
                {
// only pluralize/singularize the last part of a camelCased variable (e.g. in "websiteStatusUpdate" we only change the "update" part)
// also set a variable with the unchanged part of the string (to be prepended before returning final result)
                    loc.upperCasePos = REFind("[A-Z]", Reverse(loc.text));
                    loc.prepend = Mid(loc.text, 1, Len(loc.text)-loc.upperCasePos);
                    loc.text = Reverse(Mid(Reverse(loc.text), 1, loc.upperCasePos));
                }
                loc.uncountables = "advice,air,blood,deer,equipment,fish,food,furniture,garbage,graffiti,grass,homework,housework,information,knowledge,luggage,mathematics,meat,milk,money,music,pollution,research,rice,sand,series,sheep,soap,software,species,sugar,traffic,transportation,travel,trash,water,feedback";
                loc.irregulars = "child,children,foot,feet,man,men,move,moves,person,people,sex,sexes,tooth,teeth,woman,women";
                if (ListFindNoCase(loc.uncountables, loc.text))
                    loc.returnValue = loc.text;
                else if (ListFindNoCase(loc.irregulars, loc.text))
                {
                    loc.pos = ListFindNoCase(loc.irregulars, loc.text);
                    if (arguments.which == "singularize" && loc.pos MOD 2 == 0)
                        loc.returnValue = ListGetAt(loc.irregulars, loc.pos-1);
                    else if (arguments.which == "pluralize" && loc.pos MOD 2 != 0)
                        loc.returnValue = ListGetAt(loc.irregulars, loc.pos+1);
                    else
                        loc.returnValue = loc.text;
                }
                else
                {
                    if (arguments.which == "pluralize")
                        loc.ruleList = "(quiz)$,\1zes,^(ox)$,\1en,([m|l])ouse$,\1ice,(matr|vert|ind)ix|ex$,\1ices,(x|ch|ss|sh)$,\1es,([^aeiouy]|qu)y$,\1ies,(hive)$,\1s,(?:([^f])fe|([lr])f)$,\1\2ves,sis$,ses,([ti])um$,\1a,(buffal|tomat|potat|volcan|her)o$,\1oes,(bu)s$,\1ses,(alias|status)$,\1es,(octop|vir)us$,\1i,(ax|test)is$,\1es,s$,s,$,s";
                    else if (arguments.which == "singularize")
                        loc.ruleList = "(quiz)zes$,\1,(matr)ices$,\1ix,(vert|ind)ices$,\1ex,^(ox)en,\1,(alias|status)es$,\1,([octop|vir])i$,\1us,(cris|ax|test)es$,\1is,(shoe)s$,\1,(o)es$,\1,(bus)es$,\1,([m|l])ice$,\1ouse,(x|ch|ss|sh)es$,\1,(m)ovies$,\1ovie,(s)eries$,\1eries,([^aeiouy]|qu)ies$,\1y,([lr])ves$,\1f,(tive)s$,\1,(hive)s$,\1,([^f])ves$,\1fe,(^analy)ses$,\1sis,((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$,\1\2sis,([ti])a$,\1um,(n)ews$,\1ews,(.*)?ss$,\1ss,s$,#Chr(7)#";
                    loc.rules = ArrayNew(2);
                    loc.count = 1;
                    loc.iEnd = ListLen(loc.ruleList);
                    for (loc.i=1; loc.i <= loc.iEnd; loc.i=loc.i+2)
                    {
                        loc.rules[loc.count][1] = ListGetAt(loc.ruleList, loc.i);
                        loc.rules[loc.count][2] = ListGetAt(loc.ruleList, loc.i+1);
                        loc.count = loc.count + 1;
                    }
                    loc.iEnd = ArrayLen(loc.rules);
                    for (loc.i=1; loc.i <= loc.iEnd; loc.i++)
                    {
                        if(REFindNoCase(loc.rules[loc.i][1], loc.text))
                        {
                            loc.returnValue = REReplaceNoCase(loc.text, loc.rules[loc.i][1], loc.rules[loc.i][2]);
                            break;
                        }
                    }
                    loc.returnValue = Replace(loc.returnValue, Chr(7), "", "all");
                }

// this was a camelCased string and we need to prepend the unchanged part to the result
                if (StructKeyExists(loc, "prepend"))
                    loc.returnValue = loc.prepend & loc.returnValue;

            }

// return the count number in the string (e.g. "5 sites" instead of just "sites")
            if (arguments.returnCount && arguments.count != -1)
                loc.returnValue = LSNumberFormat(arguments.count) & " " & loc.returnValue;
        </cfscript>
        <cfreturn loc.returnValue>
    </cffunction>

    <cffunction name="logout" access="public" returntype="void">
        <cfset structDelete(session,'userid')>
        <cfset structDelete(session,'username')>
        <cfset structDelete(session,'secret')>
        <cfset structDelete(session,'cart')>
        <cfset structDelete(session,'rule')>
    </cffunction>

	<cffunction name="buildlink" access="private" returntype="String">
		<cfargument name="slug" type="string" required="true">
		<cfargument name="template" type="string" required="true">
		<cfargument name="otherparams" type="string" required="false" default="">
		<cfinclude template="/cfc/config.cfm"/>
		<cfset var htmloutput = replaceNoCase(patternurl, "{template}", "#arguments['template']#", 'all')>
		<cfset htmloutput = replaceNoCase(htmloutput, "{slug}", "#arguments['slug']#", 'all')>
		<cfset htmloutput = replaceNoCase(htmloutput, "{otherparams}", "#arguments['otherparams']#", 'all')>
		<cfloop list="#rewritepatternurl#" delimiters="," index="reurl">
			<cfset htmloutput = replaceNoCase(htmloutput, reurl, "", 'all')>
		</cfloop>
		<cfset outputdata = "#get('siteaddress')#/#htmloutput#">
		<cfreturn outputdata>
	</cffunction>

</cfcomponent>
