<!---
  Created with IntelliJ IDEA.
  User: arunwatd
  Date: 1/15/13
  Time: 2:41 PM
  To change this template use File | Settings | File Templates.
--->
<cfcomponent>
    <cffunction name="genUpload" access="public" returntype="string">
        <cfargument name="filesize" type="numeric" required="true">
        <cfargument name="uploadscript" type="string" required="true">
        <cfargument name="typeassets" type="string" required="true">
        <cfargument name="autoupload" type="boolean" required="false" default="false">
        <cfoutput>
            #stylesheetinclude("uploadifive")#
            #javaScriptInclude("jquery.uploadifive-v1.0.min")#
            <cfsavecontent variable="htmlstr">
                <script type="text/javascript">
                    $(function() {
                        $('##file_upload').uploadifive({
                            'auto'         : #arguments['autoupload']#,
                            'fileSizeLimit' : #arguments['filesize']#,
                            'multi'     : false,
                            'height':25,
                            'queueID'      : 'queue',
                            'uploadScript' : '#arguments['uploadscript']#.cfm?typeassets=#arguments['typeassets']#',
                            'queueSizeLimit': 1,
                            'onUploadComplete' : function(file, data) {
                                myData = JSON.parse(data);
                                if(myData.result == 'error')
                                {
                                    $('##queue').html('');
                                    $('##resultupload').html('Invalid File Type');
                                    $('##file_upload').uploadifive('clearQueue');
                                }else{
                                    $('##resultupload').html('Upload Complete');
                                    upfilecomplete(myData.serverfile,myData.clientfile);
                                }
                                $('##file_upload').uploadifive('clearQueue');
                            }
                        });
                    });
                </script>
                <div class="control-group">
                    <div class="controls">
                        <div id="resultupload"></div>
                        <div id="queue"></div>
                        <input id="file_upload" name="file_upload" type="file" multiple="true">
                    </div>
                </div>
            </cfsavecontent>
        </cfoutput>
        <cfreturn htmlstr>
    </cffunction>

    <cffunction name="uptotemp" returntype="any" access="public" output="false">
        <cfargument name="filetemp" required="true" type="any">
        <cfif directoryExists(get('temppathuploadfolder')) eq false>
            <cfdirectory action = "create" directory = "#get('temppathuploadfolder')#" >
        </cfif>
        <cffile action="upload" destination="#get('temppathuploadfolder')#" nameconflict="MAKEUNIQUE" filefield="FILEDATA[]" mode="777" result="fileupload">
        <cfreturn fileupload>
    </cffunction>

    <cffunction name="deletefromtemp" returntype="void" access="public" output="false">
        <cfargument name="filename" required="true" type="string">
        <cffile action="delete" file="#get('temppathuploadfolder')##trim(arguments.filename)#"/>
    </cffunction>

    <cffunction name="movefile" access="public" returntype="Struct">
        <cfargument name="filename" required="true" type="string">
        <cfargument name="destinationpath" type="string" required="false" default="files">
        <cfargument name="oldfilename" type="string" required="false" default="">
        <cfset var filename = trim(arguments['filename'])>
        <cfset returnst = structNew()>
        <cfif arguments['oldfilename'] eq ''>
            <cfset newserverfile = session['username']&replaceNoCase(listfirst(filename,"."),' ','','all')&DateFormat(Now(),"YYYYMMDD")&TimeFormat(Now(),"HHmmss")&"."&listLast(filename,".") />
        <cfelse>
            <cfset newserverfile = arguments['oldfilename']>
        </cfif>
        <cfset var targetpath = "#get('pathupload')##arguments.destinationpath#">
        <cfset newserverfilePath = targetpath&newserverfile />
        <cfif directoryExists(targetpath) eq false>
            <cfdirectory action = "create" directory = "#targetpath#" >
        </cfif>
        <cffile action="move" destination="#newserverfilePath#" source="#get('temppathuploadfolder')##filename#" mode="777">
        <cfset returnst.newserverfile = newserverfile>
        <cfreturn returnst>
    </cffunction>

    <cffunction name="deletefile" access="public" returntype="void">
        <cfargument name="filename" required="true" type="string">
        <cfargument name="sourcepath" type="string" required="false" default="trash/">
        <cfset filedel = get('pathupload')&''&arguments.sourcepath&''&trim(arguments.filename)>
        <cfif FileExists(filedel)>
            <cffile action="delete" file="#filedel#"/>
        </cfif>
    </cffunction>
</cfcomponent>
