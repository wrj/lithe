<!---
  Created with IntelliJ IDEA.
  User: arunwatd
  Date: 1/16/13
  Time: 3:39 PM
  To change this template use File | Settings | File Templates.
--->
<cfcomponent>
    <cffunction name="treequery" returntype="array" access="public" >
        <cfargument name="obj" type="any" required="true">
        <cfargument name="currentid" type="string" default="" required="false">
        <cfargument name="id" type="string" default="_id">
        <cfargument name="name" type="string" default="title">
        <cfargument name="parent" type="string" default="PARENT">
        <cfset qnew = QueryNew("_id,title","varchar,varchar") />
        <cfset nameroot = ''>
        <cfloop array="#arguments.obj#" index="item">
            <cfset depfound = false>
            <cfset str="">
            <cfif item['_id'].toString() eq arguments['currentid']>
                <cfset nameroot = item['title']>
            <cfelse>
                <cfif structKeyExists(item,arguments.parent)>
                    <cfset str = treenodequery(objnode=item[arguments.parent].fetch(),name=arguments.name,parent=arguments.parent)>
                </cfif>
                <cfloop list="#str#" index="depnamelist" delimiters=">">
                    <cfif (Trim(depnamelist) eq nameroot) && (Trim(depnamelist) neq '')>
                        <cfset depfound = true>
                        <cfbreak>
                    </cfif>
                </cfloop>
                <cfif depfound eq false>
                    <cfset temp = QueryAddRow(qnew)>
                    <cfset temp = QuerySetCell(qnew,'_id',#item['_id'].toString()#)>
                    <cfset temp = QuerySetCell(qnew,arguments['name'],#str#&#item['TITLE']#) >
                </cfif>
            </cfif>
        </cfloop>
        <cfset var resultarr = arraynew()>
        <cfloop query="qnew">
            <cfset cst = structNew()>
            <cfset cst['_id'] = _id>
            <cfset cst['TITLE'] = title>
            <cfset arrayAppend(resultarr,cst)>
        </cfloop>
        <cfreturn resultarr>
    </cffunction>

    <cffunction name="treenodequery" returntype="string" access="public" >
        <cfargument name="objnode" type="any">
        <cfargument name="name" type="string">
        <cfargument name="parent" type="string">
        <cfset str="">
        <cfif structKeyExists(objnode,'PARENT')>
            <cfset str = treenodequery(objnode=objnode[arguments.parent].fetch(),name=arguments.name,parent=arguments.parent)>
        </cfif>
        <cfreturn str & #objnode[arguments.name]# & ' > '>
    </cffunction>

    <cffunction name="genRecursiveList" access="public" returntype="string" output="false">
        <cfargument name="Data" type="Query" required="true" />
        <cfargument name="parentID" type="string" default="" />
        <cfargument name="Level" type="numeric" default="0" required="false"/>
        <cfset var RELOCAL = structNew() />
        <cfset var strreturn = "" />
        <cfset var parentstr = ""/>
        <cfset var itemstr = ""/>
        <cfset var treeselect = 'false'/>
        <cfset var visitefirst = true/>
        <cfquery name="RELOCAL.Children" dbtype="query" >
	        select id,name,pid
	        from arguments.Data
	        where pid = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments['parentID']#"/>
            order by id
        </cfquery>
        <cfif RELOCAL.Children.RecordCount>
            <cfloop query="RELOCAL.Children">
                <cfset parentstr = genRecursiveList(arguments.Data,RELOCAL.Children.id,(arguments['Level']+1))>
                <cfif arguments['Level'] eq 0>
                    <cfif visitefirst eq true>
                        <cfset itemstr = '"title": "#RELOCAL.Children.name#", "key": "#RELOCAL.Children.id#","activate":true'>
                        <cfset visitefirst = false>
                        <cfelse>
                        <cfset itemstr = '"title": "#RELOCAL.Children.name#", "key": "#RELOCAL.Children.id#"'>
                    </cfif>
                    <cfelse>
                    <cfset itemstr = '"title": "#RELOCAL.Children.name#", "key": "#RELOCAL.Children.id#"'>
                </cfif>


                <cfif parentstr neq ''>
                    <cfset itemstr = itemstr & ',"isFolder": true,"children": ['&parentstr&']'>
                </cfif>
                <cfif (strreturn neq '')>
                    <cfset strreturn = strreturn&','>
                </cfif>
                <cfset strreturn = strreturn&"{#itemstr#}">
            </cfloop>
        </cfif>
        <cfreturn strreturn />
    </cffunction>

    <cffunction name="treecategoryoutput" access="public" returnType="string">
        <cfset var catetreedata = MongoCollectionfind(application.applicationname,'category',{"USER"=dbref('user',session['userid'])})>
        <cfoutput>
            #stylesheetinclude("ui.dynatree")#
            #javaScriptInclude("jquery.dynatree.min,createtree")#
            <cfsavecontent variable="htmlstr">
                <div id="tree">

                </div>
            </cfsavecontent>
        </cfoutput>
        <cfreturn htmlstr>
    </cffunction>
</cfcomponent>
