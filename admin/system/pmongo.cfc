<!---
  Created with IntelliJ IDEA.
  User: arunwatd
  Date: 1/11/13
  Time: 10:00 AM
  To change this template use File | Settings | File Templates.
--->
<cfcomponent>
    <cffunction name="newid" access="public" returntype="Any">
        <cfargument name="objid" type="string" required="true">
        <cfset var objreturn = MongoObjectid(arguments.objid)>
        <cfreturn objreturn>
    </cffunction>

    <cffunction name="dbref" access="public" returntype="struct">
        <cfargument name="collection" type="string" required="true">
        <cfargument name="objectid" type="string" required="true">
        <cfset var dbreturn = structNew()>
        <cfset dbreturn['$ref'] = lcase(arguments['collection'])>
        <cfset dbreturn['$id'] =  MongoObjectid(arguments['objectid'])>
        <cfreturn dbreturn>
    </cffunction>

	<cffunction name="rewritejsontext" access="public" returntype="String">
		<cfargument name="rawtext" type="string" required="true">
		<cfset var newtext = ''>
		<cfset newtext = arguments['rawtext']>
		<cfset newtext = rereplaceNoCase(newtext,'"',' ','all')>
		<cfset newtext = rereplaceNoCase(newtext,"'",' ','all')>
		<cfreturn newtext>
	</cffunction>

    <cffunction name="rewritetext" access="public" returntype="String">
        <cfargument name="rawtext" type="string" required="true">
        <cfset var newtext = ''>
        <cfset newtext = arguments['rawtext']>
        <cfset newtext = REReplace(newtext,'[^a-z0-9A-Zก-๙\-]','-','all')>
        <cfset newtext = REReplace(newtext,'(-)+','-','all')>
        <cfset newtext = REReplace(newtext,'(-)*$','','all')>
        <cfreturn newtext>
    </cffunction>

    <cffunction name="mongomodel" access="public" returnType="any">
        <cfargument name="modelname" type="string" required="true" hint="Name to model">
        <cfinclude template="/admin/mongomodels/#arguments.modelname#.cfm"/>
        <cfreturn table />
    </cffunction>

    <cffunction name="mongonew" access="public" returnType="any">
        <cfargument name="modelname" type="string" required="true" hint="Name to model">
        <cfargument name="structvalue" type="struct" required="false" hint="Value from struct">
        <cfargument name="lang" type="string" required="false" hint="language update">
        <cfset table = mongomodel(arguments['modelname'])/>
        <cfset fieldstruct = StructNew()/>
        <cfloop list="#table['tablefield']#" index="i" delimiters=",">
            <!--- Loop field in table --->
            <cfset valuefield = ''/>
            <!--- check structvalue variable --->
            <cfif IsDefined("structvalue")>
                <!--- Have value --->
                <!--- Check field in structvalue eq field in tablefield --->
                <cfif StructKeyExists(arguments['structvalue'],i)>
                    <!--- structvalue have eq field in table field --->
                    <!--- Check multi language field --->
                    <cfif ListFind(table['I18N'],i) neq 0>
                        <!--- Have multi language field --->
                        <cfif arguments['lang'] neq ''>
                            <!--- lange no empty --->
                            <cfset valuefield = i18n(arguments['structvalue'][i],arguments['lang'])/>
                            <!--- User function i18n --->
                        </cfif>
                    <cfelse>
                        <!--- No multi language field --->
                        <!--- check field is struct --->
                        <cfif IsStruct(arguments['structvalue'][i])>
                            <!--- Is struct --->
                            <cfset valuefield = i18n(arguments['structvalue'][i],arguments['lang'])/>
                        <cfelse>
                            <!--- Not struct --->
                            <cfset valuefield = arguments['structvalue'][i]/>
                        </cfif>
                    </cfif>
                </cfif>
            </cfif>
            <!--- Set value to field --->
            <cfset fieldstruct[i] = valuefield/>
        </cfloop>
        <cfif IsDefined("structvalue")>
            <cfif StructKeyExists(arguments['structvalue'],'id')>
                <!--- structvalue have field id --->
                <cfset fieldstruct['id'] = arguments['structvalue']['id'].toString()/>
            </cfif>
        </cfif>
        <cfreturn  fieldstruct/>
    </cffunction>

    <cffunction name="mongomapvalue" access="public" returnType="struct" output="false">
        <cfargument name="modelname" type="string" required="true" hint="Model Name">
        <cfargument name="structmodel" type="struct" required="true" hint="Value form database">
        <cfargument name="structvalue" type="struct" required="true" hint="Value from struct">
        <cfargument name="inputlang" type="string" required="true" hint="language update">
        <cfset var table = mongomodel(arguments['modelname'])/>
        <cfset var resultvalue = ''/>
            <cfoutput>
            <cfloop list="#table['tablefield']#" index="tblfield" delimiters=",">
                <!--- check in old data --->
                <cfif StructKeyExists(arguments['structmodel'],tblfield)>
                <!--- Found field in old data--->
                    <!--- Check in new data --->
                    <cfif StructKeyExists(arguments['structvalue'],tblfield)>
                    <!--- Found field in new data --->
                    <!--- Check I18N from field --->
                        <cfif ListFind(table['I18N'],tblfield,',') neq 0>
                        <!--- I18N --->
                            <cfif IsStruct(arguments['structmodel'][tblfield])>
                                <cfif arguments['inputlang'] eq 'all'>
                                    <cfloop list="#get('weblanguage')#" index="listlang" delimiters=",">
                                        <cfset arguments['structmodel'][tblfield][listlang] = arguments['structvalue'][tblfield]>
                                    </cfloop>
                                <cfelse>
                                    <cfset arguments['structmodel'][tblfield][arguments['inputlang']] = arguments['structvalue'][tblfield]/>
                                </cfif>
                            <cfelse>
                                <cfset rc = StructDelete(arguments['structmodel'], tblfield, "True")>
                                <cfloop list="#get('weblanguage')#" index="listlang" delimiters=",">
                                    <cfif listlang eq arguments['inputlang']>
                                        <cfset arguments['structmodel'][tblfield][listlang] = arguments['structvalue'][tblfield]>
                                    <cfelse>
                                        <cfset arguments['structmodel'][tblfield][listlang] = ''>
                                    </cfif>
                                </cfloop>
                            </cfif>
                        <cfelse>
                            <!--- No I18N --->
                            <cfif IsStruct(arguments['structmodel'][tblfield])>
                                <cfset rc = StructDelete(arguments['structmodel'], tblfield, "True")>
                                <cfset arguments['structmodel'][tblfield] = arguments['structvalue'][tblfield]/>
                            <cfelse>
                                <cfset arguments['structmodel'][tblfield] = arguments['structvalue'][tblfield]/>
                            </cfif>
                        </cfif>
                    </cfif>
                <cfelse>
                <!--- Not Found field in old data --->
                    <!--- Check I18N from field --->
                    <cfif StructKeyExists(arguments['structvalue'],tblfield)>
                        <cfset resultvalue = arguments['structvalue'][tblfield]>
                    <cfelse>
                        <cfset resultvalue = ''>
                    </cfif>
                    <cfif ListFind(table['I18N'],tblfield,',') neq 0>
                        <cfloop list="#get('weblanguage')#" index="listlang" delimiters=",">
                            <cfif listlang eq arguments['inputlang']>
                                <cfset arguments['structmodel'][tblfield][listlang] = resultvalue>
                            <cfelse>
                                <cfset arguments['structmodel'][tblfield][listlang] = ''>
                            </cfif>
                        </cfloop>
                    <cfelse>
                        <cfset arguments['structmodel'][tblfield] = resultvalue/>
                    </cfif>
                </cfif>
                    <br>
            </cfloop>
            </cfoutput>
        <cfset arguments['structmodel']['updatedat'] = now()>
        <cfreturn arguments['structmodel']/>
    </cffunction>

    <cffunction name="mongovalidate" access="public" returnType="any">
        <cfargument name="modelname" type="string" required="true" hint="Name to model">
        <cfargument name="whenevent" type="string" required="true" hint="Event">
        <cfargument name="structvalue" type="struct" required="false" hint="Value from struct">
        <cfset var resultreturn = StructNew()>
        <cfset var noerror = true>
        <cfset var table = mongomodel(arguments['modelname'])/>
        <cfset var allerror = arrayNew()>
        <cfloop list="#table['tablefield']#" index="i" delimiters=",">
            <cfif IsDefined("table['validate']")>
                <cfif StructKeyExists(table['validate'],i)>
                    <cfset rulevalidate = DeserializeJSON(table['validate'][i])>
                    <cfset ruleevent = 'create,update'>
                    <cfif StructKeyExists(rulevalidate,'when')>
                        <cfset ruleevent = rulevalidate['when']>
                    </cfif>
                    <cfloop list="#rulevalidate['typevalidate']#" index="namevalidate">
                        <cfswitch expression="#namevalidate#">
                            <cfcase value="validatespresence">
                                <cfif validatespresence(arguments['structvalue'][i],arguments['whenevent'],ruleevent) eq false>
                                    <cfset fielderror = i & 'error'>
                                    <cfset arguments['structvalue'][fielderror] = 'This value must not blank.'>
                                    <cfset allelementerror = structNew()>
                                    <cfset arrayappend(allerror,"#i# value must not blank.")>
                                    <cfset noerror = false>
                                </cfif>
                            </cfcase>
                            <cfcase value="validatesuniqueness">
                                <cfif validatesuniqueness(arguments['modelname'],i,arguments['structvalue'][i],arguments['whenevent'],ruleevent) eq false>
                                    <cfset fielderror = i & 'error'>
                                    <cfset arguments['structvalue'][fielderror] = 'This value have been use.'>
                                    <cfset allelementerror = structNew()>
                                    <cfset arrayappend(allerror,"#i# value have been use.")>
                                    <cfset noerror = false>
                                </cfif>
                            </cfcase>
                            <cfcase value="confirmation">
                                <cfset revalue = ''>
                                <cfif StructKeyExists(rulevalidate,'confirmation')>
                                    <cfset fieldre = rulevalidate['confirmation']>
                                    <cfif StructKeyExists(arguments['structvalue'],fieldre)>
                                        <cfset revalue = arguments['structvalue'][fieldre]>
                                    </cfif>
                                </cfif>
                                <cfif confirmation(arguments['structvalue'][i],revalue,arguments['whenevent'],ruleevent) eq false>
                                    <cfset fielderror = i & 'error'>
                                    <cfset arguments['structvalue'][fielderror] = 'This value missmatch.'>
                                    <cfset allelementerror = structNew()>
                                    <cfset arrayappend(allerror,"#i# value missmatch.")>
                                    <cfset noerror = false>
                                </cfif>
                            </cfcase>
                            <cfcase value="validateslength">
                                <cfset lencheck = ''>
                                <cfif StructKeyExists(rulevalidate,'validateslength')>
                                    <cfset lencheck = rulevalidate['validateslength']>
                                </cfif>
                                <cfif validateslength(lencheck,arguments['structvalue'][i],arguments['whenevent'],ruleevent) eq false>
                                    <cfset fielderror = i & 'error'>
                                    <cfset arguments['structvalue'][fielderror] = 'This value is between '& replace(lencheck,',',' - ','all') &' charecter.'>
                                    <cfset allelementerror = structNew()>
                                    <cfset arrayappend(allerror,"#i# value is between "& replace(lencheck,',',' - ','all') &" charecter.")>
                                    <cfset noerror = false>
                                </cfif>
                            </cfcase>
                            <cfcase value="validatesminmax">
                                <cfset lencheck = ''>
                                <cfif StructKeyExists(rulevalidate,'validatesminmax')>
                                    <cfset lencheck = rulevalidate['validatesminmax']>
                                </cfif>
                                <cfif validateslength(lencheck,arguments['structvalue'][i],arguments['whenevent'],ruleevent) eq false>
                                    <cfset fielderror = i & 'error'>
                                    <cfset arguments['structvalue'][fielderror] = 'This value is between '& replace(lencheck,',',' - ','all') &' charecter.'>
                                    <cfset allelementerror = structNew()>
                                    <cfset arrayappend(allerror,"#i# value is between "& replace(lencheck,',',' - ','all') &" charecter.")>
                                    <cfset noerror = false>
                                </cfif>
                            </cfcase>
                            <cfcase value="validateemail">
                                <cfif validateemail(arguments['structvalue'][i],arguments['whenevent'],ruleevent) eq false>
                                    <cfset fielderror = i & 'error'>
                                    <cfset arguments['structvalue'][fielderror] = 'This value not email format.'>
                                    <cfset allelementerror = structNew()>
                                    <cfset arrayappend(allerror,"#i# value is not email format.")>
                                    <cfset noerror = false>
                                </cfif>
                            </cfcase>
                            <cfcase value="validatesnumericality">
                                <cfset typecheck = ''>
                                <cfif StructKeyExists(rulevalidate,'validatesnumericality')>
                                    <cfset typecheck = rulevalidate['validatesnumericality']>
                                </cfif>
                                <cfif validatesnumericality(typecheck,arguments['structvalue'][i],arguments['whenevent'],ruleevent) eq false>
                                    <cfset fielderror = i & 'error'>
                                    <cfset arguments['structvalue'][fielderror] = 'This value not '& replace(typecheck,',',' - ','all')&'.'>
                                    <cfset allelementerror = structNew()>
                                    <cfset arrayappend(allerror,"#i# value not #replace(typecheck,',',' - ','all')#.")>
                                    <cfset noerror = false>
                                </cfif>
                            </cfcase>
                            <cfcase value="rewritetext">
                                <cfset arguments['structvalue'][i] = rewritetext(arguments['structvalue'][i])>
                            </cfcase>
                        </cfswitch>
                    </cfloop>
                </cfif>
            </cfif>
        </cfloop>
        <cfif arguments['whenevent'] eq 'create'>
            <cfset arguments['structvalue']['CREATEDAT'] = now()>
        </cfif>
        <cfset arguments['structvalue']['UPDATEDAT'] = now()>
        <cfset resultreturn['result'] = noerror>
        <cfset resultreturn['allerror'] = allerror>
        <cfset resultreturn['value'] = arguments['structvalue']>
        <cfif StructKeyExists(arguments['structvalue'],'password')>
            <cfif arguments['structvalue']['password'] neq ''>
                <cfset arguments['structvalue']['password'] = hash(arguments['structvalue']['password'],"SHA")/>
            </cfif>
        </cfif>
        <cfreturn resultreturn>
    </cffunction>

    <cffunction name="validatespresence" access="public" returnType="boolean">
        <cfargument name="valuecheck" type="string" required="false" hint="Value for check">
        <cfargument name="whenevent" type="string" required="true" hint="Event for check">
        <cfargument name="whenrule" type="string" required="true" hint="Rule from model">
        <cfset checkresult = true>
        <cfif ListFind(arguments['whenrule'],arguments['whenevent'],',') neq 0>
            <cfif Trim(arguments['valuecheck']) eq ''>
                <cfset checkresult = false>
            </cfif>
        </cfif>
        <cfreturn checkresult>
    </cffunction>

    <cffunction name="validatesuniqueness" access="public" returnType="boolean">
        <cfargument name="modelname" type="string" required="true" hint="Model name">
        <cfargument name="fieldname" type="string" required="true" hint="Field check">
        <cfargument name="valuecheck" type="string" required="false" hint="Value for check">
        <cfargument name="whenevent" type="string" required="true" hint="Event for check">
        <cfargument name="whenrule" type="string" required="true" hint="Rule from model">
        <cfset checkresult = true>
        <cfif ListFind(arguments['whenrule'],arguments['whenevent'],',') neq 0>
            <cfset fieldsearch = structNew()>
            <cfset fieldsearch[arguments["fieldname"]] = arguments["valuecheck"]>
            <cfset searchresult = Mongocollectioncount(application.applicationname,arguments["modelname"],fieldsearch)>
            <cfif searchresult gt 0>
                <cfset checkresult = false>
            </cfif>
        </cfif>
        <cfreturn checkresult>
    </cffunction>

    <cffunction name="validateemail" access="public" returnType="boolean">
        <cfargument name="valuecheck" type="string" required="false" hint="Value for check">
        <cfargument name="whenevent" type="string" required="true" hint="Event for check">
        <cfargument name="whenrule" type="string" required="true" hint="Rule from model">
        <cfset checkresult = true>
        <cfif ListFind(arguments['whenrule'],arguments['whenevent'],',') neq 0>
            <cfif not IsValid("email", arguments['valuecheck'])>
                <cfset checkresult = false>
            </cfif>
        </cfif>
        <cfreturn checkresult>
    </cffunction>

    <cffunction name="validatesnumericality" access="public" returnType="boolean">
        <cfargument name="typecheck" type="string" required="false" hint="type for check">
        <cfargument name="valuecheck" type="string" required="false" hint="Value for check">
        <cfargument name="whenevent" type="string" required="true" hint="Event for check">
        <cfargument name="whenrule" type="string" required="true" hint="Rule from model">
        <cfset checkresult = true>
        <cfif arguments['valuecheck'] neq ''>
            <cfif ListFind(arguments['whenrule'],arguments['whenevent'],',') neq 0>
                <cfloop list="#arguments['typecheck']#" index="itype">
                    <cfif Not IsValid(itype,arguments['valuecheck'])>
                        <cfset checkresult = false>
                    </cfif>
                </cfloop>
            </cfif>
        </cfif>
        <cfreturn checkresult>
    </cffunction>

    <cffunction name="validateslength" access="public" returnType="boolean">
        <cfargument name="lencheck" type="string" required="false" hint="Length for check">
        <cfargument name="valuecheck" type="string" required="false" hint="Value for check">
        <cfargument name="whenevent" type="string" required="true" hint="Event for check">
        <cfargument name="whenrule" type="string" required="true" hint="Rule from model">
        <cfset checkresult = true>
        <cfif ListFind(arguments['whenrule'],arguments['whenevent'],',') neq 0>
            <cfset minimum = Val(ListFirst(arguments['lencheck'],','))>
            <cfset maximum = Val(ListLast(arguments['lencheck'],','))>
            <cfif not IsValid("range", Len(arguments['valuecheck']), minimum, maximum)>
                <cfset checkresult = false>
            </cfif>
        </cfif>
        <cfreturn checkresult>
    </cffunction>

    <cffunction name="validatesminmax" access="public" returnType="boolean">
        <cfargument name="lencheck" type="string" required="false" hint="Length for check">
        <cfargument name="valuecheck" type="string" required="false" hint="Value for check">
        <cfargument name="whenevent" type="string" required="true" hint="Event for check">
        <cfargument name="whenrule" type="string" required="true" hint="Rule from model">
        <cfset checkresult = true>
        <cfif ListFind(arguments['whenrule'],arguments['whenevent'],',') neq 0>
            <cfset minimum = Val(ListFirst(arguments['lencheck'],','))>
            <cfset maximum = Val(ListLast(arguments['lencheck'],','))>
            <cfif not IsValid("range", arguments['valuecheck'], minimum, maximum)>
                <cfset checkresult = false>
            </cfif>
        </cfif>
        <cfreturn checkresult>
    </cffunction>

    <cffunction name="confirmation" access="public" returnType="boolean">
        <cfargument name="valuecheck" type="string" required="false" hint="Value for check">
        <cfargument name="valueconfirm" type="string" required="false" hint="Value for check confirm">
        <cfargument name="whenevent" type="string" required="true" hint="Event for check">
        <cfargument name="whenrule" type="string" required="true" hint="Rule from model">
        <cfset checkresult = true>
        <cfif ListFind(arguments['whenrule'],arguments['whenevent'],',') neq 0>
            <cfif Trim(arguments['valuecheck']) eq '' OR Trim(arguments['valueconfirm'] eq '')>
                <cfset checkresult = false>
            </cfif>
            <cfif Trim(arguments['valuecheck']) neq Trim(arguments['valueconfirm'])>
                <cfset checkresult = false>
            </cfif>
        </cfif>
        <cfreturn checkresult>
    </cffunction>
</cfcomponent>
