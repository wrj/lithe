<!---
  Created with IntelliJ IDEA.
  User: arunwatd
  Date: 1/29/13
  Time: 1:27 PM
  To change this template use File | Settings | File Templates.
--->
<cfcomponent>
    <cffunction name="i18n" access="public" returntype="String" output="false">
        <cfargument name="textdata" type="any" required="true">
        <cfargument name="getlang" type="string" required="false" default="">
        <cfset var textreturn = ''>
        <cfif arguments['getlang'] eq ''>
            <cfif structKeyExists(session,'language')>
                <cfset arguments['getlang'] = session['language']>
            <cfelse>
                <cfset arguments['getlang'] = listfirst(get('weblanguage'),',')>
            </cfif>
        </cfif>
        <cfif IsStruct(arguments['textdata'])>
            <cfif structKeyExists(arguments['textdata'],arguments['getlang'])>
                <cfset textreturn = arguments['textdata'][arguments['getlang']]>
            </cfif>
        <cfelse>
            <cfset textreturn = arguments['textdata']>
        </cfif>
        <cfreturn textreturn>
    </cffunction>

    <cffunction name="langselect" access="public" returnType="string">
        <cfargument name="selectlang" type="string" required="false" default="default">
        <cfset selectlang = arguments['selectlang']/>
        <cfoutput>
            <div class="control-group pull-right">
                <label class="control-label">Language</label>
            <div class="controls">
            <select name="lang" id="lang" onchange="changeinput()">
            <cfloop list="#get('weblanguage')#" index="listlang" delimiters=",">
                    <option value="#listlang#" <cfif listlang eq selectlang>selected="selected"</cfif>>#listlang#</option>
            </cfloop>
            </select>
            </div>
            </div>
            <cfset var newurl = CGI['query_string']/>
            <cfloop list="#get('weblanguage')#" index="listlang" delimiters=",">
                <cfset newurl = replaceNoCase(newurl, "&langinput=#listlang#", "", 'all')>
            </cfloop>
            <script type="text/javascript">
            function changeinput()
        {
            <cfif CGI['query_string'] neq ''>
                <cfset pathurl = "#CGI['script_name']##CGI['path_info']#?#newurl#&langinput=">
                <cfelse>
                <cfset pathurl = "#CGI['script_name']##CGI['path_info']#?langinput=">
            </cfif>
            window.location = "#pathurl#"+$('##lang').val();
        }
        </script>
        </cfoutput>
    </cffunction>

    <cffunction name="assignlang" access="public" returnType="struct">
        <cfargument name="modelname" type="string" required="true" hint="Name to model">
        <cfargument name="objdata" type="struct" required="true">
        <cfset table = mongomodel(arguments['modelname'])/>
        <cfset objreturn = StructNew()>
        <cfloop list="#StructKeyList(arguments['objdata'],',')#" index="column" delimiters=",">
            <cfif ListFind(table['I18N'],column,',') neq 0>
                <cfloop list="#get('weblanguage')#" index="listlang" delimiters=",">
                    <cfif listlang eq session['language']>
                        <cfset objreturn[column][listlang] = objdata[column]>
                    <cfelse>
                        <cfset objreturn[column][listlang] = ''>
                    </cfif>
                </cfloop>
                <cfelse>
                <cfset objreturn[column] = objdata[column]>
            </cfif>
        </cfloop>
        <cfreturn objreturn/>
    </cffunction>
</cfcomponent>
