<!---
  Created with IntelliJ IDEA.
  User: arunwatd
  Date: 1/11/13
  Time: 10:19 AM
  To change this template use File | Settings | File Templates.
--->
<cfcomponent output="false">

    <cffunction name="linkTo" returntype="string" access="public" output="false" hint="Creates a link to another page in your application. Pass in the name of a `route` to use your configured routes or a `controller`/`action`/`key` combination. Note: Pass any additional arguments like `class`, `rel`, and `id`, and the generated tag will also include those values as HTML attributes.">
        <cfargument name="text" type="string" required="false" default="" hint="The text content of the link.">
        <cfargument name="action" type="string" required="false" default="" hint="See documentation for @URLFor.">
        <cfargument name="key" type="any" required="false" default="" hint="See documentation for @URLFor.">
        <cfargument name="href" type="string" required="false" hint="Pass a link to an external site here if you want to bypass the Wheels routing system altogether and link to an external URL.">
        <cfargument name="onclick" type="string" required="false" hint="Pass a link to an external site here if you want to bypass the Wheels routing system altogether and link to an external URL.">

        <cfset var textreturn = '<a'>
        <cfif arguments['action'] neq ''>
            <cfset textreturn = "#textreturn# href='#arguments['action']#.cfm'">
        </cfif>
        <cfif arguments['href'] neq ''>
            <cfset textreturn = "#textreturn# href='#arguments['href']#'">
        </cfif>
        <cfif arguments['key'] neq ''>
            <cfset textreturn = "#textreturn# key='#arguments['key']#'">
        </cfif>
        <cfif arguments['onclick'] neq ''>
            <cfset textreturn = '#textreturn# onclick="#arguments['onclick']#"'>
        </cfif>
        <cfset textreturn = "#textreturn# >#capitalize(arguments['text'])#</a>">
        <cfreturn textreturn>
    </cffunction>

    <cffunction name="textfield" access="public" returntype="string" output="false">
        <cfargument name="name" required="true" type="string">
        <cfargument name="id" required="false" type="string" default="">
        <cfargument name="label" required="false" default="" type="string">
        <cfargument name="class" required="false" default="span11">
        <cfargument name="value" required="false" default="">
        <cfargument name="readonly" required="false" default="">
	    <cfargument name="require" required="false" default=false>
        <cfargument name="nodiv" required="false" default="false">
        <cfargument name="helper" required="false" default="">
	    <cfargument name="placeholder" required="false" default="">
        <cfif arguments['id'] eq ''>
            <cfset var elementid = replacenocase(arguments['name'],'[','-','all')>
            <cfset elementid = replacenocase(arguments['name'],']','','all')>
        <cfelse>
            <cfset elementid = arguments['id']>
        </cfif>
        <cfset var readonlytext = ''>
        <cfif arguments['readonly'] eq true>
            <cfset readonlytext = 'readonly'>
        </cfif>
	    <cfset var requireclass = ''>
	    <cfset var requirestar = ''>
	    <cfif arguments['require'] eq true>
		    <cfset requireclass = 'requirefield'>
		    <cfset requirestar = '<span class="required">*</span>'>
	    </cfif>
	    <cfset var placeholdertext="">
	    <cfif arguments['placeholder'] neq ''>
		    <cfset placeholdertext = 'placeholder="#arguments["placeholder"]#"'>
	    </cfif>
        <cfoutput>
        <cfsavecontent variable="textreturn">
            <cfif arguments['nodiv'] eq 'false'>
                <div class="control-group">
                    <label class="control-label">#capitalize(arguments['label'])# #requirestar#</label>
                    <div class="controls">
                        <input type="text" name="#arguments['name']#" id="#elementid#" value="#arguments['value']#" class="#arguments['class']# #requireclass#" #readonlytext# #placeholdertext#/>
                        <cfif arguments['helper'] neq ''>
                            <span class="help-block">#arguments['helper']#</span>
                        </cfif>
                    </div>
                </div>
            <cfelse>
                <div class="normal-label">
                    <label>#capitalize(arguments['label'])# #requirestar#</label>
                    <input type="text" name="#arguments['name']#" id="#elementid#" value="#arguments['value']#" class="#arguments['class']# #requireclass#" #readonlytext# #placeholdertext#/>
                    <cfif arguments['helper'] neq ''>
                        <span class="help-block">#arguments['helper']#</span>
                    </cfif>
                </div>
            </cfif>
        </cfsavecontent>
        </cfoutput>
        <cfreturn textreturn>
    </cffunction>

    <cffunction name="datefield" access="public" returntype="string" output="false">
        <cfargument name="name" required="true" type="string">
        <cfargument name="id" required="false" type="string" default="">
        <cfargument name="label" required="false" default="" type="string">
        <cfargument name="class" required="false" default="span11">
        <cfargument name="value" required="false" default="">
        <cfargument name="readonly" required="false" default="">
        <cfargument name="nodiv" required="false" default="false">
        <cfargument name="helper" required="false" default="">
        <cfif arguments['id'] eq ''>
            <cfset var elementid = replacenocase(arguments['name'],'[','-','all')>
            <cfset elementid = replacenocase(arguments['name'],']','','all')>
            <cfelse>
            <cfset elementid = arguments['id']>
        </cfif>
        <cfset var readonlytext = ''>
        <cfif arguments['readonly'] eq true>
            <cfset readonlytext = 'readonly'>
        </cfif>
        <cfoutput>
            <cfsavecontent variable="textreturn">
                <cfif arguments['nodiv'] eq 'false'>
                    <div class="control-group">
                        <label class="control-label">#capitalize(arguments['label'])#</label>
                        <div class="controls">
                            <input type="text" name="#arguments['name']#" id="#elementid#" value="#arguments['value']#" class="#arguments['class']# dateinput" #readonlytext#/>
                            <cfif arguments['helper'] neq ''>
                                    <span class="help-block">#arguments['helper']#</span>
                            </cfif>
                        </div>
                    </div>
                <cfelse>
                    <div class="normal-label">
                    <label>#capitalize(arguments['label'])#</label>
                        <input type="text" name="#arguments['name']#" id="#elementid#" value="#arguments['value']#" class="#arguments['class']# dateinput" #readonlytext#/>
                        <cfif arguments['helper'] neq ''>
                            <span class="help-block">#arguments['helper']#</span>
                        </cfif>
                    </div>
                </cfif>
            </cfsavecontent>
        </cfoutput>
        <cfreturn textreturn>
    </cffunction>

    <cffunction name="passwordfield" access="public" returntype="string" output="false">
        <cfargument name="name" required="true" type="string">
        <cfargument name="label" required="false" default="" type="string">
        <cfargument name="class" required="false" default="span11">
        <cfargument name="value" required="false" default="">
        <cfargument name="helper" required="false" default="">
	    <cfargument name="require" required="false" default=false>
        <cfset var elementid = replacenocase(arguments['name'],'[','-','all')>
        <cfset elementid = replacenocase(arguments['name'],']','','all')>
	    <cfset var requireclass = ''>
	    <cfset var requirestar = ''>
	    <cfif arguments['require'] eq true>
		    <cfset requireclass = 'requirefield'>
		    <cfset requirestar = '<span class="required">*</span>'>
	    </cfif>
        <cfoutput>
            <cfsavecontent variable="textreturn">
                <div class="control-group">
                <label class="control-label">#capitalize(arguments['label'])# #requirestar#</label>
                <div class="controls">
                    <input type="password" name="#arguments['name']#" id="#elementid#" value="#arguments['value']#" class="#arguments['class']# #requireclass#"/>
                    <cfif arguments['helper'] neq ''>
                            <span class="help-block">#arguments['helper']#</span>
                    </cfif>
                </div>
            </div>
            </cfsavecontent>
        </cfoutput>
        <cfreturn textreturn>
    </cffunction>

    <cffunction name="hiddenfield" access="public" returntype="string" output="false">
        <cfargument name="name" required="true" type="string">
        <cfargument name="value" required="false" default="">
        <cfset var elementid = replacenocase(arguments['name'],'[','-','all')>
        <cfset elementid = replacenocase(arguments['name'],']','','all')>
        <cfoutput>
            <cfsavecontent variable="textreturn">
                    <input type="hidden" name="#arguments['name']#" id="#elementid#" value="#arguments['value']#"/>
            </cfsavecontent>
        </cfoutput>
        <cfreturn textreturn>
    </cffunction>

    <cffunction name="textarea" access="public" returntype="string" output="false">
        <cfargument name="name" required="true" type="string">
        <cfargument name="label" required="false" default="" type="string">
        <cfargument name="class" required="false" default="span11">
        <cfargument name="value" required="false" default="">
	    <cfargument name="require" required="false" default=false>
        <cfargument name="rows" required="false" default="10">
        <cfargument name="helper" required="false" default="">
        <cfset var elementid = replacenocase(arguments['name'],'[','-','all')>
        <cfset elementid = replacenocase(arguments['name'],']','','all')>
	    <cfset var requireclass = ''>
	    <cfset var requirestar = ''>
	    <cfif arguments['require'] eq true>
		    <cfset requireclass = 'requirefield'>
		    <cfset requirestar = '<span class="required">*</span>'>
	    </cfif>
        <cfoutput>
            <cfsavecontent variable="textreturn">
                <div class="control-group">
                <label class="control-label">#capitalize(arguments['label'])# #requirestar#</label>
                    <div class="controls">
                    <textarea rows="#arguments['rows']#" name="#arguments['name']#" id="#elementid#" class="#arguments['class']# #requireclass#">#arguments['value']#</textarea>
                    <cfif arguments['helper'] neq ''>
                            <span class="help-block">#arguments['helper']#</span>
                    </cfif>
                </div>
            </div>
            </cfsavecontent>
        </cfoutput>
        <cfreturn textreturn>
    </cffunction>

    <cffunction name="select" access="public" returntype="string" output="false">
        <cfargument name="name" required="true" type="string">
        <cfargument name="label" required="false" default="" type="string">
        <cfargument name="class" required="false" default="span11">
        <cfargument name="select" required="false" default="">
        <cfargument name="option" required="true" default="">
        <cfargument name="field" required="true" default="">
        <cfargument name="key" required="true">
        <cfargument name="nodiv" required="false" default="false">
        <cfargument name="helper" required="false" default="">
	    <cfargument name="require" required="false" default=false>
        <cfset var elementid = replacenocase(arguments['name'],'[','-','all')>
        <cfset elementid = replacenocase(arguments['name'],']','','all')>
		<cfset var requireclass = ''>
		<cfset var requirestar = ''>
		<cfif arguments['require'] eq true>
			<cfset requireclass = 'requirefieldselect'>
			<cfset requirestar = '<span class="required">*</span>'>
		</cfif>
        <cfoutput>
            <cfsavecontent variable="textreturn">
                <cfif arguments['nodiv'] eq 'false'>
                <div class="control-group">
                    <label class="control-label">#capitalize(arguments['label'])# #requirestar#</label>
                    <div class="controls">
                        <select name="#arguments['name']#" id="#elementid#" class="#arguments['class']# #requireclass#">
                            <cfloop array="#arguments['option']#" index="item">
                            <option value="#item[arguments['key']]#" <cfif item[arguments['key']] eq arguments['select']>selected="selected"</cfif>>#item[arguments['field']]#</option>
                            </cfloop>
                        </select>
                        <cfif arguments['helper'] neq ''>
                                <span class="help-block">#arguments['helper']#</span>
                        </cfif>
                    </div>
                </div>
                <cfelse>
                    <label>#capitalize(arguments['label'])# #requirestar#</label>
                    <select name="#arguments['name']#" id="#elementid#" class="#arguments['class']# #requireclass#">
                    <cfloop array="#arguments['option']#" index="item">
                            <option value="#item[arguments['key']]#" <cfif item[arguments['key']] eq arguments['select']>selected="selected"</cfif>>#item[arguments['field']]#</option>
                    </cfloop>
                    </select>
                    <cfif arguments['helper'] neq ''>
                        <span class="help-block">#arguments['helper']#</span>
                    </cfif>
                </cfif>
            </cfsavecontent>
        </cfoutput>
        <cfreturn textreturn>
    </cffunction>

    <cffunction name="radio" access="public" returntype="string">
        <cfargument name="name" required="true" type="string">
        <cfargument name="label" required="false" default="" type="string">
        <cfargument name="check" required="false" default="">
        <cfargument name="option" required="true" default="">
        <cfargument name="field" required="true" default="">
        <cfargument name="key" required="true">
        <cfargument name="nodiv" required="false" default="false">
        <cfargument name="helper" required="false" default="">
        <cfset var elementid = replacenocase(arguments['name'],'[','-','all')>
        <cfset elementid = replacenocase(arguments['name'],']','','all')>
        <cfset var checkradio = ''>
        <cfif arguments['check'] neq ''>
            <cfset checkradio = arguments['check']>
        </cfif>
        <cfoutput>
            <cfsavecontent variable="textreturn">
                <cfif arguments['nodiv'] eq 'false'>
                    <div class="control-group">
                        <label class="control-label">#capitalize(arguments['label'])#</label>
                        <div class="controls">
                            <cfset var iradio = 1>
                            <cfloop array="#arguments['option']#" index="item">
                                <cfif checkradio eq '' && iradio eq 1>
                                    <cfset checkradio = item[arguments['key']]>
                                </cfif>
                                    <label class="radio">
                                            <input type="radio" name="#arguments['name']#" id="#arguments['name']##iradio#" value="#item[arguments['key']]#" <cfif item[arguments['key']] eq checkradio>checked</cfif>>
                                    #item[arguments['field']]#
                                    </label>
                                <cfset iradio++>
                            </cfloop>
                        <cfif arguments['helper'] neq ''>
                                <span class="help-block">#arguments['helper']#</span>
                        </cfif>
                        </div>
                    </div>
                <cfelse>
                    <label>#capitalize(arguments['label'])#</label>
                        <cfset var iradio = 1>
                        <cfloop array="#arguments['option']#" index="item">
                            <cfif checkradio eq '' && iradio eq 1>
                                <cfset checkradio = item[arguments['key']]>
                            </cfif>
                            <label class="radio">
                                <input type="radio" name="#arguments['name']#" id="#arguments['name']##iradio#" value="#item[arguments['key']]#" <cfif item[arguments['key']] eq checkradio>checked</cfif>>
                            #item[arguments['field']]#
                            </label>
                            <cfset iradio++>
                        </cfloop>
                    <cfif arguments['helper'] neq ''>
                        <span class="help-block">#arguments['helper']#</span>
                    </cfif>
                </cfif>
            </cfsavecontent>
        </cfoutput>
        <cfreturn textreturn>
    </cffunction>

    <cffunction name="submit" access="public" returntype="string" output="false">
        <cfargument name="label" required="false" default="Submit" type="string">
        <cfargument name="class" required="false" default="btn" type="string">
        <cfoutput>
            <cfsavecontent variable="textreturn">
            <div class="control-group">
                <div class="controls">
                    <input type="submit" value="#arguments['label']#" class="#arguments['class']#">
                </div>
            </div>
            </cfsavecontent>
        </cfoutput>
        <cfreturn textreturn>
    </cffunction>

    <cffunction name="button" access="public" returntype="string" output="false">
        <cfargument name="label" required="false" default="Submit" type="string">
        <cfargument name="class" required="false" default="btn" type="string">
        <cfargument name="onclick" required="false" default="" type="string">
        <cfset var onclickbtn = ''>
        <cfif arguments['onclick'] neq ''>
            <cfset onclickbtn = "onclick='#arguments['onclick']#'">
        </cfif>
        <cfoutput>
            <cfsavecontent variable="textreturn">
                <div class="control-group">
                <div class="controls">
                    <input type="button" #onclickbtn# value="#arguments['label']#" class="#arguments['class']#">
                </div>
            </div>
            </cfsavecontent>
        </cfoutput>
        <cfreturn textreturn>
    </cffunction>

    <cffunction name="startform" access="public" returntype="string" output="false">
        <cfargument name="action" type="string" required="true">
        <cfargument name="key" type="string" required="false" default="">
        <cfargument name="class" type="string" required="false" default="form-horizontal">
        <cfset linkaction = "#arguments['action']#.cfm">
        <cfif arguments['key'] neq ''>
            <cfset linkaction = "#arguments['action']#.cfm?key=#arguments['key']#">
        </cfif>
        <cfoutput>
            <cfsavecontent variable="textreturn">
                <form class="#arguments['class']#" method="post" action="#linkaction#">
            </cfsavecontent>
        </cfoutput>
        <cfreturn textreturn>
    </cffunction>

    <cffunction name="endform" access="public" returntype="String" output="false">
        <cfreturn '</form>'>
    </cffunction>
</cfcomponent>
