<!---
  Created with IntelliJ IDEA.
  User: arunwatd
  Date: 1/17/13
  Time: 2:33 PM
  To change this template use File | Settings | File Templates.
--->
<cfcomponent>
    <cffunction name="writesystemlog" access="public" returntype="void" output="false">
        <cfargument name="datainput" type="struct" required="false">
        <cfargument name="filename" type="string" required="false" default="access">
        <cfif structKeyExists(application,'blog') && application['blog']['logaccess'] eq 'on'>
            <cfset var targetpage = CGI['SCRIPT_NAME']>
            <cfset var workpage = listlast(targetpage,'/')>
            <cfset var listnotallow = 'adminlogin,login,logout,categoriesgrid,filesgrid,imagesgrid,pagesgrid,postsgrid,tagsgrid,usersgrid,videogrid,install,installgenerate,treeprocess'>
            <cfif listfind(listnotallow,listfirst(workpage,'.')) eq 0>
                <cfset var filelog = "#expandPath('./')#files/log/#arguments['filename']#.txt">
                <cfset var filelogpath = "#expandPath('./')#files/log/">
                <cfif directoryExists(filelogpath) eq false>
                    <cfdirectory action = "create" directory = "#filelogpath#" >
                </cfif>
                <cfset var useraccess = ''>
                <cfset var userrule = ''>
                <cfif structKeyExists(session,'username') && session['username'] neq ''>
                    <cfset useraccess = session['username']>
                    <cfset userrule = session['rule']>
                </cfif>
                <cfif structKeyExists(arguments, "datainput")>
                    <cfset var mylog= "#application.applicationname#,#workpage#,#useraccess#,#userrule#,#CGI.remote_addr#,#now()#,#CGI.query_string#,#structtotextlog(arguments.datainput)#|"/>
                <cfelse>
                    <cfset var mylog= "#application.applicationname#,#workpage#,#useraccess#,#userrule#,#CGI.remote_addr#,#now()#,#CGI.query_string#|"/>
                </cfif>
                <cfif fileExists(filelog) eq "YES">
                    <cffile action="append" charset="utf-8" mode="777" addnewline="true" output="#mylog#" file="#filelog#">
                    <cfelse>
                    <cffile action="write" charset="utf-8" mode="777" addnewline="true" output="#mylog#" file="#filelog#">
                </cfif>
            </cfif>
        </cfif>
    </cffunction>

    <cffunction name="structtotextlog" access="public" returntype="String" output="true">
        <cfargument name="mystruct" type="struct">
        <cfset var columnname = StructKeyList(arguments.mystruct,",")>
        <cfset var columnlen = listLen(columnname,",")>
        <cfset var i = 1>
        <cfset var logdata = "{"/>
        <cfoutput>
            <cfloop list="#columnname#" index="fieldname">
                <cfif not isArray(arguments.mystruct[fieldname])>
                    <cfif not isNumeric(arguments.mystruct[fieldname]) && not isBoolean(arguments.mystruct[fieldname]) && not isDate(arguments.mystruct[fieldname])>
                        <cfif not isStruct(arguments.mystruct[fieldname])>
                            <cfset logdata = logdata&#fieldname#&'="'&#arguments.mystruct[fieldname]#&'"'/>
                            <cfelse>
                            <cfset logdata = logdata&#fieldname#&'="'&#structtotextlog(arguments.mystruct[fieldname])#&'"'/>
                        </cfif>
                        <cfelse>
                        <cfset logdata = logdata&#fieldname#&'='&#arguments.mystruct[fieldname]#/>
                    </cfif>
                    <cfelse>
                    <cfset logdata = logdata&#fieldname#&'=['&#arraytostringlog(arguments.mystruct[fieldname])#&']'/>
                </cfif>
                <cfif i neq columnlen>
                    <cfset logdata = logdata&","/>
                    <cfset i++>
                </cfif>
            </cfloop>
            <cfset logdata = logdata&'}'/>
        </cfoutput>
        <cfreturn logdata>
    </cffunction>

    <cffunction name="arraytostringlog" access="public" returntype="String" output="false">
        <cfargument name="arrayobj" type="array">
        <cfset myarray = arguments.arrayobj>
        <cfset arraycolumnname = StructKeyList(myarray[1],",")>
        <cfset logdataarr = "{"/>
        <cfoutput>
            <cfloop from="1" to="#arraylen(myarray)#" index="a">
                <cfset indexloop = 1>
                <cfloop list="#arraycolumnname#" index="cn">
                    <cfif structKeyExists(myarray[a], cn)>
                        <cfif not IsArray(myarray[a][cn])>
                            <cfif not isNumeric(myarray[a][cn]) && not isBoolean(myarray[a][cn]) && not isDate(myarray[a][cn])>
                                <cfset logdataarr = logdataarr&#cn#&'="'&#myarray[a][cn]#&'"'/>
                                <cfelse>
                                <cfset logdataarr = logdataarr&#cn#&'='&#myarray[a][cn]#/>
                            </cfif>
                            <cfif listLen(arraycolumnname,",") neq indexloop>
                                <cfset logdataarr = logdataarr&','/>
                                <cfset indexloop ++/>
                            </cfif>
                        </cfif>
                    </cfif>
                </cfloop>
                <cfset logdataarr = logdataarr&'}'/>
                <cfif arraylen(myarray) neq a>
                    <cfset logdataarr = logdataarr&','/>
                </cfif>
            </cfloop>
        </cfoutput>
        <cfreturn logdataarr>
    </cffunction>
</cfcomponent>
