<cfinclude template="system/loadplugins.cfm">
<cfset sortlabel = ""/>
<cfset sortvalue = 1/>
<cfset searchtxt = ""/>
<cfset listColumns = "FILENAME" />
<cfif not isDefined('iSortCol_0')>
    <cfset iSortCol_0 = 0>
    <cfset sSortDir_0 = 1>
    <cfset sSearch = "">
    <cfset sEcho = 1>
    <cfset iDisplayStart =0>
    <cfset iDisplayLength = 10>
</cfif>
<cfset sortlabel = listGetAt(listColumns,(Val(iSortCol_0) + 1))/>
<cfif sSortDir_0 eq "desc">
    <cfset sortvalue = -1/>
</cfif>
<cfset sortst = structNew()>
<cfset sortst[sortlabel] = sortvalue>
<cfoutput>
    <cfset defaultsearch = structnew()>
<!---    <cfset defaultsearch["USER"] = dbref('user',session['userid'])>--->
    <cfif sSearch neq "">
        <cfset searchtxt = structnew()>
        <cfset searchtxt["$regex"] = ".*#Trim(sSearch)#.*">
        <cfset searchtxt["$options"] = "i">
        <cfset fieldarray = arraynew()>
        <cfloop list="#listColumns#" delimiters="," index="item">
            <cfset fieldsearch = structNew()>
            <cfset fieldsearch[item] = searchtxt>
            <cfset arrayappend(fieldarray,fieldsearch)>
        </cfloop>
        <cfset defaultsearch["$or"] = fieldarray>
    </cfif>
    <cfset mydatatotal = MongoCollectioncount(datasource=application.applicationname,collection='file',query=defaultsearch)>
    <cfset mydata = MongoCollectionFind(datasource=application.applicationname,collection='file',query=defaultsearch,skip=iDisplayStart,size=iDisplayLength,sort=sortst)>
    <cfsavecontent variable="sourcedatatable">
        {"sEcho": #val(sEcho)#,
    "iTotalRecords": #mydatatotal#,
    "iTotalDisplayRecords": #mydatatotal#,
    "aaData": [
        <cfset i = 1/>
        <cfloop array="#mydata#" index="rowitem">
            [
            <cfloop list="#listColumns#" index="thisColumn">
                <cfif thisColumn neq listFirst(listColumns)>,</cfif>
                <cfif ListFind("createdat,updatedat",thisColumn,',') gt 0>
                    "#jsStringFormat(DateFormat(rowitem[thisColumn],'dd/mm/yy'))#"
                <cfelse>
                    "#jsStringFormat(checkdata(rowitem,thisColumn))#"
                </cfif>
            </cfloop>
            ,"<a onclick=\"javascript:goedit('#rowitem['_id'].toString()#')\" href=\"javascript:void(0)\" class=\"btn\">#get('labeledit')#</a> <a onclick=\"javascript:deletedata('#rowitem['_id'].toString()#','#checkdata(rowitem,ListFirst(listColumns))#')\" href=\"javascript:void(0)\" class=\"btn\">#get('labeldelete')#</a>"]
            <cfif i neq arraylen(mydata)>,</cfif>
            <cfset i++/>
        </cfloop>]}
    </cfsavecontent>
    <cfoutput>
        #sourcedatatable#
    </cfoutput>
</cfoutput>