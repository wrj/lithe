<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/23/13 AD
  Time: 11:50 AM
  To change this template use File | Settings | File Templates.
--->

<cfinclude template="system/loadplugins.cfm">
<cfset structDelete(form,'FIELDNAMES')>
<cfswitch expression="#mode#">
	<cfcase value="create">
		<cfset parentid = form['CATEGORY']>
		<cfset structDelete(form,'mode')>
		<cfset structDelete(form,'CATEGORY')>
		<cfset result = mongovalidate('categoryimage','create',form)>
		<cfif result['result'] eq true>
<!---			<cfdump var="#result['value']#">--->
			<cfset result['value']['USER'] = dbref('user',session['userid'])>
			<cfif parentid neq 0>
				<cfset result['value']['PARENT'] = dbref('categoryimage',parentid)>
			</cfif>
<!---			<cfdump var="#result['value']#">--->
<!---			<cfabort>--->
			<cfset MongoCollectioninsert(application.applicationname,'categoryimage',result['value'])>
			<cfset flashinsert('success','Create category Image complete')>
			<cflocation url="categoriesimages.cfm" addtoken="false">
		<cfelse>
			<cfset flashinsert('error','There was an error creating the category.')>
			<cfset result['value']['PARENT'] = parentid>
			<cfset storedata(result)>
			<cflocation url="categoriesimagesnew.cfm" addtoken="false">
		</cfif>
	</cfcase>
	<cfcase value="update">
		<cfset keyid = key>
		<cfset parentid = form['CATEGORY']>
		<cfset displaystart = form['DISPLAYSTART']>
		<cfset structDelete(form,'KEY')>
		<cfset structDelete(form,'MODE')>
		<cfset structDelete(form,'CATEGORY')>
		<cfset structDelete(form,'DISPLAYSTART')>
		<cfset result = mongovalidate('categoryimage','update',form)>
		<cfif result['result'] eq true>
			<cfset updateobj = result['value']>
			<cfset olddata = MongoCollectionfindone(application.applicationname,'categoryimage',{'_id'=newid(keyid)})>
			<cfset oldparent=0>
			<cfif structKeyExists(olddata,'PARENT')>
				<cfset oldparent=olddata['PARENT'].getId().toString()>
			</cfif>
			<cfset dataupdate = mongomapvalue('categoryimage',olddata,updateobj,'default')>
			<cfset dataupdate['USER'] = dbref('user',session['userid'])>
			<cfif parentid neq 0>
				<cfset dataupdate['PARENT'] = dbref('categoryimage',parentid)>
			<cfelse>
				<cfset rc = StructDelete(dataupdate, "PARENT", "True")>
			</cfif>
			<cfset cateidref = dbref('categoryimage',dataupdate["_id"].toString())>
			<cfset checkparent = MongoCollectioncount(application.applicationname,'categoryimage',{"PARENT"=cateidref})>
			<cfif oldparent IS parentid OR checkparent EQ 0>
				<cfset structDelete(dataupdate,'mode')>
				<cfset MongoCollectionsave(application.applicationname,'categoryimage',dataupdate)>
				<cfset flashinsert('success','Update category complete')>
				<cflocation url="categoriesimages.cfm?start=#displaystart#" addtoken="false">
			<cfelse>
				<cfset result['value']['PARENT'] = parentid>
				<cfset flashinsert('error','This category image have parent. Can not change parent category.')>
				<cfset storedata(result)>
				<cflocation url="categoriesimagesupdate.cfm?key=#keyid#&start=#displaystart#" addtoken="false">
			</cfif>
		<cfelse>
			<cfset result['value']['PARENT'] = parentid>
			<cfset flashinsert('error','There was an error update the category image.')>
			<cfset storedata(result)>
			<cflocation url="categoriesimagesupdate.cfm?key=#keyid#&start=#displaystart#" addtoken="false">
		</cfif>
	</cfcase>
	<cfcase value="delete">
		<cfset cateid = dbref('categoryimage',key)>
		<cfset checkparent = MongoCollectioncount(application.applicationname,'categoryimage',{"PARENT"=cateid})>
		<cfset result = structNew()>
		<cfif checkparent neq 0>
			<cfset result['result'] = 'false'>
			<cfset result['message'] = "This category image have parent.Can't delete.">
		<cfelse>
			<!---            Delete post and page in category--->
			<cfset defaultsearch = structnew()>
			<cfset defaultsearch["CATEGORY"] = cateid>
			<cfset images = MongoCollectionfind(application.applicationname,'image',defaultsearch)>
			<cfloop index="image" array="#images#">
				<cfset temp = deletefile(image['NEWFILENAME'],'image/')>
			</cfloop>
			<cfset MongoCollectionremove(application.applicationname,'image',defaultsearch)>
			<cfset MongoCollectionremove(application.applicationname,'categoryimage',{'_id'=newid(key)})>
			<cfset result['result'] = 'true'>
			<cfset result['message'] = "Delete category image complete">
		</cfif>
		<cfoutput>
			#serializeJSON(result)#
		</cfoutput>
	</cfcase>

</cfswitch>