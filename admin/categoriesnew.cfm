<cfinclude template="include/header.cfm">
<cfset cateraw = MongoCollectionfind(application.applicationname,'category',{})>
<cfset caterawquery = treequery(cateraw)/>
<cfset catequery = arraynew()>
<cfset rowquery = structNew()>
<cfset rowquery["_id"]="0">
<cfset rowquery["TITLE"]="No Parent">
<cfset arrayAppend(catequery,rowquery)>
<cfloop array="#caterawquery#" index="item">
    <cfset rowquery = structNew()>
    <cfset rowquery["_id"]=item['_id']>
    <cfset rowquery["TITLE"]=item['TITLE']>
    <cfset arrayAppend(catequery,rowquery)>
</cfloop>
<cfif isdefined('ERROR')>
    <cfset category = getstoredata()>
    <cfif structIsEmpty(category)>
        <cfset category = mongonew('category')>
    </cfif>
    <cfset parentid = ''>
    <cfif structKeyExists(category,'PARENT')>
        <cfset parentid = category['PARENT']>
    </cfif>
<cfelse>
    <cfset category = mongonew('category')>
    <cfset parentid = ''>
    <cfif category['template'] eq ''>
        <cfset category['template'] = get('template')>
    </cfif>
</cfif>
<cfoutput>
<div class="row-fluid">
    <legend class="titlepage">New Category</legend>
    #startform(action='categoriescontroller')#
    #hiddenfield(name='mode',value='create')#
        <cfinclude template="categoriesform.cfm"/>
    #submit(label=get('labelbtnadd'))#
    #endform()#
</div>
</cfoutput>
<cfinclude template="include/footer.cfm">