<cfinclude template="system/loadplugins.cfm">
<!---Create User--->
<cfset MongoCollectiondrop(application.applicationname,"tag")>
<cfset MongoCollectiondrop(application.applicationname,"user")>
<cfset userobj = mongonew('user')>
<cfset userobj["USERNAME"] = "sample">
<cfset userobj["PASSWORD"] = "password">
<cfset userobj["FIRSTNAME"] = "Sample">
<cfset userobj["LASTNAME"] = "Sample">
<cfset userobj["EMAIL"] = "sample@mymail.com">
<cfset userobj["RULE"] = "admin">
<cfset userobj['SECRET'] = "S#replaceNoCase(createuuid(),'-','','all')#">
<cfset result = mongovalidate('user','create',userobj)>
<cfset userdata = result['value']>
<cfset userid = MongoCollectioninsert(application.applicationname,'user',userdata)>
<cfset userobjid = dbref('user',userid.toString())>
<!---Create setting--->
<cfset MongoCollectiondrop(application.applicationname,"usersetting")>
<cfset usersetting = mongonew('usersetting')>
<cfset usersetting['FACEBOOK'] = ''>
<cfset usersetting['TWITTER'] = ''>
<cfset usersetting['GOOGLEPLUS'] = ''>
<cfset usersetting['LINKEDIN'] = ''>
<cfset usersetting['USER'] = userobjid>
<cfset MongoCollectioninsert(application.applicationname,'usersetting',usersetting)>
<!---End User--->
<!---Image--->
<cfset MongoCollectiondrop(application.applicationname,"image")>
<cfset MongoCollectiondrop(application.applicationname,"file")>
<cfset MongoCollectiondrop(application.applicationname,"video")>
<cfset MongoCollectiondrop(application.applicationname,"categoryimage")>
<cfset targetimagedel = "#get('pathupload')#image">
<cfset targetfiledel = "#get('pathupload')#file">
<cfset samplepath = "#expandPath('./')#sampledata/image">
<cfif directoryExists(targetimagedel)>
    <cfset DirectoryDelete(targetimagedel,true)>
</cfif>
<cfif directoryExists(targetfiledel)>
    <cfset DirectoryDelete(targetfiledel,true)>
</cfif>
<cfset setimage = arraynew()>
<cfset slideimage = arraynew()>
<cfset productimage = arraynew()>
<cfset etcpath = "#samplepath#/etc">
<cfset newspath = "#samplepath#/news">
<cfset slidepath = "#samplepath#/slide">

<!--- Etc Image --->
<cfset cateimgobjid = $createcategoryimage("Etc")>
<cfloop array="#directoryList(path=etcpath,listInfo="name",filter="*.jpg")#" index="item">
    <cfset newimage = movefilesample(item,'etc','image',userobj["USERNAME"])>
    <cfset arrayAppend(setimage,"#get('pathuploadfolder')#image/#newimage#")>
    <cfset image = mongonew('image')>
    <cfset image["FILENAME"] = listFirst(item,'.')>
    <cfset image["NEWFILENAME"] = newimage>
	<cfset image['CATEGORY'] = cateimgobjid>
    <cfset imgresult = mongovalidate('image','create',image)>
    <cfset imgresult['value']['USER'] = userobjid>
    <cfset MongoCollectioninsert(application.applicationname,'image',imgresult['value'])>
</cfloop>
<!--- News Image --->
<cfset cateimgobjid = $createcategoryimage("News")>
<cfloop array="#directoryList(path=newspath,listInfo="name",filter="*.jpg")#" index="item">
    <cfset newimage = movefilesample(item,'news','image',userobj["USERNAME"])>
    <cfset arrayAppend(productimage,"#get('pathuploadfolder')#image/#newimage#")>
    <cfset image = mongonew('image')>
    <cfset image["FILENAME"] = listFirst(item,'.')>
    <cfset image["NEWFILENAME"] = newimage>
	<cfset image['CATEGORY'] = cateimgobjid>
    <cfset imgresult = mongovalidate('image','create',image)>
    <cfset imgresult['value']['USER'] = userobjid>
    <cfset MongoCollectioninsert(application.applicationname,'image',imgresult['value'])>
</cfloop>
<!--- Slide Image --->
<cfset cateimgobjid = $createcategoryimage("Slide")>
<cfloop array="#directoryList(path=slidepath,listInfo="name",filter="*.jpg")#" index="item">
    <cfset newimage = movefilesample(item,'slide','image',userobj["USERNAME"])>
    <cfset arrayAppend(slideimage,"#get('pathuploadfolder')#image/#newimage#")>
    <cfset image = mongonew('image')>
    <cfset image["FILENAME"] = listFirst(item,'.')>
    <cfset image["NEWFILENAME"] = newimage>
	<cfset image['CATEGORY'] = cateimgobjid>
    <cfset imgresult = mongovalidate('image','create',image)>
    <cfset imgresult['value']['USER'] = userobjid>
    <cfset MongoCollectioninsert(application.applicationname,'image',imgresult['value'])>
</cfloop>
<!---End Image--->
<cfset lipsumtitle = structnew()>
<cfset lipsumtitle["1"] = "The standard Lorem Ipsum passage">
<cfset lipsumtitle["2"] = "Section of de Finibus Bonorum et Malorum">
<cfset lipsumtitle["3"] = "1914 translation by Rackham">
<cfset lipsumtitle["4"] = "written by Cicero in 45 BC">
<cfset lipsumtitle["5"] = "translation by Rackham">
<cfset lipsumtitle["6"] = "What is Lorem Ipsum">
<cfset lipsumtitle["7"] = "Where does it come from">
<cfset lipsumtitle["8"] = "Why do we use it">
<cfset lipsumtitle["9"] = "Where can I get some">
<cfset lipsumtitle["10"] = "used since the 1500s">
<cfset lipsumtitle["11"] = "Finibus Bonorum et Malorum">
<cfset lipsumtitle["12"] = "1914 translation Rackham">
<cfset lipsumtitle["13"] = "Section de Finibus in 45 BC">
<cfset lipsumtitle["14"] = "1914 translation by">
<cfset lipsumtitle["15"] = "What is Lorem Ipsum">
<cfset lipsumtitle["16"] = "Where does it">
<cfset lipsumtext = structnew()>
<cfset lipsumtext["1"] = "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?">
<cfset lipsumtext["2"] = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?">
<cfset lipsumtext["3"] = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.">
<cfset lipsumtext["4"] = "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.">
<cfset lipsumtext["5"] = "On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.">
<cfset lipsumtext["6"] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.">
<cfset lipsumtext["7"] = "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de Finibus Bonorum et Malorum' (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, 'Lorem ipsum dolor sit amet..', comes from a line in section 1.10.32.">
<cfset lipsumtext["8"] = "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).">
<cfset lipsumtext["9"] = "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.">
<cfset lipsumtext["10"] = "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?">
<cfset lipsumtext["11"] = "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?">
<cfset lipsumtext["12"] = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.">
<cfset lipsumtext["13"] = "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.">
<cfset lipsumtext["14"] = "On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.">
<cfset lipsumtext["15"] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.">
<cfset lipsumtext["16"] = "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de Finibus Bonorum et Malorum' (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, 'Lorem ipsum dolor sit amet..', comes from a line in section 1.10.32.">
<cfset listcategory = 'News,Blog,Work,FAQ,Feature,Portfolio'>
<!---Category--->
<cfset MongoCollectiondrop(application.applicationname,"category")>
<cfset MongoCollectiondrop(application.applicationname,"post")>
<cfset catecount = 1>
<cfset postcount = 1>
<cfloop list="#listcategory#" index="catetitle">
    <cfset category = mongonew('category')>
    <cfset category["TITLE"] = catetitle>
    <cfset category["SLUG"] = replaceNoCase("#catetitle#", " ", "-","all")>
    <cfset category["IMAGEREF"] = setimage[catecount]>
    <cfset category["DETAIL"] = lipsumtitle[catecount]>
    <cfset category["URL"] = "#catetitle#.cfm">
    <cfset category["PATTERN"] = "">
    <!--- <cfif catetitle eq "Product">
        <cfset category["TEMPLATE"] = "item.cfm">
    <cfelse> --->
        <cfset category["TEMPLATE"] = "blogitem.cfm">
    <!--- </cfif> --->
    <cfset result = mongovalidate('category','create',category)>
    <cfset result['value']['USER'] = userobjid>
    <cfset cateid = MongoCollectioninsert(application.applicationname,'category',result['value'])>
    <cfset cateobjid = dbref('category',cateid.toString())>
    <cfset catecount++>
    <!---        Post--->
    <cfif catetitle eq "News">
        <cfloop list="#structKeyList(lipsumtitle,',')#" index="title">
            <cfset post = mongonew('post')>
            <cfset post["TITLE"] = "#lipsumtitle[title]##postcount#">
            <cfset post["SLUG"] = replaceNoCase("#lipsumtitle[title]##catetitle##postcount#", " ", "-","all")>
            <cfset usetext = lipsumtext[title]>
            <cfset countlist = arraylen(ListToArray(usetext,'.'))>
            <cfset introtext = "#listgetat(usetext,RandRange(1,countlist))#.#listgetat(usetext,RandRange(1,countlist))#">
            <cfset post["INTRO"] = introtext>
            <cfset post["DETAIL"] = usetext>
            <!--- <cfif catetitle neq "Product"> --->
                <cfset post["PRICE"] = 0>
                <cfset post["SKU"] = "">
            <!--- <cfelse>
                <cfset post["PRICE"] = RandRange(100,1000)>
                <cfset post["SKU"] = "SK#replaceNoCase(createuuid(),'-','','all')#">
            </cfif> --->
            <cfset post["GALLERY"] = arrayNew()>
            <cfset rawtag = replaceNoCase(usetext,'.',',','all')>
            <cfset rawtag = replaceNoCase(rawtag,';',',','all')>
            <cfset rawtag = replaceNoCase(rawtag,':',',','all')>
            <cfset rawtag = replaceNoCase(rawtag,' ',',','all')>
            <cfset rawtag = replaceNoCase(rawtag,'"',',','all')>
            <cfset rawtag = replaceNoCase(rawtag,"'",",",'all')>
            <cfset countlisttag = arraylen(ListToArray(rawtag,','))>
            <cfset tagtext = "">
            <cfloop from="1" to="#RandRange(1,6)#" index="i">
                <cfset tagtext = listappend(tagtext,listgetat(rawtag,RandRange(1,countlisttag)),',')>
            </cfloop>
            <cfset result = mongovalidate('post','create',post)>
            <cfset postdata = assignlang('post',result['value'])>
            <cfset postdata['TAG'] = ListToArray(tagtext,',')>
            <cfset postdata['STHUMB'] = productimage[title]>
            <cfset postdata['LTHUMB'] = productimage[title]>
            <cfset postdata['TYPECONTENT'] = 1>
            <cfset postdata['PUBSTATUS'] = 1>
            <cfset postdata['PRIVATE'] = "false">
            <cfset startdate = DateFormat(now(),'dd/mm/yyyy')>
            <cfset postdata['PUBDATE'] = createDate(listGetAt(startdate,3,'/'),listGetAt(startdate,2,'/'),listGetAt(startdate,1,'/'))>
            <cfset postdata['CATEGORY'] = cateobjid>
            <cfset postdata['USER'] = userobjid>
            <cfset postid = MongoCollectioninsert(application.applicationname,'post',postdata)>
            <cfset checktag(tagtext)>
            <cfset addtag(tagtext,postid.toString())>
            <cfset postcount++>
        </cfloop>
    <cfelse>
        <cfloop list="#structKeyList(lipsumtitle,',')#" index="title">
            <cfset post = mongonew('post')>
            <cfset post["TITLE"] = "#lipsumtitle[title]##postcount#">
            <cfset post["SLUG"] = replaceNoCase("#lipsumtitle[title]##catetitle##postcount#", " ", "-","all")>
            <cfset usetext = lipsumtext[title]>
            <cfset countlist = arraylen(ListToArray(usetext,'.'))>
            <cfset introtext = "#listgetat(usetext,RandRange(1,countlist))#.#listgetat(usetext,RandRange(1,countlist))#">
            <cfset post["INTRO"] = introtext>
            <cfset post["DETAIL"] = usetext>
            <!--- <cfif catetitle neq "Product"> --->
                <cfset post["PRICE"] = 0>
                <cfset post["SKU"] = "">
            <!--- <cfelse>
                <cfset post["PRICE"] = RandRange(100,1000)>
                <cfset post["SKU"] = "SK#replaceNoCase(createuuid(),'-','','all')#">
            </cfif> --->
            <cfset post["GALLERY"] = arrayNew()>
            <cfset rawtag = replaceNoCase(usetext,'.',',','all')>
            <cfset rawtag = replaceNoCase(rawtag,';',',','all')>
            <cfset rawtag = replaceNoCase(rawtag,':',',','all')>
            <cfset rawtag = replaceNoCase(rawtag,' ',',','all')>
            <cfset rawtag = replaceNoCase(rawtag,'"',',','all')>
            <cfset rawtag = replaceNoCase(rawtag,"'",",",'all')>
            <cfset countlisttag = arraylen(ListToArray(rawtag,','))>
            <cfset tagtext = "">
            <cfloop from="1" to="#RandRange(1,6)#" index="i">
                <cfset tagtext = listappend(tagtext,listgetat(rawtag,RandRange(1,countlisttag)),',')>
            </cfloop>
            <cfset result = mongovalidate('post','create',post)>
            <cfset postdata = assignlang('post',result['value'])>
            <cfset postdata['TAG'] = ListToArray(tagtext,',')>
            <cfset postdata['STHUMB'] = setimage[title]>
            <cfset postdata['LTHUMB'] = setimage[title]>
            <cfset postdata['TYPECONTENT'] = 1>
            <cfset postdata['PUBSTATUS'] = 1>
            <cfset postdata['PRIVATE'] = "false">
            <cfset startdate = DateFormat(now(),'dd/mm/yyyy')>
            <cfset postdata['PUBDATE'] = createDate(listGetAt(startdate,3,'/'),listGetAt(startdate,2,'/'),listGetAt(startdate,1,'/'))>
            <cfset postdata['CATEGORY'] = cateobjid>
            <cfset postdata['USER'] = userobjid>
            <cfset postid = MongoCollectioninsert(application.applicationname,'post',postdata)>
            <cfset checktag(tagtext)>
            <cfset addtag(tagtext,postid.toString())>
            <cfset postcount++>
        </cfloop>
    </cfif>
</cfloop>
<!--- Html --->
<!--- Contact--->
<cfset post = mongonew('post')>
<cfset post["TITLE"] = "Contact">
<cfset post["SLUG"] = "Contact">
<cfset usetext = "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?">
<cfset countlist = arraylen(ListToArray(usetext,'.'))>
<cfset introtext = "#listgetat(usetext,RandRange(1,countlist))#.#listgetat(usetext,RandRange(1,countlist))#">
<cfset post["INTRO"] = introtext>
<cfset post["DETAIL"] = usetext>
<cfset post["PRICE"] = 0>
<cfset post["SKU"] = "">
<cfset post["GALLERY"] = arrayNew()>
<cfset result = mongovalidate('post','create',post)>
<cfset postdata = assignlang('post',result['value'])>
<cfset postdata['TAG'] = "[]">
<cfset postdata['STHUMB'] = setimage[1]>
<cfset postdata['LTHUMB'] = setimage[1]>
<cfset postdata['TYPECONTENT'] = 2>
<cfset postdata['PUBSTATUS'] = 1>
<cfset postdata['PRIVATE'] = "false">
<cfset startdate = DateFormat(now(),'dd/mm/yyyy')>
<cfset postdata['PUBDATE'] = createDate(listGetAt(startdate,3,'/'),listGetAt(startdate,2,'/'),listGetAt(startdate,1,'/'))>
<cfset postdata['USER'] = userobjid>
<cfset postid = MongoCollectioninsert(application.applicationname,'post',postdata)>
<!---        About--->
<cfset post = mongonew('post')>
<cfset post["TITLE"] = "About">
<cfset post["SLUG"] = "About">
<cfset usetext = "On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.">
<cfset countlist = arraylen(ListToArray(usetext,'.'))>
<cfset introtext = "#listgetat(usetext,RandRange(1,countlist))#.#listgetat(usetext,RandRange(1,countlist))#">
<cfset post["INTRO"] = introtext>
<cfset post["DETAIL"] = usetext>
<cfset post["PRICE"] = 0>
<cfset post["SKU"] = "">
<cfset post["GALLERY"] = arraynew()>
<cfset result = mongovalidate('post','create',post)>
<cfset postdata = assignlang('post',result['value'])>
<cfset postdata['TAG'] = "[]">
<cfset postdata['STHUMB'] = setimage[2]>
<cfset postdata['LTHUMB'] = setimage[2]>
<cfset postdata['TYPECONTENT'] = 2>
<cfset postdata['PUBSTATUS'] = 1>
<cfset postdata['PRIVATE'] = "false">
<cfset startdate = DateFormat(now(),'dd/mm/yyyy')>
<cfset postdata['PUBDATE'] = createDate(listGetAt(startdate,3,'/'),listGetAt(startdate,2,'/'),listGetAt(startdate,1,'/'))>
<cfset postdata['USER'] = userobjid>
<cfset postid = MongoCollectioninsert(application.applicationname,'post',postdata)>
<!--- End html --->
<!--- Slider --->
<cfset MongoCollectiondrop(application.applicationname,"slide")>
<cfset numrandom = RandRange(1,16)>
<cfset slide = mongonew('slide')>
<cfset slide["TITLE"] = lipsumtitle[numrandom]>
<cfset slide["SLUG"] = "Slider">
<cfset usetext = lipsumtext[numrandom]>
<cfset countlist = arraylen(ListToArray(usetext,'.'))>
<cfset introtext = "#listgetat(usetext,RandRange(1,countlist))#.#listgetat(usetext,RandRange(1,countlist))#">
<cfset slide["INTRO"] = introtext>
<cfset slide["DETAIL"] = usetext>
<cfset result = mongovalidate('slide','create',slide)>
<cfset slidedata = assignlang('slide',result['value'])>
<cfset slidedata['TAG'] = ListToArray(tagtext,',')>
<cfset slidedata['STHUMB'] = setimage[numrandom]>
<cfset slidedata['LTHUMB'] = setimage[numrandom]>
<cfset slidedata['USER'] = userobjid>
<!--- Gallery --->
<cfset arrayslide = arrayNew()>
<cfloop array="#slideimage#" index="sitem">
    <cfset imagenotpath = listLast(sitem, '/')>
    <cfset slidest = structNew()>
    <cfset slidest["title"] = listFirst(imagenotpath,'.')>
    <cfset slidest["detail"] = listFirst(imagenotpath,'.')>
    <cfset slidest["link"] = "">
    <cfset slidest["image"] = sitem>
    <cfset arrayAppend(arrayslide, slidest)>
</cfloop>
<cfset slidedata["GALLERY"] = arrayslide>
<!--- End Gallery --->
<cfset slideid = MongoCollectioninsert(application.applicationname,'slide',slidedata)>
<!--- End slide --->
<!--- Gallery --->
<cfset MongoCollectiondrop(application.applicationname,"gallery")>
<cfset numrandom = RandRange(1,16)>
<cfset gallery = mongonew('gallery')>
<cfset gallery["TITLE"] = lipsumtitle[numrandom]>
<cfset gallery["SLUG"] = replaceNoCase("#lipsumtitle[numrandom]##catetitle#-gallery", " ", "-","all")>
<cfset usetext = lipsumtext[numrandom]>
<cfset countlist = arraylen(ListToArray(usetext,'.'))>
<cfset introtext = "#listgetat(usetext,RandRange(1,countlist))#.#listgetat(usetext,RandRange(1,countlist))#">
<cfset gallery["INTRO"] = introtext>
<cfset gallery["DETAIL"] = usetext>
<cfset result = mongovalidate('gallery','create',gallery)>
<cfset gallerydata = assignlang('gallery',result['value'])>
<cfset gallerydata['TAG'] = ListToArray(tagtext,',')>
<cfset gallerydata['STHUMB'] = setimage[numrandom]>
<cfset gallerydata['LTHUMB'] = setimage[numrandom]>
<cfset gallerydata['USER'] = userobjid>
<!--- Gallery --->
<cfset arraygallery = arrayNew()>
<cfloop array="#setimage#" index="sitem">
    <cfset imagenotpath = listLast(sitem, '/')>
    <cfset galleryst = structNew()>
    <cfset galleryst["title"] = listFirst(imagenotpath,'.')>
    <cfset galleryst["detail"] = listFirst(imagenotpath,'.')>
    <cfset galleryst["link"] = "">
    <cfset galleryst["image"] = sitem>
    <cfset arrayAppend(arraygallery, galleryst)>
</cfloop>
<cfset gallerydata["GALLERY"] = arraygallery>
<!--- End Gallery --->
<cfset galleryid = MongoCollectioninsert(application.applicationname,'gallery',gallerydata)>
<!--- End Gallery --->
<!---Create Menu--->
<cfset MongoCollectiondrop(application.applicationname,"frontendmenu")>
<cfset frontendmenu = mongonew('frontendmenu')>
<cfset frontendmenu['NAME'] = 'mainmenu'>
<cfset frontendmenu['DESCRIPTION'] = 'main menu'>
<cfset frontendmenu['MENUID'] = ''>
<cfset frontendmenu['MENUCLASS'] = 'nav'>
<cfset frontendmenu['MENU'] = '{"menu":[{"index":"1","name":"Home","description":"Home","class":"","id":"","linkurl":"index.cfm","linkname":"Home","linktype":"3","imagefile":"","imagename":""},{"index":"2","name":"News","description":"News","class":"","id":"","linkurl":"news.cfm?category=News","linkname":"News","linktype":"3","imagefile":"","imagename":""},{"index":"3","name":"Blog","description":"Blog","class":"","id":"","linkurl":"blog.cfm","linkname":"Blog","linktype":"3","imagefile":"","imagename":""},{"index":"4","name":"Gallery","description":"Gallery","class":"","id":"","linkurl":"gallery.cfm","linkname":"gallery","linktype":"3","imagefile":"","imagename":""},{"index":"5","name":"About","description":"About","class":"","id":"","linkurl":"about.cfm","linkname":"About","linktype":"3","imagefile":"","imagename":""},{"index": "6","name": "Categories","description": "Categories","class": "","id": "","linkurl": "categories.cfm","linkname": "categories","linktype": "3","imagefile": "","imagename": ""},{"index": "10","name": "Form","description": "Form New","class": "","id": "","linkurl": "formnew.cfm","linkname": "formnew","linktype": "3","imagefile": "","imagename": "","children": [{"index": "7","name": "Form New","description": "Form New","class": "","id": "","linkurl": "formnew.cfm","linkname": "formnew","linktype": "3","imagefile": "","imagename": ""},{"index": "8","name": "Form List","description": "Form List","class": "","id": "","linkurl": "formlist.cfm","linkname": "formlist","linktype": "3","imagefile": "","imagename": ""}]},{"index": "9","name": "Contact Us","description": "Contact us","class": "","id": "","linkurl": "contact.cfm","linkname": "contact","linktype": "3","imagefile": "","imagename": ""}]}'>
<cfset menuresult = mongovalidate('frontendmenu','create',frontendmenu)>
<cfset menuid = MongoCollectioninsert(application.applicationname,'frontendmenu',menuresult['value'])>
<cfset menuresult['value']['MENU']=deserializeJSON(menuresult['value']['MENU'])>
<cfset menuresult['value']['ID']=menuid>
<cfset menufilename = menuresult['value']['NAME']>
<cfset jsondata = serializeJSON(menuresult['value'])>
<cfset menufilepath="#expandPath("/menu")#">
<cfif NOT DirectoryExists(menufilepath)>
    <cfdirectory action="create" mode="777"  directory="#menufilepath#">
</cfif>
<cffile action = "write" file = "#menufilepath#/#menufilename#.json" output = "#jsondata#" addNewLine = "yes" charset = "utf-8" fixnewline = "no" mode = "777">
<!---End Menu--->

<cffunction name="movefilesample" access="public" returntype="string">
    <cfargument name="filename" required="true" type="string">
    <cfargument name="sourcefolder" type="string" required="false" default="files">
    <cfargument name="destinationpath" type="string" required="false" default="files">
    <cfargument name="username" required="true" type="string">
    <cfset var filename = trim(arguments['filename'])>
    <cfset var username = arguments['username']>
    <cfset var despath = arguments['destinationpath']>
    <cfset var newserverfile = username&replaceNoCase(listfirst(filename,"."),' ','','all')&DateFormat(Now(),"YYYYMMDD")&TimeFormat(Now(),"HHmmss")&"."&listLast(filename,".") />
    <cfset var targetpath = "#get('pathupload')##despath#">
    <cfset var newserverfilePath = "#targetpath#/#newserverfile#" />
    <cfif directoryExists(targetpath) eq false>
        <cfdirectory action = "create" directory = "#targetpath#" >
    </cfif>
    <cffile action="copy" destination="#newserverfilePath#" source="#expandPath('./')#sampledata/image/#arguments['sourcefolder']#/#filename#" mode="777">
    <cfreturn newserverfile>
</cffunction>

<!--- Create CategoryImage --->
<cffunction name="$createcategoryimage" access="public" returntype="any">
	<cfargument name="categoryimagetitle" required="true" type="string">
	<cfset var categoryimage = mongonew('categoryimage')>
	<cfset categoryimage["TITLE"] = arguments.categoryimagetitle>
	<cfset categoryimage["PATTERN"] = "">
	<cfset var result = mongovalidate('categoryimage','create',categoryimage)>
	<cfset result['value']['USER'] = userobjid>
	<cfset var cateimgid = MongoCollectioninsert(application.applicationname,'categoryimage',result['value'])>
	<cfset var cateimgobjid = dbref('categoryimage',cateimgid.toString())>
	<cfreturn cateimgobjid>
</cffunction>

