<cfinclude template="imagedefaultsize.cfm">
<cfoutput>
    #stylesheetinclude('jquery-ui-1.9.0.custom,customui')#
    <script src="ckeditor/ckeditor.js"></script>
    <script src="ckeditor/adapters/jquery.js"></script>
    <script src="ckeditor/toolbargroupconfig.js"></script>
    <div class="row-fluid">
        <ul class="nav nav-tabs" id="tabs">
            <li class="active"><a href="##tabs-general" data-toggle="tab">General</a></li>
            <li><a href="##tabs-content" data-toggle="tab">Content</a></li>
            <li><a href="##tabs-gallery" data-toggle="tab">Gallery</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tabs-general">
                #textfield(name='title',label='title',value=htmlEditFormat(gallery['title']),require=true)#
                #textfield(name='slug',label='slug',value=gallery['slug'],require=true)#
                <div class="control-group">
                    <label class="control-label">Thumbnail</label>
                    <div class="controls">
                        <ul class="thumbnails">
                            <li class="span2">
                                <a href="javascript:selectthumb('simage')" class="btn">Select</a>
                                #hiddenfield(name='sthumb',value=gallery['sthumb'])#
                                #hiddenfield(name='newsimage')#
                            </li>
                            <li class="span3" id="showsimage">
                                <div class="thumbnail thumbnail-empty">
                                    <div class="thumbnail-empty-text">
                                        New Image
                                    </div>
                                    <div class="thumbnail-empty-size">
										<!---   slideshow   largeimage  thumbnail   categoryimage   menuimage   otherimage	--->
										#$showimagedefaultsize("thumbnail","text")#
                                    </div>
                                </div>
                            </li>
                            <cfif gallery['sthumb'] neq ''>
                                <li class="span3">
                                    <div class="thumbnail">
                                        <a class="icon-remove pull-left" href="javascript:void(0)" onclick="removeimage('newsimage',this)"></a>
                                        <a class="icon-zoom-in pull-right" href="javascript:previewimage('#gallery["sthumb"]#')"></a>
                                        <a href="javascript:previewimage('#gallery["sthumb"]#')">
                                        <img src="#gallery['sthumb']#" class="img-thumb img-rounded"/>
                                        </a>
                                        <p class="thumbnail-text">Current Thumbnail</p>
                                    </div>
                                </li>
                            </cfif>
                        </ul>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Large image</label>
                    <div class="controls">
                        <ul class="thumbnails">
                            <li class="span2">
                                <a href="javascript:selectthumb('limage')" class="btn">Select</a>
                                #hiddenfield(name='lthumb',value=gallery['lthumb'])#
                                #hiddenfield(name='newlimage')#
                            </li>
                            <li class="span3" id="showlimage">
                                <div class="thumbnail thumbnail-empty">
                                    <div class="thumbnail-empty-text">
                                        New Image
                                    </div>
                                    <div class="thumbnail-empty-size">
										<!---   slideshow   largeimage  thumbnail   categoryimage   menuimage   otherimage	--->
										#$showimagedefaultsize("largeimage","text")#
                                    </div>
                                </div>
                            </li>
                            <cfif gallery['lthumb'] neq ''>
                                <li class="span3">
                                    <div class="thumbnail">
                                        <a class="icon-remove pull-left" href="javascript:void(0)" onclick="removeimage('newlimage',this)"></a>
                                        <a class="icon-zoom-in pull-right" href="javascript:previewimage('#gallery["lthumb"]#')"></a>

			                                <a href="javascript:previewimage('#gallery["lthumb"]#')">
                                            <img src="#gallery['lthumb']#" class="img-thumb img-rounded"/>
                                            </a>

                                        <p class="thumbnail-text">Current Thumbnail</p>
                                    </div>
                                </li>
                            </cfif>
                        </ul>
                    </div>
                </div>
                <div class="form_showdefaultsize">
                <!---   slideshow   largeimage  thumbnail   categoryimage   menuimage   otherimage  --->
                    #$showimagedefaultsize("thumbnail,largeimage")#
                </div>
            </div>
            <div class="tab-pane" id="tabs-content">
                #textarea(name='intro',label='intro',value=gallery['intro'],rows=3,class='span11')#
                #textarea(name='detail',label='detail',value=gallery['detail'],class='span12')#
            </div>
            <div class="tab-pane" id="tabs-gallery">
                <div class="row-fluid">
                    <input type="button" value="Browse" onclick="browseasset('gallery')" class="btn btn-mini"/>
                    #hiddenfield(name="galleryjson")#
                </div>
                <div class="row-fluid content-preview">
                    <style>
                        .form-horizontal .gallerycontrols {
                            margin-left: 0;
                        }
                        .icon-trash{
                            margin-top: 9px;
                            margin-right: 10px;
                        }
                        .accordion-toggle{
                            margin-bottom: 0!important;
                        }
                    </style>
                    <div class="accordion" id="gallerypreview">
                        <cfif structKeyExists(gallery, "GALLERY") && IsArray(gallery["GALLERY"])>
                            <cfloop array="#gallery["GALLERY"]#" index="i" delimiters="," item="gitem">
                        <div class="accordion-group">
                            <div class="accordion-heading row-fluid">
                                <a class="accordion-toggle span11 pull-left" data-toggle="collapse" href="##collapse#i#">
                                    <img src="#gitem['image']#" class="img-relate-thumb img-rounded">
                                </a>
                                <a class="icon-trash pull-right" onclick="removeGallery(this);" href="javascript:void(0);"></a>
                            </div>
                            <div id="collapse#i#" class="accordion-body collapse">
                                <div class="accordion-inner">
                                    <div class="detailsession">
                                        <input type="hidden" value="#gitem['image']#" name="galleryimage" class="galleryimage">
                                        <div class="gallerycontrols">
                                            <label>Title</label>
                                            <input type="text" name="gallerytitle" class="gallerytitle span12" value="#gitem['title']#">
                                        </div>
                                        <div class="gallerycontrols">
                                            <label>Detail</label>
                                            <textarea name="gallerydetail" class="gallerydetail span12">#gitem['detail']#</textarea>
                                        </div>
                                        <div class="gallerycontrols">
                                            <label>Link</label>
                                            <input type="text" name="gallerylink" class="gallerylink span12" value="#gitem['link']#">
                                        </div>
                                    </div>            
                                </div>
                            </div>
                        </div>
                            </cfloop>
                        </cfif>
                    </div>
                </div>
            </div>                
        </div>
    </div>
</cfoutput>
<!--- Modal Choose Image --->
<div class="modal hide fade bigModal" id="bModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Browse</h3>
    </div>
    <div class="modal-body-bModal">

    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <a onclick="selectobject()" class="btn btn-primary">OK</a>
    </div>
</div>
<!---    Preview Thumb--->
<div class="modal hide fade" id="pModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Preview Image</h3>
	</div>
	<div class="modal-body" id="previewimage">

	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>
<cfinclude template="galleriesjsconfig.cfm">
<script type="text/javascript">
    var typeimage = '';
    var typeopen = '';
    var imagecount = 0;
    $(function(){
        imagecount = $('#gallerypreview .accordion-group').size()+1;
        $( '#detail' ).ckeditor({
                extraPlugins : 'video,downloadfile,iframe,addlink',
                toolbar: toolbarfullconfig,
                filebrowserImageBrowseUrl:seturlbase()+"/browseck.cfm?type=image",
                filebrowserBrowseUrl:seturlbase()+"/browseck.cfm",
                filebrowserWindowWidth : '770',
                filebrowserWindowHeight : '750',
                height : '400'               
            }); 
        $('#title').focusout(function(){
                if ($('#slug').val() == '')
                {
                    $('#slug').val(replacetext($('#title').val()));
                }
            });
        $('#slug').focusout(function(){
            $('#slug').val(replacetext($('#slug').val()));
        });
        $("form").submit(function() {
            $('#slug').val(replacetext($('#slug').val()));
            gallerytojson();
        });

        $(".thumbnail-empty").height($(".thumbnail-empty").parent().next().find(".thumbnail").height());
        if ($(".thumbnail-empty").parent().next().find(".thumbnail").length > 0){
            $(".thumbnail-empty-text").css("padding-top",$(".thumbnail-empty").parent().next().find(".thumbnail").height()*0.5-10);
        }else{
            $(".thumbnail-empty-text").css("padding-top",$(".thumbnail-empty").height()*0.5-10);
        }
        $( "#gallerypreview" ).sortable({
            placeholder: "ui-state-highlight"
        });
    });

    function selectthumb(type){
        typeimage = type;
        browsewindow('image');
    }

    function browseasset(){
        typeimage = "gallery";
        typeopen = "image";
        $(".modal-body-bModal").html('<iframe id="modalIframeId" width="720" height="620" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" />');
        $("#modalIframeId").attr("src",seturlbase()+"/browse.cfm?type=image&modeselect=multi");
        $('#bModal').modal('show');
    }

    function browsewindow(type){
        typeopen = type;
        $(".modal-body-bModal").html('<iframe id="modalIframeId" width="720" height="620" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="no" />');
        $("#modalIframeId").attr("src",seturlbase()+"/browse.cfm?type="+type);
        $('#bModal').modal('show');
    }

    $('#bModal').on('hidden', function () {
        $(".modal-body-bModal").html('');
    });

    function selectedcomplete(fileraw)
    {
        if (typeopen == 'image')
        {
            if(typeimage == 'markdown')
            {
                var filearr = fileraw[0].split("|");
                var linkinsert = '';
                <cfoutput>
                linkinsert = '![img](#get('pathuploadfolder')#image/'+filearr[0]+' "'+filearr[1]+'")'
                </cfoutput>
                $.markItUp({ target:'#detail',replaceWith: linkinsert });
            }else{
                if(typeimage == 'simage'){
                    var filearr = fileraw[0].split("|");
                    var linkinsert = '';
                    <cfoutput>
                    linkinsert = '#get('pathuploadfolder')#image/'+filearr[0];
                    </cfoutput>
                    imgstr = '<div class="thumbnail"><img src="'+linkinsert+'" class="img-thumb img-rounded"/><p class="thumbnail-text">New Thumbnail</p></div>'
                    $('#showsimage').html(imgstr);
                    $('#newsimage').val(linkinsert);
                }else if(typeimage == 'limage'){
                    var filearr = fileraw[0].split("|");
                    var linkinsert = '';
                    <cfoutput>
                    linkinsert = '#get('pathuploadfolder')#image/'+filearr[0];
                    </cfoutput>
                    imgstr = '<div class="thumbnail"><img src="'+linkinsert+'" class="img-thumb img-rounded"/><p class="thumbnail-text">New Thumbnail</p></div>'
                    $('#showlimage').html(imgstr);
                    $('#newlimage').val(linkinsert);
                }else{
                    $.each(fileraw,function(index,value){
                        var sp = value.split("|");
var htmlstr = '<div class="accordion-group">';
htmlstr += '<div class="accordion-heading row-fluid">';
htmlstr += '<a class="accordion-toggle span11 pull-left" data-toggle="collapse" href="##collapse'+imagecount+'">';
htmlstr += '<img src="<cfoutput>#get('pathuploadfolder')#</cfoutput>image/'+sp[0]+'" class="img-relate-thumb img-rounded"></a>';
htmlstr += '<a class="icon-trash pull-right" onclick="removeGallery(this);" href="javascript:void(0);"></a></div>';
htmlstr += '<div id="collapse'+imagecount+'" class="accordion-body collapse in">';
htmlstr += '<div class="accordion-inner">';
htmlstr += '<div class="detailsession">';
htmlstr += '<input type="hidden" value="<cfoutput>#get('pathuploadfolder')#</cfoutput>image/'+sp[0]+'" name="galleryimage" class="galleryimage">';
htmlstr += '<div class="gallerycontrols"><label>Title</label><input type="text" name="gallerytitle" class="gallerytitle span12" value="'+sp[1]+'"></div>';
htmlstr += '<div class="gallerycontrols"><label>Detail</label><textarea name="gallerydetail" class="gallerydetail span12"></textarea></div>';
htmlstr += '<div class="gallerycontrols"><label>Link</label><input type="text" name="gallerylink" class="gallerylink span12" value=""></div>';
htmlstr += '</div></div></div></div>';
                        // var htmlstr = '<li class="span11"><input name="gallery" type="hidden" value="'+value+'">'
                        // htmlstr += '<div class="thumbnail"><a href="javascript:void(0);" onclick="removeGallery(this);" class="icon-trash pull-right"></a>';
                        // htmlstr += '<img class="img-relate-thumb img-rounded" src="<cfoutput>#get('pathuploadfolder')#</cfoutput>image/'+sp[0]+'">';
                        // htmlstr += '<p>'+sp[1]+'</p>';
                        // htmlstr += '</div></li>';
                        $('#gallerypreview').append(htmlstr);
                        imagecount++;
                    })
                }
            }
        }
        if (typeopen == 'video')
        {
            $.markItUp({ target:'#detail',replaceWith: fileraw[0] });
        }
        if (typeopen == 'file')
        {
            var filearr = fileraw[0].split("|");
            var linkinsert = '';
            <cfoutput>
            linkinsert = '['+filearr[0]+'](#get('pathuploadfolder')#file/'+filearr[1]+' "'+filearr[0]+'")'
            </cfoutput>
            $.markItUp({ target:'#detail',replaceWith: linkinsert });
        }
        if (typeopen == 'link')
        {
            var filearr = fileraw[0].split("|");
            var linkinsert = '';
            linkinsert = '['+filearr[1]+']('+filearr[0]+' "'+filearr[1]+'")'
            $.markItUp({ target:'#detail',replaceWith: linkinsert });
        }
    }

    function selectobject()
    {
        document.getElementById('modalIframeId').contentWindow.selectfile();
    }

    function previewimage(imagename)
    {
        var pathpreview = '<div class="thumbnail"><img src="'+imagename+'"/></div>';
        $('#previewimage').html(pathpreview);
        $('#pModal').modal('show');
    }

    $('#pModal').on('hidden', function () {
        $("#previewimage").html('');
    });
    
    function removeimage(imageremove,obj)
    {
        $(obj).parent().remove();
        $('#'+imageremove).val('remove');
    }

    function removeGallery(obj){
        $(obj).parent().parent().remove();
    }

    function gallerytojson()
    {
        var galleryjson = [];
        $('#gallerypreview .accordion-group').each(function(){
            var gallerytitle = $(this).find(".gallerytitle").val();
            var gallerydetail = $(this).find(".gallerydetail").val();
            var gallerylink = $(this).find(".gallerylink").val();
            var galleryimage = $(this).find(".galleryimage").val();
            galleryjson.push('{"image":"'+galleryimage+'","title":"'+gallerytitle+'","detail":"'+gallerydetail+'","link":"'+gallerylink+'"}');
            $('#galleryjson').val('['+galleryjson+']');
        })
    }
</script>