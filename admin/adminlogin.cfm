<cfinclude template="include/header.cfm" runonce="true">
<cfoutput>
    <div class="row-fluid">
    #startForm(action="adminloginprocess")#
        <div class="span6 offset2">
            <legend>Administrator</legend>
            #textField(name='username',label='username')#
            #passwordField(name='password',label='password')#
            #submit(label='Login')#
        </div>
    #endForm()#
    </div>
</cfoutput>
<div id="footer">
    <div class="offset2">
        <p>Copyright &copy; Skoode Skill Co., Ltd.</p>
        <p>Lithe CMS is a LGPL license software by Skoode Skill Co.,Ltd.</p>
        <p>Find out more detail at http://www.openlithe.org</p>
    </div>
</div>
</div>
</body>
</html>
<script>
    $(document).ready(function() {
        $('#password').keypress(function(event) {
            if (event.which == '13') {
                $('form').submit();
            }
        });
    });
</script>