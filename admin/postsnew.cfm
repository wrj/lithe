<cfinclude template="include/header.cfm">
<cfif isdefined('ERROR')>
    <cfset post = getstoredata()>
    <cfif structIsEmpty(post)>
        <cfset post = mongonew('post')>
    </cfif>
    <cfset post['category'] = ''>
    <cfif isdefined('category')>
        <cfset post['category'] = category>
    </cfif>
    <cfset post['tag'] = ''>
<cfelse>
    <cfset post = mongonew('post')>
    <cfset post['category'] = ''>
    <cfif isdefined('category')>
        <cfset post['category'] = category>
    </cfif>
    <cfset post['tag'] = ''>
</cfif>
<cfset modework = 'create'>
<cfoutput>
    <div class="row-fluid">
        <legend class="titlepage">New post</legend>
        #startform(action='postscontroller')#
            #hiddenfield(name='mode',value='create')#
            <cfinclude template="postsform.cfm">
            #submit(label=get('labelbtnadd'))#
        #endform()#
    </div>
</cfoutput>
<cfinclude template="include/footer.cfm">