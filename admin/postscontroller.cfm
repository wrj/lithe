<cfinclude template="system/loadplugins.cfm">
<cfset structDelete(form,'FIELDNAMES')>
<cfset structDelete(form,'GALLERYDETAIL')>
<cfset structDelete(form,'GALLERYIMAGE')>
<cfset structDelete(form,'GALLERYLINK')>
<cfset structDelete(form,'GALLERYTITLE')>
<cfswitch expression="#mode#">
    <cfcase value="create">
        <cfset structDelete(form,'mode')>
        <cfif form['GALLERYJSON'] neq "">
            <cfset postgallery = deserializeJSON(form['GALLERYJSON'])>
        <cfelse>
            <cfset postgallery = arrayNew()>
        </cfif>
        <cfset structDelete(form,'GALLERYJSON')>
        <cfset result = mongovalidate('post','create',form)>
        <cfif result['result'] eq true>
<!---            Language--->
            <cfset postdata = assignlang('post',result['value'])>
            <cfset postdata["GALLERY"] = postgallery>
            <cfif postdata['newsimage'] neq ''>
                <cfset postdata['STHUMB'] = postdata['newsimage']>
            </cfif>
            <cfif postdata['newlimage'] neq ''>
                <cfset postdata['LTHUMB'] = postdata['newlimage']>
            </cfif>
            <cfif postdata['price'] neq ''>
                <cfset postdata['PRICE'] = val(postdata['price'])>
            </cfif>
            <cfset structdelete(postdata,'newsimage')>
            <cfset structdelete(postdata,'newlimage')>
<!---            Category--->
            <cfset postdata['CATEGORY'] = dbref('category',postdata['CATEGORY'])>
<!---            startdate--->
            <cfif form['PUBDATE'] neq ''>
                <cfset startdate = form['PUBDATE']>
            <cfelse>
                <cfset startdate = DateFormat(now(),'dd/mm/yyyy')>
            </cfif>
            <cfset listtag = form['TAG']>
            <cfset postdata['TAG'] = ListToArray(form['TAG'],',')>
            <cfset postdata['TYPECONTENT'] = 1>
            <cfset postdata['PUBSTATUS'] = val(postdata['PUBSTATUS'])>
            <cfset postdata['PUBDATE'] = createDate(listGetAt(startdate,3,'/'),listGetAt(startdate,2,'/'),listGetAt(startdate,1,'/'))>
            <cfset postdata['USER'] = dbref('user',session['userid'])>
            <cfset postid = MongoCollectioninsert(application.applicationname,'post',postdata)>
            <cfset checktag(listtag)>
            <cfset addtag(listtag,postid.toString())>
            <cfset flashinsert('success','Create post complete')>
            <cflocation url="posts.cfm?category=#result['value']['CATEGORY']#" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error creating the post.')>
            <cfset storedata(result)>
            <cflocation url="postsnew.cfm?category=#form['CATEGORY']#" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="update">
        <cfset keyid = key>
        <cfset structDelete(form,'KEY')>
        <cfset structDelete(form,'MODE')>
        <cfif form['GALLERYJSON'] neq "">
            <cfset postgallery = deserializeJSON(form['GALLERYJSON'])>
        <cfelse>
            <cfset postgallery = arrayNew()>
        </cfif>
        <cfset structDelete(form,'GALLERYJSON')>
        <cfset result = mongovalidate('post','update',form)>
        <cfif result['result'] eq true>
            <cfif form['NEWSIMAGE'] neq ''>
                <cfif form['NEWSIMAGE'] neq 'remove'>
                    <cfset result['value']['STHUMB'] = form['NEWSIMAGE']>
                <cfelse>
                    <cfset result['value']['STHUMB'] = ''>
                </cfif>
            </cfif>
            <cfif form['NEWLIMAGE'] neq ''>
                <cfif form['NEWLIMAGE'] neq 'remove'>
                    <cfset result['value']['LTHUMB'] = form['NEWLIMAGE']>
                <cfelse>
                    <cfset result['value']['LTHUMB'] = ''>
                </cfif>
            </cfif>
            <cfif form['price'] neq ''>
                <cfset result['value']['PRICE'] = form['price']>
            </cfif>
            <cfset olddata = MongoCollectionfindone(application.applicationname,'post',{'_id'=newid(keyid)})>
            <cfset dataupdate = mongomapvalue('post',olddata,result['value'],lang)>
            <cfset structDelete(dataupdate, "GALLERY")>
            <cfset dataupdate["GALLERY"] = postgallery>
            <!---            Tag--->
            <cfset listtag = form['TAG']>
            <cfset dataupdate['TAG'] = ListToArray(form['TAG'],',')>
            <!---            Category--->
            <cfset dataupdate['CATEGORY'] = dbref('category',result['value']['CATEGORY'])>
            <!---            startdate--->
            <cfif form['PUBDATE'] neq ''>
                <cfset startdate = form['PUBDATE']>
            <cfelse>
                <cfset startdate = DateFormat(now(),'dd/mm/yyyy')>
            </cfif>
            <cfset dataupdate['TYPECONTENT'] = 1>
            <cfset dataupdate['PUBSTATUS'] = val(result['value']['PUBSTATUS'])>
            <cfset dataupdate['PUBDATE'] = createDate(listGetAt(startdate,3,'/'),listGetAt(startdate,2,'/'),listGetAt(startdate,1,'/'))>
            <cfset dataupdate['USER'] = dbref('user',session['userid'])>
            <cfset checktag(listtag)>
            <cfset updatetag(listtag,keyid)>
            <cfset MongoCollectionsave(application.applicationname,'post',dataupdate)>
            <cfset flashinsert('success','Update post complete')>
            <cflocation url="posts.cfm?category=#result['value']['CATEGORY']#&start=#displaystart#" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error update the post.')>
            <cfset storedata(result)>
            <cflocation url="postsupdate.cfm?key=#keyid#&langinput=#lang#&start=#displaystart#" addtoken="false">
        </cfif>
    </cfcase>

    <cfcase value="delete">
        <!--- find tag use in post --->
        <cfset tagdata = MongoCollectionfind(application.applicationname,'tag',{"POST.REFID"=key})>
        <cfloop array="#tagdata#" index="tag">
            <cfif arrayLen(tag["POST"]) eq 1>
                <cfset MongoCollectionremove(application.applicationname,'tag',{'_id'=newid(tag["_id"].toString())})>
            </cfif>
        </cfloop>
        <!--- delete key post in tag --->
        <cfset deltag(key)>
        <!--- loop tag data --->
        <cfset MongoCollectionremove(application.applicationname,'post',{'_id'=newid(key)})>
        <cfset result = structNew()>
        <cfset result['result'] = 'true'>
        <cfset result['message'] = "Delete post complete">
        <cfoutput>
            #serializeJSON(result)#
        </cfoutput>
    </cfcase>

</cfswitch>