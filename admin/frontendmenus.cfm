<cfinclude template="include/header.cfm">
<cfimport taglib="lib" prefix="ui">
<cfoutput>
	<div class="row-fluid">
		<div class="span12">
			<div class="row-fluid">
				<ui:datatable field="#capitalize('Name')#,#capitalize('Menu Id')#,#capitalize('Menu Class')#"/>
			</div>
		</div>
	</div>
	<div class="modal hide fade bigModal" id="delModal">
	    <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h3>Delete Menu</h3>
	    </div>
	    <div class="modal-body">
	        <p id="shownamedelete"></p>
	        <h2>Please note</h2>
	        <p>After you delete a menu.It'll not collect usage data anymore.</p>
	        <input type="hidden" id="iddelete" name="iddelete">
	    </div>
	    <div class="modal-footer">
	        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	        <a onclick="confirmdelete('frontendmenus');" class="btn btn-danger">OK</a>
	    </div>
	</div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<cfoutput>#javaScriptInclude("jquery.dataTables,DT_bootstrap,createdatatable,deletedialog")#</cfoutput>
<script type="text/javascript">
	createdatatable('usetable','frontendmenusgrid','');
	function clearall()
	{
	    oTable.fnDestroy();
	}
	function gonew()
	{
		window.location = "frontendmenusnew.cfm?category="+category;
	}
	function goedit(id)
	{
		window.location = "frontendmenusupdate.cfm?key="+id+"&start="+oTable.fnSettings()._iDisplayStart;
	}
</script>
