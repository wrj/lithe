<!---
  Created with IntelliJ IDEA.
  tag: arunwatd
  Date: 1/14/13
  Time: 3:27 PM
  To change this template use File | Settings | File Templates.
--->
<cfinclude template="system/loadplugins.cfm">
<cfset structDelete(form,'FIELDNAMES')>
<cfswitch expression="#mode#">
    <cfcase value="create">
        <cfset structDelete(form,'mode')>
        <cfset result = mongovalidate('tag','create',form)>
        <cfif result['result'] eq true>
            <cfset result['value']['USER'] = dbref('user',session['userid'])>
            <cfset MongoCollectioninsert(application.applicationname,'tag',result['value'])>
            <cfset flashinsert('success','Create tag complete')>
            <cflocation url="tags.cfm" addtoken="false">
            <cfelse>
            <cfset flashinsert('error','There was an error creating the tag.')>
            <cfset storedata(result)>
            <cflocation url="tagsnew.cfm" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="update">
        <cfset keyid = key>
        <cfset structDelete(form,'KEY')>
        <cfset structDelete(form,'MODE')>
        <cfset result = mongovalidate('tag','update',form)>
        <cfif result['result'] eq true>
            <cfset updateobj = result['value']>
            <cfset olddata = MongoCollectionfindone(application.applicationname,'tag',{'_id'=newid(keyid)})>
            <cfset dataupdate = mongomapvalue('tag',olddata,updateobj,'default')>
            <cfset dataupdate['USER'] = dbref('user',session['userid'])>
            <cfset structDelete(dataupdate,'mode')>
            <cfset MongoCollectionsave(application.applicationname,'tag',dataupdate)>
            <cfset flashinsert('success','Update tag complete')>
            <cflocation url="tags.cfm" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error update the tag.')>
            <cfset storedata(result)>
            <cflocation url="tagsupdate.cfm?key=#keyid#" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="delete">
        <!--- get tag name --->
        <cfset tagdata = MongoCollectionfindone(application.applicationname,'tag',{'_id'=newid(key)})>
        <!--- find post use tag --->
        <cfset postdata = MongoCollectionfind(application.applicationname,'post',{"TAG"={"$in"=["#tagdata["NAME"]#"]}})>
        <!--- loop post --->
        <cfloop array="#postdata#" index="post">
            <!--- set category --->
            <cfset post["CATEGORY"] = dbref("category",post["CATEGORY"].fetch()["_id"].toString())>
            <!--- set user --->
            <cfset post["USER"] = dbref("user",post["USER"].fetch()["_id"].toString())>
            <!--- filter tag delete --->
            <cfset newtagarray = ArrayNew()>
            <cfloop array="#post["TAG"]#" index="tag">
                <cfif tag neq tagdata["NAME"]>
                    <cfset arrayAppend(newtagarray, tag)>    
                </cfif>
            </cfloop>
            <cfset post["TAG"] = newtagarray>
            <!--- end filter tag delete --->
            <!--- update post --->
            <cfset MongoCollectionsave(application.applicationname,'post',post)>
        </cfloop>
        <!--- delete tag --->
        <cfset MongoCollectionremove(application.applicationname,'tag',{'_id'=newid(key)})>
        <cfset result = structNew()>
        <cfset result['result'] = 'true'>
        <cfset result['message'] = "Delete tag complete">
        <cfoutput>
            #serializeJSON(result)#
        </cfoutput>
    </cfcase>

</cfswitch>