<cfinclude template="include/header.cfm">
<cfimport taglib="lib" prefix="ui">
<cfoutput>
<div class="row-fluid">
    <div class="span3">
        <p>Categories</p>
        #treecategoryoutput()#
    </div>
    <div class="span9">
        <p class="headpage">Posts</p>
        <div class="row-fluid">
            <ui:datatable field="#capitalize('title')#,#capitalize('publish date')#,#capitalize('date modified')#" size="0,150,150"/>
        </div>
    </div>
</div>
<div class="modal hide fade bigModal" id="delModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Delete Post</h3>
    </div>
    <div class="modal-body">
        <p id="shownamedelete"></p>
        <h2>Please note</h2>
        <p>After you delete an post.It'll not collect usage data anymore.</p>
        <input type="hidden" id="iddelete" name="iddelete">
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <a onclick="confirmdelete('posts');" class="btn btn-danger">OK</a>
    </div>
</div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<cfoutput>#javaScriptInclude("jquery.dataTables,DT_bootstrap,createdatatable,deletedialog")#</cfoutput>
<script type="text/javascript">
    var visitfirst = true;
    var category = '';

    $(function(){
        createtree('tree');
    })

    function reportkey(key){
        category = key;
        inittable();
    }

    function inittable()
    {
        if(visitfirst == true)
        {
            visitfirst = false;
        }else{
            startpage = 0;
            clearall();
        }
        var linkvalue = '?category='+category;
        createdatatable('usetable','postsgrid',linkvalue,'3/desc');
    }

    function clearall()
    {
        oTable.fnDestroy();
    }

    function gonew()
    {
        window.location = "postsnew.cfm?category="+category;
    }

    function goedit(id)
    {
        window.location = "postsupdate.cfm?key="+id+"&start="+oTable.fnSettings()._iDisplayStart;
    }

</script>
