<cfset table = StructNew()/>
<cfset table['tablefield'] = "TITLE,SLUG,IMAGEREF,TEMPLATE,DETAIL,THUMB,URL,PATTERN"/>
<!---
  TITLE : string, not null
  SLUG : string, not null
  IMAGEREF : string, null
  TEMPLATE : string, not null
  DETAIL : string, null
  THUMB : string, null
  URL : string, not null
  PATTERN : string, null
--->
<cfset tablevalidate = StructNew()/>
<cfset tablevalidate['TITLE'] = '{"typevalidate":"validatespresence"}'>
<cfset tablevalidate['SLUG'] = '{"typevalidate":"validatesuniqueness,rewritetext","when":"create"}'>
<cfset tablevalidate['URL'] = '{"typevalidate":"validatespresence"}'>
<cfset tablevalidate['TEMPLATE'] = '{"typevalidate":"validatespresence"}'>
<cfset table['VALIDATE'] = tablevalidate />
<cfset table['I18N'] = ''/>