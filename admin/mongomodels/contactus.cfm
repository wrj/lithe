<cfset table = StructNew()/>
<cfset table['tablefield'] = "TITLE,EMAIL,DETAIL,FILEPATH"/>
<!---
  username : string, not null
  password : string, not null
  email : string, not null
  firstname : string, not null
  lastname : string, not null
--->
<cfset tablevalidate = StructNew()/>
<cfset tablevalidate['TITLE'] = '{"typevalidate":"validatespresence"}'>
<cfset tablevalidate['EMAIL'] = '{"typevalidate":"validatespresence,validateemail"}'>
<cfset table['VALIDATE'] = tablevalidate />
<cfset table['I18N'] = ''/>