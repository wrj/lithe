<cfset table = StructNew()/>
<cfset table['tablefield'] = "TITLE,DETAIL,SLUG,INTRO,STHUMB,LTHUMB,GALLERY"/>
<!---
  title : string, not null
  detail : string, not null
--->
<cfset tablevalidate = StructNew()/>
<cfset tablevalidate['TITLE'] = '{"typevalidate":"validatespresence"}'>
<cfset tablevalidate['SLUG'] = '{"typevalidate":"validatesuniqueness,rewritetext","when":"create"}'>
<cfset table['VALIDATE'] = tablevalidate />
<cfset table['I18N'] = 'TITLE,INTRO,DETAIL'/>