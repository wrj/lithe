<cfset table = StructNew()/>
<cfset table['tablefield'] = "TITLE,DETAIL,PUBDATE,TYPECONTENT,PUBSTATUS,SLUG,PRICE,INTRO,STHUMB,LTHUMB,SKU,PRIVATE,GALLERY"/>
<!---
  title : string, not null
  detail : string, not null
  pubdate : string, not null
  typecontent : number, not null
  - 1 post
  - 2 page
  pubstatus : number, not null
  - 0 disable
  - 1 publish
  - 2 draff
  - 3 archive
--->
<cfset tablevalidate = StructNew()/>
<cfset tablevalidate['TITLE'] = '{"typevalidate":"validatespresence"}'>
<cfset tablevalidate['INTRO'] = '{"typevalidate":"validatespresence"}'>
<cfset tablevalidate['PRICE'] = '{"typevalidate":"validatesnumericality","validatesnumericality":"integer,numeric"}'>
<cfset tablevalidate['SLUG'] = '{"typevalidate":"validatesuniqueness,rewritetext","when":"create"}'>
<cfset table['VALIDATE'] = tablevalidate />
<cfset table['I18N'] = 'TITLE,INTRO,DETAIL'/>