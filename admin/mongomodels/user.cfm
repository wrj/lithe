<!---
  Created with IntelliJ IDEA.
  User: arunwatd
  Date: 1/14/13
  Time: 3:16 PM
  To change this template use File | Settings | File Templates.
--->
<cfset table = StructNew()/>
<cfset table['tablefield'] = "USERNAME,PASSWORD,FIRSTNAME,LASTNAME,EMAIL,LOGO,SECRET,RULE"/>
<!---
  username : string, not null
  password : string, not null
  email : string, not null
  firstname : string, not null
  lastname : string, not null
--->
<cfset tablevalidate = StructNew()/>
<cfset tablevalidate['USERNAME'] = '{"typevalidate":"validatespresence,validatesuniqueness,validateslength","validateslength":"5,15","when":"create"}'>
<cfset tablevalidate['PASSWORD'] = '{"typevalidate":"validatespresence","when":"create"}'>
<cfset tablevalidate['EMAIL'] = '{"typevalidate":"validatespresence,validatesuniqueness,validateemail","when":"create"}'>
<cfset tablevalidate['FIRSTNAME'] = '{"typevalidate":"validatespresence,validateslength","validateslength":"5,40"}'>
<cfset tablevalidate['LASTNAME'] = '{"typevalidate":"validatespresence,validateslength","validateslength":"5,40"}'>
<cfset table['VALIDATE'] = tablevalidate />
<cfset table['I18N'] = ''/>
