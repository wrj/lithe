<cfinclude template="include/header.cfm">
<cfif isDefined('langinput') eq false>
    <cfset langinput = session['language']>
</cfif>
<cfif isdefined('ERROR')>
    <cfset slide = getstoredata()>
    <cfif structIsEmpty(slide)>
        <cfset objdata = MongoCollectionfindone(application.applicationname,'slide',{"_id"= newid(key)})>
        <cfset slide = mongonew("slide",objdata)>
    </cfif>
<cfelse>
    <cfset objdata = MongoCollectionfindone(application.applicationname,'slide',{"_id"= newid(key)})>
    <cfset slide = mongonew("slide",objdata,langinput)>
</cfif>
<cfset modework = 'update'>
<cfoutput>
<div class="row-fluid">
    <legend class="titlepage">Edit page - #slide['title']#</legend>
    #startform(action='slidescontroller')#
        #hiddenfield(name='mode',value='update')#
        #hiddenfield(name='key',value=key)#
        #hiddenfield(name='displaystart',value=start)#
        #langselect(langinput)#
        <cfinclude template="slidesform.cfm">
        #submit(label=get('labelbtnedit'))#
    #endform()#
</div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<!--- BACK BUTTON DETECT --->
<script type="text/javascript">
history.pushState({page: 123}, "update", "");
var _firstload = true;
var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
var isIE = /*@cc_on!@*/false;                            // At least IE6
if (isFirefox){_firstload = false;}
		window.onpopstate = function(e){
		if(_firstload){_firstload = false;}
else{_firstload = false;
	<cfoutput>window.location.href = 'slides.cfm?start=#start#';</cfoutput>}
};
</script>