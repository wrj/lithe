<cfinclude template="system/loadplugins.cfm">
<cfset mydata = "">
<cfset mydataraw = MongoCollectionfind(application.applicationname,'category',{})>
<cfset nQuery = QueryNew("id,name,pid","Varchar,Varchar,Varchar") />
<cfloop array="#mydataraw#" index="item">
    <cfset queryAddRow(nQuery) />
    <cfset querySetCell(nQuery,"id",item['_id'].toString()) />
    <cfset querySetCell(nQuery,"name",item['TITLE']) />
    <cfif structKeyExists(item,'PARENT')>
        <cfset querySetCell(nQuery,"pid",item['PARENT'].getId().toString()) />
    </cfif>
</cfloop>
<cfset mydata = "[#genRecursiveList(nQuery)#]">
<cfcontent type="application/json; charset=utf-8" reset="true">
<cfoutput>
    #mydata#
</cfoutput>