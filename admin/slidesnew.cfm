<cfinclude template="include/header.cfm">
<cfif isdefined('ERROR')>
    <cfset slide = getstoredata()>
    <cfif structIsEmpty(slide)>
        <cfset slide = mongonew('slide')>
    </cfif>
<cfelse>
    <cfset slide = mongonew('slide')>
</cfif>
<cfset modework = 'new'>
<cfoutput>
    <div class="row-fluid">
        <legend class="titlepage">New slide</legend>
        #startform(action='slidescontroller')#
            #hiddenfield(name='mode',value='create')#
            <cfinclude template="slidesform.cfm">
            #submit(label=get('labelbtnadd'))#
        #endform()#
    </div>
</cfoutput>
<cfinclude template="include/footer.cfm">