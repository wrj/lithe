<!---
  Created with IntelliJ IDEA.
  User: peerabumrung
  Date: 1/21/13
  Time: 3:41 PM
  To change this template use File | Settings | File Templates.
--->


<!--- ฟังก์ชั่น --->


<cffunction name="generate" output="false" access="public" returntype="string">
	<cfargument name="jsonData" type="any" required="true">
	<cfset output = ''>
	<cfif ArrayLen(jsonData.menu) GT 0>
		<cfloop array="#jsonData.menu#" index="qmenu">

			<cfoutput>
				<cfsavecontent variable="firstmenu">
<!---	ข้อความ output	--->
                    <li id="menu-#qmenu.index#">
                        <div class="submenu">
                            <div class="submenu-name">
                                <div class="name-text">#HTMLEditFormat(qmenu.name)#</div>
                                    <i class="remove-menu icon-remove">
                                    <input type="hidden" name="countSub" class="removeIndex" value="#qmenu.index#"></i>
                                    <i class="form-up-down icon-chevron-down">
                                    <input type="hidden" name="countSub" class="countSub" value="#qmenu.index#"></i>
                                </div>
                                <div class="submenu-form submenu-form-hide-show" style="display: none;">
                                    <div class="row-fluid">
                                        <div class="span2">
                                            <div class="menu-thumbnail">
												<cfif isDefined("qmenu.imagefile") AND isDefined("qmenu.imagename") AND  qmenu.imagefile IS NOT "">
                                                    <img src="#get('pathuploadfolder')#image/#qmenu.imagefile#" alt="#qmenu.imagename#" class="menuitem-img img-thumb img-rounded">
												<cfelse>
                                                    <div class="menu-thumbnail-noimage">No Image</div>
												</cfif>
                                            </div>
                                        </div>
                                        <div class="span10">
                                            <div class="submenu-formLine">
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="submenu-Block">
                                                            <div class="submenu-Label">Name <span class="required">*</span></div>
                                                                <input type='text' name='submenuName' value='#HTMLEditFormat(qmenu.name)#' class='submenuName requirefield'>
                                                            </div>
                                                        </div>
									                </div>
								                </div>

                <div class="submenu-formLine">
                <div class="row-fluid">
                <div class="span12">
                <div class="submenu-Block">
                    <div class="submenu-Label">Description</div>
					<cfif isdefined("qmenu.description")>
                            <textarea name="submenuDescription" class="submenuDescription" id="" cols="10" rows="5">#HTMLEditFormat(qmenu.description)#</textarea>
						<cfelse>
                            <textarea name="submenuDescription" class="submenuDescription" id="" cols="10" rows="5"></textarea>
					</cfif>
                    </div>
                    </div>
                    </div>
                    </div>

                    <div class="submenu-Line">
                    <div class="row-fluid">
                    <div class="span6">
                    <div class="submenu-Block">
                        <div class="submenu-Label">CSS ID</div>
                            <input type="text" name="submenuId" class="submenuId" value="#qmenu.id#">
                </div>
                </div>
                <div class="span6">
                <div class="submenu-Block">
                    <div class="submenu-Label">CSS Class</div>
                        <input type="text" name="submenuClass" class="submenuClass" value="#qmenu.class#">
                </div>
                </div>
                </div>
                </div>


                    <div class='submenu-Line'>
                        <div class='row-fluid'>
                            <div class='span6'>
                                <div class='subgallery-Label'>Link <span class="required">*</span></div>
                            </div>
                        </div>
                    </div>
                <div class='submenu-Line'>
                <div class='row-fluid'>
                <div class='span6'>
                        <input type='text' name='linkname'  class='linkname requirefield' readonly='readonly' value="<cfif isdefined("qmenu.linkname")>#qmenu.linkname#</cfif>">
                        <input type='hidden' name='linkurl'  class='linkurl' value="<cfif isdefined("qmenu.linkurl")>#qmenu.linkurl#</cfif>">
                        <input type='hidden' name='categoryid'  class='categoryid' value="<cfif isdefined("qmenu.categoryid")>#qmenu.categoryid#</cfif>">
                        <input type='hidden' name='linktype'  class='linktype' value="<cfif isdefined("qmenu.linktype")>#qmenu.linktype#</cfif>">
                </div>
                    <div class='span6'>
                        <input type='button' value='Browse Link' class='btn add-link'/>
                    </div>
                </div>
                </div>
                    <div class='submenu-Line'>
                        <div class='row-fluid'>
                            <div class='span6'>
                                <div class='subgallery-Label'>Image</div>
                            </div>
                        </div>
                    </div>
                <div class='submenu-Line'>
                <div class='row-fluid'>
                <div class='span6'>
                        <input type='text' name='imagename'  class='imagename' readonly='readonly' value="<cfif isDefined("qmenu.imagename")>#qmenu.imagename#</cfif>">
                        <input type='hidden' name='imagefile'  class='imagefile' value="<cfif isDefined("qmenu.imagefile")>#qmenu.imagefile#</cfif>">
                </div>
                    <div class='span6'>
                        <input type='button' value='Attach Image' class='btn attach-image'/>
                        <input type='button' value='Remove Image' class='btn remove-image'/>
                    </div>
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>

					<cfif isdefined("qmenu.children")>
						#children(qmenu)#
					</cfif>

                    </li>



				</cfsavecontent>
			</cfoutput>
			<cfset output = "#output##firstmenu#">

		</cfloop>
	</cfif>
	<cfset output = "#output#">
	<cfreturn output>
</cffunction>


<cffunction name="children" output="false" access="public" returntype="string">
	<cfargument name="jsonData" type="any" required="true">

	<cfif ArrayLen(jsonData.children) GT 0>
		<cfset outputChild="">
		<cfloop array="#jsonData.children#" index="qmenu">
			<cfoutput>
				<cfsavecontent variable="outputChild">
<!---	??????? output	--->
					#outputChild#
                    <li id="menu-#qmenu.index#">
                <div class="submenu">
                <div class="submenu-name">
                <div class="name-text">#HTMLEditFormat(qmenu.name)#</div>
                <i class="remove-menu icon-remove">
                        <input type="hidden" name="countSub" class="removeIndex" value="#qmenu.index#"></i>
                <i class="form-up-down icon-chevron-down">
                        <input type="hidden" name="countSub" class="countSub" value="#qmenu.index#"></i>
                </div>
                <div class="submenu-form submenu-form-hide-show" style="display: none;">
                <div class="row-fluid">
                <div class="span2">
                <div class="menu-thumbnail">
					<cfif isDefined("qmenu.imagefile") AND isDefined("qmenu.imagename")>
                            <img src="#get('pathuploadfolder')#image/#qmenu.imagefile#" alt="#qmenu.imagename#" class="menuitem-img img-thumb img-rounded">
						<cfelse>
                            <div class="menu-thumbnail-noimage">No Image</div>
					</cfif>
                    </div>
                    </div>
                    <div class="span10">
                <div class="submenu-formLine">
	                <div class="row-fluid">
		                <div class="span12">
		                    <div class="submenu-Block">
		                        <div class="submenu-Label">Name <span class="required">*</span></div>
	                            <input type='text' name='submenuName' value='#HTMLEditFormat(qmenu.name)#' class='submenuName requirefield'>
			                </div>
	                    </div>
	                </div>
                </div>

                <div class="submenu-formLine">
                <div class="row-fluid">
                <div class="span12">
                <div class="submenu-Block">
                    <div class="submenu-Label">Description</div>
					<cfif isdefined("qmenu.description")>
                            <textarea name="submenuDescription" class="submenuDescription" id="" cols="10" rows="5">#HTMLEditFormat(qmenu.description)#</textarea>
						<cfelse>
                            <textarea name="submenuDescription" class="submenuDescription" id="" cols="10" rows="5"></textarea>
					</cfif>
                    </div>
                    </div>
                    </div>
                    </div>
                    <div class="submenu-Line">
                    <div class="row-fluid">
                    <div class="span6">
                    <div class="submenu-Block">
                        <div class="submenu-Label">Css Id</div>
                            <input type="text" name="submenuId" class="submenuId" value="#qmenu.id#">
                </div>
                </div>
                <div class="span6">

                <div class="submenu-Block">
                    <div class="submenu-Label">Css Class</div>
                        <input type="text" name="submenuClass" class="submenuClass" value="#qmenu.class#">
                </div>
                </div>
                </div>
                </div>
                    <div class='submenu-Line'>
                        <div class='row-fluid'>
                            <div class='span6'>
                                <div class='subgallery-Label'>Link <span class="required">*</span></div>
                            </div>
                        </div>
                    </div>
                <div class='submenu-Line'>
                <div class='row-fluid'>
                <div class='span6'>
                        <input type='text' name='linkname'  class='linkname requirefield' readonly='readonly' value="<cfif isdefined("qmenu.linkname")>#qmenu.linkname#</cfif>">
                        <input type='hidden' name='linkurl'  class='linkurl' value="<cfif isdefined("qmenu.linkurl")>#qmenu.linkurl#</cfif>">
                        <input type='hidden' name='categoryid'  class='categoryid' value="<cfif isdefined("qmenu.categoryid")>#qmenu.categoryid#</cfif>">
                        <input type='hidden' name='linktype'  class='linktype' value="<cfif isdefined("qmenu.linktype")>#qmenu.linktype#</cfif>">
                </div>
                    <div class='span6'>
                        <input type='button' value='Browse Link' class='btn add-link'/>
                    </div>
                </div>
                </div>
                <div class='submenu-Line'>
				<div class='row-fluid'>
					<div class='span6'>
						<div class='subgallery-Label'>Image</div>
					</div>
				</div>
                <div class='row-fluid'>
                <div class='span6'>
                        <input type='text' name='imagename'  class='imagename' readonly='readonly' value="<cfif isDefined("qmenu.imagename")>#qmenu.imagename#</cfif>">
                        <input type='hidden' name='imagefile'  class='imagefile' value="<cfif isDefined("qmenu.imagefile")>#qmenu.imagefile#</cfif>">
                </div>
                    <div class='span6'>
                        <input type='button' value='Attach Image' class='btn attach-image'/>
                        <input type='button' value='Remove Image' class='btn remove-image'/>
                    </div>
                </div>
                </div>
                </div>
                </div>
                </div>
                </div>
					<cfif isdefined("qmenu.children")>
						#children(qmenu)#
					</cfif>
                    </li>
				</cfsavecontent>
			</cfoutput>
		</cfloop>
		<cfset printChild="<ol>#outputChild#</ol>">
	</cfif>
	<cfreturn printChild>
</cffunction>