<cfimport taglib="lib" prefix="ui">
<cfoutput>
<div class="row-fluid tableclass">
	<div class="span2" style="width: 175px; float: left;">
		#treecategoryoutput()#
	</div>
	<div class="span5" style="float: left; margin-left: 10px; width: 494px;">
        <ui:datatable field="id,#capitalize('thumbnail')#,#capitalize('filename')#" tool=false size="0,100,0"/>
	</div>
</div>
</cfoutput>
<script type="text/javascript">
	var visitfirst = true;
	var category = '';

	$(function(){
		createcollectionstree('tree','categoryimage');
		$('.radiocheck').live('click',function(e){
			changetype();
		});
		if ($('input:radio[name=typelink]:checked').val() == '3')
		{
			changetype();
		}
	})

	function reportkey(key){
		category = key;
		inittable();
	}

	function inittable()
	{
		if(visitfirst == true)
		{
			visitfirst = false;
		}else{
			clearall();
		}
		var linkvalue = '?category='+category;
		createdatatablebrowse('usetable','imagesbrowsegrid',linkvalue);
	}

	function clearall()
	{
		oTable.fnDestroy();
		gaiSelected = [];
	}
    <cfif isDefined('modeselect') && modeselect eq "multi">
        var modeselect = 'multi';
    <cfelse>
        var modeselect = 'single';
    </cfif>
    $('#usetable tbody tr').live('click', function () {
        var aData = oTable.fnGetData( this );
        var iId = aData[0];
        if (modeselect == 'single')
        {
            gaiSelected = [];
            if ( $(this).hasClass('row_selected') ) {
                $(this).removeClass('row_selected');
            }else{
                oTable.$('tr.row_selected').removeClass('row_selected');
                $(this).addClass('row_selected');
                gaiSelected.push(iId);
            }
        }else{
            if ( jQuery.inArray(iId, gaiSelected) == -1 )
            {
                gaiSelected[gaiSelected.length++] = iId;
            }
            else
            {
                gaiSelected = jQuery.grep(gaiSelected, function(value) {
                    return value != iId;
                } );
            }
            $(this).toggleClass('row_selected');
        }
    } );

    function selectfile() {
        if (gaiSelected != "") {
            parent.selectedcomplete(gaiSelected);
            closewindow();
        }else{
            alert("Please select image");
        }
    }

    function closewindow()
    {
        window.parent.$("#bModal").modal('hide');
    }
</script>
