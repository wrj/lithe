<cfinclude template="include/header.cfm">
<cfif isDefined('langinput') eq false>
    <cfset langinput = session['language']>
</cfif>
<cfif isdefined('ERROR')>
    <cfset post = getstoredata()>
    <cfif structIsEmpty(post)>
        <cfset objdata = MongoCollectionfindone(application.applicationname,'post',{"_id"= newid(key)})>
        <cfset post = mongonew("post",objdata)>
        <cfset post['TAG'] = ''/>
    </cfif>
<cfelse>
    <cfset objdata = MongoCollectionfindone(application.applicationname,'post',{"_id"= newid(key)})>
    <cfset post = mongonew("post",objdata,langinput)>
    <cfset post['TAG'] = ''/>
</cfif>
<cfset modework = 'update'>
<cfoutput>
<div class="row-fluid">
    <legend class="titlepage">Edit page - #post['title']#</legend>
    #startform(action='htmlscontroller')#
        #hiddenfield(name='mode',value='update')#
        #hiddenfield(name='key',value=key)#
        #hiddenfield(name='displaystart',value=start)#
        #langselect(langinput)#
        <cfinclude template="htmlsform.cfm">
        #submit(label=get('labelbtnedit'))#
    #endform()#
</div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<!--- BACK BUTTON DETECT --->
<script type="text/javascript">
history.pushState({page: 123}, "update", "");
var _firstload = true;
var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
var isIE = /*@cc_on!@*/false;                            // At least IE6
if (isFirefox){_firstload = false;}
		window.onpopstate = function(e){
		if(_firstload){_firstload = false;}
else{_firstload = false;
	<cfoutput>window.location.href = 'htmls.cfm?start=#start#';</cfoutput>}
};
</script>