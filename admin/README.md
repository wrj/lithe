# Quick start
1. Clone project use folder name cfc
2. create file app.cfm in config folder
3. add this code in app.cfm
```
<cfset This.Sessionmanagement="True">
<cfset This.Sessiontimeout="#createtimespan(0,1,0,0)#">
<cfset This.applicationtimeout="#createtimespan(5,0,0,0)#">
```
4. go to url "serverurl/folder/install.cfm" setup software
5. url "serverurl/folder/adminlogin.cfm" for superadmin
6. url "serverurl/folder/login.cfm" for user and admin

Create custom menu
1. superadmin create file superadminmenucustom.cfm in include folder
2. admin create file adminmenucustom.cfm in include folder
3. user create file usermenucustom.cfm in include folder

Code example for menu
```
Dropdown menu
<li class="dropdown">
    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Assets<b class="caret"></b></a>
    <ul class="dropdown-menu">
        <li class="dropdown-submenu">
            <a tabindex="-1" href="#">Image</a>
            <ul class="dropdown-menu">
                <li><a href="imagesnew.cfm">Add Image</a></li>
                <li><a href="images.cfm">List Image</a></li>
            </ul>
        </li>
    </ul>
</li>

Single menu
<li>
    <a href="contactus.cfm">Contact</a>
</li>
```