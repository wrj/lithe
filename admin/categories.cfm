<cfinclude template="include/header.cfm">
<cfimport taglib="lib" prefix="ui">
<cfoutput>
<div class="row-fluid">
    <div class="span12">
        <p class="headpage">Categories</p>
        <ui:datatable field="Thumb,#capitalize('Title')#" size='100,0'/>
    </div>
</div>
<div class="modal hide fade bigModal" id="delModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Delete Category</h3>
    </div>
    <div class="modal-body">
        <p id="shownamedelete"></p>
        <h2>Please note</h2>
        <p>After you delete an Category.It'll not collect usage data anymore.</p>
        <input type="hidden" id="iddelete" name="iddelete">
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <a onclick="confirmdelete('categories');" class="btn btn-danger">OK</a>
    </div>
</div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<cfoutput>#javaScriptInclude("jquery.dataTables,DT_bootstrap,createdatatable,deletedialog")#</cfoutput>
<script type="text/javascript">
    createdatatable('usetable','categoriesgrid','');

    function gonew()
    {
        window.location = "categoriesnew.cfm";
    }

    function goedit(id)
    {
        window.location = "categoriesupdate.cfm?key="+id+"&start="+oTable.fnSettings()._iDisplayStart;
    }
</script>
