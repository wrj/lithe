<cfinclude template="system/loadplugins.cfm">
<cfset sortlabel = ""/>
<cfset sortvalue = 1/>
<cfset searchtxt = ""/>
<cfset listColumns = "STHUMB,TITLE,UPDATEDAT" />
<cfif not isDefined('iSortCol_0')>
    <cfset iSortCol_0 = 0>
    <cfset sSortDir_0 = 1>
    <cfset sSearch = "">
    <cfset sEcho = 1>
    <cfset iDisplayStart =0>
    <cfset iDisplayLength = 10>
</cfif>
<cfset sortlabel = listGetAt(listColumns,(Val(iSortCol_0) + 1))/>
<cfif sSortDir_0 eq "desc">
    <cfset sortvalue = -1/>
</cfif>
<cfset sortst = structNew()>
<cfset sortst[sortlabel] = sortvalue>
<cfoutput>
    <cfset defaultsearch = structnew()>
<!---    <cfset defaultsearch["USER"] = dbref('user',session['userid'])>--->
    <cfset notempty = structNew()>
    <cfset notempty["$ne"] = ''>
    <cfset defaultsearch["TITLE.#session['language']#"] = notempty>
    <cfif sSearch neq "">
        <cfset regsearch = structnew()>
        <cfset regsearch["$regex"] = ".*#Trim(sSearch)#.*">
        <cfset regsearch["$options"] = "i">
        <cfset fieldarray = arraynew()>
        <cfset fieldsearch = structNew()>
        <cfset fieldsearch["TITLE.#session['language']#"] = regsearch>
        <cfset arrayappend(fieldarray,fieldsearch)>
        <cfset defaultsearch["$or"] = fieldarray>
    </cfif>
    <cfset mydatatotal = MongoCollectioncount(datasource=application.applicationname,collection='slide',query=defaultsearch)>
    <cfset mydata = MongoCollectionFind(datasource=application.applicationname,collection='slide',query=defaultsearch,skip=iDisplayStart,size=iDisplayLength,sort=sortst)>
    <cfsavecontent variable="sourcedatatable">
        {"sEcho": #val(sEcho)#,
    "iTotalRecords": #mydatatotal#,
    "iTotalDisplayRecords": #mydatatotal#,
    "aaData": [
        <cfset i = 1/>
        <cfloop array="#mydata#" index="rowitem">
            [
            <cfloop list="#listColumns#" index="thisColumn">
                <cfif thisColumn neq listFirst(listColumns)>,</cfif>
                <cfif ListFind("UPDATEDAT,PUBDATE,CREATEDAT",thisColumn,',') gt 0>
                    "#jsStringFormat(DateFormat(rowitem[thisColumn],'dd/mm/yy'))#"
                <cfelseif thisColumn eq "STHUMB">
                    "<img src='#rowitem[thisColumn]#' class='img-rounded img-tablethumb'/>"
                <cfelse>
                    "#jsStringFormat(rewritejsontext(i18n(checkdata(rowitem,thisColumn))))#"
                </cfif>
            </cfloop>
            ,"<a onclick=\"javascript:goedit('#rowitem['_id'].toString()#')\" href=\"javascript:void(0)\" class=\"btn\">#get('labeledit')#</a> <a onclick=\"javascript:deletedata('#rowitem['_id'].toString()#','#rewritetext(i18n(checkdata(rowitem,ListFirst(listColumns))))#')\" href=\"javascript:void(0)\" class=\"btn\">#get('labeldelete')#</a>"]
            <cfif i neq arraylen(mydata)>,</cfif>
            <cfset i++/>
        </cfloop>]}
    </cfsavecontent>
    <cfoutput>
        #sourcedatatable#
    </cfoutput>
</cfoutput>