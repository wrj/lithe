<cfinclude template="include/header.cfm">
<cfif isdefined('ERROR')>
    <cfset contactus = getstoredata()>
    <cfif structIsEmpty(contactus)>
        <cfset contactus = mongonew('contactus')>
    </cfif>
    <cfelse>
    <cfset contactus = mongonew('contactus')>
</cfif>
<cfoutput>
    <div class="row-fluid">
        <legend>New contactus</legend>
        #startform(action='contactuscontroller')#
            #hiddenfield(name='mode',value='create')#
            <cfinclude template="contactusform.cfm"/>
            #submit(label=get('labelbtnadd'))#
        #endform()#
    </div>
</cfoutput>
<cfinclude template="include/footer.cfm">