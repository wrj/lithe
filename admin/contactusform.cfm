<!---<cfdump var="#contactus#">--->
<!---<cfabort/>--->
<cfoutput>
    #textfield(name='title',label='title',value=contactus['title'],readonly=true)#
    #textfield(name='email',label='email',value=contactus['email'],readonly=true)#

    #textarea(name='detail',label='detail',value=contactus['detail'])#
	<div class="control-group">
		<label class="control-label">Image File</label>
		<div class="controls">
			<cfif isdefined("contactus['filepath']['file']")>
				<a href="javascript:previewimage('/assets/upload/contactus/#contactus['filepath']['file']#')">
					<img class="img-thumb img-rounded" src="/assets/upload/contactus/#contactus['filepath']['file']#">
				</a>
			<cfelse>
				<div class="span3" id="showlimage">
					<div class="thumbnail thumbnail-empty">
						<div class="thumbnail-empty-text">
							No Image
						</div>
					</div>
				</div>
			</cfif>
		</div>
	</div>

<!---    Preview Thumb--->
<div class="modal hide fade" id="pModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Preview Image</h3>
	</div>
	<div class="modal-body" id="previewimage">

	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>
<script type="text/javascript">
	function previewimage(imagename)
	{
		var pathpreview = '<div class="thumbnail"><img src="'+imagename+'"/></div>';
		$('##previewimage').html(pathpreview);
		$('##pModal').modal('show');
	}
	$('##pModal').on('hidden', function () {
		$("##previewimage").html('');
	});
	if ($(".thumbnail-empty").parent().next().find(".thumbnail").length > 0){
		$(".thumbnail-empty-text").css("padding-top",$(".thumbnail-empty").parent().next().find(".thumbnail").height()*0.5-10);
	}else{
		$(".thumbnail-empty-text").css("padding-top",$(".thumbnail-empty").height()*0.5-10);
	}
</script>
</cfoutput>