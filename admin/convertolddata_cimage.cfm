<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/29/13 AD
  Time: 3:24 PM
  To change this template use File | Settings | File Templates.
--->
<cfinclude template="system/loadplugins.cfm">

<cfset user = MongoCollectionfindOne(application.applicationname,'user',{_id=newid(session.userid)})>
<cfset userobjid = dbref('user',user.toString())>

<!--- Create CategoryImage --->
<cfset categoryimagetitle = "AllImage">
<cfset categoryimage = mongonew('categoryimage')>
<cfset categoryimage["TITLE"] = categoryimagetitle>
<cfset categoryimage["PATTERN"] = "">
<cfset result = mongovalidate('categoryimage','create',categoryimage)>
<cfset result['value']['USER'] = userobjid>
<cfset cateimgid = MongoCollectioninsert(application.applicationname,'categoryimage',result['value'])>
<cfset cateimgobjid = dbref('categoryimage',cateimgid.toString())>

<!--- sample --->
<!---<cfset samimages = MongoCollectionfindOne(application.applicationname,'image',{CATEGORY={$exists=true}})>--->
<!---<cfdump var="#samimages#">--->
<!---<cfset structDelete(samimages,'CATEGORY')>--->
<!---<cfset samimages = MongoCollectionSave(application.applicationname,'image',samimages)>--->
<!--- end sample --->

<cfset images = MongoCollectionfind(application.applicationname,'image',{CATEGORY={$exists=false}})>
<cfloop array="#images#" index="image">
	<cfset structInsert(image,'CATEGORY',cateimgobjid)>
	<cfset  MongoCollectionSave(application.applicationname,'image',image)>
	<cfdump var="#image#">
</cfloop>
