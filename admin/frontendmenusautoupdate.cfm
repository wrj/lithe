<!---
  Created with IntelliJ IDEA.
  User: peerabumrung
  Date: 4/18/13
  Time: 10:52 AM
  To change this template use File | Settings | File Templates.
--->
<cfinclude template="system/loadplugins.cfm">
<cfset formdata={
	NAME=NAME,
	DESCRIPTION=DESCRIPTION,
	MENUID=MENUID,
	MENUCLASS=MENUCLASS,
	MENU=MENU
}>
<cfset keyid = key>
<cfset result = mongovalidate('frontendmenu','update',formdata)>

<cfif result['result'] eq true>
	<cfset olddata = MongoCollectionfindone(application.applicationname,'frontendmenu',{'_id'=newid(keyid)})>
	<cfset dataupdate = mongomapvalue('frontendmenu',olddata,result['value'],'default')>
	<cfset result['value']['MENU']= deserializeJSON(result['value']['MENU'])>
	<cfset result['value']['ID']=keyid>
	<cfset jsondata = serializeJSON(result['value'])>
<!---				<cfset settingdata = MongoCollectionfindone(application.applicationname,'usersetting', {'USER'=dbref('user',session['userid'])})>--->
	<cfset urlpart = "menu">
	<cfset filepath="#expandPath("/#urlpart#")#">
	<cfif NOT DirectoryExists(filepath)>
		<cfdirectory action="create" mode="777"  directory="#filepath#">
	</cfif>
	<cfset name = result['value']['NAME']>
	<cffile
			action = "write"
			file = "#filepath#/#name#.json"
			output = "#jsondata#"
			addNewLine = "yes"
			charset = "utf-8"
			fixnewline = "no"
			mode = "777">
	<cfif fileExists("#filepath#/#name#.json")>
		<cfset MongoCollectionsave(application.applicationname,'frontendmenu',dataupdate)>
	<cfelse>
		<cfoutput>Error create file <br> <ul><li> not found file: #filepath#/#name#.json</li></ul></cfoutput>
	</cfif>
<cfelse>
	<cfoutput>There was an error update the menu.</cfoutput>
	<cfset storedata(result)>
</cfif>

