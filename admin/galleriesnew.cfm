<cfinclude template="include/header.cfm">
<cfif isdefined('ERROR')>
    <cfset gallery = getstoredata()>
    <cfif structIsEmpty(gallery)>
        <cfset gallery = mongonew('gallery')>
    </cfif>
<cfelse>
    <cfset gallery = mongonew('gallery')>
</cfif>
<cfset modework = 'new'>
<cfoutput>
    <div class="row-fluid">
        <legend class="titlepage">New gallery</legend>
        #startform(action='galleriescontroller')#
            #hiddenfield(name='mode',value='create')#
            <cfinclude template="galleriesform.cfm">
            #submit(label=get('labelbtnadd'))#
        #endform()#
    </div>
</cfoutput>
<cfinclude template="include/footer.cfm">