<cfinclude template="system/loadplugins.cfm">
<cfset structDelete(form,'FIELDNAMES')>
<cfset structDelete(form,'GALLERYDETAIL')>
<cfset structDelete(form,'GALLERYIMAGE')>
<cfset structDelete(form,'GALLERYLINK')>
<cfset structDelete(form,'GALLERYTITLE')>
<cfswitch expression="#mode#">
    <cfcase value="create">
        <cfset structDelete(form,'mode')>
        <cfif form['GALLERYJSON'] neq "">
            <cfset galleryarray = deserializeJSON(form['GALLERYJSON'])>
        <cfelse>
            <cfset galleryarray = arrayNew()>
        </cfif>
        <cfset structDelete(form,'GALLERYJSON')>
        <cfset result = mongovalidate('post','create',form)>
        <cfif result['result'] eq true>
            <!---            Language--->
            <cfset pagedata = assignlang('post',result['value'])>
            <cfset pagedata['GALLERY'] = galleryarray>
            <cfif pagedata['newsimage'] neq ''>
                <cfset pagedata['STHUMB'] = pagedata['newsimage']>
            </cfif>
            <cfif pagedata['newlimage'] neq ''>
                <cfset pagedata['LTHUMB'] = pagedata['newlimage']>
            </cfif>
            <!---            startdate--->
            <cfif form['PUBDATE'] neq ''>
                <cfset startdate = form['PUBDATE']>
            <cfelse>
                <cfset startdate = DateFormat(now(),'dd/mm/yyyy')>
            </cfif>
            <cfset tagarr = arrayNew()>
            <cfset pagedata['TAG'] = tagarr>
            <cfset pagedata['TYPECONTENT'] = 2>
            <cfset pagedata['PUBSTATUS'] = val(pagedata['PUBSTATUS'])>
            <cfset pagedata['PUBDATE'] = createDate(listGetAt(startdate,3,'/'),listGetAt(startdate,2,'/'),listGetAt(startdate,1,'/'))>
            <cfset pagedata['USER'] = dbref('user',session['userid'])>
            <cfset postid = MongoCollectioninsert(application.applicationname,'post',pagedata)>
            <cfset flashinsert('success','Create page complete')>
            <cflocation url="htmls.cfm" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error creating the page.')>
            <cfset storedata(result)>
            <cflocation url="htmlsnew.cfm" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="update">
        <cfset keyid = key>
        <cfset structDelete(form,'KEY')>
        <cfset structDelete(form,'MODE')>
        <cfif form['GALLERYJSON'] neq "">
            <cfset galleryarray = deserializeJSON(form['GALLERYJSON'])>
        <cfelse>
            <cfset galleryarray = arrayNew()>
        </cfif>
        <cfset structDelete(form,'GALLERYJSON')>
        <cfset result = mongovalidate('post','update',form)>
        <cfif result['result'] eq true>
            <cfif form['NEWSIMAGE'] neq ''>
                <cfif form['NEWSIMAGE'] neq 'remove'>
                    <cfset result['value']['STHUMB'] = form['NEWSIMAGE']>
                <cfelse>
                    <cfset result['value']['STHUMB'] = ''>
                </cfif>
            </cfif>
            <cfif form['NEWLIMAGE'] neq ''>
                <cfif form['NEWLIMAGE'] neq 'remove'>
                    <cfset result['value']['LTHUMB'] = form['NEWLIMAGE']>
                <cfelse>
                    <cfset result['value']['LTHUMB'] = ''>
                </cfif>
            </cfif>
            <cfset olddata = MongoCollectionfindone(application.applicationname,'post',{'_id'=newid(keyid)})>
            <cfset dataupdate = mongomapvalue('post',olddata,result['value'],lang)>
            <cfset structDelete(dataupdate, "GALLERY")>
            <cfset dataupdate['GALLERY'] = galleryarray>
<!---            Tag--->
            <cfset tagarr = arrayNew()>
            <cfset dataupdate['TAG'] = tagarr>
<!---            startdate--->
            <cfif form['PUBDATE'] neq ''>
                <cfset startdate = form['PUBDATE']>
                <cfelse>
                <cfset startdate = DateFormat(now(),'dd/mm/yyyy')>
            </cfif>
            <cfset dataupdate['TYPECONTENT'] = 2>
            <cfset dataupdate['PUBSTATUS'] = val(result['value']['PUBSTATUS'])>
            <cfset dataupdate['USER'] = dbref('user',session['userid'])>
            <cfset MongoCollectionsave(application.applicationname,'post',dataupdate)>
            <cfset flashinsert('success','Update page complete')>
            <cflocation url="htmls.cfm?start=#displaystart#" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error update the page.')>
            <cfset storedata(result)>
            <cflocation url="htmlsupdate.cfm?key=#keyid#&start=#displaystart#&langinput=#lang#" addtoken="false">
        </cfif>
    </cfcase>

    <cfcase value="delete">
        <cfset MongoCollectionremove(application.applicationname,'post',{'_id'=newid(key)})>
        <cfset result = structNew()>
        <cfset result['result'] = 'true'>
        <cfset result['message'] = "Delete page complete">
        <cfoutput>
            #serializeJSON(result)#
        </cfoutput>
    </cfcase>

</cfswitch>