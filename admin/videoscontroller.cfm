<cfinclude template="system/loadplugins.cfm">
<cfset structDelete(form,'FIELDNAMES')>
<cfswitch expression="#mode#">
    <cfcase value="create">
        <cfset structDelete(form,'mode')>
        <cfset result = mongovalidate('video','create',form)>
        <cfif result['result'] eq true>
            <cfset result['value']['USER'] = dbref('user',session['userid'])>
            <cfset MongoCollectioninsert(application.applicationname,'video',result['value'])>
            <cfset flashinsert('success','Create video complete')>
            <cflocation url="videos.cfm" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error creating the video.')>
            <cfset storedata(result)>
            <cflocation url="videosnew.cfm" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="update">
        <cfset keyid = key>
	    <cfset displaystart = form['DISPLAYSTART']>
        <cfset structDelete(form,'KEY')>
        <cfset structDelete(form,'MODE')>
	    <cfset structDelete(form,'DISPLAYSTART')>
        <cfset result = mongovalidate('video','update',form)>
        <cfif result['result'] eq true>
            <cfset olddata = MongoCollectionfindone(application.applicationname,'video',{'_id'=newid(keyid)})>
            <cfset dataupdate = mongomapvalue('video',olddata,result['value'],'default')>
            <cfset dataupdate['USER'] = dbref('user',session['userid'])>
            <cfset MongoCollectionsave(application.applicationname,'video',dataupdate)>
            <cfset flashinsert('success','Update video complete')>
            <cflocation url="videos.cfm?start=#displaystart#" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error update the video.')>
            <cfset storedata(result)>
            <cflocation url="videosupdate.cfm?key=#keyid#" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="delete">
        <cfset MongoCollectionremove(application.applicationname,'video',{'_id'=newid(key)})>
        <cfset result = structNew()>
        <cfset result['result'] = 'true'>
        <cfset result['message'] = "Delete video complete">
        <cfoutput>
            #serializeJSON(result)#
        </cfoutput>
    </cfcase>

</cfswitch>