<!---
  Created with IntelliJ IDEA.
  User: jojoe
  Date: 10/23/13 AD
  Time: 11:51 AM
  To change this template use File | Settings | File Templates.
--->

<cfinclude template="include/header.cfm">
<cfset parentid = '0'>
<cfif isdefined('ERROR')>
	<cfset category = getstoredata()>
	<cfset parentid = ''>
	<cfif structKeyExists(category,'PARENT')>
		<cfset parentid = category['PARENT']>
	</cfif>
	<cfif structIsEmpty(category)>
		<cfset category = mongonew('categoryimage')>
	</cfif>
	<cfelse>
	<cfset objdata = MongoCollectionfindone(application.applicationname,'categoryimage',{"_id"= newid(key)})>
	<cfset category = mongonew("categoryimage",objdata)>
	<cfif structKeyExists(objdata,'PARENT')>
		<cfset parentid = objdata['PARENT'].getId().toString()>
	</cfif>
</cfif>
<!---All Data--->
<cfset cateraw = MongoCollectionfind(application.applicationname,'categoryimage',{})>
<cfset caterawquery = treequery(cateraw)/>
<cfset catequery = arraynew()>
<cfset rowquery = structNew()>
<cfset rowquery["_id"]="0">
<cfset rowquery["TITLE"]="No Parent">
<cfset arrayAppend(catequery,rowquery)>
<cfloop array="#caterawquery#" index="item">
	<cfif item['_id'] neq key>
		<cfset rowquery = structNew()>
		<cfset rowquery["_id"]=item['_id']>
		<cfset rowquery["TITLE"]=item['TITLE']>
		<cfset arrayAppend(catequery,rowquery)>
	</cfif>
</cfloop>
<cfoutput>
	<div class="row-fluid">
	<legend class="titlepage">Edit category - #category['title']#</legend>
	#startform(action='categoriesimagescontroller')#
	#hiddenfield(name='mode',value='update')#
	#hiddenfield(name='key',value=key)#
	#hiddenfield(name='displaystart',value=start)#
	<cfinclude template="categoriesimagesform.cfm"/>
	#submit(label=get('labelbtnedit'))#
	#endform()#
	</div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<!--- BACK BUTTON DETECT --->
<script type="text/javascript">
history.pushState({page: 123}, "update", "");
var _firstload = true;
var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
var isIE = /*@cc_on!@*/false;                            // At least IE6
if (isFirefox){_firstload = false;}
		window.onpopstate = function(e){
		if(_firstload){_firstload = false;}
else{_firstload = false;
	<cfoutput>window.location.href = 'categoriesimages.cfm?start=#start#';</cfoutput>}
};
</script>