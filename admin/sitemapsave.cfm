<!---
  Created with IntelliJ IDEA.
  User: skoodeskill
  Date: 6/12/13 AD
  Time: 3:07 PM
  To change this template use File | Settings | File Templates.
--->


<cfset savesitemap = deserializeJSON(form.indertdata)>
<cfset savesitemap['UPDATEDAT'] = now()>
<cfset MongoCollectiondrop(application.applicationname,'sitemap')>
<cfset MongoCollectioninsert(application.applicationname,'sitemap',savesitemap)>
<!--- GENERATE XML --->
<cfoutput>
	<cfsavecontent variable="xmlSitemap"><?xml version="1.0" encoding="utf-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">#GenerateSitemapUrl(savesitemap.MENUNAVI)##GenerateSitemapUrl(savesitemap.MENUCONTENT)##GenerateSitemapUrl(savesitemap.CATEGORY)##GenerateSitemapUrl(savesitemap.HTML)##GenerateSitemapUrl(savesitemap.POST)##GenerateSitemapUrl(savesitemap.GALLERY)##GenerateSitemapUrl(savesitemap.TAG)##GenerateSitemapUrl(savesitemap.CUSTOM)#</urlset></cfsavecontent>
</cfoutput>
<cfset filepath="#expandPath("/")#">
<cfif NOT DirectoryExists(filepath)>
	<cfdirectory action="create" mode="777"  directory="#filepath#">
</cfif>
<cffile
	action = "write"
	file = "#filepath#/sitemap_new.xml"
	output = "#xmlSitemap#"
	addNewLine = "yes"
	charset = "utf-8"
	fixnewline = "no"
	mode = "777">
<cfif fileExists("#filepath#/sitemap_new.xml")>
	<cfif fileExists("#filepath#/sitemap.xml")>
		<cffile
			action = "delete"
			file = "#filepath#/sitemap.xml">
	</cfif>
	<cffile
		action = "rename"
		destination = "#filepath#/sitemap.xml"
		source = "#filepath#/sitemap_new.xml"
		attributes = "normal"
		mode = "777">
</cfif>
<cffunction name="GenerateSitemapUrl" output="false" access="public" returntype="any">
	<cfargument name="urlArr" type="any" required="true">
	<cfoutput>
		<cfsavecontent variable="urlSitemap"><cfif arraylen('#arguments.urlArr#') GT 0><cfloop array="#arguments.urlArr#" index="i"><url><loc>#i.URL#</loc><lastmod>#i.LASTMODIFY#</lastmod><changefreq>#savesitemap.CHANGEFREQ#</changefreq><priority>#i.PRIORITY#</priority></url></cfloop></cfif></cfsavecontent>
	</cfoutput>
	<cfreturn urlSitemap>
</cffunction>

{"status":"success"}