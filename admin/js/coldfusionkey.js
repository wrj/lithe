function getoutput(){
	return '<cfoutput></cfoutput>'
}
function getcfset(){
	return '<cfset = "">'
}
function getloop(){
	return '<cfloop [array,list]="##" index="i"></cfloop>'
}
function getsavecontent(){
	return '<cfsavecontent variable="jsondata"></cfsavecontent>'
}
function getoutputlang(){
	return '#outputlang([variable],cookie[\'language\'])#'
}
function getjsonloop(){
	var returnstr = '<cfoutput>';
	returnstr += '<cfsavecontent variable="jsondata">{<cfset i = 1/>'
	returnstr += '<cfloop array="#[variable].toArray()#" index="data">'
	returnstr += '"#data[\'_id\'].toString()#":"#outputlang(data[variable],cookie[\'language\'])#"'
	returnstr += '<cfif i neq arraylen([variable].toArray())>,</cfif><cfset i++/>'
	returnstr += '</cfloop>'
	returnstr += '}</cfsavecontent>'
	returnstr += '</cfoutput>'
	returnstr += '<cfset renderText(jsondata)/>'
	return returnstr
}
function getabort()
{
	return '<cfabort/>'
}
function getdump()
{
	return '<cfdump var="#[variable]#"/>'
}