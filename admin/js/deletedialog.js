function deletedata(id,titleshow,owner,department)
{
	var alertshow = "Are you sure to delete "+titleshow+"?"
	$('#shownamedelete').text(alertshow);
    $('#iddelete').val(id);
    $('#ownerdelete').val(owner);
    $('#depdelete').val(department);
    $('#delModal').modal();
}

function confirmdelete(page)
{
    $.ajax({
        type: "POST",
        url: page+"controller.cfm?mode=delete",
        dataType:"json",
        data:{ key: $('#iddelete').val()},
        success: function(response){
            if(response.result == 'true')
            {
                reportflash('success',response.message);
                $('#delModal').modal('hide');
                oTable.fnDraw(oTable.fnSettings());
            }else{
                reportflash('error',response.message);
                $('#delModal').modal('hide');
            }
        }
    });
}

function reportflash(type,message)
{
    var messagetext = '';
    if(type == 'success')
    {
        messagetext = '<div class="alert alert-success">'+message+'</div>';
    }else{
        messagetext = '<div class="alert alert-error">'+message+'</div>';
    }
    $('#flash-session').html(messagetext);
}