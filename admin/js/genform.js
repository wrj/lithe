		var formid = "item-"+Math.uuid(17);
		var objForm = {
			FORMID:formid,
			FORMNAME:"",
			FORMTITLE:"",
			FORMCOLLECTION:"",
			FIELDS:[]
		};
		function genForm(){
			filterObjForm();
			$("#jsonform").val($.stringify(objForm));
			var $form = $('<form name="'+objForm.FORMNAME+'" id="'+objForm.FORMID+'"></form>');
			$form.append('<ul class="connectedSortable ui-helper-reset sortable"></ul>');
			$.each(objForm.FIELDS, function(index, value) { 
				var $item = ""
				switch (value.FIELDTYPE)
				{
					case 'textbox': $item = genTextBox(index, value);
						break;
					case 'textarea': $item = genTextArea(index, value);
						break;
					case 'dropdown': $item = genDropdown(index, value);
						break;
					case 'radiobutton': $item = genRadioButton(index, value);
						break;
					case 'checkbox': $item = genCheckbox(index, value);
						break;
					case 'date': $item = genDatetime(index, value);
						break;
					case 'html': $item = genHtml(index, value);
						break;
					case 'tab': $item = genTab(index, value);
						break;
					case 'rule': $item = genRule(index, value);
						break;
					case 'panel': $item = genPanel(index, value);
						break;
					case 'captcha': $item = genCaptcha(index, value);
						break;
					case 'keyword': $item = genKeyword(index, value);
						break;
					case 'subform' : $item = genSubform(index, value);
						break;
                    case 'upload' : $item = genUpload(index, value);
                        break;
				}
				if(value.POSITION==objForm.FORMID){
					$("ul.sortable:first",$form).append($item);
				}else{
					if(value.FIELDTYPE=="panel"){
						$("#"+value.POSITION+" ul.tabhead",$form).append('<li><a href="#'+value.FIELDID+'">'+value.FIELDLABEL+'</a></li>');
						$("ul.sortable #"+value.POSITION,$form).append($item);
					}else{
						$("ul.sortable #"+value.POSITION+" ul.sortable",$form).append($item);
					}
					
				}
				
			});
			$("#form-header").html('<h4>'+objForm.FORMTITLE+'</h4>');
			$("#form-body").html($form);
		}
		
		function genCaptcha(index, value){
			var $item = $('<li class="ui-state-default sortitem"><div id="'+value.FIELDID+'"><label>Captcha : '+value.FIELDLABEL+'</label><div class="field"><div id="recaptcha_div" class="simple-recaptcha"></div></div></div></li>');
			return $item;
		}

        function genUpload(index, value){
            var $item = $('<li class="ui-state-default sortitem"><div id="'+value.FIELDID+'"><label>Uploadfile : '+value.FIELDLABEL+'</label><div class="field"><div id="upload_div" class="simple-upload"></div></div></div></li>');
            $.each(value.PROPERTIES, function(key, value) {
                if(key != 'TYPE')
                {
                    $(".field input",$item).attr(key,value);
                }
            });
            return $item;
        }

		function genKeyword(index, value){
			var $item = $('<li class="ui-state-default sortitem"><div id="'+value.FIELDID+'"><label>Keyword : '+value.FIELDLABEL+'</label><div class="field"><div id="keyword_div" class="simple-keyword"></div></div></div></li>');
			return $item;
		}

		function genSubform(index, value){
			var $item = $('<li class="ui-state-default sortitem"><div id="'+value.FIELDID+'"><label>Subform : '+value.FIELDLABEL+'</label><div class="field"><div id="subform_div" class="simple-subform"></div></div></div></li>');
			return $item;
		}
		
		function genTextBox(index, value){
			var $item = $('<li class="ui-state-default sortitem"><div id="'+value.FIELDID+'"><label>Textbox : '+value.FIELDLABEL+'</label><div class="field"><input type="text"/></div></div></li>');
			$.each(value.PROPERTIES, function(key, value) { 
				if(key != 'TYPE')
                {
                    $(".field input",$item).attr(key,value);
                }
			});
			return $item;
		}

		function genDatetime(index, value){
			var $item = $('<li class="ui-state-default sortitem"><div id="'+value.FIELDID+'"><label>Datetime : '+value.FIELDLABEL+'</label><div class="field"><input type="text"/></div></div></li>');
			$.each(value.PROPERTIES, function(key, value) {
                if(key != 'TYPE')
                {
                    $(".field input",$item).attr(key,value);
                }
			});
			return $item;
		}
		
		function genTextArea(index, value){
			var $item = $('<li class="ui-state-default sortitem"><div id="'+value.FIELDID+'"><label>Textarea : '+value.FIELDLABEL+'</label><div class="field"><textarea></textarea></div></div></li>');
			$.each(value.PROPERTIES, function(key, value) { 
				if(key=="VALUE"){
					$(".field textarea",$item).text(value);
				}else{
					$(".field textarea",$item).attr(key,value);
				}
			});
			return $item;
		}
		
		function genDropdown(index, value){
			var $item = $('<li class="ui-state-default sortitem"><div id="'+value.FIELDID+'"><label>Dropdown : '+value.FIELDLABEL+'</label><div class="field"><select></select></div></div></li>');
			$.each(value.PROPERTIES, function(key, value) {
				$(".field select",$item).attr(key,value);
			});
			if(value.OPTIONS.VALUES != ""){
				$.each(value.OPTIONS.VALUES.split(/\r\n|\r|\n/), function(option_index, option_value) {
					var str = option_value.split('|');
					if($.inArray(str[0], value.OPTIONS.SELECTED.split("|"))>=0){
						$(".field select",$item).append('<option value="'+str[0]+'" selected="selected">'+str[1]+'</option>');
					}else{
						$(".field select",$item).append('<option value="'+str[0]+'">'+str[1]+'</option>');
					}
				});
			}
			if(value.OPTIONS.REMOTE != undefined){
				$.getJSON(value.OPTIONS.REMOTE,function(data){
					$.each(data.options,function(remote_index,remote_value){
						if($.inArray(remote_value.value,data.selected)>=0 || remote_value.value==data.selected){
							$(".field select",$item).append('<option value="'+remote_value.value+'" selected="selected">'+remote_value.text+'</option>');
						}else{
							$(".field select",$item).append('<option value="'+remote_value.value+'">'+remote_value.text+'</option>');
						}
					});
				});
			}
			return $item;
		}
		function genRadioButton(index, value){
			var $item = $('<li class="ui-state-default sortitem"><div id="'+value.FIELDID+'"><label> Radio : '+value.FIELDLABEL+'</label><div class="field"></div></div></li>');
			if (value.OPTIONS.VALUES != "") {
				$.each(value.OPTIONS.VALUES.split(/\r\n|\r|\n/), function(option_index, option_value){
					var str = option_value.split('|');
					if (str[0] == value.OPTIONS.SELECTED) {
						$(".field", $item).append('<input type="radio" value="' + str[0] + '" checked="checked"><p>' + str[1] + '</p>');
						$.each(value.PROPERTIES, function(key, value){
							$(".field input:last", $item).attr(key, value);
						});
					}else {
						$(".field", $item).append('<input type="radio" value="' + str[0] + '"><p>' + str[1] + '</p>');
						$.each(value.PROPERTIES, function(key, value){
							$(".field input:last", $item).attr(key, value);
						});
					}
				});
			}
			if (value.OPTIONS.REMOTE !== undefined) {
				$.getJSON(value.OPTIONS.REMOTE,function(data){
					$.each(data.options,function(remote_index,remote_value){
						if (remote_value.value == data.selected) {
							$(".field", $item).append('<input type="radio" value="' + remote_value.value + '" checked="checked"><p>' + remote_value.text + '</p>');
							$.each(value.PROPERTIES, function(key, value){
								$(".field input:last", $item).attr(key, value);
							});
						}else{
							$(".field", $item).append('<input type="radio" value="' + remote_value.value + '"><p>' + remote_value.text + '</p>');
							$.each(value.PROPERTIES, function(key, value){
								$(".field input:last", $item).attr(key, value);
							});
						}
					});
				});
			}
			return $item;
		}
		
		function genCheckbox(index, value){
			var $item = $('<li class="ui-state-default sortitem"><div id="'+value.FIELDID+'"><label>Checkbox : '+value.FIELDLABEL+'</label><div class="field"></div></div></li>');
			if (value.OPTIONS.VALUES != "") {
				$.each(value.OPTIONS.VALUES.split(/\r\n|\r|\n/), function(option_index, option_value){
					var str = option_value.split('|');
					if ($.inArray(str[0], value.OPTIONS.SELECTED.split("|")) >= 0) {
						$(".field", $item).append('<input type="checkbox" value="' + str[0] + '" checked="checked"><p>' + str[1] + '</p>');
						$.each(value.PROPERTIES, function(key, value){
							$(".field input:last", $item).attr(key, value);
						});
					}
					else {
						$(".field", $item).append('<input type="checkbox" value="' + str[0] + '"><p>' + str[1] + '</p>');
						$.each(value.PROPERTIES, function(key, value){
							$(".field input:last", $item).attr(key, value);
						});
					}
				});
			}
			if (value.OPTIONS.REMOTE !== undefined) {
				$.getJSON(value.OPTIONS.REMOTE,function(data){
					$.each(data.options,function(remote_index,remote_value){
						if($.inArray(remote_value.value,data.selected)>=0 || remote_value.value==data.selected){
							$(".field", $item).append('<input type="checkbox" value="' + remote_value.value + '" checked="checked"><p>' + remote_value.text + '</p>');
							$.each(value.PROPERTIES, function(key, value){
								$(".field input:last", $item).attr(key, value);
							});
						}else{
							$(".field", $item).append('<input type="checkbox" value="' + remote_value.value + '"><p>' + remote_value.text + '</p>');
							$.each(value.PROPERTIES, function(key, value){
								$(".field input:last", $item).attr(key, value);
							});
						}
					});
				});
			}
			return $item;
		}
			
		function genHtml(index, value){
			var $item = $('<li class="ui-state-default sortitem"><div id="'+value.FIELDID+'"><div class="field">'+value.HTML+'</div></div></li>');
			return $item;
		}

		function genTab(index, value){
			var $item = $('<li class="ui-state-default sortitem"><label>Tab</label><div id="'+value.FIELDID+'" class="sorttabs"><ul class="tabhead"></ul></div></li>');
			return $item;
		}

		function genRule(index, value){
			var $item = $('<li class="ui-state-default sortitem"><label>Rule</label><div id="'+value.FIELDID+'" class="sorttabs"><ul class="tabhead"></ul></div></li>');
			return $item;
		}
		
		function genPanel(index, value){
			var $item = $('<div class="item-panel" id="'+value.FIELDID+'"><ul class="connectedSortable ui-helper-reset sortable"></ul></div>');
			return $item;
		}
		
		
		function activeForm(){
			var $sortable_option = {
				connectWith: ".connectedSortable",
				placeholder: "ui-state-highlight",
				update: function(event, ui) {
					var field = new Array();
					var drag= $('div[id^="item"]',ui.item).attr("id");
					var dragParent= $('div[id^="item"]',ui.item).parent().parent().parent().attr("id");
					$('div[id^="item"]').each(function() { 
						var selected = $(this).attr("id");
						$.each(objForm.FIELDS, function(index, value) { 
							if(value.FIELDID == selected){
								if(value.FIELDID == drag){
									value.POSITION = dragParent;
								}
								field.push(value);
							}
						});
					});
					var tabArr = new Array();
					$(".sorttabs").each(function(){
						tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
					});
					objForm.FIELDS = field;
					genForm();
					activeForm();
					$.each(tabArr, function(index, value) {
						$("#"+value.id).tabs( "select", value.select);
					});
					
				}
			};
			var $sortable_tool =  "<div class='tool ui-state-default'><span class='ui-icon ui-icon-pencil'></span><span class='ui-icon ui-icon-trash'></span></div>";
			$( ".sortable" ).sortable($sortable_option);
			$( ".sorttabs" ).tabs();
			$(".dateinput").datepicker({
				changeMonth: true,
				changeYear: true,
				dateFormat: 'dd/mm/yy'
			});
			$( ".sortitem" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
							.addClass( "ui-widget-header ui-corner-all" )
							.prepend($sortable_tool).end();
			$( ".sorttabs" ).parent().find("div.tool:first").prepend("<span class='ui-icon ui-icon-plus'></span>");
		}
		
		function bindingTool(){
			$(".sortable .ui-icon-pencil").live('click', function() {
				var item="";
				var selectedid = $(this).parent().parent().find('div[id^="item"]:first').attr("id"); 
				$.each(objForm.FIELDS, function(index, value) { 
					if(value.FIELDID == selectedid){
						item = value;
					}
				});
				
				switch (item.FIELDTYPE)
				{
					case 'textbox': 
						$("#submit-edit-textbox #fieldid").val(item.FIELDID);
						$("#submit-edit-textbox #name").val(item.FIELDNAME);
						$("#submit-edit-textbox #label").val(item.FIELDLABEL);
						$("#submit-edit-textbox #value").val(item.PROPERTIES.VALUE);
						$("#submit-edit-textbox #type option").each(function(){
							if($(this).val()==item.PROPERTIES.TYPE){
								$(this).attr("selected","selected");
							}
						});
						$("#submit-edit-textbox #maxlength").val(item.PROPERTIES.MAXLENGTH);
						if(item.PROPERTIES.I18N=="true" || item.PROPERTIES.I18N==true){
							$("#submit-edit-textbox #i18n").attr("checked","checked");
						}
						if(item.PROPERTIES.REQUIRED=="true" || item.PROPERTIES.REQUIRED==true){
							$("#submit-edit-textbox #requeired").attr("checked","checked");
						}
						if(item.ENTRYTABLE=="true" || item.ENTRYTABLE==true){
							$("#submit-edit-textbox #entrytable").attr("checked","checked");
						}
						$( "#dialog-edit-textbox" ).dialog("open");
						break;
					case 'textarea': 
						$("#submit-edit-textarea #fieldid").val(item.FIELDID);
						$("#submit-edit-textarea #name").val(item.FIELDNAME);
						$("#submit-edit-textarea #label").val(item.FIELDLABEL);
						$("#submit-edit-textarea #value").val(item.PROPERTIES.VALUE);
						$("#submit-edit-textarea #row").val(item.PROPERTIES.ROWS);
						if(item.PROPERTIES.REQUIRED=="true" || item.PROPERTIES.REQUIRED==true){
							$("#submit-edit-textarea #requeired").attr("checked","checked");
						}
						$( "#dialog-edit-textarea" ).dialog("open");
						break;
                    case 'upload':
                        $("#submit-edit-upload #fieldid").val(item.FIELDID);
                        $("#submit-edit-upload #name").val('upload');
                        $("#submit-edit-upload #label").val('File upload');
                        $("#submit-edit-upload #imageext").val(item.PROPERTIES.IMAGEEXT);
                        $("#submit-edit-upload #videoext").val(item.PROPERTIES.VIDEOEXT);
                        $("#submit-edit-upload #audioext").val(item.PROPERTIES.AUDIOEXT);
                        $("#submit-edit-upload #fileext").val(item.PROPERTIES.FILEEXT);
                        $("#submit-edit-upload #sizefile").val(item.PROPERTIES.SIZEFILE);
                        $( "#dialog-edit-upload" ).dialog("open");
                        break;
                    case 'dropdown':
						$("#submit-edit-dropdown #fieldid").val(item.FIELDID);
						$("#submit-edit-dropdown #name").val(item.FIELDNAME);
						$("#submit-edit-dropdown #label").val(item.FIELDLABEL);
						$("#submit-edit-dropdown #value").val(item.OPTIONS.VALUES);
						$("#submit-edit-dropdown #selected").val(item.OPTIONS.SELECTED);
						$("#submit-edit-dropdown #size").val(item.PROPERTIES.SIZE);
						$("#submit-edit-dropdown #webservicename").val(item.PROPERTIES.WEBSERVICENAME);
						if(item.PROPERTIES.WEBSERVICE=="webservice"){
							$("#submit-edit-dropdown #webservice").attr("checked","checked");
						}
						if(item.PROPERTIES.MULTIPLE=="multiple"){
							$("#submit-edit-dropdown #multiple").attr("checked","checked");
						}
						$( "#dialog-edit-dropdown" ).dialog("open");
						break;
					case 'subform': 
						$("#submit-edit-subform #fieldid").val(item.FIELDID);
						$("#submit-edit-subform #name").val(item.FIELDNAME);
						$("#submit-edit-subform #label").val(item.FIELDLABEL);
						$("#submit-edit-subform #subformid").val(item.PROPERTIES.SUBFORMID);
						if(item.PROPERTIES.FORMSHOW=="list"){
							$("#submit-edit-subform #formshow").attr("checked","checked");
						}
						$( "#dialog-edit-subform" ).dialog("open");
						break;
					case 'radiobutton': 
						$("#submit-edit-radiobutton #fieldid").val(item.FIELDID);
						$("#submit-edit-radiobutton #name").val(item.FIELDNAME);
						$("#submit-edit-radiobutton #label").val(item.FIELDLABEL);
						$("#submit-edit-radiobutton #value").val(item.OPTIONS.VALUES);
						$("#submit-edit-radiobutton #selected").val(item.OPTIONS.SELECTED);
						$("#submit-edit-radiobutton #remote").val(item.OPTIONS.REMOTE);
						$( "#dialog-edit-radiobutton" ).dialog("open");
						break;
					case 'checkbox': 
						$("#submit-edit-checkbox #fieldid").val(item.FIELDID);
						$("#submit-edit-checkbox #name").val(item.FIELDNAME);
						$("#submit-edit-checkbox #label").val(item.FIELDLABEL);
						$("#submit-edit-checkbox #value").val(item.OPTIONS.VALUES);
						$("#submit-edit-checkbox #selected").val(item.OPTIONS.SELECTED);
						$("#submit-edit-checkbox #remote").val(item.OPTIONS.REMOTE);
						$( "#dialog-edit-checkbox" ).dialog("open");
						break;
					case 'date': 
						$("#submit-edit-date #fieldid").val(item.FIELDID);
						$("#submit-edit-date #name").val(item.FIELDNAME);
						$("#submit-edit-date #label").val(item.FIELDLABEL);
						$("#submit-edit-date #valuedateedit").val(item.PROPERTIES.VALUE);
						$("#submit-edit-date #minedit").val(item.PROPERTIES.MIN);
						$("#submit-edit-date #maxedit").val(item.PROPERTIES.MAX);
						if(item.PROPERTIES.REQUIRED=="true" || item.PROPERTIES.REQUIRED==true){
							$("#submit-edit-date #requeired").attr("checked","checked");
						}
						if(item.ENTRYTABLE=="true" || item.ENTRYTABLE==true){
							$("#submit-edit-date #entrytable").attr("checked","checked");
						}
						$( "#dialog-edit-date" ).dialog("open");
						break;
					case 'html': 
						$("#submit-edit-html #fieldid").val(item.FIELDID);
						$("#submit-edit-html #label").val(item.FIELDLABEL);
						$("#submit-edit-html #html").val(item.HTML);
						$( "#dialog-edit-html" ).dialog("open");
						break;
					case 'tab':
						$("#submit-edit-tab #fieldid").val(item.FIELDID);
						$("#"+selectedid +" .item-panel").each(function(index){
							$("#sort-panel").append('<li class="ui-state-default" id="'+$(this).attr("id")+'"><label>Label : </label><input class="label" type="text" value="'+$('#'+selectedid +' ul.tabhead li:eq('+index+') a').html()+'" /><span class="ui-icon ui-icon-close"></li>');
						});
						$("#sort-panel .ui-icon-close").click(function(){
							$(this).parent().remove();
						});
						$("#sort-panel").sortable();
						$( "#dialog-edit-tab" ).dialog("open");
						break;
					case 'rule':
						$("#submit-edit-rule #fieldid").val(item.FIELDID);
						$("#"+selectedid +" .item-panel").each(function(index){
							$("#sort-panel-rule").append('<li class="ui-state-default" id="'+$(this).attr("id")+'"><label>Label : </label><input class="label" type="text" value="'+$('#'+selectedid +' ul.tabhead li:eq('+index+') a').html()+'" /><span class="ui-icon ui-icon-close"></li>');
						});
						$("#sort-panel-rule .ui-icon-close").click(function(){
							$(this).parent().remove();
						});
						$("#sort-panel-rule").sortable();
						$( "#dialog-edit-rule" ).dialog("open");
						break;
					case 'panel': 
						break;
				}
				
			});
			
			$(".sortable .ui-icon-trash").live('click', function() {
				var field = new Array();
			 	var deleteid = $(this).parent().parent().find('div[id^="item"]:first').attr("id"); 
				$('div[id^="item"]').each(function() { 
					var selected = $(this).attr("id");
					$.each(objForm.FIELDS, function(index, value) { 
						if(value.FIELDID == selected){
							if(!(value.FIELDID == deleteid)){
								field.push(value);
							}
							
						}
					});
				});
				objForm.FIELDS = field;
				genForm();
				activeForm();
				
			});	
			
			$(".sortable .ui-icon-plus").live('click', function() {
				var selectedid = $(this).parent().parent().find('div[id^="item"]:first').attr("id"); 
				$("#dialog-add-panel #fieldid").val(selectedid);
			 	$( "#dialog-add-panel" ).dialog("open");
			});												

			$("#add_tabs").click(function(){
				// if($("div.sorttabs").size()>0){
				// 	alert("1 form has 1 tabs");
				// }else{
					var item = {
						FIELDID:"item-"+Math.uuid(17),
						FIELDTYPE:"tab",
						POSITION:formid
					};
					objForm.FIELDS.push(item);
					genForm();
					activeForm();	
				// }
				
			});

			$("#add_rules").click(function(){
				var item = {
					FIELDID:"item-"+Math.uuid(17),
					FIELDTYPE:"rule",
					POSITION:formid
				};
				objForm.FIELDS.push(item);
				genForm();
				activeForm();
			});
			
			$("#add_captcha").click(function(){
				if($("div#recaptcha_div").size()>0){
					alert("1 form has 1 recaptcha");
				}else{
					var item = {
						FIELDID:"item-"+Math.uuid(17),
						FIELDTYPE:"captcha",
						FIELDLABEL:"Captcha",
						POSITION:formid
					};
					objForm.FIELDS.push(item);
					genForm();
					activeForm();	
				}
			});

            $("#add_upload").click(function(){
                if($("div#upload_div").size()>0){
                    alert("1 form has 1 uploadfile");
                }else{
                    $("#dialog-add-upload").dialog("open");
                }
            });

			$("#add_keyword").click(function(){
				if($("div#keyword_div").size()>0){
					alert("1 form has 1 keyword");
				}else{
					var item = {
						FIELDID:"item-"+Math.uuid(17),
						FIELDTYPE:"keyword",
						FIELDLABEL:"Keyword",
						FIELDNAME:"keyword",
						POSITION:formid
					};
					objForm.FIELDS.push(item);
					genForm();
					activeForm();	
				}
			});

			$("#add_subform").click(function(){
				// if($("div#subform_div").size()>0){
				// 	alert("1 form has 1 subform");
				// }else{
					$("#dialog-add-subform").dialog("open");
				// }
			});
			
			$("#add_textbox").click(function(){
				$("#dialog-add-textbox").dialog("open");
			});
			
			$("#add_textarea").click(function(){
				$("#dialog-add-textarea").dialog("open");
			});
			
			$("#add_dropdown").click(function(){
				$("#dialog-add-dropdown").dialog("open");
			});
			
			$("#add_radiobutton").click(function(){
				$("#dialog-add-radiobutton").dialog("open");
			});
			
			$("#add_checkbox").click(function(){
				$("#dialog-add-checkbox").dialog("open");
			});
			
			$("#add_date").click(function(){
				$("#dialog-add-date").dialog("open");
			});
			
			$("#add_html").click(function(){
				$("#dialog-add-html").dialog("open");
			});
			
			$("#form_property").click(function(){
				$("#submit-form-property #name").val(objForm.FORMNAME);
				$("#submit-form-property #title").val(objForm.FORMTITLE);
				// $("#submit-form-property #subform").val(objForm.FORMSUBFORM);
				if(objForm.FORMSUBFORM=="true"){
					$("#submit-form-property #subform").attr("checked","checked");
				}
				$("#submit-form-property #collection").val(objForm.FORMCOLLECTION);
				$("#dialog-form-property").dialog("open");
			});
			
			$("#preview").click(function(){
				writePreview();
			});
		}
		
		function bindingDialog(){
			$( "#dialog-form-property" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Update": function() {
						objForm.FORMNAME=$("#submit-form-property #name").val();
						objForm.FORMTITLE=$("#submit-form-property #title").val();
						objForm.FORMCOLLECTION=$("#submit-form-property #collection").val();
						// objForm.FORMSUBFORM=$("#submit-form-property #subform").val();
						if($('#submit-form-property #subform:checked').val() !== undefined){
							objForm.FORMSUBFORM="true";
							$('#formgenerate-subform').val('true');
						}else{
							objForm.FORMSUBFORM="false";
							$('#formgenerate-subform').val('false');
						}
						genForm();
						activeForm();
						
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#submit-form-property input[type="text"]').val("");
				}
			});
			
			$('#dialog-edit-tab').dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Update": function() {
						var field = new Array();
						var parentindex=0;
						$.each(objForm.FIELDS, function(index, value) {
							if(value.FIELDID == $("#submit-edit-tab #fieldid").val()){
								parentindex = index;
							}
							if(!(value.POSITION == $("#submit-edit-tab #fieldid").val())){
									field.push(value);
							}
						});
						$('#sort-panel li[id^="item"]').each(function() {
							var $this = $(this);
							$.each(objForm.FIELDS, function(index, value) {
								if($this.attr("id") == value.FIELDID){
									parentindex++;
									value.FIELDLABEL = $('.label',$this).val();
									field.splice(parentindex,0,value);
								}
							});
						});
						objForm.FIELDS = field;
						genForm();
						activeForm();
						
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$("#sort-panel").html("");
					$('#submit-edit-tab input[type="text"]').val("");
				}
			});

			$('#dialog-edit-rule').dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Update": function() {
						var field = new Array();
						var parentindex=0;
						$.each(objForm.FIELDS, function(index, value) {
							if(value.FIELDID == $("#submit-edit-rule #fieldid").val()){
								parentindex = index;
							}
							if(!(value.POSITION == $("#submit-edit-rule #fieldid").val())){
									field.push(value);
							}
						});
						$('#sort-panel-rule li[id^="item"]').each(function() {
							var $this = $(this);
							$.each(objForm.FIELDS, function(index, value) {
								if($this.attr("id") == value.FIELDID){
									parentindex++;
									value.FIELDLABEL = $('.label',$this).val();
									field.splice(parentindex,0,value);
								}
							});
						});
						objForm.FIELDS = field;
						genForm();
						activeForm();
						
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$("#sort-panel-rule").html("");
					$('#submit-edit-tab input[type="text"]').val("");
				}
			});
			
			$( "#dialog-add-textbox" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Create": function() {
						var item = {
							FIELDID:"item-"+Math.uuid(17),
							FIELDNAME:$("#submit-add-textbox #name").val(),
							FIELDTYPE:"textbox",
							FIELDLABEL:$("#submit-add-textbox #label").val(),
							POSITION:formid,
							PROPERTIES:{
								NAME:$("#submit-add-textbox #name").val(),
								ID:$("#submit-add-textbox #name").val(),
								VALUE:$("#submit-add-textbox #value").val(),
								TYPE:$("#submit-add-textbox #type option:selected").val()
							}
						};
						if($('#submit-add-textbox #maxlength').val() != ""){
							item.PROPERTIES.MAXLENGTH=$('#submit-add-textbox #maxlength').val();
						}
						if($('#submit-add-textbox #i18n:checked').val() !== undefined){
							item.PROPERTIES.I18N="true";
						}
						if($('#submit-add-textbox #requeired:checked').val() !== undefined){
							item.PROPERTIES.REQUIRED="true";
						}
						if($('#submit-add-textbox #entrytable:checked').val() !== undefined){
							item.ENTRYTABLE="true";
						}
						objForm.FIELDS.push(item);
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					var defaultlabel = "#i18n('"+$('#formgenerate-name').val()+"_')#";
					$('#submit-add-textbox input[type="text"]').val('');
					$('#submit-add-textbox input[name="label"]').val(defaultlabel);
					$('#submit-add-textbox input[type="checkbox"]').attr("checked", false);
					$('#submit-add-textbox select option:first').attr('selected', 'selected');
				}
			});
			
			$( "#dialog-edit-textbox" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Update": function() {
						var current = 0;
						$.each(objForm.FIELDS, function(index, value) { 
							if(value.FIELDID == $("#submit-edit-textbox #fieldid").val()){
								current=index;
							}
						});
						
						var item = {
							FIELDID:$("#submit-edit-textbox #fieldid").val(),
							FIELDNAME:$("#submit-edit-textbox #name").val(),
							FIELDTYPE:"textbox",
							FIELDLABEL:$("#submit-edit-textbox #label").val(),
							POSITION:objForm.FIELDS[current].POSITION,
							PROPERTIES:{
								NAME:$("#submit-edit-textbox #name").val(),
								ID:$("#submit-edit-textbox #name").val(),
								VALUE:$("#submit-edit-textbox #value").val(),
								TYPE:$("#submit-edit-textbox #type option:selected").val()
							}
						};
						if($('#submit-edit-textbox #maxlength').val() != ""){
							item.PROPERTIES.MAXLENGTH=$('#submit-edit-textbox #maxlength').val();
						}else{
							delete item.PROPERTIES.MAXLENGTH;
						}
						if($('#submit-edit-textbox #i18n:checked').val() !== undefined){
							item.PROPERTIES.I18N="true";
						}else{
							delete item.PROPERTIES.I18N;
						}
						if($('#submit-edit-textbox #requeired:checked').val() !== undefined){
							item.PROPERTIES.REQUIRED="true";
						}else{
							delete item.PROPERTIES.REQUIRED;
						}
						if($('#submit-edit-textbox #entrytable:checked').val() !== undefined){
							item.ENTRYTABLE="true";
						}else{
							delete item.ENTRYTABLE;
						}
						objForm.FIELDS[current]=item;
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					var defaultlabel = "#i18n('"+$('#formgenerate-name').val()+"_')#";
					$('#submit-edit-textbox input[type="text"]').val("");
					$('#submit-edit-textbox input[name="label"]').val(defaultlabel);
					$('#submit-edit-textbox input[type="checkbox"]').attr('checked', false);
					$('#submit-edit-textbox select option:first').attr('selected', 'selected');
				}
			});

			$( "#dialog-add-keyword" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Create": function() {
						var item = {
							FIELDID:"item-"+Math.uuid(17),
							FIELDNAME:$("#submit-add-keyword #name").val(),
							FIELDTYPE:"keyword",
							FIELDLABEL:$("#submit-add-keyword #label").val(),
							POSITION:formid,
							PROPERTIES:{
								NAME:$("#submit-add-keyword #name").val(),
								ID:$("#submit-add-keyword #name").val()
							}
						};
						objForm.FIELDS.push(item);
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#submit-add-keyword input[type="text"]').val("");
					$('#submit-add-keyword input[type="checkbox"]').attr("checked", "");
					$('#submit-add-keyword select option:first').attr('selected', 'selected');
				}
			});
			
			$( "#dialog-edit-keyword" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Update": function() {
						var current = 0;
						$.each(objForm.FIELDS, function(index, value) { 
							if(value.FIELDID == $("#submit-edit-keyword #fieldid").val()){
								current=index;
							}
						});
						
						var item = {
							FIELDID:$("#submit-edit-keyword #fieldid").val(),
							FIELDNAME:$("#submit-edit-keyword #name").val(),
							FIELDTYPE:"keyword",
							FIELDLABEL:$("#submit-edit-keyword #label").val(),
							POSITION:objForm.FIELDS[current].POSITION,
							PROPERTIES:{
								NAME:$("#submit-edit-keyword #name").val(),
								ID:$("#submit-edit-keyword #name").val()
							}
						};
						objForm.FIELDS[current]=item;
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#submit-edit-keyword input[type="text"]').val("");
					$('#submit-edit-keyword input[type="checkbox"]').attr('checked', false);
					$('#submit-edit-keyword select option:first').attr('selected', 'selected');
				}
			});

            $("#dialog-add-upload").dialog({
                autoOpen: false,
                width: 550,
                modal: true,
                buttons: {
                    "Create": function() {
                        var item = {
                            FIELDID:"item-"+Math.uuid(17),
                            FIELDNAME:'upload',
                            FIELDTYPE:"upload",
                            FIELDLABEL:'File upload',
                            POSITION:formid,
                            PROPERTIES:{
                                NAME:'upload',
                                ID:'upload'
                            }
                        };
                        if($('#submit-add-upload #imageext').val() != ""){
                            item.PROPERTIES.IMAGEEXT=$('#submit-add-upload #imageext').val();
                        }
                        if($('#submit-add-upload #videoext').val() != ""){
                            item.PROPERTIES.VIDEOEXT=$('#submit-add-upload #videoext').val();
                        }
                        if($('#submit-add-upload #audioext').val() != ""){
                            item.PROPERTIES.AUDIOEXT=$('#submit-add-upload #audioext').val();
                        }
                        if($('#submit-add-upload #fileext').val() != ""){
                            item.PROPERTIES.FILEEXT=$('#submit-add-upload #fileext').val();
                        }
                        if($('#submit-add-upload #sizefile').val() != ""){
                            item.PROPERTIES.SIZEFILE=$('#submit-add-upload #sizefile').val();
                        }
                        objForm.FIELDS.push(item);
                        var tabArr = new Array();
                        $(".sorttabs").each(function(){
                            tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
                        });
                        genForm();
                        activeForm();
                        $.each(tabArr, function(index, value) {
                            $("#"+value.id).tabs( "select", value.select);
                        });
                        $( this ).dialog( "close" );
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                    }
                },
                close: function() {

                }
            });

            $( "#dialog-edit-upload" ).dialog({
                autoOpen: false,
                width: 550,
                modal: true,
                buttons: {
                    "Update": function() {
                        var current = 0;
                        $.each(objForm.FIELDS, function(index, value) {
                            if(value.FIELDID == $("#submit-edit-upload #fieldid").val()){
                                current=index;
                            }
                        });

                        var item = {
                            FIELDID:$("#submit-edit-upload #fieldid").val(),
                            FIELDNAME:'upload',
                            FIELDTYPE:"upload",
                            FIELDLABEL:'File upload',
                            POSITION:objForm.FIELDS[current].POSITION,
                            PROPERTIES:{
                                NAME:'upload',
                                ID:'upload'
                            }
                        };
                        if($('#submit-edit-upload #imageext').val() != ""){
                            item.PROPERTIES.IMAGEEXT=$('#submit-edit-upload #imageext').val();
                        }else{
                            delete item.PROPERTIES.IMAGEEXT;
                        }
                        if($('#submit-edit-upload #videoext').val() != ""){
                            item.PROPERTIES.VIDEOEXT=$('#submit-edit-upload #videoext').val();
                        }else{
                            delete item.PROPERTIES.VIDEOEXT;
                        }
                        if($('#submit-edit-upload #audioext').val() != ""){
                            item.PROPERTIES.AUDIOEXT=$('#submit-edit-upload #audioext').val();
                        }else{
                            delete item.PROPERTIES.AUDIOEXT;
                        }
                        if($('#submit-edit-upload #fileext').val() != ""){
                            item.PROPERTIES.FILEEXT=$('#submit-edit-upload #fileext').val();
                        }else{
                            delete item.PROPERTIES.FILEEXT;
                        }
                        if($('#submit-edit-upload #sizefile').val() != ""){
                            item.PROPERTIES.SIZEFILE=$('#submit-edit-upload #sizefile').val();
                        }else{
                            delete item.PROPERTIES.SIZEFILE;
                        }
                        objForm.FIELDS[current]=item;
                        var tabArr = new Array();
                        $(".sorttabs").each(function(){
                            tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
                        });
                        genForm();
                        activeForm();
                        $.each(tabArr, function(index, value) {
                            $("#"+value.id).tabs( "select", value.select);
                        });
                        $( this ).dialog( "close" );
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                    }
                },
                close: function() {

                }
            });
			
			$( "#dialog-add-date" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Create": function() {
						var item = {
							FIELDID:"item-"+Math.uuid(17),
							FIELDNAME:$("#submit-add-date #name").val(),
							FIELDTYPE:"date",
							FIELDLABEL:$("#submit-add-date #label").val(),
							POSITION:formid,
							PROPERTIES:{
								NAME:$("#submit-add-date #name").val(),
								ID:$("#submit-add-date #name").val(),
								VALUE:$("#submit-add-date #valuedateadd").val(),
								TYPE:"text",
								CLASS:"dateinput"
							}
						};
						if($('#submit-add-date #minadd').val() != ""){
							item.PROPERTIES.MIN=$('#submit-add-date #minadd').val();
						}
						if($('#submit-add-date #maxadd').val() != ""){
							item.PROPERTIES.MAX=$('#submit-add-date #maxadd').val();
						}
						if($('#submit-add-date #requeired:checked').val() !== undefined){
							item.PROPERTIES.REQUIRED="true";
						}
						if($('#submit-add-date #entrytable:checked').val() !== undefined){
							item.ENTRYTABLE="true";
						}
						objForm.FIELDS.push(item);
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#submit-add-date input[type="text"]').val("");
					var defaultlabel = "#i18n('"+$('#formgenerate-name').val()+"_')#";
					$('#submit-add-date input[name="label"]').val(defaultlabel);
					$('#submit-add-date input[type="checkbox"]').attr("checked", false);
				}
			});
			
			$( "#dialog-edit-date" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Update": function() {
						var current = 0;
						$.each(objForm.FIELDS, function(index, value) { 
							if(value.FIELDID == $("#submit-edit-date #fieldid").val()){
								current=index;
							}
						});
						
						var item = {
							FIELDID:$("#submit-edit-date #fieldid").val(),
							FIELDNAME:$("#submit-edit-date #name").val(),
							FIELDTYPE:"date",
							FIELDLABEL:$("#submit-edit-date #label").val(),
							POSITION:objForm.FIELDS[current].POSITION,
							PROPERTIES:{
								NAME:$("#submit-edit-date #name").val(),
								ID:$("#submit-edit-date #name").val(),
								VALUE:$("#submit-edit-date #valuedateedit").val(),
								TYPE:"text",
								CLASS:"dateinput"
							}
						};
						if($('#submit-edit-date #minedit').val() != ""){
							item.PROPERTIES.MIN=$('#submit-edit-date #minedit').val();
						}else{
							delete item.PROPERTIES.MIN;
						}
						if($('#submit-edit-date #maxedit').val() != ""){
							item.PROPERTIES.MAX=$('#submit-edit-date #maxedit').val();
						}else{
							delete item.PROPERTIES.MAX;
						}
						if($('#submit-edit-date #requeired:checked').val() !== undefined){
							item.PROPERTIES.REQUIRED="true";
						}else{
							delete item.PROPERTIES.REQUIRED;
						}
						if($('#submit-edit-date #entrytable:checked').val() !== undefined){
							item.ENTRYTABLE="true";
						}else{
							delete item.ENTRYTABLE;
						}
						objForm.FIELDS[current]=item;
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#submit-edit-date input[type="text"]').val("");
					var defaultlabel = "#i18n('"+$('#formgenerate-name').val()+"_')#";
					$('#submit-edit-date input[name="label"]').val(defaultlabel);
					$('#submit-edit-date input[type="checkbox"]').attr('checked', false);
				}
			});
			
			$( "#dialog-add-textarea" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Create": function() {
						var item = {
							FIELDID:"item-"+Math.uuid(17),
							FIELDNAME:$("#submit-add-textarea #name").val(),
							FIELDTYPE:"textarea",
							FIELDLABEL:$("#submit-add-textarea #label").val(),
							POSITION:formid,
							PROPERTIES:{
								NAME:$("#submit-add-textarea #name").val(),
								ID:$("#submit-add-textarea #name").val(),
								VALUE:$("#submit-add-textarea #value").val(),
								ROWS:$("#submit-add-textarea #row").val()
							}
						};
						if($('#submit-add-textarea #i18n:checked').val() !== undefined){
							item.PROPERTIES.I18N="true";
						}
						if($('#submit-add-textarea #requeired:checked').val() !== undefined){
							item.PROPERTIES.REQUIRED="true";
						}
						objForm.FIELDS.push(item);
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					var defaultlabel = "#i18n('"+$('#formgenerate-name').val()+"_')#";
					$('#submit-add-textarea input[type="text"]').val("");
					$('#submit-add-textarea input[name="label"]').val(defaultlabel);
					$('#submit-add-textarea textarea').val("");
					$('#submit-add-textarea input[type="checkbox"]').attr("checked", false);
				}
			});
			
			$( "#dialog-edit-textarea" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Update": function() {
						var current = 0;
						$.each(objForm.FIELDS, function(index, value) { 
							if(value.FIELDID == $("#submit-edit-textarea #fieldid").val()){
								current=index;
							}
						});
						
						var item = {
							FIELDID:$("#submit-edit-textarea #fieldid").val(),
							FIELDNAME:$("#submit-edit-textarea #name").val(),
							FIELDTYPE:"textarea",
							FIELDLABEL:$("#submit-edit-textarea #label").val(),
							POSITION:objForm.FIELDS[current].POSITION,
							PROPERTIES:{
								NAME:$("#submit-edit-textarea #name").val(),
								ID:$("#submit-edit-textarea #name").val(),
								VALUE:$("#submit-edit-textarea #value").val(),
								ROWS:$("#submit-edit-textarea #row").val()
							}
						};
						if($('#submit-edit-textarea #i18n:checked').val() !== undefined){
							item.PROPERTIES.I18N="true";
						}else{
							delete item.PROPERTIES.I18N;
						}
						if($('#submit-edit-textarea #requeired:checked').val() !== undefined){
							item.PROPERTIES.REQUIRED="true";
						}else{
							delete item.PROPERTIES.REQUIRED;
						}
						objForm.FIELDS[current]=item;
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					var defaultlabel = "#i18n('"+$('#formgenerate-name').val()+"_')#";
					$('#submit-edit-textarea input[type="text"]').val("");
					$('#submit-edit-textarea input[name="label"]').val(defaultlabel);
					$('#submit-add-textarea textarea').val("");
					$('#submit-edit-textarea input[type="checkbox"]').attr('checked', false);
				}
			});
			
			$( "#dialog-add-dropdown" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Create": function() {
						var item = {
							FIELDID:"item-"+Math.uuid(17),
							FIELDNAME:$("#submit-add-dropdown #name").val(),
							FIELDTYPE:"dropdown",
							FIELDLABEL:$("#submit-add-dropdown #label").val(),
							POSITION:formid,
							PROPERTIES:{
								NAME:$("#submit-add-dropdown #name").val(),
								ID:$("#submit-add-dropdown #name").val(),
								SIZE:$("#submit-add-dropdown #size").val(),
								WEBSERVICENAME:$("#submit-add-dropdown #webservicename").val()
							},
							OPTIONS:{
								VALUES:$("#submit-add-dropdown #value").val(),
								SELECTED:$("#submit-add-dropdown #selected").val()
							}
						};
						if($('#submit-add-dropdown #multiple:checked').val() !== undefined){
							item.PROPERTIES.MULTIPLE="multiple";
						}
						if($('#submit-add-dropdown #webservice:checked').val() !== undefined){
							item.PROPERTIES.WEBSERVICE="webservice";
						}
						objForm.FIELDS.push(item);
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#submit-add-dropdown input[type="text"],#submit-add-dropdown textarea').val("");
					var defaultlabel = "#i18n('"+$('#formgenerate-name').val()+"_')#";
					$('#submit-add-dropdown input[name="label"]').val(defaultlabel);
					$('#submit-add-dropdown input[type="checkbox"]').attr('checked', false);
					$('#submit-add-dropdown select option:first').attr('selected', 'selected');
				}
			});
			
			$( "#dialog-edit-dropdown" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Update": function() {
						var current = 0;
						$.each(objForm.FIELDS, function(index, value) { 
							if(value.FIELDID == $("#submit-edit-dropdown #fieldid").val()){
								current=index;
							}
						});
						
						var item = {
							FIELDID:$("#submit-edit-dropdown #fieldid").val(),
							FIELDNAME:$("#submit-edit-dropdown #name").val(),
							FIELDTYPE:"dropdown",
							FIELDLABEL:$("#submit-edit-dropdown #label").val(),
							POSITION:objForm.FIELDS[current].POSITION,
							PROPERTIES:{
								NAME:$("#submit-edit-dropdown #name").val(),
								ID:$("#submit-edit-dropdown #name").val(),
								SIZE:$("#submit-edit-dropdown #size").val(),
								WEBSERVICENAME:$("#submit-edit-dropdown #webservicename").val()
							},
							OPTIONS:{
								VALUES:$("#submit-edit-dropdown #value").val(),
								SELECTED:$("#submit-edit-dropdown #selected").val()
							}
						};
						if($('#submit-edit-dropdown #multiple:checked').val() !== undefined){
							item.PROPERTIES.MULTIPLE="multiple";
						}else{
							delete item.PROPERTIES.MULTIPLE;
						}
						if($('#submit-edit-dropdown #webservice:checked').val() !== undefined){
							item.PROPERTIES.WEBSERVICE="webservice";
						}else{
							delete item.PROPERTIES.WEBSERVICE;
						}
						objForm.FIELDS[current]=item;
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#submit-edit-dropdown input[type="text"],#submit-edit-dropdown textarea').val("");
					var defaultlabel = "#i18n('"+$('#formgenerate-name').val()+"_')#";
					$('#submit-edit-dropdown input[name="label"]').val(defaultlabel);
					$('#submit-edit-dropdown input[type="checkbox"]').attr('checked', false);
					$('#submit-edit-dropdown select option:first').attr('selected', 'selected');
				}
			});

			$( "#dialog-add-subform" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Create": function() {
						var item = {
							FIELDID:"item-"+Math.uuid(17),
							FIELDNAME:$("#submit-add-subform #name").val(),
							FIELDTYPE:"subform",
							FIELDLABEL:$("#submit-add-subform #label").val(),
							POSITION:formid,
							PROPERTIES:{
								NAME:$("#submit-add-subform #name").val(),
								ID:$("#submit-add-subform #name").val(),
								SUBFORMID:$("#submit-add-subform #subformid").val()
							}
						};
						if($('#submit-add-subform #formshow:checked').val() !== undefined){
							item.PROPERTIES.FORMSHOW="list";
						}
						objForm.FIELDS.push(item);
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#submit-add-subform input[type="text"]').val("");
					var defaultlabel = "#i18n('"+$('#formgenerate-name').val()+"_')#";
					$('#submit-add-subform input[name="label"]').val(defaultlabel);
					$('#submit-add-subform input[type="checkbox"]').attr('checked', false);
				}
			});
			
			$( "#dialog-edit-subform" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Update": function() {
						var current = 0;
						$.each(objForm.FIELDS, function(index, value) { 
							if(value.FIELDID == $("#submit-edit-subform #fieldid").val()){
								current=index;
							}
						});
						
						var item = {
							FIELDID:$("#submit-edit-subform #fieldid").val(),
							FIELDNAME:$("#submit-edit-subform #name").val(),
							FIELDTYPE:"subform",
							FIELDLABEL:$("#submit-edit-subform #label").val(),
							POSITION:objForm.FIELDS[current].POSITION,
							PROPERTIES:{
								NAME:$("#submit-edit-subform #name").val(),
								ID:$("#submit-edit-subform #name").val(),
								SUBFORMID:$("#submit-edit-subform #subformid").val()
							}
						};
						if($('#submit-edit-subform #formshow:checked').val() !== undefined){
							item.PROPERTIES.FORMSHOW="list";
						}else{
							delete item.PROPERTIES.FORMSHOW;
						}
						objForm.FIELDS[current]=item;
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#submit-edit-subform input[type="text"]').val("");
					var defaultlabel = "#i18n('"+$('#formgenerate-name').val()+"_')#";
					$('#submit-edit-subform input[name="label"]').val(defaultlabel);
					$('#submit-edit-subform input[type="checkbox"]').attr('checked', false);
				}
			});
			
			$( "#dialog-add-checkbox" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Create": function() {
						var item = {
							FIELDID:"item-"+Math.uuid(17),
							FIELDNAME:$("#submit-add-checkbox #name").val(),
							FIELDTYPE:"checkbox",
							FIELDLABEL:$("#submit-add-checkbox #label").val(),
							POSITION:formid,
							PROPERTIES:{
								NAME:$("#submit-add-checkbox #name").val(),
								ID:$("#submit-add-checkbox #name").val()
							},
							OPTIONS:{
								VALUES:$("#submit-add-checkbox #value").val(),
								SELECTED:$("#submit-add-checkbox #selected").val()
							}
						};
						if($('#submit-add-checkbox #remote').val() != ""){
							item.OPTIONS.REMOTE = $('#submit-add-checkbox #remote').val();
						}
						objForm.FIELDS.push(item);
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#submit-add-checkbox input[type="text"],#submit-add-checkbox textarea').val("");
					var defaultlabel = "#i18n('"+$('#formgenerate-name').val()+"_')#";
					$('#submit-add-checkbox input[name="label"]').val(defaultlabel);
					$('#submit-add-checkbox input[type="checkbox"]').attr('checked', false);
					$('#submit-add-checkbox select option:first').attr('selected', 'selected');
				}
			});
			
			$( "#dialog-edit-checkbox" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Update": function() {
						var current = 0;
						$.each(objForm.FIELDS, function(index, value) { 
							if(value.FIELDID == $("#submit-edit-checkbox #fieldid").val()){
								current=index;
							}
						});
						
						var item = {
							FIELDID:$("#submit-edit-checkbox #fieldid").val(),
							FIELDNAME:$("#submit-edit-checkbox #name").val(),
							FIELDTYPE:"checkbox",
							FIELDLABEL:$("#submit-edit-checkbox #label").val(),
							POSITION:objForm.FIELDS[current].POSITION,
							PROPERTIES:{
								NAME:$("#submit-edit-checkbox #name").val(),
								ID:$("#submit-edit-checkbox #name").val(),
							},
							OPTIONS:{
								VALUES:$("#submit-edit-checkbox #value").val(),
								SELECTED:$("#submit-edit-checkbox #selected").val()
							}
						};
						if($('#submit-edit-checkbox #remote').val() != ""){
							item.OPTIONS.REMOTE = $('#submit-edit-checkbox #remote').val();
						}else{
							delete item.OPTIONS.REMOTE
						}
						objForm.FIELDS[current]=item;
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#submit-edit-checkbox input[type="text"],#submit-edit-checkbox textarea').val("");
					var defaultlabel = "#i18n('"+$('#formgenerate-name').val()+"_')#";
					$('#submit-edit-checkbox input[name="label"]').val(defaultlabel);
					$('#submit-edit-checkbox input[type="checkbox"]').attr('checked', false);
					$('#submit-edit-checkbox select option:first').attr('selected', 'selected');
				}
			});
			
			$( "#dialog-add-radiobutton" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Create": function() {
						var item = {
							FIELDID:"item-"+Math.uuid(17),
							FIELDNAME:$("#submit-add-radiobutton #name").val(),
							FIELDTYPE:"radiobutton",
							FIELDLABEL:$("#submit-add-radiobutton #label").val(),
							POSITION:formid,
							PROPERTIES:{
								NAME:$("#submit-add-radiobutton #name").val(),
								ID:$("#submit-add-radiobutton #name").val()
							},
							OPTIONS:{
								VALUES:$("#submit-add-radiobutton #value").val(),
								SELECTED:$("#submit-add-radiobutton #selected").val()
							}
						};
						if($('#submit-add-radiobutton #remote').val() != ""){
							item.OPTIONS.REMOTE = $('#submit-add-radiobutton #remote').val();
						}
						objForm.FIELDS.push(item);
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#submit-add-radiobutton input[type="text"],#submit-add-radiobutton textarea').val("");
					var defaultlabel = "#i18n('"+$('#formgenerate-name').val()+"_')#";
					$('#submit-add-radiobutton input[name="label"]').val(defaultlabel);
					$('#submit-add-radiobutton input[type="checkbox"]').attr('checked', false);
					$('#submit-add-radiobutton select option:first').attr('selected', 'selected');
				}
			});
			
			$( "#dialog-edit-radiobutton" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Update": function() {
						var current = 0;
						$.each(objForm.FIELDS, function(index, value) { 
							if(value.FIELDID == $("#submit-edit-radiobutton #fieldid").val()){
								current=index;
							}
						});
						
						var item = {
							FIELDID:$("#submit-edit-radiobutton #fieldid").val(),
							FIELDNAME:$("#submit-edit-radiobutton #name").val(),
							FIELDTYPE:"radiobutton",
							FIELDLABEL:$("#submit-edit-radiobutton #label").val(),
							POSITION:objForm.FIELDS[current].POSITION,
							PROPERTIES:{
								NAME:$("#submit-edit-radiobutton #name").val(),
								ID:$("#submit-edit-radiobutton #name").val(),
							},
							OPTIONS:{
								VALUES:$("#submit-edit-radiobutton #value").val(),
								SELECTED:$("#submit-edit-radiobutton #selected").val()
							}
						};
						if($('#submit-edit-radiobutton #remote').val() != ""){
							item.OPTIONS.REMOTE = $('#submit-edit-radiobutton #remote').val();
						}else{
							delete item.OPTIONS.REMOTE
						}
						objForm.FIELDS[current]=item;
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#submit-edit-radiobutton input[type="text"],#submit-edit-radiobutton textarea').val("");
					var defaultlabel = "#i18n('"+$('#formgenerate-name').val()+"_')#";
					$('#submit-edit-radiobutton input[name="label"]').val(defaultlabel);
					$('#submit-edit-radiobutton input[type="checkbox"]').attr('checked', false);
					$('#submit-edit-radiobutton select option:first').attr('selected', 'selected');
				}
			});
			
			$( "#dialog-add-html" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Update": function() {
						var item = {
							FIELDID:"item-"+Math.uuid(17),
							FIELDTYPE:"html",
							FIELDLABEL:$("#submit-add-html #label").val(),
							POSITION:formid,
							HTML:$("#submit-add-html #html").val()
						};
						objForm.FIELDS.push(item);
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#submit-add-html input[type="text"],#submit-add-checkbox textarea').val("");
				}
			});
			
			$( "#dialog-edit-html" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Update": function() {
						var current = 0;
						$.each(objForm.FIELDS, function(index, value) { 
							if(value.FIELDID == $("#submit-edit-html #fieldid").val()){
								current=index;
							}
						});
						
						var item = {
							FIELDID:$("#submit-edit-html #fieldid").val(),
							FIELDTYPE:"html",
							FIELDLABEL:$("#submit-edit-html #label").val(),
							POSITION:objForm.FIELDS[current].POSITION,
							HTML:$("#submit-edit-html #html").val()
						};
						objForm.FIELDS[current]=item;
						var tabArr = new Array();
						$(".sorttabs").each(function(){
							tabArr.push({"id":$(this).attr("id"),"select":$(this).tabs('option','selected')});
						});
						genForm();
						activeForm();
						$.each(tabArr, function(index, value) {
							$("#"+value.id).tabs( "select", value.select);
						});
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#submit-edit-html input[type="text"],#submit-edit-radiobutton textarea').val("");
				}
			});
			
			$( "#dialog-add-panel" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Add": function() {
					 	var item = {
							FIELDID:"item-"+Math.uuid(17),
							FIELDTYPE:"panel",
							FIELDLABEL:$("#dialog-add-panel #label").val(),
							POSITION:$("#dialog-add-panel #fieldid").val()
						};
						objForm.FIELDS.push(item);
						genForm();
						activeForm();
						$("#"+$("#dialog-add-panel #fieldid").val()).tabs( "select", $("#"+$("#dialog-add-panel #fieldid").val()).tabs("length")-1);
						
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#dialog-add-panel input[type="text"]').val("");
					var defaultlabel = "#i18n('"+$('#formgenerate-name').val()+"_')#";
					$('#submit-add-panel input[name="label"]').val(defaultlabel);
				}
			});

			$( "#dialog-add-rule" ).dialog({
				autoOpen: false,
				width: 550,
				modal: true,
				buttons: {
					"Add": function() {
					 	var item = {
							FIELDID:"item-"+Math.uuid(17),
							FIELDTYPE:"rule",
							FIELDLABEL:$("#dialog-add-rule #label").val(),
							POSITION:$("#dialog-add-rule #fieldid").val()
						};
						objForm.FIELDS.push(item);
						genForm();
						activeForm();
						$("#"+$("#dialog-add-rule #fieldid").val()).tabs( "select", $("#"+$("#dialog-add-rule #fieldid").val()).tabs("length")-1);
						
						$( this ).dialog( "close" );
					},
					Cancel: function() {
						$( this ).dialog( "close" );
					}
				},
				close: function() {
					$('#dialog-add-rule input[type="text"]').val("");
					var defaultlabel = "#i18n('"+$('#formgenerate-name').val()+"_')#";
					$('#submit-add-rule input[name="label"]').val(defaultlabel);
				}
			});
		}
		
		function filterObjForm(){
			var checker = function(id){
				var check = false;
				$.each(objForm.FIELDS, function(index, value) { 
					if(value.FIELDID == id || objForm.FORMID == id){
						check = true;
					}
				});
				return check;
			};
			var loop = false;
			var field = new Array();
			$.each(objForm.FIELDS, function(index, value) { 
				if(checker(value.POSITION)){
					field.push(value);
				}else{
					loop = true;
				}
			});
			objForm.FIELDS = field;
			if(loop==true){
				filterObjForm();
			}
		}
		
		function writePreview() {
			var strVar="";
			strVar += "<!DOCTYPE HTML PUBLIC \"-\/\/W3C\/\/DTD XHTML 1.0 Strict\/\/EN\"";
			strVar += "	\"http:\/\/www.w3.org\/TR\/xhtml1\/DTD\/xhtml1-strict.dtd\">";
			strVar += "<html xmlns=\"http:\/\/www.w3.org\/1999\/xhtml\">";
			strVar += "<head>";
			strVar += "	<title>Form Preview<\/title>";
			strVar += "	<meta http-equiv=\"Content-Type\" content=\"text\/html; charset=UTF-8\"\/>";
			strVar += "<link href=\"\/sitemongo\/stylesheets\/reset.css\" media=\"all\" rel=\"stylesheet\" type=\"text\/css\" \/>";
			strVar += "<link href=\"\/sitemongo\/stylesheets\/960.css\" media=\"all\" rel=\"stylesheet\" type=\"text\/css\" \/>";
			strVar += "<link href=\"\/sitemongo\/stylesheets\/text.css\" media=\"all\" rel=\"stylesheet\" type=\"text\/css\" \/>";
			strVar += "<link href=\"\/sitemongo\/stylesheets\/layout.css\" media=\"all\" rel=\"stylesheet\" type=\"text\/css\" \/>";
			strVar += "<link href=\"\/sitemongo\/stylesheets\/jquery-ui-1.9.0.custom.css\" media=\"all\" rel=\"stylesheet\" type=\"text\/css\" \/>";
			strVar += "<script src=\"\/sitemongo\/javascripts\/jquery-1.8.2.js\" type=\"text\/javascript\"><\/script>";
			strVar += "<script src=\"\/sitemongo\/javascripts\/jquery.tools.min.js\" type=\"text\/javascript\"><\/script>";
			strVar += "<script src=\"\/sitemongo\/javascripts\/jquery-ui-1.9.0.custom.js\" type=\"text\/javascript\"><\/script>";
			strVar += "<script src=\"\/sitemongo\/javascripts\/main.js\" type=\"text\/javascript\"><\/script>";
			strVar += "<script src=\"\/sitemongo\/javascripts\/showform.js\" type=\"text\/javascript\"><\/script>";
			strVar += "<script type=\"text\/javascript\" src=\"http:\/\/www.google.com\/recaptcha\/api\/js\/recaptcha_ajax.js\"><\/script> ";
			strVar += "<script type=\"text\/javascript\">";
			strVar += "		$(function(){";
			strVar += "			showForm(\"#preview\","+$.stringify(objForm)+",\"\/sitemongo\/index.cfm\/Forms\/submit\",\""+publickey+"\");";
			strVar += "			$('input[type=\"submit\"]\').hide();";
			strVar += "		});";
			strVar += "<\/script>";
			strVar += "<head><body>";
			strVar += "<div id=\"wrap-bg\">";
			strVar += "<div id=\"onecol\" class=\"container_16\">";
			strVar += "	<div id=\"main\" class=\"grid_16\">";
			strVar += "		<div class=\"grid_16 alpha omega\">";
			strVar += "			<div class=\"contentwrap\">";
			strVar += "				<div id=\"preview\" class=\"showform\" ><\/div>";
			strVar += "			<\/div>	";
			strVar += "		<\/div>";
			strVar += "	<\/div>";
			strVar += "	<div class=\"clear\"><\/div>";
			strVar += "<\/div>";
			strVar += "<\/div>";
			strVar += "</body></html>";

	
			
			
		 top.consoleRef=window.open('','myconsole',
		  'width=1000,height=590'
		   +',menubar=0'
		   +',toolbar=0'
		   +',status=0'
		   +',scrollbars=1'
		   +',resizable=1');
		 top.consoleRef.document.writeln(strVar);
		 top.consoleRef.document.close();
		}

		$.stringify = function (obj) {
		    var t = typeof (obj);
		    if (t != "object" || obj === null) {
		        if (t == "string") obj = '"'+obj+'"';
		        return String(obj);
		    }
		    else {
		        var n, v, json = [], arr = (obj && obj.constructor == Array);
		        for (n in obj) {
		            v = obj[n]; t = typeof(v);
		            if (t == "string") v = '"'+v+'"';
		            else if (t == "object" && v !== null) v = JSON.stringify(v);
		            json.push((arr ? "" : '"' + n + '":') + String(v));
		        }
		        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
		    }
		};
		
		function setCollection(collection){
			objForm.FORMCOLLECTION = collection;
			objForm.FORMNAME = collection;
			objForm.FORMTITLE = collection;
		}
		
		function verifyCollection(verifyurl){
			$.getJSON(verifyurl+'step1?collection='+$("#collectionname").val(), function(data) {
				$("#verifyfield").show();
				if(data.pass==true){
					$("#showverify").append("<li>Collection Available Pass.</li>");
					$.getJSON(verifyurl+'step2?collection='+$("#collectionname").val(), function(data) {
						if(data.pass==true){
							$("#showverify").append("<li>Create Collection Pass.</li>");
							$.getJSON(verifyurl+'step3?collection='+$("#collectionname").val(), function(data) {
								if(data.pass==true){
									$("#showverify").append("<li>Insert Pass.</li>");
									$.getJSON(verifyurl+'step4?collection='+$("#collectionname").val(), function(data) {
										if(data.pass==true){
											$("#showverify").append("<li>Find Pass.</li>");
											$.getJSON(verifyurl+'step5?collection='+$("#collectionname").val(), function(data) {
												if (data.pass == true) {
													$("#showverify").append("<li>Update Pass.</li>");
													$.getJSON(verifyurl+'step6?collection='+$("#collectionname").val(), function(data) {
														if (data.pass == true) {
															$("#showverify").append("<li>Remove Pass.</li>");
															$("#collectionname").attr("readonly","readonly");
															$("#Verify").val("Create");
														}else{
															$("#showverify").append("<li class=\"verifyerror\">Remove Error.</li>");
														}
														$.getJSON(verifyurl+'drop?collection='+$("#collectionname").val());
													});
												}else{
													$("#showverify").append("<li class=\"verifyerror\">Update Error.</li>");
													$.getJSON(verifyurl+'drop?collection='+$("#collectionname").val());
												}
											});
										}else{
											$("#showverify").append("<li class=\"verifyerror\">Find Error.</li>");
											$.getJSON(verifyurl+'drop?collection='+$("#collectionname").val());
										}
									});
								}else{
									$("#showverify").append("<li class=\"verifyerror\">Insert Error.</li>");
									$.getJSON(verifyurl+'drop?collection='+$("#collectionname").val());
								}
							});
						}else{
							$("#showverify").append("<li class=\"verifyerror\">Create Collection Error.</li>");
						}
					});
				}else{
					$("#showverify").append("<li class=\"verifyerror\">Collection Not Available.</li>");
				}
			});
		}
