var oTable = '';
var gaiSelected = [];
function createdatatable(tableid,control,paramsquery,sortdefault) {
    var sortfield = [0,"asc"]
    if(sortdefault != undefined)
    {
        var sortarr = sortdefault.split("/");
        sortfield = [sortarr[0]-1,sortarr[1]];
    }
    oTable = $("#"+tableid).dataTable({
//        "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
        "sPaginationType": "bootstrap",
        "bProcessing": true,
        "bServerSide": true,
        "bJQueryUI": true,
        "fnDestroy": true,
        "bRetrieve":true,
        "aaSorting": [sortfield],
        "iDisplayStart": startpage,
        //"iDisplayLength": 5,
//        "sPaginationType": "full_numbers",
        "sAjaxSource": seturlbase()+"/"+control+".cfm"+paramsquery
    });

    $("#"+tableid).css("width","100%");
}

function createdatatablebrowse(tableid,control,paramsquery,sortdefault) {
    var sortfield = [0,"asc"]
    if(sortdefault != undefined)
    {
        var sortarr = sortdefault.split("/");
        sortfield = [sortarr[0]-1,sortarr[1]];
    }

    oTable = $("#"+tableid).dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "bJQueryUI": true,
        "fnDestroy": true,
        "bRetrieve":true,
	    "bLengthChange": false,
        "aaSorting": [sortfield],
        "sPaginationType": "full_numbers",
        "sAjaxSource": seturlbase()+"/"+control+".cfm"+paramsquery,
        "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
            if ( jQuery.inArray(aData[0], gaiSelected) !== -1 ) {
                $(nRow).addClass('row_selected');
            }
            return nRow;
        },
        "aoColumnDefs": [
            { "bVisible": false, "aTargets": [ 0 ] }
        ]
    });
    $("#"+tableid).css("width","100%");
}

function settooltip()
{
	$('.generatebtn').tipsy({gravity: 's'});
	$('.designbtn').tipsy({gravity: 's'});
	$('.archivebtn > input[type="submit"]').tipsy({gravity: 's'});
	$('.deletebtn > input[type="submit"]').tipsy({gravity: 's'});
	$('.unarchivebtn > input[type="submit"]').tipsy({gravity: 's'});
}