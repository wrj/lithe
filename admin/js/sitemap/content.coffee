websiteurl = $(".websiteUrl").val()

window.updateContent =(element,type,contentdata) ->
	sList = "";
	newData=[]
	element.$('input[type=checkbox]').each ->
		if @.checked
			newd = {}
			newd['_id'] = $(@).val()
			showurl = ""
			$(@).parent().parent().find(".priority option:selected").each ->
				newd['PRIORITY'] = $(@).val() ;
			showurl = window.urlDom @,type
			newd['URL'] = showurl
			newd['LASTMODIFY'] = $(@).parent().parent().find(".lastmodify").val()
			newd['FILETEMPLATE'] = $(@).parent().parent().find(".template").val()
			newData.push newd
	newContent = []
	for i in contentdata
		noaddItem = true
		if newData.length > 0
			for j in newData
				if i._id is j._id
					ContentItem = i
					ContentItem['LASTMODIFY'] = j.LASTMODIFY
					ContentItem['PRIORITY']= j.PRIORITY
					ContentItem['FILETEMPLATE']= j.FILETEMPLATE
					ContentItem['URL'] = j.URL
					ContentItem['USE']=1
					newContent.push ContentItem
					noaddItem = false
			if noaddItem
				i['USE'] = 0
				newContent.push i
		else
			i['USE'] = 0
			newContent.push i
	return newContent

window.mapvalueContent = (type,contentupdate,contentdata) ->
	newContent = []
	for i in contentdata
		noaddItem = true
		if contentupdate.length > 0
			for j in contentupdate
				if i._id is j.CONTENTID
					ContentItem = i
					ContentItem['LASTMODIFY'] = dateFormat(i['UPDATEDAT'],"yyyy-MM-dd")
					ContentItem['PRIORITY']= j.PRIORITY
					ContentItem['FILETEMPLATE']= j.TEMPLATE
					if type is "TAG"
						ContentItem['URL'] = window.urlData(type,i.NAME,j.TEMPLATE,"")
					else if type is "CATEGORY" or type is "GALLERY" or type is "HTML"
						ContentItem['URL'] = window.urlData(type,i.SLUG,j.TEMPLATE,"")
					ContentItem['USE']=1
					newContent.push ContentItem
					noaddItem = false
			if noaddItem
				newContent.push i
		else
			newContent.push i
	return newContent

