websiteurl = $(".websiteUrl").val()
window.updatePost =(category,postDataTable,type,postdata) ->
	newData=[]
	postDataTable.$('input[type=checkbox]').each ->
		if @.checked
			newd = {}
			newd['_id'] = $(@).val()
			$(@).parent().parent().find(".priority option:selected").each ->
				newd['PRIORITY'] = $(@).val() ;
			newd['URL'] = window.urlDom @,type
			newd['LASTMODIFY'] = $(@).parent().parent().find(".lastmodify").val()
			newData.push newd
	newPost = []
	for i in postdata
		if i.CATEGORYID is category
			newPostItem = []
			for j in i[type]
				noaddItem = true
				if newData.length > 0
					for k in newData
						if j._id is k._id
							PostItem = j
							PostItem['LASTMODIFY'] = k.LASTMODIFY
							PostItem['PRIORITY']= k.PRIORITY
							PostItem['URL'] = k.URL
							PostItem['USE']=1
							newPostItem.push PostItem
							noaddItem = false
					if noaddItem
						j['USE'] = 0
						newPostItem.push j
				else
					j['USE'] = 0
					newPostItem.push j
			newcatepost={}
			newcatepost['CATEGORYID'] = i.CATEGORYID
			newcatepost[type] = newPostItem
			if $(".all"+type+":checked").val()?
				newcatepost['ALL'] = true
			else
				newcatepost['ALL'] = false
			newPost.push newcatepost
		else
			newPost.push i
	return newPost

window.mapvaluePost = (type,checkall,postupdate,postdata) ->
	newPost=[]
	for i in postdata
		newPostItem = []
		for j in i[type]
			noaddItem = true
			if postupdate.length > 0
				for k in postupdate
					if j._id is k.CONTENTID
						PostItem = j
						PostItem['LASTMODIFY'] = dateFormat(j['UPDATEDAT'],"yyyy-MM-dd")
						PostItem['PRIORITY'] = k.PRIORITY
						PostItem['URL'] = window.urlData(type,j.SLUGURL,j.TEMPLATE,j.PATTERN)
						PostItem['USE']=1
						newPostItem.push PostItem
						noaddItem = false
				if noaddItem
					newPostItem.push j
			else
				newPostItem.push j
		newcatepost={}
		newcatepost['CATEGORYID'] = i.CATEGORYID
		newcatepost[type] = newPostItem
		a=[]
		for qcheckall in checkall
			if i.CATEGORYID is qcheckall
				newcatepost['ALL'] = true
		newPost.push newcatepost
	return newPost

window.reloadPost = (type,postdata) ->
	newPost=[]
	for i in postdata
		newPostItem = []
		for j in i[type]
			PostItem = j
			PostItem['LASTMODIFY'] = j.LASTMODIFY
			PostItem['PRIORITY'] = j.PRIORITY
			PostItem['URL'] = window.urlData(type,j.SLUGURL,j.TEMPLATE,j.PATTERN)
			PostItem['USE']= j.USE
			newPostItem.push PostItem
			noaddItem = false
		newcatepost={}
		newcatepost['CATEGORYID'] = i.CATEGORYID
		newcatepost[type] = newPostItem
		newcatepost['ALL'] = i.ALL
		newPost.push newcatepost
	return newPost
