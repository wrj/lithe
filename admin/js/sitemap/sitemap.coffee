websiteurl = $(".websiteUrl").val()

removeslash = (text) ->
	if text.substr(text.length-1) is "/"
		text=text.slice(0,-1)
		if text.substr(text.length-1) is "/"
			text = removeslash(text)
	text


buildurl = (websiteurl,template,value,key) ->
	pattern = $("#otherlinkpattern").val()
	showurl="#{websiteurl}#{pattern}"
	showurl=showurl.replace("{template}",template)
	showurl=showurl.replace("{key}",key)
	showurl=showurl.replace("{value}",value)
	if key is "" and value is ""
		showurl=showurl.replace("?","")
		showurl=showurl.replace("=","")
	showurl=showurl.replace("{otherparams}","")
	templatetype = $("#rewritepattern").val().split(',')
	for typedel in templatetype
		regex = new RegExp(typedel, 'g')
		showurl=showurl.replace(regex,"")
	showurl=removeslash(showurl)
	showurl

buildslugurl = (websiteurl,template,slug,pattern) ->
	usedefalsepattern = false
	if pattern is ""
		pattern = $("#linkpattern").val()
		usedefalsepattern = true
	showurl="#{websiteurl}#{pattern}"
	showurl=showurl.replace("{template}",template);
	showurl=showurl.replace("{slug}",slug);
	showurl=showurl.replace("{otherparams}","");
	templatetype = $("#rewritepattern").val().split(',');
	if usedefalsepattern
		for typedel in templatetype
			regex = new RegExp(typedel, 'g');
			showurl=showurl.replace(regex,"");
	showurl

$ ->
	# ส่วนที่แสดง url ตัวอย่าง
	$(".Urlhideshow").live ("click"),(e) ->
		if $(@).is(".icon-chevron-down")
			$(@).removeClass("icon-chevron-down")
			$(@).addClass("icon-chevron-up")
			urltype = $(@).parent().find(".urltype").val()
			showurl=""
			if urltype is "MENUNAVI"
				menunaviUrl = $(@).parent().find(".menunaviurl").val()
				if menunaviUrl.substr(0,1) is '/'
					menunaviUrl=menunaviUrl.substr(1)
				else if menunaviUrl.substr(0,2) is './'
					menunaviUrl=menunaviUrl.substr(2)
				showurl = "#{websiteurl}#{menunaviUrl}"
			else if urltype is "SUBMENUCONTENT"
				slug = $(@).parent().find(".menucontentname").val()
				template = $(@).parent().parent().find(".template").val()
				if template is ""
					template = $(".menucontentdefaulttemplate").val()
				showurl = buildurl(websiteurl,template,slug,"submenu")
			else if urltype is "HEADMENUCONTENT"
				slug = $(@).parent().find(".menucontentname").val()
				template = $(@).parent().parent().find(".template").val()
				if template is ""
					template = $(".menucontentdefaulttemplate").val()
				showurl = buildurl(websiteurl,template,slug,"menu")
			else if urltype is "CATEGORY"
				slug = $(@).parent().find(".cateslug").val()
				template = $(@).parent().parent().find(".template").val()
				pattern = $(@).parent().parent().find(".pattern").val()
				if template is ""
					template = $(".categorydefaulttemplate").val()
				showurl = buildurl(websiteurl,template,slug,"category")
			else if urltype is "HTML"
				slug = $(@).parent().find(".htmlslug").val()
				template = $(@).parent().parent().find(".template").val()
				if template is ""
					template = $(".htmldefaulttemplate").val()
				showurl = buildslugurl(websiteurl,template,slug,"")
			else if urltype is "GALLERY"
				slug = $(@).parent().find(".galleryslug").val()
				template = $(@).parent().parent().find(".template").val()
				if template is ""
					template = $(".gallerydefaulttemplate").val()
				showurl = buildslugurl(websiteurl,template,slug,"")
			else if urltype is "POST"
				slug = $(@).parent().find(".slugurl").val()
				pattern = $(@).parent().find(".sitemappattern").val()
				template = $(@).parent().find(".template").val()
				showurl = buildslugurl(websiteurl,template,slug,pattern)
			else if urltype is "TAG"
				slug = $(@).parent().find(".tagname").val()
				template = $(@).parent().parent().find(".template").val()
				if template is ""
					template = $(".tagdefaulttemplate").val()
				showurl = buildurl(websiteurl,template,slug,"tags")
			$(@).parent().parent().after("""<tr class='showurl' style='display: none;'><td colspan=6><a href='#{showurl}' target='_blank'>#{showurl}</a></td></tr>""")
			$(@).parent().parent().next().slideToggle()
		else
			$(@).addClass("icon-chevron-down")
			$(@).removeClass("icon-chevron-up")
			$(@).parent().parent().next().slideToggle()
			if $(@).parent().parent().next().is(".showurl")
				$(@).parent().parent().next().remove()
	$(".allMenunaviTable").hide()

window.treeCategory = (targetid) ->
	$("." + targetid).dynatree
		initAjax:
#			เด๋วจะทำให้เหลือคิวรี่ครั้งเดียว
			url: seturlbase() + "/treeprocess.cfm"
		onPostInit: (isReloading, isError) ->
			if categoryurl is ""
				@reactivate()
			else
				@selectKey categoryurl
				@activateKey categoryurl
		onActivate: (node) ->
			reportkey node
		debugLevel: 0

window.changePriority = (priority,name) ->
	selected1 = "selected" if priority is "0"
	selected2 = "selected" if priority is "0.2"
	selected3 = "selected" if priority is "0.5"
	selected4 = "selected" if priority is "0.7"
	selected5 = "selected" if priority is "1"
	text ="""
         <select class="priority" name="priority">
         <option value="0" #{selected1}>Very Low</option>
         <option value="0.2" #{selected2}>Low</option>
         <option value="0.5" #{selected3}>Medium</option>
         <option value="0.7" #{selected4}>high</option>
         <option value="1" #{selected5}>very high</option>
         </select>
         """

window.urlDom = ($this,type) ->
	showurl=""
	if type is "MENUNAVI"
		menunaviUrl = $($this).parent().parent().find(".menunaviurl").val()
		if menunaviUrl.substr(0,1) is '/'
			menunaviUrl=menunaviUrl.substr(1)
		else if menunaviUrl.substr(0,2) is './'
			menunaviUrl=menunaviUrl.substr(2)
		showurl = "#{websiteurl}#{menunaviUrl}"
	else if type is "SUBMENUCONTENT"
		slug = $($this).parent().parent().find(".menucontentname").val()
		template = $($this).parent().parent().find(".template").val()
		if template is ""
			template = $(".menucontentdefaulttemplate").val()
		showurl = buildurl(websiteurl,template,slug,"submenu")
	else if type is "HEADMENUCONTENT"
		slug = $($this).parent().parent().find(".menucontentname").val()
		template = $($this).parent().parent().find(".template").val()
		if template is ""
			template = $(".menucontentdefaulttemplate").val()
		showurl = buildurl(websiteurl,template,slug,"menu")
	else if type is "CATEGORY"
		slug = $($this).parent().parent().find(".cateslug").val()
		template = $($this).parent().parent().find(".template").val()
		if template is ""
			template = $(".categorydefaulttemplate").val()
		showurl = buildurl(websiteurl,template,slug,"category")
	else if type is "HTML"
		slug = $($this).parent().parent().find(".htmlslug").val()
		template = $($this).parent().parent().find(".template").val()
		if template is ""
			template = $(".htmldefaulttemplate").val()
		showurl = buildslugurl(websiteurl,template,slug,"")
	else if type is "POST"
		slug = $($this).parent().parent().find(".slugurl").val()
		pattern = $($this).parent().parent().find(".sitemappattern").val()
		template = $($this).parent().parent().find(".template").val()
		showurl = buildslugurl(websiteurl,template,slug,pattern)
	else if type is "GALLERY"
		slug = $($this).parent().parent().find(".galleryslug").val()
		template = $($this).parent().parent().find(".template").val()
		if template is ""
			template = $(".gallerydefaulttemplate").val()
		showurl = buildslugurl(websiteurl,template,slug,"")
	else if type is "TAG"
		slug = $($this).parent().parent().find(".tagname").val()
		template = $($this).parent().parent().find(".template").val()
		if template is ""
			template = $(".tagdefaulttemplate").val()
		showurl = buildurl(websiteurl,template,slug,"tags")
	return showurl

window.urlData = (type,slugname,template,pattern) ->
	showurl=""
	if type is "MENUNAVI"
		menunaviUrl = slugname
		if menunaviUrl.substr(0,1) is '/'
			menunaviUrl=menunaviUrl.substr(1)
		else if menunaviUrl.substr(0,2) is './'
			menunaviUrl=menunaviUrl.substr(2)
		showurl = "#{websiteurl}#{menunaviUrl}"
	else if type is 'SUBMENUCONTENT'
		slug = slugname
		if template is ""
			template = $('.menucontentdefaulttemplate').val()
		showurl = buildurl(websiteurl,template,slug,"submenu")
	else if type is 'HEADMENUCONTENT'
		slug = slugname
		if template is ""
			template = $('.menucontentdefaulttemplate').val()
		showurl = buildurl(websiteurl,template,slug,"menu")
	else if type is "CATEGORY"
		slug = slugname
		if template is ""
			template = $(".categorydefaulttemplate").val()
		showurl = buildurl(websiteurl,template,slug,"category")
	else if type is "HTML"
		slug = slugname
		if template is ""
			template = $(".htmldefaulttemplate").val()
		showurl = buildslugurl(websiteurl,template,slug,"")
	else if type is "POST"
		slug = slugname
		showurl = buildslugurl(websiteurl,template,slug,pattern)
	else if type is "GALLERY"
		slug = slugname
		if template is ""
			template = $(".gallerydefaulttemplate").val()
		showurl = buildslugurl(websiteurl,template,slug,"")
	else if type is "TAG"
		slug = slugname
		if template is ""
			template = $(".tagdefaulttemplate").val()
		showurl = buildurl(websiteurl,template,slug,"tags")
	return showurl



