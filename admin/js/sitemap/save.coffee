websiteurl = $(".websiteUrl").val()

removeslash = (text) ->
	if text.substr(text.length-1) is "/"
		text=text.slice(0,-1)
		if text.substr(text.length-1) is "/"
			text = removeslash(text)
	text

buildurl = (websiteurl,template,value,key) ->
	pattern = $("#otherlinkpattern").val()
	showurl="#{websiteurl}#{pattern}"
	showurl=showurl.replace("{template}",template)
	showurl=showurl.replace("{key}",key)
	showurl=showurl.replace("{value}",value)
	if key is "" and value is ""
		showurl=showurl.replace("?","")
		showurl=showurl.replace("=","")
	showurl=showurl.replace("{otherparams}","")
	templatetype = $("#rewritepattern").val().split(',')
	for typedel in templatetype
		regex = new RegExp(typedel, 'g')
		showurl=showurl.replace(regex,"")
	showurl=removeslash(showurl)
	showurl
$ ->
	window.saveSitemap = (menunavi,menucontent,category,html,post,gallery,tag) ->
		savedata = {}
		savemenunavi=[]
		savemenucontent = []
		savecategory = []
		savehtml = []
		savepost = []
		savegallery = []
		savetag = []
		savecustom = []
		allmenunavi=[]
		allmenucontent = []
		allcategory = false
		allhtml = false
		allpost = []
		allgallery = false
		alltag = false
# ++++++++++++++++++++ MENU NAVI ++++++++++++++++++++
		for qmenu in menunavi
			if qmenu.ALL
				allmenunavi.push(qmenu._id)
			for qmenuitem in qmenu.MENU
				if qmenuitem.USE is 1
					insertmenunavi = {}
					insertmenunavi['CONTENTID'] = qmenuitem._id
					insertmenunavi['LASTMODIFY'] = qmenuitem.LASTMODIFY
					insertmenunavi['PRIORITY'] = qmenuitem.PRIORITY
					insertmenunavi['URL'] = qmenuitem.URL
					savemenunavi.push(insertmenunavi)
# ++++++++++++++++++++ END MENU NAVI ++++++++++++++++++++
# ++++++++++++++++++++ MENU CONTENT ++++++++++++++++++++
		for qmenu in menucontent
			if qmenu.ALL
				allmenucontent.push(qmenu._id)
			for qmenuitem in qmenu.MENU
				if qmenuitem.USE is 1
					insertmenucontent = {}
					insertmenucontent['CONTENTID'] = qmenuitem._id
					insertmenucontent['LASTMODIFY'] = qmenuitem.LASTMODIFY
					insertmenucontent['PRIORITY'] = qmenuitem.PRIORITY
					insertmenucontent['URL'] = qmenuitem.URL
					insertmenucontent['TEMPLATE'] = qmenuitem.FILETEMPLATE if qmenuitem.FILETEMPLATE?
					savemenucontent.push(insertmenucontent)
# ++++++++++++++++++++ END MENU CONTENT ++++++++++++++++++++
# ++++++++++++++++++++ CATEGORY ++++++++++++++++++++
		if $(".allcategory:checked").val()?
				allcategory = true
			else
				allcategory = false
		for qcategory in category
			if qcategory.USE is 1
				insertcategory = {}
				insertcategory['CONTENTID'] = qcategory._id
				insertcategory['LASTMODIFY'] = qcategory.LASTMODIFY
				insertcategory['PRIORITY'] = qcategory.PRIORITY
				insertcategory['TEMPLATE'] = qcategory.FILETEMPLATE if qcategory.FILETEMPLATE?
				insertcategory['URL'] = qcategory.URL
				savecategory.push(insertcategory)
# ++++++++++++++++++++ END CATEGORY ++++++++++++++++++++
# ++++++++++++++++++++ HTML ++++++++++++++++++++
		if $(".allhtml:checked").val()?
			allhtml = true
		else
			allhtml = false
		for qhtml in html
			if qhtml.USE is 1
				inserthtml = {}
				inserthtml['CONTENTID'] = qhtml._id
				inserthtml['LASTMODIFY'] = qhtml.LASTMODIFY
				inserthtml['PRIORITY'] = qhtml.PRIORITY
				inserthtml['TEMPLATE'] = qhtml.FILETEMPLATE if qhtml.FILETEMPLATE?
				inserthtml['URL'] = qhtml.URL
				savehtml.push(inserthtml)
# ++++++++++++++++++++ END HTML ++++++++++++++++++++
# ++++++++++++++++++++ POST ++++++++++++++++++++
		for qcate in post
			if qcate.ALL
				allpost.push(qcate.CATEGORYID)
			for qpost in qcate.POST
				if qpost.USE is 1
					insertpost = {}
					insertpost['CONTENTID'] = qpost._id
					insertpost['LASTMODIFY'] = qpost.LASTMODIFY
					insertpost['PRIORITY'] = qpost.PRIORITY
					insertpost['URL'] = qpost.URL
					savepost.push(insertpost)
# ++++++++++++++++++++ END POST ++++++++++++++++++++
# ++++++++++++++++++++ GALLERY ++++++++++++++++++++
		if $(".allgallery:checked").val()?
			allgallery = true
		else
			allgallery = false
		for qgallery in gallery
			if qgallery.USE is 1
				insertgallery = {}
				insertgallery['CONTENTID'] = qgallery._id
				insertgallery['LASTMODIFY'] = qgallery.LASTMODIFY
				insertgallery['PRIORITY'] = qgallery.PRIORITY
				insertgallery['TEMPLATE'] = qgallery.FILETEMPLATE if qgallery.FILETEMPLATE?
				insertgallery['URL'] = qgallery.URL
				savegallery.push(insertgallery)
		if savegallery.length > 0
			insertmaingallery = {}
			insertmaingallery['NAME'] = "gallerymaintemplate"
			insertmaingallery['LASTMODIFY'] = dateFormat(new Date(),"yyyy-MM-dd")
			insertmaingallery['PRIORITY'] = $(".gallerymaintemplate-priority").val()
			insertmaingallery['TEMPLATE'] = $(".gallerymaintemplate").val()
			template = $(".gallerymaintemplate").val()
			insertmaingallery['URL'] = buildurl(websiteurl,template,"","")
			savecustom.push(insertmaingallery)
# ++++++++++++++++++++ END GALLERY ++++++++++++++++++++
# ++++++++++++++++++++ TAG ++++++++++++++++++++
		if $(".alltag:checked").val()?
			alltag = true
		else
			alltag = false
		for qtag in tag
			if qtag.USE is 1
				inserttag = {}
				inserttag['CONTENTID'] = qtag._id
				inserttag['LASTMODIFY'] = qtag.LASTMODIFY
				inserttag['PRIORITY'] = qtag.PRIORITY
				inserttag['TEMPLATE'] = qtag.FILETEMPLATE if qtag.FILETEMPLATE?
				inserttag['URL'] = qtag.URL
				savetag.push(inserttag)
# ++++++++++++++++++++ END TAG ++++++++++++++++++++
		$("#changefreq option:selected").each ->
			savedata['CHANGEFREQ'] = $(@).val()
		savedata['MENUNAVI']=savemenunavi
		savedata['MENUCONTENT']=savemenucontent
		savedata['CATEGORY']=savecategory
		savedata['HTML']=savehtml
		savedata['POST']=savepost
		savedata['gallery']=savegallery
		savedata['TAG']=savetag
		savedata['CUSTOM']=savecustom
		savedata['ALLMENUNAVI']=allmenunavi
		savedata['ALLMENUCONTENT']=allmenucontent
		savedata['ALLCATEGORY']=allcategory
		savedata['ALLHTML']=allhtml
		savedata['ALLPOST']=allpost
		savedata['ALLGALLERY']=allgallery
		savedata['ALLTAG']=alltag
		params=
			indertdata : JSON.stringify(savedata)
		jqxhr = $.ajax(
			url: "http://#{window.location.host}#{seturlbase()}/sitemapsave.cfm",
			dataType: 'json',
			type: "POST",
			timeout:99999,
			data: params
		).done(->
			showtext = '<div class="alert alert-success">generate success</div>';
			$('#showgenflash').html(showtext);
			$('#saveprogress').addClass('hidden');
			$('.button-action').removeClass('hidden');
		).fail(->
			showtext = '<div class="alert alert-error">generate fail</div>';
			$('#showgenflash').html(showtext);
			$('#saveprogress').addClass('hidden');
			$('.button-action').removeClass('hidden');
		).always(->
			showtext = '<div class="alert alert-success">generate success</div>';
			$('#showgenflash').html(showtext);
			$('#saveprogress').addClass('hidden');
			$('.button-action').removeClass('hidden');
		)

