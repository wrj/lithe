websiteurl = $(".websiteUrl").val()
window.updateMenunavi =(menuid,menuDataTable,menudata,type) ->
	newData=[]
	newMenu = []
	menuCheckall = $(".allmenunavi:checked").val()
	menuDataTable.$('input[type=checkbox]').each ->
		if @.checked
			newd = {}
			newd['_id'] = $(@).val()
			$(@).parent().parent().find(".priority option:selected").each ->
				newd['PRIORITY'] = $(@).val()
			urltype = $(@).parent().parent().find(".urltype").val()
			showurl = window.urlDom @,urltype
			newd['URL'] = showurl
			newd['FILETEMPLATE'] = $(@).parent().parent().find(".template").val()
			newd['LASTMODIFY'] = $(@).parent().parent().find(".lastmodify").val()
			newData.push newd
	newMenu = someMenunavi menuid,newData,menudata,type
	return newMenu

window.mapvalueMenunavi = (type,checkall,menuupdate,menudata) ->
	menunaviArr=[]
	for i in menudata
		newMenu = []
		for j in i['MENU']
			if menuupdate.length > 0
				noaddItem = true
				for k in menuupdate
					if j._id is k.CONTENTID
						newMenuItem = {}
						newMenuItem=j
						newMenuItem['LASTMODIFY'] = dateFormat(j['UPDATEDAT'],"yyyy-MM-dd")
						newMenuItem['PRIORITY'] = k.PRIORITY
						template=k.TEMPLATE if k.TEMPLATE || ""
						if type is "MENUNAVI"
							newMenuItem['URL'] = window.urlData("MENUNAVI",j.LINKURL,template,"")
						else if type is "MENUCONTENT"
							newMenuItem['URL'] = window.urlData(j.TYPE,j.NAME,template,"")
						newMenuItem['FILETEMPLATE']= template
						newMenuItem['USE'] = 1
						newMenu.push(newMenuItem)
						noaddItem = false
				if noaddItem
					newMenu.push j
			else
				newMenu.push j
		menunavi = {}
		menunavi['_id'] = i._id
		menunavi['NAME'] = i.NAME
		menunavi['UPDATEDAT'] = i.UPDATEDAT
		menunavi['MENU'] = newMenu
		for qcheckall in checkall
			if i._id is qcheckall
				menunavi['ALL'] = true
		menunaviArr.push(menunavi)
	return menunaviArr

window.reloadMenucontent = (menudata) ->
	menunaviArr=[]
	for i in menudata
		newMenu = []
		for j in i['MENU']
			newMenuItem = {}
			newMenuItem=j
			newMenuItem['LASTMODIFY'] = j.LASTMODIFY
			newMenuItem['PRIORITY'] = j.PRIORITY
			template=j.FILETEMPLATE if j.FILETEMPLATE || ""
			newMenuItem['URL'] = window.urlData(j.TYPE,j.NAME,template,"")
			newMenuItem['FILETEMPLATE']= template
			newMenuItem['USE'] = j.USE
			newMenu.push(newMenuItem)
			noaddItem = false
		menunavi = {}
		menunavi['_id'] = i._id
		menunavi['NAME'] = i.NAME
		menunavi['UPDATEDAT'] = i.UPDATEDAT
		menunavi['MENU'] = newMenu
		menunavi['ALL'] = i.ALL
		menunaviArr.push(menunavi)
	return menunaviArr
someMenunavi = (menuid,newData,menudata,type) ->
	newMenu = []
	for i in menudata
		if i._id is menuid
			newMenuItem = []
			for j in i['MENU']
				noaddItem = true
				if newData.length > 0
					for k in newData
						if j._id is k._id
							Item = j
							Item['LASTMODIFY'] = k.LASTMODIFY
							Item['PRIORITY']= k.PRIORITY
							Item['URL'] = k.URL
							Item['FILETEMPLATE']= k.FILETEMPLATE
							Item['USE']=1
							newMenuItem.push Item
							noaddItem = false
					if noaddItem
						j['USE'] = 0
						newMenuItem.push j
				else
					j['USE'] = 0
					newMenuItem.push j
			newHeadMenu={}
			newHeadMenu['_id'] = i._id
			newHeadMenu['UPDATEDAT'] = i.UPDATEDAT
			newHeadMenu['NAME'] = i.NAME
			newHeadMenu['MENU'] = newMenuItem
			if type is "menunavi"
				if $(".allmenunavi:checked").val()?
					newHeadMenu['ALL'] = true
				else
					newHeadMenu['ALL'] = false
			else if type is "menucontent"
				if $(".allmenucontent:checked").val()?
					newHeadMenu['ALL'] = true
				else
					newHeadMenu['ALL'] = false
			newMenu.push newHeadMenu
		else
			newMenu.push i
	return newMenu

