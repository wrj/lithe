function dologout()
{
	window.location = seturlbase()+"/logout.cfm";
}

function replacetext(replacetext)
{
    var oldtext = $.trim(replacetext);
    var newtext = oldtext.replace(/[^a-zA-Z0-9ก-๙\\-]/g,'-');
    newtext = newtext.replace(/(-)+/g, "-");
    newtext = newtext.replace(/(-)*$/g, "");
    return newtext;
}