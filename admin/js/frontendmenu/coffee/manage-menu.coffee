$formUpDown = (updown, hideshow) ->
    $(updown).click ->
        $(hideshow).slideToggle()
        if $(this).is(".icon-chevron-down")
            $(this).addClass "icon-chevron-up"
            $(this).removeClass "icon-chevron-down"

        else
            $(this).addClass "icon-chevron-down"
            $(this).removeClass "icon-chevron-up"

$ ->

    $('.sortable').nestedSortable
        handle: 'div',
        items: 'li',
        toleranceElement: '> div',
        tabSize:40,
        placeholder: 'placeholder',
        disableNesting: 'no-nest',
        forcePlaceholderSize: true,
        helper: 'clone',
        revert: 250,
        tolerance: 'pointer',
        opacity: .6


    $formUpDown "#main-icon-up-down .form-up-down", "#main-icon-up-down .main-form-hide-show"


    $('.form-up-down').live 'click',(e) ->
        val = $(this).find("#countSub").val()
        $(this).parent().next().slideToggle()
        if $(this).is(".icon-chevron-down")
            $(this).addClass "icon-chevron-up"
            $(this).removeClass "icon-chevron-down"
        else
            $(this).addClass "icon-chevron-down"
            $(this).removeClass "icon-chevron-up"

    $(".submenuName").live 'keyup',(e) ->
        $(this).parent().parent().parent().parent().parent().parent().parent().prev().find(".name-text").text($(this).val())

    $("#MainName").live 'keyup',(e) ->
        $(this).parent().parent().parent().prev().find(".head-name").text("Menu : "+$(this).val()+" ")




