angular.module('mongoResource', ['ngResource']).
	factory('$mongoResource', ['$resource', function ($resource) {
		function MmongoResourceFactory(collectionName) {
			var resource = $resource('http://localhost:port/angularservice/service.cfc',
				{
					port: ":8181",
					apiKey: 'CD9B744C619529C4988E0E94344EAF12'
				}, {
					query: {method: 'GET', params: {method: 'query', collection: collectionName}, isArray: true},
					get: {method: 'GET', params: {method: 'get', collection: collectionName}},
					update: { method: 'POST', params: {method: 'update', collection: collectionName} },
					save: {method: 'POST', params: {method: 'save', collection: collectionName}},
					remove: {method: 'GET', params: {method: 'delete', collection: collectionName}}
				}
			);
			resource.getById = function (id, cb, errorcb) {
				return resource.get({id: id}, cb, errorcb);
			};

			resource.count = function (params, cb, errorcb) {
				params = angular.extend({}, params, {method: 'count'});
				return resource.get(params, cb, errorcb);
			};

			resource.prototype.update = function (cb, errorcb) {
				return resource.update({id: this._id.$oid},
					angular.extend({}, this, {_id: undefined}), cb, errorcb);
			};

			resource.prototype.destroy = function (cb, errorcb) {
				return resource.remove({id: this._id.$oid}, cb, errorcb);
			};


			return resource;

		}

		return MmongoResourceFactory;
	}]);