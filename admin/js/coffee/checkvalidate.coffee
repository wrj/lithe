$ ->
	capitaliseFirstLetter = (string)->
		string.charAt(0).toUpperCase() + string.slice(1);

	isValidEmailAddress = (v) ->
		r = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
		(v.match(r) == null) ? false : true;

	$( "form" ).submit (event) ->
		window.validateform(@)

	window.validateform = ($this)->
		emptytitle=[]
		isError=false
		validateclass = "fieldempty"
		$(".requirefield").each ->
			isempty = $(@).val();
			if isempty is ""
				isError=true
				emptytitle.push(capitaliseFirstLetter($(@).attr("name")))
				$(@).addClass(validateclass)
			else
				$(@).removeClass(validateclass)
		$(".requirefieldselect").each ->
			isrequirefieldselect = ""
			$(@).find("option:selected").each ->
				isrequirefieldselect = $(@).val()
			if isrequirefieldselect is ""
				isError=true
				emptytitle.push(capitaliseFirstLetter($(@).attr("name")))
				$(@).addClass(validateclass)
			else
				$(@).removeClass(validateclass)
		between5_15=[]
		$(".between5-15").each ->
			isbetween5_15 = $(@).val();
			if isbetween5_15.length < 5 or isbetween5_15.length > 15
				isError=true
				between5_15.push(capitaliseFirstLetter($(@).attr("name")))
				$(@).addClass(validateclass)
			else
				$(@).removeClass(validateclass)
		between5_40=[]
		$(".between5-40").each ->
			isbetween5_40 = $(@).val();
			if isbetween5_40.length < 5 or isbetween5_15.length > 40
				isError=true
				between5_40.push(capitaliseFirstLetter($(@).attr("name")))
				$(@).addClass(validateclass)
			else
				$(@).removeClass(validateclass)
		emailformat=[]
		$(".emailformat").each ->
			isemailformat = $(@).val();
			if isValidEmailAddress(isemailformat)
				isError=true
				emailformat.push(capitaliseFirstLetter($(@).attr("name")))
				$(@).addClass(validateclass)
			else
				$(@).removeClass(validateclass)
		if isError
			errormessage = ""
			for i in emptytitle
				errormessage="""#{errormessage}<li>#{i} value must not blank.</li>"""
			for j in between5_15
				errormessage="""#{errormessage}<li>#{j} value is between 5 - 15 charecter.</li>"""
			for k in between5_40
				errormessage="""#{errormessage}<li>#{k} value is between 5 - 40 charecter.</li>"""
			for l in emailformat
				errormessage="""#{errormessage}<li>#{l} value is not email format.</li>"""
			errormessage="""<ul>#{errormessage}</ul>"""
			title = $(".titlepage").text()
			tmpl =  """
			        <div class="modal hide fade" data-backdrop="static" tabindex="-1">
			        <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			        <h3>Error : #{title}</h3>
			        </div>
			        <div class="modal-body">
			            <div class="alert alert-error">
			                #{errormessage}
			            </div>
			        </div>
			        <div class="modal-footer">
			        <a href="#" data-dismiss="modal" class="btn">Close</a>
			        </div>
			        </div>
			        """
			$(tmpl).modal()
			false
		else
			true





