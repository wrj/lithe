var totalrow = 0;
$(function(){
	inittable();
});

function inittable(){
	var i = 1;
	for (i;i<=3;i++)
	{
		createrowtable();
	}
}
function createrowtable(){
	totalrow++;
	var tr = '<tr id=\'ti'+totalrow+'\' class=\'tbl_quotation_task_items\'>';
		tr+='<td class=\'tbl_quotation_task_tools\'><a href=\'javascript:createrowtable();\' class=\'a_task_items_add\' title=\'Add line\'><img src=\'../../images/add.png\' border=0></a> <a href=\'\' title=\'move\' class=\'a_move\'><img src=\'../../images/dragdrop.png\' border=0></a></td>';
		tr+='<td class=\'tbl_detail\'><input class=\'tbl_combobox_id\' name=\'ti_taskid_'+ totalrow +'\' id=\'ti_taskid_'+ totalrow +'\'><img class=\'tbl_combobox_id_img\' src=\'../../images/combo_select.gif\'><input type=\'hidden\' id=\'ti_taskid_productid_'+ totalrow +'\'/></td>';
		tr+='<td class=\'tbl_detail\'><input type=\'text\' class=\'tbl_quotation_input_detail\'></td>';
		tr+='<td class=\'tbl_detail\'><input class=\'tbl_input_price\' name=\'ti_price_'+ totalrow +'\' id=\'ti_price_'+ totalrow +'\'></td>';
		tr+='<td class=\'tbl_detail\'><input class=\'tbl_input_qty\' name=\'ti_qty_'+ totalrow +'\' id=\'ti_qty_'+ totalrow +'\'></td>';
		tr+='<td class=\'tbl_detail\'><input class=\'tbl_combobox_tax\' name=\'ti_tax1_'+ totalrow +'\' id=\'ti_tax1_'+ totalrow +'\'><img class=\'tbl_combobox_tax_img\' src=\'../../images/combo_select.gif\'><input type=\'hidden\' id=\'ti_tax1_tax_'+ totalrow +'\'/></td>';
		tr+='<td class=\'tbl_detail\'><input class=\'tbl_combobox_tax\' name=\'ti_tax2_'+ totalrow +'\' id=\'ti_tax2_'+ totalrow +'\'><img class=\'tbl_combobox_tax_img\' src=\'../../images/combo_select.gif\'><input type=\'hidden\' id=\'ti_tax2_tax_'+ totalrow +'\'/></td class=\'tbl_detail\'>';
		tr+='<td class=\'tbl_linetotal tbl_detail\'>0.00</td>';
		tr+='<td class=\'tbl_quotation_task_tools\'><a href=\'javascript:{}\' title=\'Delete line\' class=\'a_task_items_remove\'><img src=\'../../images/delete.png\' border=0></a></td>';
		tr+='</tr>';
		$('#tbl_quotation_item').append(tr);
		deleteRow();
		sortRow();
		setautocomplete(totalrow);
}
function setautocomplete(rowid)
{
	$("#ti_taskid_"+ rowid +"").autocomplete({
		minLength: 0,
		source: products,
		focus: function( event, ui ) {
			$( "#ti_taskid_"+ rowid +"" ).val( ui.item.label );
			return false;
		},
		select: function( event, ui ) {
			$( "#ti_taskid_"+ rowid +"" ).val( ui.item.label );
			$( "#ti_taskid_productid_"+ rowid +"" ).val( ui.item.value );
			$( "#ti_timeentrynotes_productname_"+ rowid +"" ).html( ui.item.name );
			$( "#ti_timeentrynotes_productdetail_"+ rowid +"" ).html( ui.item.detail );
			$( "#ti_rate_productprice_"+ rowid +"" ).html( ui.item.price );
			return false;
		}
	}).data("autocomplete")._renderItem = function(ul, item) {
		return $("<li></li>")
		.data("item.autocomplete", item)
		.append("<a>" + item.label + "<br>" + item.desc + "</a>")
		.appendTo(ul);
	};
}
/*

$(document).ready(function(){
	$(function() {
		$( ".quotation_btn_submit, .quotation_btn_draft, .quotation_btn_new" ).click(function() { return false; });
	});
	$.ajax({
		url: 'productlist',
		type: 'get',
		dataType: 'JSON',
		success:function(json){
			var products = json.items;
			
			deleteGroup();
			
			deleteRow();
			
			sortRow();
			
			$('.a_task_items_add').live("click",function(){
				var prefixt = $('tr.tbl_quotation_task_items').size()+1;
				var tr = $("<tr id='ti"+ prefixt +"' class='tbl_quotation_task_items'>
				<td class='tbl_quotation_task_tools'></td>
				<td class='tbl_detail'><input class='tbl_combobox_id' name='ti_taskid_"+ prefixt +"' id='ti_taskid_"+ prefixt +"'><img class='tbl_combobox_id_img' src='../../images/combo_select.gif'><input type='hidden' id='ti_taskid_productid_"+ prefixt +"'/></td>
				<td class='tbl_detail'><input type='text' class='tbl_quotation_input_detail'></td>
				<td class='tbl_detail'><input class='tbl_input_price' name='ti_price_"+ prefixt +"' id='ti_price_"+ prefixt +"'></td>
				<td class='tbl_detail'><input class='tbl_input_qty' name='ti_qty_"+ prefixt +"' id='ti_qty_"+ prefixt +"'></td>
				<td class='tbl_detail'><input class='tbl_combobox_tax' name='ti_tax1_"+ prefixt +"' id='ti_tax1_"+ prefixt +"'><img class='tbl_combobox_tax_img' src='../../images/combo_select.gif'><input type='hidden' id='ti_tax1_tax_"+ prefixt +"'/></td>
				<td class='tbl_detail'><input class='tbl_combobox_tax' name='ti_tax2_"+ prefixt +"' id='ti_tax2_"+ prefixt +"'><img class='tbl_combobox_tax_img' src='../../images/combo_select.gif'><input type='hidden' id='ti_tax2_tax_"+ prefixt +"'/></td class='tbl_detail'>
				<td class='tbl_linetotal tbl_detail'>0.00</td>
				<td class='tbl_quotation_task_items_delete'><a href='javascript:{}' title='Delete line' class='a_task_items_remove'>&nbsp;&nbsp;&nbsp;&nbsp;</a></td>
				</tr>");
				$('#tbl_quotation_task').append(tr);
				$("tr.tbl_quotation_task_items").each(function(t){
					var a = t+1;
					$("#ti"+ a +"").mouseenter(function(){
						$("#ti"+ a +" .tbl_quotation_task_items_delete").html("<a href='javascript:{}' title='Delete line' class='a_task_items_remove'><img src='../../images/delete.png' border=0></a>");
						$("#ti"+ a +" .tbl_quotation_task_tools").html("<a href='javascript:{}' class='a_task_items_add' title='Add line'><img src='../../images/add.png' border=0></a> <a href='' title='move' class='a_move'><img src='../../images/dragdrop.png' border=0></a>");
					});
					$("#ti"+ a +"").mouseleave(function(){
						$("#ti"+ a +" .tbl_quotation_task_items_delete").html("");
						$("#ti"+ a +" .tbl_quotation_task_tools").html("");
					});
					$(function(){
						$("#ti_taskid_"+ a +"").autocomplete({
							minLength: 0,
							source: products,
							focus: function( event, ui ) {
								$( "#ti_taskid_"+ a +"" ).val( ui.item.label );
								return false;
							},
							select: function( event, ui ) {
								$( "#ti_taskid_"+ a +"" ).val( ui.item.label );
								$( "#ti_taskid_productid_"+ a +"" ).val( ui.item.value );
								$( "#ti_timeentrynotes_productname_"+ a +"" ).html( ui.item.name );
								$( "#ti_timeentrynotes_productdetail_"+ a +"" ).html( ui.item.detail );
								$( "#ti_rate_productprice_"+ a +"" ).html( ui.item.price );
								return false;
							}
						}).data("autocomplete")._renderItem = function(ul, item) {
							return $("<li></li>")
							.data("item.autocomplete", item)
							.append("<a>" + item.label + "<br>" + item.desc + "</a>")
							.appendTo(ul);
						};
					});
				});


				$('.a_item_items_add').live("click",function(){
					var prefixi = $('tr.tbl_quotation_item_items').size()+1;
					var tr = $("<tr id='ii"+ prefixi +"' class='tbl_quotation_item_items'><td class='tbl_quotation_item_tools'></td><td class='tbl_detail'><input class='tbl_combobox_id' name='ii_itemid_"+ prefixi +"' id='ii_itemid_"+ prefixi +"'><img class='tbl_combobox_id_img' src='../../images/combo_select.gif'><input type='hidden' id='ii_itemid_productid_"+ prefixi +"'/></td><td class='tbl_detail'><input type='text' class='tbl_quotation_input_detail'><!-----<span id='ii_description_productname_"+ prefixi +"'><pan><br><span id='ii_description_productdetail_"+ prefixi +"'><span>------></td><td class='tbl_detail'><input class='tbl_input_price' name='ii_price_"+ prefixi +"' id='ii_price_"+ prefixi +"'></td><td class='tbl_detail'><input class='tbl_input_qty' name='ii_qty_"+ prefixi +"' id='ii_qty_"+ prefixi +"'></td><td class='tbl_detail'><input class='tbl_combobox_tax' name='ii_tax1_"+ prefixi +"' id='ii_tax1_"+ prefixi +"'><img class='tbl_combobox_tax_img' src='../../images/combo_select.gif'><input type='hidden' id='ii_tax1_tax_"+ prefixi +"'/></td><td class='tbl_detail'><input class='tbl_combobox_tax' name='ii_tax2_"+ prefixi +"' id='ii_tax2_"+ prefixi +"'><img class='tbl_combobox_tax_img' src='../../images/combo_select.gif'><input type='hidden' id='ii_tax2_tax_"+ prefixi +"'/></td class='tbl_detail'><td class='tbl_linetotal tbl_detail'>0.00</td><td class='tbl_quotation_item_items_delete'><a href='javascript:{}' title='Delete line' class='a_item_items_remove'>&nbsp;&nbsp;&nbsp;&nbsp;</a></td></tr>");
					$('#tbl_quotation_item').append(tr);
					$("tr.tbl_quotation_item_items").each(function(i){
						var b = i+1;
						$("#ii"+ b +"").mouseenter(function(){
							$("#ii"+ b +" .tbl_quotation_item_items_delete").html("<a href='javascript:{}' title='Delete line' class='a_item_items_remove'><img src='../../images/delete.png' border=0></a>");
							$("#ii"+ b +" .tbl_quotation_item_tools").html("<a href='javascript:{}' class='a_item_items_add' title='Add line'><img src='../../images/add.png' border=0></a> <a href='' title='move' class='a_move'><img src='../../images/dragdrop.png' border=0></a>");
						});
						$("#ii"+ b +"").mouseleave(function(){
							$("#ii"+ b +" .tbl_quotation_item_items_delete").html("");
							$("#ii"+ b +" .tbl_quotation_item_tools").html("");
						});
						$( "#ii_itemid_"+ b +"" ).autocomplete({
							minLength: 0,
							source: products,
							focus: function( event, ui ) {
								$( "#ii_itemid_"+ b +"" ).val( ui.item.label );
								return false;
							},
							select: function( event, ui ) {
								$( "#ii_itemid_"+ b +"" ).val( ui.item.label );
								$( "#ii_itemid_productid_"+ b +"" ).val( ui.item.value );
								$( "#ii_description_productname_"+ b +"" ).html( ui.item.name );
								$( "#ii_description_productdetail_"+ b +"" ).html( ui.item.detail );
								$( "#ii_unitcost_productprice_"+ b +"" ).html( ui.item.price );
								return false;
							}
						}).data("autocomplete")._renderItem = function(ul, item) {
							return $("<li></li>")
							.data("item.autocomplete", item)
							.append("<a>" + item.label + "<br>" + item.desc + "</a>")
							.appendTo(ul);
						};
					});
				});
			
			});


			$("tr.tbl_quotation_task_items").each(function(t){
				var a = t+1;
				$("#ti"+ a +"").mouseenter(function(){
					$("#ti"+ a +" .tbl_quotation_task_items_delete").html("<a href='javascript:{}' title='Delete line' class='a_task_items_remove'><img src='../../images/delete.png' border=0></a>");
					$("#ti"+ a +" .tbl_quotation_task_tools").html("<a href='javascript:{}' class='a_task_items_add' title='Add line'><img src='../../images/add.png' border=0></a> <a href='' title='move' class='a_move'><img src='../../images/dragdrop.png' border=0></a>");
				});
				$("#ti"+ a +"").mouseleave(function(){
					$("#ti"+ a +" .tbl_quotation_task_items_delete").html("");
					$("#ti"+ a +" .tbl_quotation_task_tools").html("");
				});
				$(function() {
					$("#ti_taskid_"+ a +"").autocomplete({
						minLength: 0,
						source: products,
						focus: function( event, ui ) {
							$( "#ti_taskid_"+ a +"" ).val( ui.item.label );
							return false;
						},
						select: function( event, ui ) {
							$( "#ti_taskid_"+ a +"" ).val( ui.item.label );
							$( "#ti_taskid_productid_"+ a +"" ).val( ui.item.value );
							$( "#ti_timeentrynotes_productname_"+ a +"" ).html( ui.item.name );
							$( "#ti_timeentrynotes_productdetail_"+ a +"" ).html( ui.item.detail );
							$( "#ti_rate_productprice_"+ a +"" ).html( ui.item.price );
							return false;
						}
					}).data("autocomplete")._renderItem = function(ul, item) {
						return $("<li></li>")
						.data("item.autocomplete", item)
						.append("<a>" + item.label + "<br>" + item.desc + "</a>")
						.appendTo(ul);
					};
				});
			});


			$("tr.tbl_quotation_item_items").each(function(i){
				var b = i+1;
				$("#ii"+ b +"").mouseenter(function(){
					$("#ii"+ b +" .tbl_quotation_item_items_delete").html("<a href='javascript:{}' title='Delete line' class='a_item_items_remove'><img src='../../images/delete.png' border=0></a>");
					$("#ii"+ b +" .tbl_quotation_item_tools").html("<a href='javascript:{}' class='a_item_items_add' title='Add line'><img src='../../images/add.png' border=0></a> <a href='' title='move' class='a_move'><img src='../../images/dragdrop.png' border=0></a>");
				});
				$("#ii"+ b +"").mouseleave(function(){
					$("#ii"+ b +" .tbl_quotation_item_items_delete").html("");
					$("#ii"+ b +" .tbl_quotation_item_tools").html("");
				});
				$(function() {
					$("#ii_itemid_"+ b +"").autocomplete({
						minLength: 0,
						source: products,
						focus: function( event, ui ) {
							$( "#ii_itemid_"+ b +"" ).val( ui.item.label );
							return false;
						},
						select: function( event, ui ) {
							$( "#ii_itemid_"+ b +"" ).val( ui.item.label );
							$( "#ii_itemid_productid_"+ b +"" ).val( ui.item.value );
							$( "#ii_description_productname_"+ b +"" ).html( ui.item.name );
							$( "#ii_description_productdetail_"+ b +"" ).html( ui.item.detail );
							$( "#ii_unitcost_productprice_"+ b +"" ).html( ui.item.price );
							return false;
						}
					}).data("autocomplete")._renderItem = function(ul, item) {
						return $("<li></li>")
						.data("item.autocomplete", item)
						.append("<a>" + item.label + "<br>" + item.desc + "</a>")
						.appendTo(ul);
					};
				});
			});
		}
	});
});
*/

function deleteGroup(){
	//set หัวตาราง task ให้ลบเป็น group ได้
	$(".tbl_quotation_task_head").mouseenter(function(){
		$(".tbl_quotation_task_head_delete").html("<a href='javascript:{}' title='Delete section' class='a_task_remove'><img src='../../images/delete.png' border=0></a>");
	});
	$(".tbl_quotation_task_head").mouseleave(function(){
		$(".tbl_quotation_task_head_delete").html("");
	});
	
	//set หัวตาราง item ให้ลบเป็น group ได้
	$(".tbl_quotation_item_head").mouseenter(function(){
		$(".tbl_quotation_item_head_delete").html("<a href='javascript:{}' title='Delete section' class='a_item_remove'><img src='../../images/delete.png' border=0></a>");
	});
	$(".tbl_quotation_item_head").mouseleave(function(){
		$(".tbl_quotation_item_head_delete").html("");
	});
}

function deleteRow(){
	$('.a_task_items_remove').live("click",function(){
		$(this).parent().parent().remove();
	});
	
	$('.a_item_items_remove').live("click",function(){
		$(this).parent().parent().remove();
	});
	
	$('.a_task_remove').live("click",function(){
		$(this).parent().parent().parent().remove();
	});
	
	$('.a_item_remove').live("click",function(){
		$(this).parent().parent().parent().remove();
	});
}

function sortRow(){
	$("table#tbl_quotation_task tbody").sortable();
	$("table#tbl_quotation_item tbody").sortable();
}