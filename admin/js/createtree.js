var tree = '';
var oTree = '';
function createtree(targetid) {
    oTree = $("#"+targetid).dynatree({
        initAjax: {
            url: seturlbase()+"/treeprocess.cfm"
        },
        onPostInit: function(isReloading, isError) {
            if(categoryurl == ''){
                this.reactivate();
            }else{
                this.selectKey(categoryurl);
	            this.activateKey("");
                this.activateKey(categoryurl);
            }
        },
        onActivate: function(node){
            reportkey(node.data.key);
        },
        debugLevel:0
    });
}
function createcollectionstree(targetid,collection) {
	oTree = $("#"+targetid).dynatree({
		initAjax: {
			url: seturlbase()+"/treecollectionprocess.cfm?collectionname="+collection
		},
		onPostInit: function(isReloading, isError) {
			if(categoryurl == ''){
				this.reactivate();
			}else{
				this.selectKey(categoryurl);
				this.activateKey("");
				this.activateKey(categoryurl);
			}
		},
		onActivate: function(node){
			reportkey(node.data.key);
		},
		debugLevel:0
	});
}
