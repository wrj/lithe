<cfimport taglib="lib" prefix="ui">
<cfset valuelinkname = ""/>
<cfset valuelinkurl = ""/>
<cfset valuetypelink = "1"/>
<cfif isDefined('linkname')>
    <cfset valuelinkname = linkname>
</cfif>
<cfif isDefined('linkurl')>
    <cfset valuelinkurl = linkurl>
</cfif>
<cfif isDefined('typelink')>
    <cfset valuetypelink = typelink>
</cfif>
<cfoutput>
    <div class="row-fluid">
        <label class="radio inline">
            <input type="radio" id="typelink1" class="radiocheck" name="typelink" value="1" <cfif valuetypelink eq "1">checked</cfif>> Post
        </label>
        <label class="radio inline">
            <input type="radio" id="typelink3" class="radiocheck" name="typelink" value="3" <cfif valuetypelink eq "3">checked</cfif>> Custom
        </label>
    </div>
    <div class="row-fluid formclass hidden">
        #startform(action='linkscontroller')#
            #textfield(name='title',class='span6',label='title',nodiv=true,value=valuelinkname)#
            #textfield(name='link',class='span6',label='link',nodiv=true,value=valuelinkurl)#
        #endform()#
    </div>
    <div class="row-fluid tableclass">
        <div class="span2" style="width: 175px; float: left;">
            #treecategoryoutput()#
        </div>
        <div class="span5" style="float: left; margin-left: 10px; width: 494px;">
            <ui:datatable field="id,Title" tool=false />
        </div>
    </div>
</cfoutput>
<script type="text/javascript">
    var visitfirst = true;
    var category = '';
    $(function(){
        createtree('tree');
        $('.radiocheck').live('click',function(e){
            changetype();
        });
        if ($('input:radio[name=typelink]:checked').val() == '3')
        {
            changetype();
        }
    })

    function reportkey(key){
        category = key;
        inittable();
    }

    function inittable()
    {
        if(visitfirst == true)
        {
            visitfirst = false;
        }else{
            clearall();
        }
        var linkvalue = '?category='+category+'&typecontent='+$('input:radio[name=typelink]:checked').val();
        createdatatablebrowse('usetable','linksbrowsegrid',linkvalue);
    }

    function clearall()
    {
        oTable.fnDestroy();
        gaiSelected = [];
    }

    <cfif isDefined('modeselect') && modeselect eq "multi">
        var modeselect = 'multi';
        <cfelse>
        var modeselect = 'single';
    </cfif>
    $('#usetable tbody tr').live('click', function () {
        var aData = oTable.fnGetData( this );
        var iId = aData[0];
        if (modeselect == 'single')
        {
            gaiSelected = [];
            if ( $(this).hasClass('row_selected') ) {
                $(this).removeClass('row_selected');
            }else{
                oTable.$('tr.row_selected').removeClass('row_selected');
                $(this).addClass('row_selected');
                gaiSelected.push(iId);
            }
        }else{
            if ( jQuery.inArray(iId, gaiSelected) == -1 )
            {
                gaiSelected[gaiSelected.length++] = iId;
            }
            else
            {
                gaiSelected = jQuery.grep(gaiSelected, function(value) {
                    return value != iId;
                } );
            }
            $(this).toggleClass('row_selected');
        }
    } );

    function selectfile() {
        if ($('input:radio[name=typelink]:checked').val() == '3')
        {
            arrayreturn = [''+$('#link').val()+'|'+$('#title').val()+'|'+$('input:radio[name=typelink]:checked').val()];
            parent.selectedcomplete(arrayreturn);
            closewindow();
        }else{
            if (gaiSelected != "") {
                parent.selectedcomplete(gaiSelected);
                closewindow();
            }else{
                alert("Please select link");
            }
        }
    }

    function closewindow()
    {
        window.parent.$("#bModal").modal('hide');
    }

    function changetype()
    {
        if ($('input:radio[name=typelink]:checked').val() == '3')
        {
            $('.formclass').removeClass('hidden');
            $('.tableclass').addClass('hidden');
        }else{
            $('.tableclass').removeClass('hidden');
            $('.formclass').addClass('hidden');
            inittable();
        }
    }
</script>