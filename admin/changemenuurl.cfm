<!---
  Created with IntelliJ IDEA.
  User: skoodeskill
  Date: 5/28/13 AD
  Time: 10:26 AM
  To change this template use File | Settings | File Templates.
--->
<!---<cfinclude template="system/loadplugins.cfm">--->
<!---
ChangeMenuUrl
1.categoryid
2.old link url
3.link url change
Example:
<cfset odata=ChangeMenuUrl("categoryid","index.cfm","new.cfm")>
--->

<cffunction name="ChangeMenuUrl" output="false" access="public" returntype="any">
	<cfargument name="categoryid" type="any" required="true">
	<cfargument name="oldlinkurl" type="any" required="true">
	<cfargument name="newlinkurl" type="any" required="true">
	<cfset pattern = createObject('java', 'java.util.regex.Pattern')/>
	<cfset searchtxt = pattern.compile('\(.*|'&arguments.categoryid,pattern.CASE_INSENSITIVE)/>
	<cfset finddata = MongoCollectionfind(application.applicationname,'frontendmenu',{'MENU':searchtxt})>
		<cfloop array="#finddata#" index="qmenu">
			<cfset oldmenu = qmenu.MENU>
			<cfset menuItem = deserializeJSON(oldmenu)>
			<cfset newmenu=trim(ReplaceMenu(menuItem.menu,arguments.categoryid,arguments.oldlinkurl,arguments.newlinkurl))>
<!---			SET STRING FOR JSON FORMAT--->
			<cfset newmenuCut = Right(newmenu,Len(newmenu)-1)>
			<cfset newmenuitem = '{"menu":[#newmenuCut#]}'>
			<cfset newmenuitem = replaceNoCase(newmenuitem,'[,','[','all')>
			<cfset qmenu["MENU"]=newmenuitem>
			<cfset issave = SaveMenudata(qmenu)>
		</cfloop>
		<cfreturn "success">
</cffunction>


<cffunction name="ReplaceMenu" output="false" access="public" returntype="any">
	<cfargument name="menuItem" type="any" required="true">
	<cfargument name="categoryid" type="any" required="true">
	<cfargument name="oldlinkurl" type="any" required="true">
	<cfargument name="newlinkurl" type="any" required="true">
	<cfset output = arraynew()>
	<cfif ArrayLen(menuItem) GT 0>
		<cfset stringdata="">
		<cfset var m="">
		<cfoutput>
<!---		INPUT NEW URL IN JSON DATA  --->
			<cfsavecontent variable="stringdata"><cfloop array="#arguments.menuItem#" index="qmenuitem"><cfset children=[]><cfif isdefined("qmenuitem.children")><cfset children=qmenuitem.children></cfif><cfset menuitem=qmenuitem><cfset structDelete(menuitem,"children")><cfif menuitem.categoryid IS arguments.categoryid><cfset menuitem["linkurl"]=replaceNoCase(menuitem["linkurl"],"#arguments.oldlinkurl#","#arguments.newlinkurl#","all")></cfif>,{<cfset j=0><cfloop list='#StructKeyList(menuitem)#' index='i'><cfif j IS NOT 0>,</cfif>"#i#":"#menuitem[i]#"<cfset j++></cfloop><cfif arraylen("#children#") GT 0>,"children":[#ReplaceMenu(children,arguments.categoryid,arguments.oldlinkurl,arguments.newlinkurl)#]</cfif>}</cfloop></cfsavecontent>
		</cfoutput>
	</cfif>
	<cfreturn stringdata>
</cffunction>


<cffunction name="SaveMenudata" output="false" access="public" returntype="any">
	<cfargument name="menudata" type="any" required="true">
    <cfset id = arguments.menudata._id>
	<cfset structDelete(menudata,'_id')>
	<cfset result = mongovalidate('frontendmenu','update',arguments.menudata)>
	<cfif result['result'] eq true>
		<cfset var menuolddata = MongoCollectionfindone(application.applicationname,'frontendmenu',{'_id'=id})>
<!---		SAVE NEW MENU DATA  --->
		<cfset var menudataupdate = mongomapvalue('frontendmenu',menuolddata,result['value'],'default')>
		<cfset result['value']['MENU']= deserializeJSON(result['value']['MENU'])>
		<cfset result['value']['ID']=id.tostring()>
		<cfset jsondata = serializeJSON(result['value'])>
		<cfset urlpart = "menu">
		<cfset filepath="#expandPath("/#urlpart#")#">
		<cfif NOT DirectoryExists(filepath)>
			<cfdirectory action="create" mode="777"  directory="#filepath#">
		</cfif>
		<cfset name = result['value']['NAME']>
<!---		APPEND FILE MENU DATA  --->
		<cffile
			action = "write"
			file = "#filepath#/#name#.json"
			output = "#jsondata#"
			addNewLine = "yes"
			charset = "utf-8"
			fixnewline = "no"
			mode = "777">
		<cfif fileExists("#filepath#/#name#.json")>
			<cfset MongoCollectionsave(application.applicationname,'frontendmenu',menudataupdate)>
			<cfreturn "save success">
		<cfelse>
			<cfreturn "save error file no found">
		</cfif>
	<cfelse>
		<cfreturn "save error data false">
	</cfif>
</cffunction>