<cfinclude template="system/loadplugins.cfm">
<cfset totalrecord = 2>
<cfset limit = 3>
<cfset listran = getrandom(totalrecord,limit)>
<cfdump var="#listran#"/>


<cffunction name="getrandom" access="public" returntype="string" output="true">
	<cfargument name="totalrecord" type="numeric" required="true">
	<cfargument name="limit" type="numeric" required="true">
	<cfset var randomlist = "">
	<cfset var stopnum = 50>
	<cfif arguments['totalrecord'] gt stopnum>
		<cfset stopnum = (arguments['totalrecord'] * 5)>
	</cfif>
	<cfif arguments['limit'] gt arguments['totalrecord']>
		<cfset arguments['limit'] = arguments['totalrecord']>
	</cfif>
	<cfloop from="1" to="#stopnum#" index="i">
		<cfset var numrandom = RandRange(1,arguments['totalrecord'])>		
		<cfif listFindNoCase(randomlist, numrandom, ',') eq 0>
			<!--- not found number in list --->
			<cfset randomlist = listAppend(randomlist, numrandom, ',')>
			<cfif listlen(randomlist) eq arguments['limit']>
				<cfbreak/>
			</cfif>
		</cfif>
	</cfloop>
	<cfreturn randomlist>
</cffunction>