<cfinclude template="include/header.cfm">
<cfif isdefined('ERROR')>
    <cfset user = getstoredata()>
    <cfif structIsEmpty(user)>
        <cfset objdata = MongoCollectionfindone(application.applicationname,'user',{"_id"= newid(session['userid'])})>
        <cfset user = mongonew("user",objdata)>
    </cfif>
<cfelse>
    <cfset objdata = MongoCollectionfindone(application.applicationname,'user',{"_id"= newid(session['userid'])})>
    <cfset user = mongonew("user",objdata)>
</cfif>
<cfoutput>
    <div class="row-fluid">
    <legend>Profile #session['username']#</legend>
    #startform(action='profilecontroller')#
    #hiddenfield(name='secret',label='secret',value=user['SECRET'],readonly=true)#
    #textfield(name='username',label='username',value=user['username'],readonly=true)#
    #passwordfield(name='password',label='password')#
    #textfield(name='email',label='email',value=user['email'],readonly=true)#
    #textfield(name='firstname',label='firstname',value=user['firstname'])#
    #textfield(name='lastname',label='lastname',value=user['lastname'])#
    #hiddenfield(name='oldfilename',value=user['LOGO'])#
    #hiddenfield(name='newfilename')#
    <div class="control-group">
        <label class="control-label">Logo</label>
        <div class="controls">
            <ul class="thumbnails">
                <cfif user['logo'] neq ''>
                    <li class="span3">
                        <div class="thumbnail">
                            <a class="icon-zoom-in pull-right" href="javascript:previewimage('#user['logo']#')"></a>
                            <a href="javascript:previewimage('#user['logo']#')">
                            <img src="#get('pathuploadfolder')##session['username']#/image/#image['newfilename']#" class="img-thumb img-rounded"/>
                            </a>
                            <p>Current Logo</p>
                        </div>
                    </li>
                </cfif>
                <li class="span3" id="previewthumb"></li>
            </ul>
        </div>
    </div>
    #genUpload('#get('filesizeimage')#','uploadprocess','image',true)#
    #submit(label=get('labelbtnedit'))#
    #endform()#
    </div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<script type="text/javascript">
    function upfilecomplete(serverfile,clientfile)
    {
        $('#newfilename').val(serverfile);
        <cfoutput>
            var pathpreview = '<a class="thumbnail"><img src="#get('temppathfolder')#'+serverfile+'"/></a>';
        </cfoutput>
        $('#previewthumb').html(pathpreview);
    }
</script>