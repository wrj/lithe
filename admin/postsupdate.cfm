<cfinclude template="include/header.cfm">
<cfif isDefined('langinput') eq false>
    <cfset langinput = session['language']>
</cfif>
<cfif isdefined('ERROR')>
    <cfset post = getstoredata()>
	<cfset objdata = MongoCollectionfindone(application.applicationname,'post',{"_id"= newid(key)})>
    <cfif structIsEmpty(post)>
        <cfset post = mongonew("post",objdata)>
        <cfset post['category'] = objdata['CATEGORY'].getId().toString()>
        <cfset post['TAG'] = objdata['TAG']/>
    </cfif>
<cfelse>
    <cfset objdata = MongoCollectionfindone(application.applicationname,'post',{"_id"= newid(key)})>
    <cfset post = mongonew("post",objdata,langinput)>
    <cfset post['category'] = objdata['CATEGORY'].getId().toString()>
    <cfset post['TAG'] = objdata['TAG']/>
</cfif>
<cfset modework = 'update'>
<cfoutput>
<div class="row-fluid">
    <legend class="titlepage">Edit post - #post['title']#</legend>
    #startform(action='postscontroller')#
        #hiddenfield(name='mode',value='update')#
        #hiddenfield(name='key',value=key)#
        #hiddenfield(name='staycategory',value=post['category'])#
        #hiddenfield(name='displaystart',value=start)#
        #langselect(langinput)#
        <cfinclude template="postsform.cfm">
        #submit(label=get('labelbtnedit'))#
    #endform()#
</div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
<!--- BACK BUTTON DETECT --->
<script type="text/javascript">
		history.pushState({page: 123}, "update", "");
		var _firstload = true;
		var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
		var isIE = /*@cc_on!@*/false;                            // At least IE6
		if (isFirefox){_firstload = false;}
		window.onpopstate = function(e){
			if(_firstload){_firstload = false;}
			else{_firstload = false;
				<cfoutput>window.location.href = 'posts.cfm?category=#post['category']#&start=#start#';</cfoutput>}
		};
</script>
