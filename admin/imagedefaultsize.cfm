<!---
  Created with IntelliJ IDEA.
  User: skoodeskill
  Date: 5/3/13 AD
  Time: 5:34 PM
  To change this template use File | Settings | File Templates.
--->
<cffunction name="$showimagedefaultsize" output="false" access="public" returntype="string">

	<cfargument name="field" type="any" required="true">
	<cfargument name="show" type="any" default="table" required="false">

<!---	<cfset imagesizeq = MongoCollectionfindone(application.applicationname,'setting',{})>--->
	<cfset fieldarr = ListToArray(arguments['field'])>
<!---	<cfdump var="#fieldarr#"> <cfabort/>--->
	<cfoutput>
		<cfsavecontent variable="outputdata">
		<cfif arguments['show'] IS  "table">
			<div class="thumbnail">
			    <table class="defaultsize">
	            <tr class="">
	                <td colspan="2" class="defaultsize-header">
	                    Default image size
	                </td>
	            </tr>
					<cfloop array="#fieldarr#" index="i">
						<cfswitch expression="#i#">
							<cfcase value="slideshow">
								<tr class="">
									<td class="defaultsize-key">Slide show</td>
									<td class="defaultsize-value">#get("slideshowwidth")# x #get("slideshowheight")#</td>
	                            </tr>
							</cfcase>
							<cfcase value="largeimage">
	                            <tr>
	                                <td class="defaultsize-key">Large image</td>
	                                <td class="defaultsize-value">#get("largeimagewidth")# x #get("largeimageheight")#</td>
	                            </tr>
							</cfcase>
							<cfcase value="thumbnail">
	                            <tr>
	                                <td class="defaultsize-key">Thumbnail</td>
	                                <td class="defaultsize-value">#get("thumbnailwidth")# x #get("thumbnailheight")#</td>
	                            </tr>
							</cfcase>
							<cfcase value="categoryimage">
	                            <tr>
	                                <td class="defaultsize-key">Category image</td>
	                                <td class="defaultsize-value">#get("categoryimagewidth")# x #get("categoryimageheight")#</td>
	                            </tr>
							</cfcase>
							<cfcase value="menuimage">
	                            <tr>
	                                <td class="defaultsize-key">Menu image</td>
	                                <td class="defaultsize-value">#get("menuimagewidth")# x #get("menuimageheight")#</td>
	                            </tr>
							</cfcase>
							<cfcase value="otherimage">
	                            <tr>
									<td colspan="2" class="defaultsize-otherheader">Other</td>
	                            </tr>
	                            <tr>
	                                <td colspan="2" class="defaultsize-othervalue">#get("otherimage")#</td>
	                            </tr>
							</cfcase>


						</cfswitch>
					</cfloop>
			    </table>
			</div>
		<cfelseif arguments['show'] IS  "hidden">
			<cfloop array="#fieldarr#" index="i">
				<cfswitch expression="#i#">
					<cfcase value="slideshow">
	                    <input type="hidden" name="slideshowwidth" class="slideshowwidth" value="#get("slideshowwidth")#"> <input type="hidden" name="slideshowheight" class="slideshowheight" value="#get("slideshowheight")#">
					</cfcase>
					<cfcase value="largeimage">
	                    <input type="hidden" name="largeimagewidth" class="largeimagewidth" value="#get("largeimagewidth")#"> <input type="hidden" name="largeimageheight" class="largeimageheight" value="#get("largeimageheight")#">
	                </tr>
					</cfcase>
					<cfcase value="thumbnail">
	                    <input type="hidden" name="thumbnailwidth" class="thumbnailwidth" value="#get("thumbnailwidth")#">  <input type="hidden" name="thumbnailheight" class="thumbnailheight" value="#get("thumbnailheight")#">
	                </tr>
					</cfcase>
					<cfcase value="categoryimage">
	                    <input type="hidden" name="categoryimagewidth" class="categoryimagewidth" value="#get("categoryimagewidth")#"> <input type="hidden" name="categoryimageheight" class="categoryimageheight" value="#get("categoryimageheight")#">
	                </tr>
					</cfcase>
					<cfcase value="menuimage">
	                    <input type="hidden" name="menuimagewidth" class="menuimagewidth" value="#get("menuimagewidth")#">  <input type="hidden" name="menuimageheight" class="menuimageheight" value="#get("menuimageheight")#">
	                </tr>
					</cfcase>
					<cfcase value="otherimage">
	                    <input type="hidden" name="otherimage" class="otherimage" value="#get("otherimage")#">
					</cfcase>
				</cfswitch>
			</cfloop>
		<cfelseif arguments['show'] IS  "text">
			<cfloop array="#fieldarr#" index="i">
				<cfswitch expression="#i#">
					<cfcase value="slideshow">
                        <div class="defaultsize-value">#get("slideshowwidth")# x #get("slideshowheight")#</div>
					</cfcase>
					<cfcase value="largeimage">
                        <div class="defaultsize-value">#get("largeimagewidth")# x #get("largeimageheight")#</div>
					</cfcase>
					<cfcase value="thumbnail">
                        <div class="defaultsize-value">#get("thumbnailwidth")# x #get("thumbnailheight")#</div>
					</cfcase>
					<cfcase value="categoryimage">
                        <div class="defaultsize-value">#get("categoryimagewidth")# x #get("categoryimageheight")#</div>
					</cfcase>
					<cfcase value="menuimage">
                        <div class="defaultsize-value">#get("menuimagewidth")# x #get("menuimageheight")#</div>
					</cfcase>
					<cfcase value="otherimage">
                        <div class="defaultsize-othervalue">#get("otherimage")#</div>
					</cfcase>
				</cfswitch>
			</cfloop>
		</cfif>
		</cfsavecontent>
	</cfoutput>
<!---	<cfdump var="#outputdata#">--->
<!---	<cfabort/>--->
	<cfreturn outputdata>
<!---	<cfabort/>--->

</cffunction>