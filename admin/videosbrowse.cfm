<cfimport taglib="lib" prefix="ui">
<cfoutput>
    <div class="row-fluid selectfile">
        <div class="pull-left">
            <h4>Video</h4>
        </div>
        <div class="pull-right selectbtn">
	        <input type="button" class="btn" value="Close" onClick="javascript:window.close();">
            <input type="button" class="btn btn-primary" value="Select" onClick="selectfile()">
        </div>
    </div>
    <div class="row-fluid">
        <ui:datatable field="id,#capitalize('title')#" tool=false />
    </div>
</cfoutput>
<script type="text/javascript">
    $(document).ready(function() {
        createdatatablebrowse('usetable','videosbrowsegrid','');
    })

    $('#usetable tbody tr').live('click', function () {
        var aData = oTable.fnGetData( this );
        var iId = aData[0];
        gaiSelected = [];
        if ( $(this).hasClass('row_selected') ) {
            $(this).removeClass('row_selected');
        }else{
            oTable.$('tr.row_selected').removeClass('row_selected');
            $(this).addClass('row_selected');
            gaiSelected.push(iId);
        }
    } );

    function selectfile() {
        if (gaiSelected != "") {
            // parent.selectedcomplete(gaiSelected);
            <cfoutput>
            window.top.opener.CKEDITOR.tools.callFunction(#CKEditorFuncNum#, gaiSelected[0]);
            </cfoutput>
            closewindow();
        }else{
            alert("Please select video");
        }
    }

    function closewindow()
    {
        window.top.close();
    }
</script>
