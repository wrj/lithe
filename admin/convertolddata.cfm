<cfinclude template="system/loadplugins.cfm">
<cfif isDefined("c") eq 'NO'>
	<cfset c = "default">
</cfif>
<cfoutput>
<cfswitch expression="#c#">
	<cfcase value="1">
		<!--- for no convert data --->
		<cfset olddata = MongoCollectionfind(application.applicationname,'post',{})>
		<cfloop array="#olddata#" index="item">
			<cfset galleryarray = arrayNew()>
			<cfif structKeyExists(item,'GALLERY')>
				<cfloop list="#item['GALLERY']#" index="gitem">
					<cfset galleryst = Structnew()>
					<cfset galleryst['image'] = "#get('pathuploadfolder')#image/#listFirst(gitem,'|')#">
					<cfset galleryst['title'] = listLast(gitem,'|')>
					<cfset galleryst['detail'] = listLast(gitem,'|')>
					<cfset galleryst['link'] = ''>
					<cfset arrayAppend(galleryarray, galleryst)>
				</cfloop>
			</cfif>
			<cfset userid = dbref('user',item['USER'].fetch()['_id'].toString())>
			<cfset cateid = dbref('category',item['CATEGORY'].fetch()['_id'].toString())>
			<cfset item['USER'] = userid>
			<cfset item['CATEGORY'] = cateid>
			<cfset item['GALLERY'] = galleryarray>
			<cfset MongoCollectionsave(application.applicationname,'post',item)>
		</cfloop>
	</cfcase>
	<cfcase value="2">
		<!--- for convert data --->
		<cfset postdata = MongoCollectionfind(application.applicationname,'post',{})>
		<!--- Post --->
		<cfloop array="#postdata#" index="item">
			<cfif IsArray(item['GALLERY'])>
				<cfif arrayLen(item['GALLERY']) gt 0>
					<cfloop array="#item['GALLERY']#" index="gitem">
						<cfset gitem['image'] = "#get('pathuploadfolder')#image/#gitem['image']#">
					</cfloop>
					<cfset userid = dbref('user',item['USER'].fetch()['_id'].toString())>
					<cfset item['USER'] = userid>
					<cfif item['TYPECONTENT'] eq 1>
						<cfset cateid = dbref('category',item['CATEGORY'].fetch()['_id'].toString())>
						<cfset item['CATEGORY'] = cateid>	
					</cfif>
					<cfset MongoCollectionsave(application.applicationname,'post',item)>
				</cfif>
			<cfelse>
				<cfset galleryarray = arrayNew()>
				<cfif structKeyExists(item,'GALLERY')>
					<cfloop list="#item['GALLERY']#" index="gitem">
						<cfset galleryst = Structnew()>
						<cfset galleryst['image'] = "#get('pathuploadfolder')#image/#listFirst(gitem,'|')#">
						<cfset galleryst['title'] = listLast(gitem,'|')>
						<cfset galleryst['detail'] = listLast(gitem,'|')>
						<cfset galleryst['link'] = ''>
						<cfset arrayAppend(galleryarray, galleryst)>
					</cfloop>
				</cfif>
				<cfset userid = dbref('user',item['USER'].fetch()['_id'].toString())>
				<cfset item['USER'] = userid>
				<cfif item['TYPECONTENT'] eq 1>
					<cfset cateid = dbref('category',item['CATEGORY'].fetch()['_id'].toString())>
					<cfset item['CATEGORY'] = cateid>	
				</cfif>
				<cfset item['GALLERY'] = galleryarray>
				<cfset MongoCollectionsave(application.applicationname,'post',item)>
			</cfif>
		</cfloop>
		<!--- End Post --->
	</cfcase>
	<cfcase value="3">
		<cfset postdata = MongoCollectionfind(application.applicationname,'post',{})>
		<cfset gallerydata = MongoCollectionfind(application.applicationname,'gallery',{})>
		<cfset slidedata = MongoCollectionfind(application.applicationname,'slide',{})>
		<!--- Post --->
		<cfloop array="#postdata#" index="item">
			<cfif arrayLen(item['GALLERY']) gt 0>
				<cfloop array="#item['GALLERY']#" index="gitem">
					<cfset gitem['image'] = "#get('pathuploadfolder')#image/#gitem['image']#">
				</cfloop>
				<cfset userid = dbref('user',item['USER'].fetch()['_id'].toString())>
				<cfset item['USER'] = userid>
				<cfif item['TYPECONTENT'] eq 1>
					<cfset cateid = dbref('category',item['CATEGORY'].fetch()['_id'].toString())>
					<cfset item['CATEGORY'] = cateid>	
				</cfif>
			<cfset MongoCollectionsave(application.applicationname,'post',item)>
			</cfif>
		</cfloop>
		<!--- End Post --->
		<!--- Gallery --->
		<cfloop array="#gallerydata#" index="item">
			<cfloop array="#item['GALLERY']#" index="gitem">
				<cfset gitem['image'] = "#get('pathuploadfolder')#image/#gitem['image']#">
			</cfloop>
			<cfset userid = dbref('user',item['USER'].fetch()['_id'].toString())>
			<cfset item['USER'] = userid>
			<cfset MongoCollectionsave(application.applicationname,'gallery',item)>
		</cfloop>
		<!--- End Gallery --->
		<!--- Slide --->
		<cfloop array="#slidedata#" index="item">
			<cfloop array="#item['GALLERY']#" index="gitem">
				<cfset gitem['image'] = "#get('pathuploadfolder')#image/#gitem['image']#">
			</cfloop>
			<cfset userid = dbref('user',item['USER'].fetch()['_id'].toString())>
			<cfset item['USER'] = userid>
			<cfset MongoCollectionsave(application.applicationname,'slide',item)>
		</cfloop>
		<!--- End Slide --->
	</cfcase>	
	<cfcase value="cate1">
		<cfset catedata = MongoCollectionfind(application.applicationname,'category',{})>
		<cfloop array="#catedata#" index="item">
			<cfset userid = dbref('user',item['USER'].fetch()['_id'].toString())>
			<cfset item['USER'] = userid>
			<cfif structKeyExists(item, "PARENT")>
				<cfset cateid = dbref('category',item['PARENT'].fetch()['_id'].toString())>
				<cfset item['PARENT'] = cateid>
			</cfif>
			<cfset item['URL'] = "#lcase(item['title'])#.cfm">
			<cfset item['PATTERN'] = "">
			<cfset MongoCollectionsave(application.applicationname,'category',item)>
		</cfloop>
		<cfabort/>
	</cfcase>
	<cfdefaultcase>
		use param as c=case<br>
		<ul>
			<li>1 = for no convert data</li>
			<li>2 = for convert data</li>
			<li>3 = for new sample data</li>
			<li>cate1 = add url page for category</li>
		</ul>
	</cfdefaultcase>
</cfswitch>
</cfoutput>