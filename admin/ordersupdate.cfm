<cfinclude template="include/header.cfm">
<cfset order = MongoCollectionfindone(application.applicationname,'order',{"_id"=newid(key)})>
<cfset statusq = arraynew()>
<cfset statusst = structNew()>
<cfset statusst['id'] = 'order'>
<cfset statusst['text'] = 'order'>
<cfset arrayappend(statusq,statusst)>
<cfset statusst = structNew()>
<cfset statusst['id'] = 'payment'>
<cfset statusst['text'] = 'payment'>
<cfset arrayappend(statusq,statusst)>
<cfset statusst = structNew()>
<cfset statusst['id'] = 'cancel'>
<cfset statusst['text'] = 'cancel'>
<cfset arrayappend(statusq,statusst)>
<cfoutput>
    <div class="row-fluid">
        <legend>Order</legend>
        #startform(action='orderscontroller')#
        #hiddenfield(name='key',value=key)#
        <ul class="nav nav-tabs" id="tabs">
            <li class="active"><a href="##tabs-general" data-toggle="tab">Customer</a></li>
            <li><a href="##tabs-shipping" data-toggle="tab">Shipping</a></li>
            <li><a href="##tabs-payment" data-toggle="tab">Payment</a></li>
            <li><a href="##tabs-product" data-toggle="tab">Product</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tabs-general">
                <cfset customer = order['customer']>
                #textfield(name='email',label='email',value=customer['email'],readonly=true)#
                #select(name='status',label='status',select=order['status'],option=statusq,field='text',key='id')#
                #textarea(name='note',label='note',value=order['note'])#
                <cfif structKeyExists(order,'FILE') && order['FILE'] neq "">
                    <div class="control-group">
                        <label class="control-label">File</label>
                        <div class="controls">
                            <a href="/assets/upload/order/#order['FILE']#" target="_blank">Download</a>
                        </div>
                    </div>
                </cfif>
            </div>
            <div class="tab-pane" id="tabs-shipping">
                <cfset shipping = order['SHIPPING']>
                #textfield(name='type',label='type',value=shipping['type'],readonly=true)#
                <div class="control-group">
                    <div class="controls">
                        <h5>Shipping Address</h5>
                    </div>
                </div>
                <cfloop list="#structkeylist(shipping['SHIPPING_ADDRESS'],',')#" delimiters="," index="shippingfield">
                    #textfield(name=shippingfield,label=shippingfield,value=shipping['SHIPPING_ADDRESS'][shippingfield],readonly=true)#
                </cfloop>
                <div class="control-group">
                    <div class="controls">
                        <h5>Option</h5>
                    </div>
                </div>
                <cfif isstruct(shipping['OPTIONS'])>
                    <cfloop list="#structkeylist(shipping['OPTIONS'],',')#" delimiters="," index="shippingfield">
                        #textfield(name=shippingfield,label=shippingfield,value=shipping['OPTIONS'][shippingfield],readonly=true)#
                    </cfloop>
                </cfif>
            </div>
            <div class="tab-pane" id="tabs-payment">
                <cfset payment = order['payment']>
                #textfield(name='type',label='type',value=payment['type'],readonly=true)#
                <div class="control-group">
                    <div class="controls">
                        <h5>Billing Address</h5>
                    </div>
                </div>
                <cfloop list="#structkeylist(payment['BILLING_ADDRESS'],',')#" delimiters="," index="paymentfield">
                    #textfield(name=paymentfield,label=paymentfield,value=payment['BILLING_ADDRESS'][paymentfield],readonly=true)#
                </cfloop>
                <div class="control-group">
                    <div class="controls">
                        <h5>Option</h5>
                    </div>
                </div>
                <cfif isstruct(payment['OPTIONS'])>
                    <cfloop list="#structkeylist(payment['OPTIONS'],',')#" delimiters="," index="paymentfield">
                        #textfield(name=paymentfield,label=paymentfield,value=payment['OPTIONS'][paymentfield],readonly=true)#
                    </cfloop>
                </cfif>
            </div>
            <div class="tab-pane" id="tabs-product">
                <table class="table table-striped table-bordered table-condensed">
                    <thead>
                        <tr>
                            <td>Name</td>
                            <td class="colqty">Qty</td>
                            <td class="colprice">Unit price</td>
                            <td class="colamount">Amount</td>
                        </tr>
                    </thead>
                    <tbody>
                        <cfloop array="#order['products']#" index="item">
                            <tr>
                                <td>#item['name']#</td>
                                <td class="qty">#item['qty']#</td>
                                <td class="price">#item['price']#</td>
                                <cfset amount = item['qty'] * item['price']>
                                <td class="amount">#amount#</td>
                            </tr>
                        </cfloop>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3">Discount</td>
                            <td>
                                <input type="text" name="discount" class="span10" value="0" id="discountcost">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">Shipping</td>
                            <td>
                                <input type="text" name="shippingprice" class="span10" value="0" id="shippingcost">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">Total</td>
                            <td class="totalprice">
                                #order['totalprice']#
                            </td>
                        </tr>
                    </tfoot>
                </table>
                #hiddenfield(name='newtotal',value=order['totalprice'])#
                #textarea(name='mailnote',label='email note',rows=3)#
            </div>
        </div>
        #submit(label=get('labelbtnedit'))#
        #endform()#
    </div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<style>
    table thead tr td{
        text-align: center!important;
    }
    .colqty,.colprice,.colamount{
        width: 150px;
    }
    .qty,.price,.amount,.totalprice{
        text-align: right!important;
    }
</style>
<script type="text/javascript">
    var totalprice = 0;
    $(function(){
        totalprice = $('.totalprice').html();
        $('#discountcost').keyup(function(){
            calprice();
        });
        $('#shippingcost').keyup(function(){
            calprice();
        });
    })

    function calprice()
    {
//        var price = $('.price');
//        $.each( price, function( key, value ) {
//            console.log($(value).html())
//        });
        var disprice = $('#discountcost').val();
        var shipprice = $('#shippingcost').val();
        var newprice = (totalprice - disprice) + parseInt(shipprice);
        $('.totalprice').html(newprice);
        $('#newtotal').val(newprice)
    }
</script>