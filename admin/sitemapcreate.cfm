<!---
  Created with IntelliJ IDEA.
  User: skoodeskill
  Date: 5/28/13 AD
  Time: 2:25 PM
  To change this template use File | Settings | File Templates.
--->
<cfoutput>
	#stylesheetinclude("ui.dynatree")#
	#javaScriptInclude("jquery.dynatree.min,createtree")#
</cfoutput>
<link rel="stylesheet/less" type="text/css" href="stylesheets/sitemap/sitemap.less" />
<cfset lang = session['language']>
<cfset sitemapMenu = MongoCollectionfind(application.applicationname,'frontendmenu',{},'{"_id":true,"NAME":true,"MENU":true,"UPDATEDAT":true}')>
<cfset sitemapCategory = MongoCollectionfind(application.applicationname,'category',{},'{"_id":true,"TITLE":true,"TEMPLATE":true,"UPDATEDAT":true,"SLUG":true,"URL":true,"PATTERN":true}')>
<cfset sitemapPost = MongoCollectionfind(application.applicationname,'post',{'PUBSTATUS':1},'{"TYPECONTENT":true,"_id":true,"TITLE":true,"UPDATEDAT":true,"CATEGORY":true,"SLUG":true}')>
<!---<cfset sitemapHtml = MongoCollectionfind(application.applicationname,'post',{'PUBSTATUS':1,'TYPECONTENT':2},'{"TYPECONTENT":true,"_id":true,"TITLE":true,"UPDATEDAT":true,"CATEGORY":true,"SLUG":true}')>--->
<cfset sitemapGallery= MongoCollectionfind(application.applicationname,'gallery',{},'{"_id":true,"TITLE":true,"UPDATEDAT":true,"SLUG":true}')>
<cfset sitemapTag = MongoCollectionfind(application.applicationname,'tag',{},'{"_id":true,"NAME":true,"UPDATEDAT":true}')>
<cfset sitemap = MongoCollectionfindOne(application.applicationname,'sitemap',{})>
<cffunction name="ConvertMenuNaviToArray" output="false" access="public" returntype="any">
	<cfargument name="menuItem" type="any" required="true">
	<cfargument name="_id" type="any" required="true">
	<cfset output = arraynew()>
	<cfif ArrayLen(menuItem) GT 0>
		<cfset stringdata="">
		<cfset var m="">
		<cfoutput>
<!---	CONVERT MENU CHILD TO ONE ARRAY  --->
			<cfsavecontent variable="stringdata"><cfloop array="#arguments.menuItem#" index="qmenuitem"><cfset children=[]><cfif isdefined("qmenuitem.children")><cfset children=qmenuitem.children></cfif><cfset structDelete(qmenuitem,"children")>,{"NAME":"#qmenuitem['name']#","INDEX":"#qmenuitem['index']#","LINKURL":"#qmenuitem['linkurl']#","_id":"#_id##qmenuitem['index']#"}<cfif arraylen("#children#") GT 0>#ConvertMenuNaviToArray(children,arguments._id)#</cfif></cfloop></cfsavecontent>
		</cfoutput>
	</cfif>
	<cfreturn stringdata>
</cffunction>
<cffunction name="ConvertMenuContentToArray" output="false" access="public" returntype="any">
	<cfargument name="menuItem" type="any" required="true">
	<cfargument name="_id" type="any" required="true">
	<cfset output = arraynew()>
	<cfif ArrayLen(menuItem) GT 0>
		<cfset stringdata="">
		<cfset var m="">
		<cfoutput>
			<!---	CONVERT MENU CHILD TO ONE ARRAY  --->
			<cfsavecontent variable="stringdata"><cfloop array="#arguments.menuItem#" index="qmenuitem"><cfset children=[]><cfif isdefined("qmenuitem.children")><cfset children=qmenuitem.children></cfif><cfset structDelete(qmenuitem,"children")><cfif arraylen("#children#") GT 0>,{"NAME":"#qmenuitem['name']#","INDEX":"#qmenuitem['index']#","_id":"#_id##qmenuitem['index']#","TYPE":"SUBMENUCONTENT"}#ConvertMenuContentToArray(children,arguments._id)#</cfif></cfloop></cfsavecontent>
		</cfoutput>
	</cfif>
	<cfreturn stringdata>
</cffunction>
<cfset menunavi = arraynew()>
<cfset menucontent = arrayNew()>
<cfloop array="#sitemapMenu#" index="qconvertmenu">
	<cfset rowmenu = qconvertmenu.MENU>
	<cfset menuItem = deserializeJSON(rowmenu)>
	<cfset newmenu=trim(ConvertMenuNaviToArray(menuItem.menu,qconvertmenu._id.toString()))>
<!---			SET STRING FOR JSON FORMAT--->
	<cfset newmenuCut = Right(newmenu,Len(newmenu)-1)>
	<cfset newmenuitem = '[#newmenuCut#]'>
	<cfset newmenuitem = replaceNoCase(newmenuitem,'[,','[','all')>
	<cfset menust = structNew()>
	<cfset menust['MENU']= deserializeJSON(newmenuitem)>
	<cfset menust['NAME']= qconvertmenu.NAME>
	<cfset menust['UPDATEDAT']= qconvertmenu.UPDATEDAT>
	<cfset menust['_id']= qconvertmenu._id.toString()>
	<cfset arrayAppend(menunavi,menust)>
</cfloop>

<cfloop array="#sitemapMenu#" index="qconvertmenu">
	<cfset rowmenu = qconvertmenu.MENU>
	<cfset menuItem = deserializeJSON(rowmenu)>


<!---			SET STRING FOR JSON FORMAT--->
<!---	<cfdump var="#menuItemContent.menu#">--->
	<cfset menustContent=structNew()>
	<cfset newmenuContent=trim(ConvertMenuContentToArray(menuItem.menu,qconvertmenu._id.toString()))>
	<cfif newmenuContent IS NOT "">
		<cfset newmenuCutContent = Right(newmenuContent,Len(newmenuContent)-1)>
		<cfset newmenuitemContent = '[#newmenuCutContent#]'>
		<cfset newmenuitemContent = replaceNoCase(newmenuitemContent,'[,','[','all')>
		<cfset menustContent['MENU']= deserializeJSON(newmenuitemContent)>
	<cfelse>
		<cfset menustContent['MENU']=arrayNew()>
	</cfif>
	<cfset menuHeadContent=structNew()>
	<cfset menuHeadContent['INDEX']= qconvertmenu.NAME>
	<cfset menuHeadContent['NAME']= qconvertmenu.NAME>
	<cfset menuHeadContent['_id']= qconvertmenu._id.toString()>
	<cfset menuHeadContent['TYPE']= "HEADMENUCONTENT">

	<cfset ArrayPrepend(menustContent['MENU'],menuHeadContent)>
	<cfset menustContent['NAME']= qconvertmenu.NAME>
	<cfset menustContent['UPDATEDAT']= qconvertmenu.UPDATEDAT>
	<cfset menustContent['_id']= qconvertmenu._id.toString()>
	<cfset arrayAppend(menucontent,menustContent)>
</cfloop>
<!---<cfdump var="#menunavi#">--->
<!---<cfdump var="#menucontent#">--->
<!---<cfdump var="#sitemap#">--->
<!---<cfabort/>--->
<!--- CONVERT CATEGORY _ID --->
<cfloop array="#sitemapCategory#" index="qcategory">
	<cfset qcategory['_id'] = qcategory._id.toString() >
</cfloop>
<!--- CONVERT TAG _ID --->
<cfloop array="#sitemapTag#" index="qtag">
	<cfset qtag['_id'] = qtag._id.toString() >
</cfloop>
<cfset selectchangefreq = arraynew()>
<cfset changefreqlist = structNew()>
<cfset changefreqlist['id'] = 'always'>
<cfset changefreqlist['title'] = 'always'>
<cfset arrayAppend(selectchangefreq,changefreqlist)>
<cfset changefreqlist = structNew()>
<cfset changefreqlist['id'] = 'hourly'>
<cfset changefreqlist['title'] = 'hourly'>
<cfset arrayAppend(selectchangefreq,changefreqlist)>
<cfset changefreqlist = structNew()>
<cfset changefreqlist['id'] = 'daily'>
<cfset changefreqlist['title'] = 'daily'>
<cfset arrayAppend(selectchangefreq,changefreqlist)>
<cfset changefreqlist = structNew()>
<cfset changefreqlist['id'] = 'weekly'>
<cfset changefreqlist['title'] = 'weekly'>
<cfset arrayAppend(selectchangefreq,changefreqlist)>
<cfset changefreqlist = structNew()>
<cfset changefreqlist['id'] = 'monthly'>
<cfset changefreqlist['title'] = 'monthly'>
<cfset arrayAppend(selectchangefreq,changefreqlist)>
<cfset changefreqlist = structNew()>
<cfset changefreqlist['id'] = 'yearly'>
<cfset changefreqlist['title'] = 'yearly'>
<cfset arrayAppend(selectchangefreq,changefreqlist)>
<cfset changefreqlist = structNew()>
<cfset changefreqlist['id'] = 'never'>
<cfset changefreqlist['title'] = 'never'>
<cfset arrayAppend(selectchangefreq,changefreqlist)>

<cfset selectpriority = arraynew()>
<cfset changepriority = structNew()>
<cfset changepriority['value'] = 0>
<cfset changepriority['title'] = 'Very Low'>
<cfset arrayAppend(selectpriority,changepriority)>
<cfset changepriority = structNew()>
<cfset changepriority['value'] = 0.2>
<cfset changepriority['title'] = 'Low'>
<cfset arrayAppend(selectpriority,changepriority)>
<cfset changepriority = structNew()>
<cfset changepriority['value'] = 0.5>
<cfset changepriority['title'] = 'Medium'>
<cfset arrayAppend(selectpriority,changepriority)>
<cfset changepriority = structNew()>
<cfset changepriority['value'] = 0.7>
<cfset changepriority['title'] = 'high'>
<cfset arrayAppend(selectpriority,changepriority)>
<cfset changepriority = structNew()>
<cfset changepriority['value'] = 1>
<cfset changepriority['title'] = 'very high'>
<cfset arrayAppend(selectpriority,changepriority)>

<cfset urltype = arraynew()>
<cfset urltypes = structNew()>
<cfset urltypes['id'] = 'longurl'>
<cfset urltypes['title'] = 'longurl'>
<cfset arrayAppend(urltype,urltypes)>
<cfset urltypes = structNew()>
<cfset urltypes['id'] = 'shorturl'>
<cfset urltypes['title'] = 'shorturl'>
<cfset arrayAppend(urltype,urltypes)>

    <div class="row-fluid">
       <cfoutput>
	       <cfif isdefined("sitemap")>
           #select(name='changefreq',label='Change Freq',select=sitemap['CHANGEFREQ'],option=selectchangefreq,field='title',key='id',class="span3")#
           <div class="control-group">
                   <label class="control-label">Last Update</label>
               <div class="controls controls-text">#dateformat(sitemap.UPDATEDAT,"dd-MM-yyyy")#  #timeformat(sitemap.UPDATEDAT,"HH:mm:ss")#</div>
           </div>
	       <cfelse>
		       #select(name='changefreq',label='Change Freq',select='never',option=selectchangefreq,field='title',key='id',class="span3")#
		       <div class="control-group">
			       <label class="control-label">Url Type</label>
			       <div class="controls controls-text">
			       <label class="radio">
					       <input type="radio" name="sitemapurltype" class="sitemapurltype" value="longurl" checked="checked">
				         Long Url
				       </label>
				       <label class="radio">
						       <input type="radio" name="sitemapurltype" class="sitemapurltype" value="shorturl">
				       Short Url
			       </label>
			       </div>
		       </div>
               <div class="control-group">
                   <label class="control-label">Last Update</label>
                   <div class="controls controls-text">-</div>
               </div>
	       </cfif>
            <div class="control-group button-action">
                <label class="control-label"></label>
				<button type="button" class="btn btn-primary saveSitemap">Save</button>
                <button type="button" class="btn updateSitemap">Update</button>
            </div>
			<div id="saveprogress" class="progress progress-striped active hidden">
			   <div class="bar" style="width: 100%;"></div>
			</div>
	       <div id="showgenflash"></div>
       </cfoutput>
	</div>
	<div class="sitemapInput">
	    <ul class="nav nav-tabs" id="tabs">
	        <li class="active"><a href="#tabs-menunavi" data-toggle="tab">Menu Navi</a></li>
	        <li><a href="#tabs-menucontent" data-toggle="tab">Menu Content</a></li>
			<li><a href="#tabs-category" data-toggle="tab">Category</a></li>
			<li><a href="#tabs-html" data-toggle="tab">HTML</a></li>
			<li><a href="#tabs-post" data-toggle="tab">Post</a></li>
            <li><a href="#tabs-gallery" data-toggle="tab">Gallery</a></li>
			<li><a href="#tabs-tag" data-toggle="tab">Tag</a></li>
		</ul>
		<div class="tab-content sitemap-main">
	<!--- +++++++++++++++++++++++++++ MENU NAVI +++++++++++++++++++++++++++ --->
		    <script type="text/javascript">
		        var menunaviData =  <cfoutput>#serializeJSON(menunavi)#</cfoutput>
		    </script>
		    <div class="tab-pane active" id="tabs-menunavi">
		        <div class="row-fluid">
		            <div class="span3">
		                <p>Menu</p>
		                <div class="menulist">
							<ul class="menunaviList nav nav-list bs-docs-sidenav affix-top">
								<cfset menui=0>
								<cfoutput>
									<cfloop array="#menunavi#" index="qmenunamelist">
										<li class="">
											<a href="javascript:void(0)" class="chooseMenuNavi <cfif menui IS 0>menunaviActive</cfif>">#qmenunamelist.NAME#</a>
		                                    <input class="menunaviIdShow" name="menunaviIdShow" type="hidden" value="#qmenunamelist._id.toString()#">
										</li>
										<cfset menui++>
									</cfloop>
								</cfoutput>
							</ul>
		                </div>
		            </div>
		            <div class="span9">
						<label class="checkbox"><input type="checkbox" name="allmenunavi" class="allmenunavi">All Menu Navi</label>
						<div class="menunaviTable"></div>
		            </div>
		        </div>
		    </div>
	<!--- +++++++++++++++++++++++++++MENU CONTENT +++++++++++++++++++++++++++ --->
			<script type="text/javascript">
				var menucontentData =  <cfoutput>#serializeJSON(menucontent)#</cfoutput>
			</script>
			<div class="tab-pane" id="tabs-menucontent">
				<div class="row-fluid">
		            <div class="span3">
		                <p>Menu</p>
			            <div class="menulist">
			                <ul class="menucontentList nav nav-list bs-docs-sidenav affix-top">
								<cfset menui=0>
								<cfoutput>
									<cfloop array="#menucontent#" index="qmenunamelist">
						                <li class="">
					                        <a href="javascript:void(0)" class="chooseMenuContent <cfif menui IS 0>menucontentActive</cfif>">#qmenunamelist.NAME#</a>
						                    <input class="menucontentIdShow" name="menucontentIdShow" type="hidden" value="#qmenunamelist._id.toString()#">
							            </li>
										<cfset menui++>
									</cfloop>
								</cfoutput>
						    </ul>
					    </div>
				    </div>
					<div class="span9">
			            <div class="menucontent">
							<cfoutput>#textfield(name='menucontentdefaulttemplate',class='menucontentdefaulttemplate',label='Default Template',value="index.cfm")#</cfoutput>
							<div class="control-group">
								<label class="control-label">All Menu Content</label>
								<div class="controls">
									<input type="checkbox" name="allmenucontent" class="allmenucontent" value="allcontent">
								</div>
							</div>
							<div class="menucontentTable"></div>
			            </div>
					</div>
                </div>
		    </div>
<!---	<!--- +++++++++++++++++++++++++++ CATEGORY +++++++++++++++++++++++++++ --->--->
			<script type="text/javascript">
				var categoryData =  <cfoutput>#serializeJSON(sitemapCategory)#</cfoutput>
			</script>
			<div class="tab-pane" id="tabs-category">
				<div class="cateTable">
					<cfoutput>#textfield(name='categorydefaulttemplate',class='categorydefaulttemplate',label='Default Template',value="index.cfm")#</cfoutput>
	                <div class="control-group">
	                    <label class="control-label">All Categories</label>
	                    <div class="controls">
							<input type="checkbox" name="allcategory" class="allcategory" value="allcategory">
		                </div>
	                </div>
                    <div class="categoryTable"></div>
				</div>
			</div>
<!---	<!--- +++++++++++++++++++++++++++ HTML +++++++++++++++++++++++++++ --->--->
			<cfset htmlarr=arrayNew()>
			<cfloop array="#sitemapPost#" index="qhtml">
				<cfif qhtml.TYPECONTENT IS 2>
					<cfset htmlitem = structNew()>
					<cfset htmlitem['_id'] = qhtml._id.toString()>
					<cfset htmlitem['TITLE'] = i18n(qhtml['TITLE'],lang)>
					<cfset htmlitem['UPDATEDAT'] = qhtml.UPDATEDAT>
					<cfset htmlitem['SLUG'] ="#qhtml.SLUG#">
					<cfset arrayAppend(htmlarr,htmlitem)>
				</cfif>
			</cfloop>
		    <script type="text/javascript">
		        var htmlData =  <cfoutput>#serializeJSON(htmlarr)#</cfoutput>
		    </script>
		    <div class="tab-pane" id="tabs-html">
			    <div class="htmltab">
					<cfoutput>#textfield(name='htmldefaulttemplate',class='htmldefaulttemplate',label='Default Template',value="index.cfm")#</cfoutput>
			        <div class="control-group">
			            <label class="control-label">All HTML</label>
			            <div class="controls">
			                <input type="checkbox" name="allhtml" class="allhtml" value="allhtml">
			            </div>
			        </div>
			        <div class="htmlTable"></div>
			    </div>
		    </div>
<!---	<!--- +++++++++++++++++++++++++++ POST +++++++++++++++++++++++++++ --->--->
			<cfoutput>
				<cfset postArrData = arrayNew()>
				<cfloop array="#sitemapCategory#" index="qcateonpost">
					<cfset postCategoryItem = structNew()>
					<cfset postCategoryItem['CATEGORYID'] = qcateonpost._id.toString()>
					<cfset postarr = arrayNew()>
					<cfloop array="#sitemapPost#" index="qpost">
						<cfif qpost.TYPECONTENT IS 1 AND qpost.category.getId().toString() IS qcateonpost._id.toString()>
							<cfset postitem = structNew()>
							<cfset postitem['_id'] = qpost._id.toString()>
							<cfset postitem['TITLE'] = i18n(qpost['TITLE'],lang)>
							<cfset postitem['UPDATEDAT'] = qpost.UPDATEDAT>
							<cfset postitem['SLUGURL'] ="#qpost.SLUG#">
							<cfset postitem['TEMPLATE'] ="#qcateonpost.TEMPLATE#">
							<cfset postitem['PATTERN'] ="#qcateonpost.PATTERN#">
							<cfset arrayAppend(postarr,postitem)>
						</cfif>
					</cfloop>
					<cfset postCategoryItem['POST'] = postarr>
					<cfset arrayAppend(postArrData,postCategoryItem)>
				</cfloop>
			</cfoutput>
		    <script type="text/javascript">
		        var postData =  <cfoutput>#serializeJSON(postArrData)#</cfoutput>
		    </script>
	        <div class="tab-pane" id="tabs-post">
		        <div class="row-fluid">
			        <div class="span3">
			            <p>Categories</p>
	                    <div class="treeCategory"></div>
	                    <input type="hidden" name="excTable" class="excTable" value="allPostTable">
			        </div>
			        <div class="span9">
			            <div class="allPostTable">
							<label class="checkbox"><input type="checkbox" name="allPOST" class="allPOST">All Posts</label>
							<div class="genTable"></div>
						</div>
			        </div>
		        </div>
	        </div>
	<!---	<!--- +++++++++++++++++++++++++++ GALLERY +++++++++++++++++++++++++++ --->--->
	<cfset galleryarr=arrayNew()>
	<cfloop array="#sitemapGallery#" index="qgallery">
			<cfset galleryitem = structNew()>
			<cfset galleryitem['_id'] = qgallery._id.toString()>
			<cfset galleryitem['TITLE'] = i18n(qgallery['TITLE'],lang)>
			<cfset galleryitem['UPDATEDAT'] = qgallery.UPDATEDAT>
			<cfset galleryitem['SLUG'] ="#qgallery.SLUG#">
			<cfset arrayAppend(galleryarr,galleryitem)>
	</cfloop>
    <script type="text/javascript">
        var galleryData =  <cfoutput>#serializeJSON(galleryarr)#</cfoutput>
    </script>
    <div class="tab-pane" id="tabs-gallery">
	    <div class="gallerytab">
		<cfoutput>
			<cfset gallerymaintemplate_value = 0>
			<cfset gallerymaintemplate_template = "gallery.cfm">
			<cfif isDefined("sitemap['CUSTOM']")>
				<cfloop array="#sitemap['CUSTOM']#" index="custom">
					<cfif custom['NAME'] IS "gallerymaintemplate">
						<cfset gallerymaintemplate_value = custom['PRIORITY']>
						<cfset gallerymaintemplate_template = custom['TEMPLATE']>
					</cfif>
				</cfloop>
			</cfif>
			#textfield(name='gallerymaintemplate',class='gallerymaintemplate',label='Main Template',value=gallerymaintemplate_template)#
            #select(name='gallerymaintemplate-priority',label='Priority',select=gallerymaintemplate_value,option=selectpriority,field='title',key='value',class="gallerymaintemplate-priority")#
			#textfield(name='gallerydefaulttemplate',class='gallerydefaulttemplate',label='Default Template',value="gallerydetail.cfm")#
		</cfoutput>
	        <div class="control-group">
	            <label class="control-label">All Galleries</label>
	            <div class="controls">
	                <input type="checkbox" name="allgallery" class="allgallery" value="allgallery">
	            </div>
	        </div>
	        <div class="galleryTable"></div>
	    </div>
    </div>
	<!--- +++++++++++++++++++++++++++ TAG +++++++++++++++++++++++++++ --->
	        <div class="tab-pane" id="tabs-tag">
				<script type="text/javascript">
					var tagData =  <cfoutput>#serializeJSON(sitemapTag)#</cfoutput>
                </script>
				<div class="tag">
					<cfoutput>
						#textfield(name='tagdefaulttemplate',class='tagdefaulttemplate',label='Default Template',value="index.cfm")#
					</cfoutput>
					<div class="control-group">
						<label class="control-label">All Tags</label>
							<div class="controls">
							<input type="checkbox" name="alltag" class="alltag" value="alltag">
						</div>
					</div>
					<div class="tagTable"></div>
				</div>
	        </div>
	<!--- ++++++++++++++++++++++++++ END RENDER SITEMAP +++++++++++++++++++++++++ --->
	    </div>
	</div>
	<cfoutput><input type="hidden" name="websiteUrl" class="websiteUrl" value="http://#CGI.HTTP_HOST#/"></cfoutput>
<script type="text/javascript" src="js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/sitemap/date.format.js"></script>
<script type="text/javascript" src="js/less-1.3.3.min.js"></script>
<script type="text/javascript" src="js/sitemap/sitemap.js"></script>
<script type="text/javascript" src="js/sitemap/post.js"></script>
<script type="text/javascript" src="js/sitemap/menunavi.js"></script>
<script type="text/javascript" src="js/sitemap/content.js"></script>
<script type="text/javascript" src="js/sitemap/save.js"></script>
<!--- +++++++++++++++++++++++++ JAVASCRIPT +++++++++++++++++++++++++++++++ --->
<script type="text/javascript" charset="utf-8">
<!--- UPADEDATA --->
	var categoryUpdateCheckAll="NO"
	var tagUpdateCheckAll="NO"
    var htmlUpdateCheckAll="NO"
    var galleryUpdateCheckAll="NO"
	<cfif isdefined('sitemap')>
		var menunaviUpdateData =  <cfoutput>#serializeJSON(sitemap.MENUNAVI)#</cfoutput>
        var menunaviUpdateCheckAll =  <cfoutput>#serializeJSON(sitemap.ALLMENUNAVI)#</cfoutput>
		menunaviData = window.mapvalueMenunavi("MENUNAVI",menunaviUpdateCheckAll,menunaviUpdateData,menunaviData)
		var menucontentUpdateData =  <cfoutput>#serializeJSON(sitemap.MENUCONTENT)#</cfoutput>
		var menucontentUpdateCheckAll =  <cfoutput>#serializeJSON(sitemap.ALLMENUCONTENT)#</cfoutput>
		menucontentData = window.mapvalueMenunavi("MENUCONTENT",menucontentUpdateCheckAll,menucontentUpdateData,menucontentData)
		var categoryUpdateData =  <cfoutput>#serializeJSON(sitemap.CATEGORY)#</cfoutput>
        categoryUpdateCheckAll = "<cfoutput>#serializeJSON(sitemap.ALLCATEGORY)#</cfoutput>"
		categoryData = window.mapvalueContent('CATEGORY',categoryUpdateData,categoryData)
		var postUpdateData =  <cfoutput>#serializeJSON(sitemap.POST)#</cfoutput>
        var postUpdateCheckAll =  <cfoutput>#serializeJSON(sitemap.ALLPOST)#</cfoutput>
		postData = window.mapvaluePost('POST',postUpdateCheckAll,postUpdateData,postData)
        var htmlUpdateData =  <cfoutput>#serializeJSON(sitemap.HTML)#</cfoutput>
        htmlUpdateCheckAll =  "<cfoutput>#sitemap.ALLHTML#</cfoutput>"
        htmlData = window.mapvalueContent('HTML',htmlUpdateData,htmlData)
        var galleryUpdateData =  <cfoutput>#serializeJSON(sitemap.GALLERY)#</cfoutput>
        galleryUpdateCheckAll =  "<cfoutput>#sitemap.ALLGALLERY#</cfoutput>"
        galleryData = window.mapvalueContent('GALLERY',galleryUpdateData,galleryData)
		var tagUpdateData =  <cfoutput>#serializeJSON(sitemap.TAG)#</cfoutput>
		tagUpdateCheckAll =  "<cfoutput>#sitemap.ALLTAG#</cfoutput>"
		tagData = window.mapvalueContent('TAG',tagUpdateData,tagData)
	</cfif>
	var visitfirst = true;
    var category = '';
<!---	NEW PARAMITER	--->
	var menucontentElement,categoryElement,htmlElement,galleryElement,tagElement,newmenunavi,newmenucontent,oldpagecategory,newpagecategory,oldpostcategory,newpostcategory
	var menunaviDataTable,menucontentDataTable,categoryDataTable,htmlDataTable,galleryDataTable,postDataTable,tagDataTable
<!---	END NEW PARAMITER	--->
    $(document).ready(function() {
        window.treeCategory('treeCategory');
<!--- +++++++++++++++++ MENU NAVI +++++++++++++++++ --->
	    showMenunaviFirst = $(".menunaviActive").parent().find(".menunaviIdShow").val()
        newmenunavi = showMenunaviFirst
		$('.allmenunavi').val(newmenunavi)
		showMenunavi(showMenunaviFirst)
	    $(".chooseMenuNavi").click(function(){
            $(this).parent().parent().find("a").removeClass("menunaviActive")
            $(this).addClass("menunaviActive")
		    showId = $(this).parent().find(".menunaviIdShow").val()
            menunaviData = window.updateMenunavi(newmenunavi,menunaviDataTable,menunaviData,"menunavi")
		    $(".menunaviTable").html("")
            newmenunavi = showId
			showMenunavi(showId)
	    });
		$( ".allmenunavi" ).on( "click", function() {
            menuid = $(".allmenunavi:checked").val()
			if (menuid != undefined){
                menunaviDataTable.$('.use').prop('checked',true);
                menunaviDataTable.$('.use').prop('disabled',true);
			}
			else{
                menunaviDataTable.$('.use').prop('checked',false);
                menunaviDataTable.$('.use').prop('disabled',false);
			}
		});
<!--- +++++++++++++++++ END MENU NAVI +++++++++++++++++ --->
<!--- +++++++++++++++++ MENU CONTENT +++++++++++++++++ --->
	showMenucontentFirst = $(".menucontentActive").parent().find(".menucontentIdShow").val()
	newmenucontent = showMenucontentFirst
	$('.allmenucontent').val(newmenucontent)
	showMenucontent(showMenucontentFirst)
	$(".chooseMenuContent").click(function(){
		$(this).parent().parent().find("a").removeClass("menucontentActive")
		$(this).addClass("menucontentActive")
		showId = $(this).parent().find(".menucontentIdShow").val()
		menucontentData = window.updateMenunavi(newmenucontent,menucontentDataTable,menucontentData,"menucontent")
		$(".menucontentTable").html("")
		newmenucontent = showId
		showMenucontent(showId)
	});
	$( ".allmenucontent" ).on( "click", function() {
		menuid = $(".allmenucontent:checked").val()
		if (menuid != undefined){
			menucontentDataTable.$('.use').prop('checked',true);
			menucontentDataTable.$('.use').prop('disabled',true);
		}
		else{
			menucontentDataTable.$('.use').prop('checked',false);
			menucontentDataTable.$('.use').prop('disabled',false);
		}
	});
<!--- +++++++++++++++++ END MENU CONTENT +++++++++++++++++ --->
<!--- +++++++++++++++++ CATEGORY +++++++++++++++++ --->
	var categoryArr=[];
    for (var i = 0; i < categoryData.length; i++) {
        var j = categoryData[i];
        var priority
        if(j.PRIORITY === null ){priority=window.changePriority(0,"")}else{priority=window.changePriority(j.PRIORITY,"")}
        var categoryItem=[];
        var use = ""
        if (j.USE != null && j.USE === 1){
            use = "checked"
        }
        var template = "";
        if (j.FILETEMPLATE != null && j.FILETEMPLATE != ''){
            template = j.FILETEMPLATE
        }else{
	        template = j.URL
        }
        categoryItem.push('<input type="checkbox" name="use" class="use" value="'+ j._id+'" '+use+'>');
        categoryItem.push('<i class="Urlhideshow icon-chevron-down"></i>' +
            '<input class="urltype" type="hidden" name="urltype" value="CATEGORY">' +
	        '<input class="pattern" type="hidden" name="pattern" value="'+j.PATTERN+'">' +
            '<input class="cateslug" type="hidden" name="cateslug" value="'+j.SLUG+'">');
        categoryItem.push(j.TITLE+'<input type="hidden" name="'+ j._id+'" class="'+ j._id+'" value="CATEGORY">');
        categoryItem.push('<input name="sitemaptemplate" type="text" value="'+template+'" class="template">');
        categoryItem.push(dateFormat(j['UPDATEDAT'],"yyyy-mm-dd")+'<input type="hidden" class="lastmodify" name="lastmodify" value="'+dateFormat(j['UPDATEDAT'],"yyyy-mm-dd")+'">');
        categoryItem.push(priority);
        categoryArr.push(categoryItem);
    }
	$('.categoryTable').html("<table class='createTableData table table-striped'></table>")
    categoryDataTable = $('.categoryTable').find('.createTableData').dataTable({
		"sPaginationType": "full_numbers",
		"aaData": categoryArr,
		"aoColumns": [
		    { "sTitle": "Use" },
		    { "sTitle": "" },
		    { "sTitle": "Title" },
            { "sTitle": "Template" },
		    { "sTitle": "Last Modify" },
		    { "sTitle": "Priority" }
		]
	});
    if (categoryUpdateCheckAll === "YES"){
        $('.allcategory').prop('checked',true);
        categoryDataTable.$('.use').prop('checked',true);
        categoryDataTable.$('.use').prop('disabled',true);
    }
    else{
        $('.allcategory').prop('checked',false);
        categoryDataTable.$('.use').prop('disabled',false);
    }
	$( ".allcategory" ).on( "click", function() {
	    categoryCheck = $(".allcategory:checked").val()
	    if (categoryCheck != undefined){
			categoryDataTable.$('.use').prop('checked',true);
            categoryDataTable.$('.use').prop('disabled',true);
	    }
	    else{
			categoryDataTable.$('.use').prop('checked',false);
            categoryDataTable.$('.use').prop('disabled',false);
	    }
	});
<!--- +++++++++++++++++ CATEGORY +++++++++++++++++ --->
<!--- +++++++++++++++++ HTML +++++++++++++++++ --->
    var htmlArr=[];
    for (var i = 0; i < htmlData.length; i++) {
        var j = htmlData[i];
        var priority
        if(j.PRIORITY === null ){priority=window.changePriority(0,"")}else{priority=window.changePriority(j.PRIORITY,"")}
        var htmlItem=[];
        var use = ""
        if (j.USE != null && j.USE === 1){
            use = "checked"
        }
        var template = "";
        if (j.FILETEMPLATE != null && j.FILETEMPLATE != ''){
            template = j.FILETEMPLATE
        }
        htmlItem.push('<input type="checkbox" name="use" class="use" value="'+ j._id+'" '+use+'>');
        htmlItem.push('<i class="Urlhideshow icon-chevron-down"></i>' +
                '<input class="urltype" type="hidden" name="urltype" value="HTML">' +
                '<input class="htmlslug" type="hidden" name="htmlslug" value="'+j.SLUG+'">');
        htmlItem.push(j.TITLE+'<input type="hidden" name="'+ j._id+'" class="'+ j._id+'" value="HTML">');
        htmlItem.push('<input name="sitemaptemplate" type="text" value="'+template+'" class="template">');
        htmlItem.push(dateFormat(j['UPDATEDAT'],"yyyy-mm-dd")+'<input type="hidden" class="lastmodify" name="lastmodify" value="'+dateFormat(j['UPDATEDAT'],"yyyy-mm-dd")+'">');
        htmlItem.push(priority);
        htmlArr.push(htmlItem);
    }
    $('.htmlTable').html("<table class='createTableData table table-striped'></table>")
    htmlDataTable = $('.htmlTable').find('.createTableData').dataTable({
        "sPaginationType": "full_numbers",
        "aaData": htmlArr,
        "aoColumns": [
            { "sTitle": "Use" },
            { "sTitle": "" },
            { "sTitle": "Title" },
            { "sTitle": "Template" },
            { "sTitle": "Last Modify" },
            { "sTitle": "Priority" }
        ]
    });
    if (htmlUpdateCheckAll === "YES"){
        $('.allhtml').prop('checked',true);
        htmlDataTable.$('.use').prop('checked',true);
        htmlDataTable.$('.use').prop('disabled',true);
    }
    else{
        $('.allhtml').prop('checked',false);
        htmlDataTable.$('.use').prop('disabled',false);
    }
    $( ".allhtml" ).on( "click", function() {
        htmlCheck = $(".allhtml:checked").val()
        if (htmlCheck != undefined){
            htmlDataTable.$('.use').prop('checked',true);
            htmlDataTable.$('.use').prop('disabled',true);
        }
        else{
            htmlDataTable.$('.use').prop('checked',false);
            htmlDataTable.$('.use').prop('disabled',false);
        }
    });
<!--- +++++++++++++++++ HTML +++++++++++++++++ --->
<!--- +++++++++++++++++ GALLERY +++++++++++++++++ --->
    var galleryArr=[];
    for (var i = 0; i < galleryData.length; i++) {
        var j = galleryData[i];
        var priority
        if(j.PRIORITY === null ){priority=window.changePriority(0,"")}else{priority=window.changePriority(j.PRIORITY,"")}
        var galleryItem=[];
        var use = ""
        if (j.USE != null && j.USE === 1){
            use = "checked"
        }
        var template = "";
        if (j.FILETEMPLATE != null && j.FILETEMPLATE != ''){
            template = j.FILETEMPLATE
        }
        galleryItem.push('<input type="checkbox" name="use" class="use" value="'+ j._id+'" '+use+'>');
        galleryItem.push('<i class="Urlhideshow icon-chevron-down"></i>' +
                '<input class="urltype" type="hidden" name="urltype" value="GALLERY">' +
                '<input class="galleryslug" type="hidden" name="galleryslug" value="'+j.SLUG+'">');
        galleryItem.push(j.TITLE+'<input type="hidden" name="'+ j._id+'" class="'+ j._id+'" value="GALLERY">');
        galleryItem.push('<input name="sitemaptemplate" type="text" value="'+template+'" class="template">');
        galleryItem.push(dateFormat(j['UPDATEDAT'],"yyyy-mm-dd")+'<input type="hidden" class="lastmodify" name="lastmodify" value="'+dateFormat(j['UPDATEDAT'],"yyyy-mm-dd")+'">');
        galleryItem.push(priority);
        galleryArr.push(galleryItem);
    }
    $('.galleryTable').html("<table class='createTableData table table-striped'></table>")
    galleryDataTable = $('.galleryTable').find('.createTableData').dataTable({
        "sPaginationType": "full_numbers",
        "aaData": galleryArr,
        "aoColumns": [
            { "sTitle": "Use" },
            { "sTitle": "" },
            { "sTitle": "Title" },
            { "sTitle": "Template" },
            { "sTitle": "Last Modify" },
            { "sTitle": "Priority" }
        ]
    });
    if (galleryUpdateCheckAll === "YES"){
        $('.allgallery').prop('checked',true);
        galleryDataTable.$('.use').prop('checked',true);
        galleryDataTable.$('.use').prop('disabled',true);
    }
    else{
        $('.allgallery').prop('checked',false);
        galleryDataTable.$('.use').prop('disabled',false);
    }
    $( ".allgallery" ).on( "click", function() {
        isCheck = $(".allgallery:checked").val()
        if (isCheck != undefined){
            galleryDataTable.$('.use').prop('checked',true);
            galleryDataTable.$('.use').prop('disabled',true);
        }
        else{
            galleryDataTable.$('.use').prop('checked',false);
            galleryDataTable.$('.use').prop('disabled',false);
        }
    });
<!--- +++++++++++++++++ GALLERY +++++++++++++++++ --->
<!--- +++++++++++++++++ TAG +++++++++++++++++ --->
		var tagArr=[];
		for (var i = 0; i < tagData.length; i++) {
		    var j = tagData[i];
		    var priority
		    if(j.PRIORITY === null ){priority=window.changePriority(0,"")}else{priority=window.changePriority(j.PRIORITY,"")}
		    var tagItem=[];
			var use = ""
            if (j.USE != null && j.USE === 1){
                use = "checked"
            }
            var template = "";
            if (j.FILETEMPLATE != null && j.FILETEMPLATE != ''){
                template = j.FILETEMPLATE
            }
            tagItem.push("<input type='checkbox' name='use' class='use' value='"+ j._id+"' "+use+">");
            tagItem.push("<i class='Urlhideshow icon-chevron-down'></i>" +
		            "<input class='urltype' type='hidden' name='urltype' value='TAG'>" +
		            "<input class='tagname' type='hidden' name='tagname' value='"+j.NAME+"'>");
            tagItem.push(j.NAME+"<input type='hidden' name='"+j._id+"' class='"+j._id+"' value='TAG'>");
            tagItem.push("<input name='sitemaptemplate' class='template' type='text' value='"+template+"'>");
            tagItem.push(dateFormat(j['UPDATEDAT'],"yyyy-mm-dd")+'<input type="hidden" class="lastmodify" name="lastmodify" value="'+dateFormat(j['UPDATEDAT'],"yyyy-mm-dd")+'">');
            tagItem.push(priority);
            tagArr.push(tagItem);
		}
		$('.tagTable').html("<table class='createTableData table table-striped'></table>")
		tagDataTable = tagDataTable = $('.tagTable').find('.createTableData').dataTable({
		    "sPaginationType": "full_numbers",
		    "aaData": tagArr,
		    "aoColumns": [
		        { "sTitle": "Use" },
		        { "sTitle": "" },
		        { "sTitle": "Name" },
		        { "sTitle": "Template" },
		        { "sTitle": "Last Modify" },
		        { "sTitle": "Priority" }
		    ]
		});
    if (tagUpdateCheckAll === "YES"){
        $('.alltag').prop('checked',true);
        tagDataTable.$('.use').prop('checked',true);
        tagDataTable.$('.use').prop('disable',true);
    }
    else{
        $('.alltag').prop('checked',false);
        tagDataTable.$('.use').prop('disable',false);
    }
    $( ".alltag" ).on( "click", function() {
        tagCheck = $(".alltag:checked").val()
        if (tagCheck != undefined){
            tagDataTable.$('.use').prop('checked',true);
            tagDataTable.$('.use').prop('disable',true);
        }
        else{
            tagDataTable.$('.use').prop('checked',false);
            tagDataTable.$('.use').prop('disable',false);
        }
    });
<!--- +++++++++++++++++ END TAG +++++++++++++++++ --->
} );
<!--- +++++++++++++++++ MENU NAVI +++++++++++++++++ --->
    function showMenunavi(showMenunavi){
        var menunaviarr = []
	    var isAllMenunavi = false
        for(var i = 0; i < menunaviData.length; i++){
            var j = menunaviData[i];
            if(showMenunavi === j._id){
                menunaviarr=createMenunaviArr(j._id,j.MENU)
                isAllMenunavi = j.ALL
            }
        }
        $('.menunaviTable').html("<table class='createTableData table table-striped'></table>")
        menunaviDataTable = $('.menunaviTable').find('.createTableData').dataTable({
            "sPaginationType": "full_numbers",
            "aaData": menunaviarr,
            "bStateSave": true,
            "aoColumns": [
                { "sTitle": "Use" },
                { "sTitle": "" },
                { "sTitle": "Name" },
                { "sTitle": "Last Modify" },
                { "sTitle": "Priority" }
            ]
        })
        $(".allmenunavi").val(showMenunavi)
        if(isAllMenunavi){
            $(".allmenunavi").prop('checked',true);
            menunaviDataTable.$(".use").prop('disabled',true);
        }else{
            $(".allmenunavi").prop('checked',false);
            menunaviDataTable.$(".use").prop('disabled',false);
        }
    }

    function createMenunaviArr(id,menunaviData){
        var menunaviArr=[];
        for (var i = 0; i < menunaviData.length; i++) {
            var j = menunaviData[i];
            var priority
            if(j.PRIORITY === null ){priority=window.changePriority(0,"")}else{priority=window.changePriority(j.PRIORITY,"")}
            var menunaviItem=[];
            var use = ""
            if (j.USE != null && j.USE === 1){
                use = "checked"
            }
            menunaviItem.push('<input type="checkbox" name="use" class="use" value="'+j._id+'" '+use+'>');
            menunaviItem.push('<i class="Urlhideshow icon-chevron-down"></i>'+
				'<input class="urltype" type="hidden" name="urltype" value="MENUNAVI">'+
				'<input class="menunaviurl" type="hidden" name="menunaviurl" value="'+j.LINKURL+'">');
            menunaviItem.push(j.NAME+'<input type="hidden" name="'+j._id+'" class="'+j._id+'" value="MENUNAVI">');
            menunaviItem.push(dateFormat(j['UPDATEDAT'],"yyyy-mm-dd")+'<input type="hidden" class="lastmodify" name="lastmodify" value="'+dateFormat(j['UPDATEDAT'],"yyyy-mm-dd")+'">');
            menunaviItem.push(priority);
            menunaviArr.push(menunaviItem);

        }
        return menunaviArr
    }
<!--- +++++++++++++++++ END MENU NAVI +++++++++++++++++ --->
<!--- +++++++++++++++++ MENU CONTENT +++++++++++++++++ --->
function showMenucontent(showMenucontent){
	var menucontentarr = []
	var isAllMenucontent = false
	for(var i = 0; i < menucontentData.length; i++){
		var j = menucontentData[i];
		if(showMenucontent === j._id && j.MENU != undefined){
			menucontentarr=createMenucontentArr(j._id,j.MENU)
			isAllMenucontent = j.ALL
		}
	}
	$('.menucontentTable').html("<table class='createTableData table table-striped'></table>")
	menucontentDataTable = $('.menucontentTable').find('.createTableData').dataTable({
		"sPaginationType": "full_numbers",
		"aaData": menucontentarr,
		"bStateSave": true,
		"aoColumns": [
			{ "sTitle": "Use" },
			{ "sTitle": "" },
			{ "sTitle": "Name" },
			{ "sTitle": "Template" },
			{ "sTitle": "Last Modify" },
			{ "sTitle": "Priority" }
		]
	})
	$(".allmenucontent").val(showMenucontent)
	if(isAllMenucontent){
		$(".allmenucontent").prop('checked',true);
		menucontentDataTable.$(".use").prop('disabled',true);
	}else{
		$(".allmenucontent").prop('checked',false);
		menucontentDataTable.$(".use").prop('disabled',false);
	}
}

function createMenucontentArr(id,menucontentInnerData){
	var menucontentArr=[];
	for (var i = 0; i < menucontentInnerData.length; i++) {
		var j = menucontentInnerData[i];
		var priority
		if(j.PRIORITY === null ){priority=window.changePriority(0,"")}else{priority=window.changePriority(j.PRIORITY,"")}
		var menucontentItem=[];
		var use = ""
		if (j.USE != null && j.USE === 1){
			use = "checked"
		}
       var template = "";
	    if (j.FILETEMPLATE != null && j.FILETEMPLATE != ''){
	        template = j.FILETEMPLATE
	    }
		menucontentItem.push('<input type="checkbox" name="use" class="use" value="'+j._id+'" '+use+'>');
		menucontentItem.push('<i class="Urlhideshow icon-chevron-down"></i>'+
			'<input class="urltype" type="hidden" name="urltype" value="'+j.TYPE+'">'+
			'<input class="menucontentname" type="hidden" name="menucontentname" value="'+j.NAME+'">');
		menucontentItem.push(j.NAME+'<input type="hidden" name="'+j._id+'" class="'+j._id+'" value="MENUCONTENT">');
		menucontentItem.push('<input name="sitemaptemplate" type="text" value="'+template+'" class="template">');
		menucontentItem.push(dateFormat(j['UPDATEDAT'],"yyyy-mm-dd")+'<input type="hidden" class="lastmodify" name="lastmodify" value="'+dateFormat(j['UPDATEDAT'],"yyyy-mm-dd")+'">');
		menucontentItem.push(priority);
		menucontentArr.push(menucontentItem);


	}
	return menucontentArr
}
<!--- +++++++++++++++++ END MENU CONTENT +++++++++++++++++ --->
<!--- POST --->
function createPostArr(postData){
    var Arr=[];
    for (var i = 0; i < postData.length; i++) {
        var j = postData[i];
        var priority
        var use = ""
        if(j.PRIORITY === null ){priority=window.changePriority("0","")}else{priority=window.changePriority(j.PRIORITY,"")}
        if (j.USE != undefined && j.USE === 1){
            use = "checked"
        }
        var postItem=[];
        postItem.push("<input type='checkbox' name='use' class='use' value='"+j._id+"' "+use+">");
        postItem.push("<i class='Urlhideshow icon-chevron-down'></i>" +
	            "<input type='hidden' name='urltype' class='urltype' value='POST'>" +
		        "<input type='hidden' name='sitemaptemplate' class='template' value='"+j.TEMPLATE+"'>" +
		        "<input type='hidden' name='sitemappattern' class='sitemappattern' value='"+j.PATTERN+"'>" +
                "<input type='hidden' name='slugurl' class='slugurl' value='"+j.SLUGURL+"'>");
        postItem.push(j.TITLE+"<input type='hidden' name='"+ j._id+"' class='"+ j._id+"' value='POST'>");
        postItem.push(dateFormat(j['UPDATEDAT'],"yyyy-mm-dd")+'<input type="hidden" class="lastmodify" name="lastmodify" value="'+dateFormat(j['UPDATEDAT'],"yyyy-mm-dd")+'">');
        postItem.push(priority);
        Arr.push(postItem);
    }
    return Arr
}
$(document).ready(function() {
	$( ".allPOST" ).on( "click", function() {
	    postCheck = $(".allPOST:checked").val()
	    if (postCheck != undefined){
	        postDataTable.$('.use').prop('checked',true);
            postDataTable.$('.use').prop('disabled',true);
	    }
	    else{
            postDataTable.$('.use').prop('checked',false);
            postDataTable.$('.use').prop('disabled',false);
	    }
	});
});
<!--- POST --->
<!--- POST & PAGE --->
    function reportkey(node){
        category = node.data.key;
	    treeEle = node.tree.$tree
	    excTable = $(treeEle).parent().parent().find(".excTable").val()
	    var ArrGenTableData=[]
        $('.'+excTable).find('.genTable').html("<table class='createTableData table table-striped'></table>")
	    if (excTable === "allPostTable"){
		    var noSaveArr = true
            newpostcategory = category
		    if (oldpostcategory === undefined){
                oldpostcategory=category
		    }
		    if (postDataTable != undefined){
                postData = window.updatePost(oldpostcategory,postDataTable,'POST',postData)
                noSaveArr = false
		    }
		    var isAllPost=false
	        for (var i = 0; i < postData.length; i++) {
	            var j = postData[i];
	            if(j.CATEGORYID === category){
	                ArrGenTableData = createPostArr(j.POST)
                    isAllPost = j.ALL
	            }
	        }
            postDataTable = $('.'+excTable).find('.createTableData').dataTable({
                "sPaginationType": "full_numbers",
                "aaData": ArrGenTableData,
                "aoColumns": [
                    { "sTitle": "Use" },
                    { "sTitle": "" },
                    { "sTitle": "Title" },
                    { "sTitle": "Last Modify" },
                    { "sTitle": "Priority" }
                ]
            });
            $(".allPOST").val(category)
            if(isAllPost){
                $(".allPOST").prop('checked',true);
                postDataTable.$(".use").prop('disabled',true);
            }else{
                $(".allPOST").prop('checked',false);
                postDataTable.$(".use").prop('disabled',false);
            }
            if (noSaveArr){
                postData = window.updatePost(oldpostcategory,postDataTable,'POST',postData)
            }
		    oldpostcategory = category
	    }
	    else if (excTable === "allPageTable"){
	    }
    }
<!--- POST & PAGE --->
<!--- SAVE SITEMAP --->
        $(".saveSitemap").click(function(){
            $('#showgenflash').html('');
            $('.button-action').addClass('hidden');
            $('#saveprogress').removeClass('hidden');
            menunaviData = window.updateMenunavi(newmenunavi,menunaviDataTable,menunaviData,"menunavi")
            menucontentData = window.updateMenunavi(newmenucontent,menucontentDataTable,menucontentData,"menucontent")
	        menucontentData = window.reloadMenucontent(menucontentData)
            categoryData = window.updateContent(categoryDataTable,'CATEGORY',categoryData)
            htmlData = window.updateContent(htmlDataTable,'HTML',htmlData)
            postData = window.updatePost(newpostcategory,postDataTable,'POST',postData)
	        postData = window.reloadPost('POST',postData)
            galleryData = window.updateContent(galleryDataTable,'GALLERY',galleryData)
            tagData = window.updateContent(tagDataTable,'TAG',tagData)
            window.saveSitemap(menunaviData,menucontentData,categoryData,htmlData,postData,galleryData,tagData)
        });
        $(".updateSitemap").click(function(){
	        $('#showgenflash').html('');
            $('.button-action').addClass('hidden');
            $('#saveprogress').removeClass('hidden');
            menunaviData = window.updateMenunavi(newmenunavi,menunaviDataTable,menunaviData,"menunavi")
	        menucontentData = window.updateMenunavi(newmenucontent,menucontentDataTable,menucontentData,"menucontent")
	        menucontentData = window.reloadMenucontent(menucontentData)
            categoryData = window.updateContent(categoryDataTable,'CATEGORY',categoryData)
            htmlData = window.updateContent(htmlDataTable,'HTML',htmlData)
            postData = window.updatePost(newpostcategory,postDataTable,'POST',postData)
	        postData = window.reloadPost('POST',postData)
            galleryData = window.updateContent(galleryDataTable,'CATEGORY',galleryData)
            tagData = window.updateContent(tagDataTable,'TAG',tagData)
            window.saveSitemap(menunaviData,menucontentData,categoryData,htmlData,postData,galleryData,tagData)
        });
<!--- SAVE SITEMAP --->
</script>


