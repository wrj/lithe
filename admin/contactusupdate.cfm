<cfinclude template="include/header.cfm">
<cfif isdefined('ERROR')>
    <cfset contactus = getstoredata()>
    <cfif structIsEmpty(contactus)>
        <cfset contactus = mongonew('contactus')>
    </cfif>
    <cfelse>
    <cfset objdata = MongoCollectionfindone(application.applicationname,'contactus',{"_id"= newid(key)})>
    <cfset contactus = mongonew("contactus",objdata)>
</cfif>
	<cfset contactus['FILEPATH'] = deserializeJSON(contactus['FILEPATH'])>
<cfoutput>
    <div class="row-fluid">
    <legend>Edit Contactus - #contactus['TITLE']#</legend>
    #startform(action='contactuscontroller')#
        #hiddenfield(name='mode',value='update')#
        #hiddenfield(name='key',value=key)#
        <cfinclude template="contactusform.cfm">
        #button(label='Back',onclick='backmenu()')#
    #endform()#
    </div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<script type="text/javascript">
    function backmenu(){
        window.location = seturlbase()+'/contactus.cfm';
    }
</script>