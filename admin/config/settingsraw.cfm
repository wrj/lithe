<!---	Path Folder Upload file--->
<cfset set('pathupload','#expandPath("/")#assets/upload/')>
<!---    Path Folder in web root--->
<cfset set('pathuploadfolder','/assets/upload/')>
<!---Path Temp Upload--->
<cfset set('temppathuploadfolder','#expandPath("/")#temp/')>
<cfset set('temppathfolder','/temp/')>

<!---  Asset Setting --->
<cfset set('imageformat','replaceimageformat')>
<cfset set('videoformat','replacevideoformat')>
<cfset set('fileformat','replacefileformat')>
<!---Size File Upload k--->
<cfset set('filesizeimage','replacefilesizeimage')/>
<cfset set('filesizevideo','replacefilesizevideo')/>
<cfset set('filesizefile','replacefilesizefile')/>

<!---    Form Label--->
<cfset set('labelbtnadd','replacelabelbtnadd')>
<cfset set('labelbtnedit','replacelabelbtnedit')>
<!---    Datatable Label--->
<cfset set('labeledit','replacelabeledit')>
<cfset set('labeldelete','replacelabeldelete')>

<!---Email--->
<cfset set('emailhost','replaceemailhost')>
<cfset set('emailssl','replaceemailssl')>
<cfset set('emailport',replaceemailport)>
<cfset set('emailuser','replaceemailuser')>
<cfset set('emailpass','replaceemailpass')>
<cfset set('emailcontactaddress','replaceemailcontactaddress')>
<!---Template--->
<cfset set('template','replacetemplate')>

<!---Admin--->
<cfset set('adminuser','replaceadminuser')>
<cfset set('adminpass','replaceadminpass')>

<!---Language--->
<cfset set('weblanguage','replaceweblanguage')>

<!---Shopping Cart--->
<cfset set('shoppingcart','replaceshoppingcart')>

<!---Log--->
<cfset set('logaccess','replacelogaccess')>

<!---Site--->
<cfset set('sitetitle','replacesitetitle')>
<cfset set('siteaddress','replacesiteaddress')>
<cfset set('pathfrontend','replacepathfrontend')>
<cfset set('dateformat','replacedateformat')>
<cfset set('timeformat','replacetimeformat')>


<!---ImageDefaultSize--->
<cfset set('slideshowwidth','replaceslideshowwidth')>
<cfset set('slideshowheight','replaceslideshowheight')>
<cfset set('largeimagewidth','replacelargeimagewidth')>
<cfset set('largeimageheight','replacelargeimageheight')>
<cfset set('thumbnailwidth','replacethumbnailwidth')>
<cfset set('thumbnailheight','replacethumbnailheight')>
<cfset set('categoryimagewidth','replacecategoryimagewidth')>
<cfset set('categoryimageheight','replacecategoryimageheight')>
<cfset set('menuimagewidth','replacemenuimagewidth')>
<cfset set('menuimageheight','replacemenuimageheight')>
<cfset set('otherimage','replaceotherimage')>
