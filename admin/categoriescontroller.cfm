<cfinclude template="system/loadplugins.cfm">
<cfset structDelete(form,'FIELDNAMES')>
<cfswitch expression="#mode#">
    <cfcase value="create">
        <cfset parentid = form['CATEGORY']>
        <cfset structDelete(form,'mode')>
        <cfset structDelete(form,'CATEGORY')>
        <cfset result = mongovalidate('category','create',form)>
        <cfif result['result'] eq true>
            <cfset result['value']['USER'] = dbref('user',session['userid'])>
            <cfif parentid neq 0>
                <cfset result['value']['PARENT'] = dbref('category',parentid)>
            </cfif>
            <cfif result['value']['IMAGEREF'] neq ''>
                <cfset result['value']['IMAGEREF'] = result['value']['IMAGEREF']>
            </cfif>
            <cfset MongoCollectioninsert(application.applicationname,'category',result['value'])>
            <cfset flashinsert('success','Create category complete')>
            <cflocation url="categories.cfm" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error creating the category.')>
            <cfset result['value']['PARENT'] = parentid>
            <cfset storedata(result)>
            <cflocation url="categoriesnew.cfm" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="update">
        <cfset keyid = key>
        <cfset parentid = form['CATEGORY']>
	    <cfset displaystart = form['DISPLAYSTART']>
        <cfset structDelete(form,'KEY')>
        <cfset structDelete(form,'MODE')>
        <cfset structDelete(form,'CATEGORY')>
	    <cfset structDelete(form,'DISPLAYSTART')>
        <cfset result = mongovalidate('category','update',form)>
        <cfif result['result'] eq true>
            <cfset updateobj = result['value']>
            <cfif form['IMAGEREF'] neq ''>
                <cfif form["IMAGEREF"] neq "remove">
                    <cfset updateobj['IMAGEREF'] = form['IMAGEREF']>
                <cfelse>
                    <cfset updateobj['IMAGEREF'] = "">
                </cfif>
            </cfif>
            <cfset olddata = MongoCollectionfindone(application.applicationname,'category',{'_id'=newid(keyid)})>
	        <cfset oldparent=0>
	        <cfif structKeyExists(olddata,'PARENT')>
		        <cfset oldparent=olddata['PARENT'].getId().toString()>
	        </cfif>
<!---	    START CHANGE IN MENU     --->
		    <cfinclude template="changemenuurl.cfm">
		    <cfset menureturn = ChangeMenuUrl(keyid,olddata.TEMPLATE,form.TEMPLATE)>
<!---	    END CHANGE IN MENU     --->
            <cfset dataupdate = mongomapvalue('category',olddata,updateobj,'default')>
            <cfset dataupdate['USER'] = dbref('user',session['userid'])>
            <cfif parentid neq 0>
                <cfset dataupdate['PARENT'] = dbref('category',parentid)>
            <cfelse>
                <cfset rc = StructDelete(dataupdate, "PARENT", "True")>
            </cfif>
	        <cfset cateidref = dbref('category',dataupdate["_id"].toString())>
	        <cfset checkparent = MongoCollectioncount(application.applicationname,'category',{"PARENT"=cateidref})>
	        <cfif oldparent IS parentid OR checkparent EQ 0>
		        <cfset structDelete(dataupdate,'mode')>
		        <cfset MongoCollectionsave(application.applicationname,'category',dataupdate)>
		        <cfset flashinsert('success','Update category complete')>
		        <cflocation url="categories.cfm?start=#displaystart#" addtoken="false">
	        <cfelse>
		        <cfset result['value']['PARENT'] = parentid>
		        <cfset flashinsert('error','This category have parent. Can not change parent category.')>
		        <cfset storedata(result)>
		        <cflocation url="categoriesupdate.cfm?key=#keyid#&start=#displaystart#" addtoken="false">
	        </cfif>
        <cfelse>
            <cfset result['value']['PARENT'] = parentid>
            <cfset flashinsert('error','There was an error update the category.')>
            <cfset storedata(result)>
            <cflocation url="categoriesupdate.cfm?key=#keyid#&start=#displaystart#" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="delete">
        <cfset cateid = dbref('category',key)>
        <cfset checkparent = MongoCollectioncount(application.applicationname,'category',{"PARENT"=cateid})>
        <cfset result = structNew()>
        <cfif checkparent neq 0>
            <cfset result['result'] = 'false'>
            <cfset result['message'] = "This category have parent.Can't delete.">
        <cfelse>
            <!---            Delete post and page in category--->
            <cfset defaultsearch = structnew()>
            <cfset defaultsearch["CATEGORY"] = cateid>
            <cfset MongoCollectionremove(application.applicationname,'post',defaultsearch)>
            <cfset MongoCollectionremove(application.applicationname,'category',{'_id'=newid(key)})>
            <cfset result['result'] = 'true'>
            <cfset result['message'] = "Delete category complete">
        </cfif>
        <cfoutput>
            #serializeJSON(result)#
        </cfoutput>
    </cfcase>

</cfswitch>