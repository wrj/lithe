<cfinclude template="system/loadplugins.cfm">
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <cfinclude template="include/stylesheet.cfm">
    <title><cfoutput>#application.applicationname#</cfoutput></title>
    <cfinclude template="include/javascript.cfm">
    <cfoutput>
        #stylesheetinclude('datatable-clean')#
        #javaScriptInclude("jquery.dataTables,createdatatable,deletedialog")#
    </cfoutput>
</head>
<body>
<div class="container">
    <cfif type eq 'image'>
        <cfinclude template="imagebrowseck.cfm">
    <cfelseif type eq 'video'>
        <cfinclude template="videosbrowse.cfm">
    <cfelseif type eq 'file'>
        <cfinclude template="filesbrowse.cfm">
    <cfelse>
        <cfinclude template="linksbrowseck.cfm">
    </cfif>
</div>
</body>
</html>