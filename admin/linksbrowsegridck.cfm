<cfinclude template="system/loadplugins.cfm">
<cfset sortlabel = ""/>
<cfset sortvalue = 1/>
<cfset searchtxt = ""/>
<cfset listColumns = "id,TITLE,UPDATEDAT,CREATEDAT" />
<cfif not isDefined('iSortCol_0')>
    <cfset iSortCol_0 = 0>
    <cfset sSortDir_0 = 1>
    <cfset sSearch = "">
    <cfset sEcho = 1>
    <cfset iDisplayStart =0>
    <cfset iDisplayLength = 10>
</cfif>
<cfset sortlabel = listGetAt(listColumns,(Val(iSortCol_0) + 1))/>
<cfif sSortDir_0 eq "desc">
    <cfset sortvalue = -1/>
</cfif>
<cfset sortst = structNew()>
<cfset sortst[sortlabel] = sortvalue>
<cfoutput>
    <cfset defaultsearch = structnew()>
    <cfset defaultsearch["CATEGORY"] = dbref('category',category)>
<!---    <cfset defaultsearch["USER"] = dbref('user',session['userid'])>--->
    <cfset defaultsearch["TYPECONTENT"] = val(typecontent)>
    <cfset notempty = structNew()>
    <cfset notempty["$ne"] = ''>
    <cfset defaultsearch["TITLE.#session['language']#"] = notempty>
    <cfif sSearch neq "">
        <cfset searchtxt = structnew()>
        <cfset searchtxt["$regex"] = ".*#Trim(sSearch)#.*">
        <cfset searchtxt["$options"] = "i">
        <cfset fieldarray = arraynew()>
        <cfset fieldsearch = structNew()>
        <cfset fieldsearch["TITLE.#session['language']#"] = searchtxt>
        <cfset arrayappend(fieldarray,fieldsearch)>
        <cfset defaultsearch["$or"] = fieldarray>
    </cfif>
    <cfset mydatatotal = MongoCollectioncount(datasource=application.applicationname,collection='post',query=defaultsearch)>
    <cfset mydata = MongoCollectionFind(datasource=application.applicationname,collection='post',query=defaultsearch,skip=iDisplayStart,size=iDisplayLength,sort=sortst)>
    <cfsavecontent variable="sourcedatatable">
        {"sEcho": #val(sEcho)#,
    "iTotalRecords": #mydatatotal#,
    "iTotalDisplayRecords": #mydatatotal#,
    "aaData": [
        <cfset i = 1/>
        <cfloop array="#mydata#" index="rowitem">
            [
            <cfloop list="#listColumns#" index="thisColumn">
                <cfif thisColumn neq listFirst(listColumns)>,</cfif>
                <cfif ListFind("UPDATEDAT,CREATEDAT",thisColumn,',') gt 0>
                    "#jsStringFormat(DateFormat(rowitem[thisColumn],'dd/mm/yy'))#"
                <cfelseif thisColumn eq 'id'>
                    "#rowitem['CATEGORY'].fetch()['TEMPLATE']#|#checkdata(rowitem,'SLUG')#|#jsStringFormat(rewritejsontext(i18n(checkdata(rowitem,'TITLE'))))#|#rowitem['CATEGORY'].fetch()['_id'].toString()#"
                <cfelse>
                    "#jsStringFormat(rewritejsontext(i18n(checkdata(rowitem,thisColumn))))#"
                </cfif>
            </cfloop>
            ]
            <cfif i neq arraylen(mydata)>,</cfif>
            <cfset i++/>
        </cfloop>]}
    </cfsavecontent>
    <cfoutput>
        #sourcedatatable#
    </cfoutput>
</cfoutput>

