<cfinclude template="system/loadplugins.cfm">
<cfset structDelete(form,'FIELDNAMES')>
<cfset structDelete(form,'FILE_UPLOAD')>
<cfswitch expression="#mode#">
    <cfcase value="create">
        <cfset structDelete(form,'mode')>
	    <cfif form.newfilename IS "">
			<cfset form.newfilename=form.oldfilename>
	    </cfif>
        <cfset result = mongovalidate('image','create',form)>
        <cfif result['result'] eq true>
            <cfset result['value']['USER'] = dbref('user',session['userid'])>
            <cfset filestruct = movefile(form['NEWFILENAME'],'image/')>
            <cfset result['value']['NEWFILENAME'] = filestruct['newserverfile']>
<!---            Category--->
	        <cfset result['value']['CATEGORY'] = dbref('categoryimage',result['value']['CATEGORY'])>
            <cfset MongoCollectioninsert(application.applicationname,'image',result['value'])>
            <cfset flashinsert('success','Create image complete')>
            <cflocation url="images.cfm" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error creating the image.')>
            <cfset storedata(result)>
            <cflocation url="imagesnew.cfm" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="update">
        <cfset keyid = key>
	    <cfset displaystart = form['DISPLAYSTART']>
        <cfset structDelete(form,'KEY')>
        <cfset structDelete(form,'MODE')>
	    <cfset structDelete(form,'DISPLAYSTART')>
        <cfset oldfilename = form['oldfilename']>
        <cfset structDelete(form,'oldfilename')>
        <cfset result = mongovalidate('image','update',form)>
        <cfif result['result'] eq true>
            <cfset updateobj = result['value']>
            <cfset olddata = MongoCollectionfindone(application.applicationname,'image',{'_id'=newid(keyid)})>
            <cfset dataupdate = mongomapvalue('image',olddata,updateobj,'default')>
            <cfset dataupdate['USER'] = dbref('user',session['userid'])>
<!---            File on server--->
            <cfif form['NEWFILENAME'] neq ''>
                <cfset filestruct = movefile(form['NEWFILENAME'],'image/',oldfilename)>
                <cfset dataupdate['NEWFILENAME'] = filestruct['newserverfile']>
            <cfelse>
                <cfset dataupdate['NEWFILENAME'] = oldfilename>
            </cfif>
	        <!---            Category--->
	        <cfset dataupdate['CATEGORY'] = dbref('categoryimage',result['value']['CATEGORY'])>
	        <!---            startdate--->
            <cfset MongoCollectionsave(application.applicationname,'image',dataupdate)>
            <cfset flashinsert('success','Update image complete')>
            <cflocation url="images.cfm?category=#result['value']['CATEGORY']#&start=#displaystart#" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error update the image.')>
            <cfset storedata(result)>
            <cflocation url="imagesupdate.cfm?key=#keyid#" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="delete">
        <cfset image = MongoCollectionfindone(application.applicationname,'image',{'_id'=newid(key)})>
        <cfset temp = deletefile(image['NEWFILENAME'],'image/')>
        <cfset MongoCollectionremove(application.applicationname,'image',{'_id'=newid(key)})>
        <cfset result = structNew()>
        <cfset result['result'] = 'true'>
        <cfset result['message'] = "Delete image complete">
        <cfoutput>
            #serializeJSON(result)#
        </cfoutput>
    </cfcase>
</cfswitch>