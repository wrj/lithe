<!---
  Created with IntelliJ IDEA.
  User: arunwatd
  Date: 10/2/12
  Time: 9:33 AM
  To change this template use File | Settings | File Templates.
--->
<cfparam name="attributes.field" type="string" default="">
<cfparam name="attributes.size" type="string" default="">
<cfparam name="attributes.tool" type="boolean" default="true">
<cfparam name="attributes.toolsize" default="160">
<cfparam name="attributes.tableid" type="string" default="usetable">
<cfoutput>
    <cfif thistag.executionmode eq "start">
        <cfset ifield = 1>
        <table class="table" id="#attributes.tableid#">
            <thead>
                <tr>
                    <cfloop list="#attributes.field#" index="item">
                        <cfset width = "">
                        <cfif attributes.size neq "">
                            <cfif listgetat(attributes.size,ifield,',') neq 0>
                                <cfset width = "#listgetat(attributes.size,ifield,',')#px">
                            </cfif>
                        </cfif>
                        <th width="#width#">#item#</th>
                        <cfset ifield++>
                    </cfloop>
                    <cfif attributes.tool>
                        <th width="#attributes.toolsize#px;"></th>
                    </cfif>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="1">
                        Load
                    </td>
                </tr>
            </tbody>
        </table>
    </cfif>
</cfoutput>