<cfinclude template="system/loadplugins.cfm">
<cfset sortlabel = ""/>
<cfset sortvalue = 1/>
<cfset searchtxt = ""/>
<cfset listColumns = "NEWFILENAME,FILENAME" />
<cfset listColumnssort = "NEWFILENAME,FILENAME,CREATEDAT" />
<cfif not isDefined('iSortCol_0')>
    <cfset iSortCol_0 = 0>
    <cfset sSortDir_0 = 1>
    <cfset sSearch = "">
    <cfset sEcho = 1>
    <cfset iDisplayStart =0>
    <cfset iDisplayLength = 10>
</cfif>
<cfset sortlabel = listGetAt(listColumnssort,(Val(iSortCol_0) + 1))/>
<cfif sSortDir_0 eq "desc">
    <cfset sortvalue = -1/>
</cfif>
<cfset sortst = structNew()>
<cfset sortst[sortlabel] = sortvalue>
<cfoutput>
    <cfset defaultsearch = structnew()>
    <cfif sSearch neq "">
        <cfset searchtxt = structnew()>
        <cfset searchtxt["$regex"] = ".*#Trim(sSearch)#.*">
        <cfset searchtxt["$options"] = "i">
        <cfset searchst = structNew()>
        <cfset fieldarray = arraynew()>
        <cfset fieldsearch = structNew()>
        <cfset fieldsearch['FILENAME'] = searchtxt>
        <cfset arrayappend(fieldarray,fieldsearch)>
        <cfset defaultsearch["$or"] = fieldarray>
    <cfelse>
	    <cfset defaultsearch["CATEGORY"] = dbref('categoryimage',category)>
    </cfif>
    <cfset mydatatotal = MongoCollectioncount(datasource=application.applicationname,collection='image',query=defaultsearch)>
    <cfset mydata = MongoCollectionFind(datasource=application.applicationname,collection='image',query=defaultsearch,skip=iDisplayStart,size=iDisplayLength,sort=sortst)>
    <cfsavecontent variable="sourcedatatable">
        {"sEcho": #val(sEcho)#,
    "iTotalRecords": #mydatatotal#,
    "iTotalDisplayRecords": #mydatatotal#,
    "aaData": [
        <cfset i = 1/>
        <cfloop array="#mydata#" index="rowitem">
            [
            <cfloop list="#listColumns#" index="thisColumn">
                <cfif thisColumn neq listFirst(listColumns)>,</cfif>
                <cfif ListFind("createdat,updatedat",thisColumn,',') gt 0>
                    "#jsStringFormat(DateFormat(rowitem[thisColumn],'dd/mm/yy'))#"
                <cfelseif thisColumn eq 'NEWFILENAME'>
                    "<img src='#get('pathuploadfolder')#image/#rowitem[thisColumn]#' class='img-rounded img-tablethumb'/>"
                <cfelse>
                    "#jsStringFormat(rewritejsontext(checkdata(rowitem,thisColumn)))#"
                </cfif>
            </cfloop>
            ,"<a onclick=\"javascript:goedit('#rowitem['_id'].toString()#')\" href=\"javascript:void(0)\" class=\"btn\">#get('labeledit')#</a> <a onclick=\"javascript:deletedata('#rowitem['_id'].toString()#','#rewritetext(checkdata(rowitem,ListFirst(listColumns)))#')\" href=\"javascript:void(0)\" class=\"btn\">#get('labeldelete')#</a>"]
            <cfif i neq arraylen(mydata)>,</cfif>
            <cfset i++/>
        </cfloop>]}
    </cfsavecontent>
    <cfoutput>
        #sourcedatatable#
    </cfoutput>
</cfoutput>