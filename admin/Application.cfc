<cfcomponent>
    <cfif fileExists("#expandPath('./')#config/app.cfm")>
        <cfinclude template="config/app.cfm">
    </cfif>


    <cffunction name="onApplicationStart" returnType="boolean" output="false">
        <cfset settingfile = "#expandPath('./')#config/install.log">
        <cfif fileExists(settingfile)>
            <cfset application['rootpath'] = listlast(getDirectoryFromPath(getCurrentTemplatePath()),'/') />
            <cfinclude template="events/onapplicationstart.cfm" runonce="true">
            <cfinclude template="system/registerdatasource.cfm" runonce="true">
            <cfinclude template="system/systemfunction.cfc" runonce="true">
            <cfinclude template="config/settings.cfm" runonce="true">
        </cfif>
        <cfreturn true>
    </cffunction>

    <cffunction name="OnRequestStart" returntype="boolean" output="false">
        <cfargument name = "targetPage" type="String" required=true/>
        <cfset settingfile = "#expandPath('./')#config/install.log">
        <cfif fileExists(settingfile)>
            <cfif structKeyExists(url,"reload") && url.reload eq true>
                <cfset structDelete(application,'blog')>
                <cfif MongoIsvalid(application.applicationname)>
                    <cfset MongoDeregister(application.applicationname)>
                </cfif>
                <cfinclude template="system/registerdatasource.cfm" runonce="true">
                <cfinclude template="system/systemfunction.cfc" runonce="true">
                <cfinclude template="config/settings.cfm" runonce="true">
                <cfbreak/>
            </cfif>
            <cfinclude template="events/onrequeststart.cfm" runonce="true">
        <cfelse>
            <cfset pagename = listlast(arguments.targetPage,'/')>
            <cfset installpage = 'install.cfm,installgenerate.cfm'>
            <cfif listfind(installpage,pagename,',') eq 0>
                <cflocation url="install.cfm" addtoken="false">
            </cfif>
        </cfif>
        <cfif listLast(arguments['targetPage'],'/') eq "adminlogin.cfm" OR listLast(arguments['targetPage'],'/') eq "login.cfm">
            <cfif structKeyExists(session, "username") && session["username"] neq "">
                <cfif structKeyExists(session, "rule") && session['rule'] eq "superadmin">
                    <cflocation url="users.cfm" addtoken="false">
                <cfelseif structKeyExists(session, "rule") && session['rule'] neq "">
                    <cflocation url="posts.cfm" addtoken="false">
                </cfif>
            </cfif>
        </cfif>
        <cfreturn true>
    </cffunction>

<!--- The web.xml welcome-file-list includes index.cfm.
To allow web browsing, specify index.cfm in This.welcomFileList. --->
    <cfset This.welcomeFileList="index.cfm">

    <cffunction name="onMissingTemplate">
        <cfargument name="targetPage" type="string" required=true/>
<!--- Use a try block to catch errors. --->
        <cftry>
<!--- Log all errors. --->
            <cflog type="error" text="Missing template: #Arguments.targetPage#">
<!--- Display an error message. --->
	        <cfoutput>
		        <style type="text/css">
		        ##error {
		            color: ##333333;
			        font-family: Arial,sans-serif;
			        font-size: 14px;
			        line-height: 1.42857;
			        font-family: Arial,sans-serif;
			        margin: 125px 0 50px;
			        text-align: center;
		        }
			        ##error .icon.icon-404 {
			        background-position: 0 -275px;
		        }
			        ##error .icon {
		            background: url("http://#CGI.HTTP_HOST#/private/images/errors.png") no-repeat scroll 0 0 / 200px 800px transparent;
			        display: block;
			        height: 130px;
			        margin: 0 auto 50px;
			        width: 200px;
		        }
			        ##error h1 {
			        font-size: 24px;
			        font-weight: normal;
			        line-height: 1.25;
			        margin: 20px 0 10px;
			        padding: 0;
		        }
		        </style>
		        <div role="main" id="content">
			        <div class="404" id="error">
				        <span class="icon icon-404"></span>
				        <h1>Oops, you've found a dead link.</h1>
				        <p>Use the links at the top to get back.</p>
			        </div>
		        </div>
	        </cfoutput>
            <cfreturn true />
<!--- If an error occurs, return false and the default error
    handler will run. --->
            <cfcatch>
                <cfreturn false />
            </cfcatch>
        </cftry>
    </cffunction>

    <cffunction name="onError">
        <cfargument name="Exception" required=true/>
        <cfargument type="String" name="EventName" required=true/>
<!---        <cfdump var="#arguments.Exception#">--->
        <!--- Log all errors. --->
        <cflog file="#This.Name#" type="error"
                text="Event Name: #Arguments.Eventname#" >
        <cflog file="#This.Name#" type="error"
                text="Message: #Arguments.Exception.message#">
        <cflog file="#This.Name#" type="error"
                text="Root Cause Message: #Arguments.Exception.rootcause.message#">
        <!--- Display an error message if there is a page context. --->
        <cfif NOT (Arguments.EventName IS "onSessionEnd") OR
        (Arguments.EventName IS "onApplicationEnd")>
	        <cfset errorfile="#dateFormat(now(),"dd-mm-yyyy")##timeFormat(now(),"-HH-MM-ss-L")#-log.html">
	        <cfset filepath="#expandPath("/logs")#">
	        <cfif NOT DirectoryExists(filepath)>
		        <cfdirectory action="create" mode="777"  directory="#filepath#">
	        </cfif>
	        <cfif fileexists("#filepath#/#errorfile#")>
		        <cfset errorfile="#dateFormat(now(),"dd-mm-yyyy")##timeFormat(now(),"-HH-MM-ss-L")#-2-log.html">
	        </cfif>
	        <cfsavecontent variable="savedump">
		        <cfdump var="#Arguments.Exception#" expand="yes">
	        </cfsavecontent>
	        <cffile
			        action = "write"
			        file = "#filepath#/#errorfile#"
			        output = "#savedump#"
			        addNewLine = "yes"
			        charset = "utf-8"
			        fixnewline = "no"
			        mode = "777">
            <cfoutput>
	            <style type="text/css">
	            ##error {
	                color: ##333333;
		            font-family: Arial,sans-serif;
		            font-size: 14px;
		            line-height: 1.42857;
		            font-family: Arial,sans-serif;
		            margin: 125px 0 50px;
		            text-align: center;
	            }
	            ##error .icon.icon-500 {
		            background-position: 0 -413px;
	            }
	            ##error .icon {
	                background: url("http://#CGI.HTTP_HOST#/private/images/errors.png") no-repeat scroll 0 0 / 200px 800px transparent;
		            display: block;
		            height: 130px;
		            margin: 0 auto 50px;
		            width: 200px;
	            }
	            ##error h1 {
		            font-size: 24px;
		            font-weight: normal;
		            line-height: 1.25;
		            margin: 20px 0 10px;
		            padding: 0;
	            }
	            ##error a{
                    color:##0088CC;
	            }
	            ##error a:hover{
		            text-decoration: underline;
	            }
	            </style>
	            <div role="main" id="content">
		            <div class="500" id="error">
			            <span class="icon icon-500"></span>
			            <h1>Oops, the system encountered a problem.</h1>
			            <p><a href="http://#CGI.HTTP_HOST#/logs/#errorfile#">View more error detail</a></p>
		            </div>
	            </div>
            </cfoutput>
        </cfif>
    </cffunction>

</cfcomponent>

