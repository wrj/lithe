<cfinclude template="include/header.cfm">
<cfimport taglib="lib" prefix="ui">
<cfoutput>
    <div class="row-fluid">
        <div class="span12">
            <ui:datatable field="Name,Email"/>
        </div>
    </div>
    <div class="modal hide fade bigModal" id="delModal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Delete Member</h3>
        </div>
        <div class="modal-body">
            <p id="shownamedelete"></p>
            <h2>Please note</h2>
            <p>After you delete an member.It'll not collect usage data anymore.</p>
            <input type="hidden" id="iddelete" name="iddelete">
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <a onclick="confirmdelete('members');" class="btn btn-danger">OK</a>
        </div>
    </div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<cfoutput>#javaScriptInclude("jquery.dataTables,createdatatable,deletedialog")#</cfoutput>
<script type="text/javascript">
    createdatatable('usetable','membersgrid','');

    function goedit(id)
    {
        window.location = "membersupdate.cfm?key="+id;
    }
</script>
