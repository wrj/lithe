<cfinclude template="system/loadplugins.cfm">
<cfset logout()>
<cfset passencode = hash(password,"SHA")>
<cfset stsearch = structNew()>
<cfset stsearch['USERNAME'] = username>
<cfset stsearch['PASSWORD'] = passencode>
<cfset userdata = MongoCollectionfindone(application.applicationname,'user',stsearch)>
<cfif isNull(userdata)>
    <cfset flashinsert('error','Username or Password miss match')>
    <cflocation url="login.cfm" addtoken="false">
<cfelse>
    <cfset setting = MongoCollectionfindone(application.applicationname,'usersetting',{"USER"=dbref('user',userdata['_id'].toString())})>
    <cfset session['username'] = userdata['USERNAME']>
    <cfset session['userid'] = userdata['_id'].toString()>
    <cfset session['secret'] = userdata['SECRET']>
    <cfset session['cart'] = get('shoppingcart')>
    <cfset session['rule'] = userdata['rule']>
    <cfset session['language'] = listFirst(get('weblanguage'),',')>
    <cflocation url="posts.cfm" addtoken="false">
</cfif>