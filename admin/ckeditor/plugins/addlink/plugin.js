CKEDITOR.plugins.add( 'addlink',
	{
		icons:'addlink',
		init: function( editor )
		{
			editor.addCommand( 'addlinkDialog',new CKEDITOR.dialogCommand( 'addlinkDialog' ) );
			editor.ui.addButton( 'Addlink',
			{
				label: 'Insert Link',
				command: 'addlinkDialog',
				toolbar: 'insert'
			});
			CKEDITOR.dialog.add( 'addlinkDialog', function( editor )
			{
				return {
					title : 'Link Properties',
					minWidth : 400,
					minHeight : 200,
					contents :
					[
						{
							id : 'tab1',
							label : 'Basic Settings',
							elements :
							[
								{
									type : 'text',
									id : 'titletext',
									label : 'Title',
									validate : CKEDITOR.dialog.validate.notEmpty( "title field cannot be empty" )
								},
								{
									type : 'text',
									id : 'slugtext',
									label : 'SLUG'
								},
								{
									type : 'text',
									id : 'pagetext',
									label : 'page',
									validate : CKEDITOR.dialog.validate.notEmpty( "url field cannot be empty" )
								},
								{
									type : 'text',
									id : 'paramtext',
									label : 'Parameter'
								},
								{
								    type : 'button',
								    hidden : true,
								    id : 'id0',
								    label : editor.lang.common.browseServer,
								    filebrowser :
								    {
							            action : 'Browse',
							            params : { type : 'link'},
							            onSelect : function( fileUrl, data ) 
							            {
						                    var dialog = this.getDialog();
						                    var rawlink = fileUrl.split("|");
						                    var titlelink = rawlink[2];
						                    var sluglink = rawlink[1];
						                    var catetemplate = rawlink[0];
						                    dialog.getContentElement( 'tab1', 'titletext' ).setValue( titlelink );
						                    dialog.getContentElement( 'tab1', 'slugtext' ).setValue( sluglink );
						                    dialog.getContentElement( 'tab1', 'pagetext' ).setValue( catetemplate );
						                    // Do not call the built-in onSelect command 
						                    return false;
							            }
								    }
								}
							]
						}
					],
					onOk : function()
					{
						var dialog = this;
					    var functionlink = "#buildlink('"+dialog.getValueOf( 'tab1', 'slugtext' )+"','"+dialog.getValueOf( 'tab1', 'pagetext' )+"','"+dialog.getValueOf( 'tab1', 'paramtext' )+"')#";
					    var completelink = '<a href="'+functionlink+'">'+dialog.getValueOf( 'tab1', 'titletext' )+'</a>'
					    editor.insertHtml( completelink );
					}
				};
			} );
		}
	}
);