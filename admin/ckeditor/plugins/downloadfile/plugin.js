CKEDITOR.plugins.add( 'downloadfile',
	{
		icons:'downloadfile',
		init: function( editor )
		{
			editor.addCommand( 'downloadfileDialog',new CKEDITOR.dialogCommand( 'downloadfileDialog' ) );
			editor.ui.addButton( 'Downloadfile',
			{
				label: 'Insert File',
				command: 'downloadfileDialog',
				toolbar: 'insert'
			});
			CKEDITOR.dialog.add( 'downloadfileDialog', function( editor )
			{
				return {
					title : 'File Properties',
					minWidth : 400,
					minHeight : 200,
					contents :
					[
						{
							id : 'tab1',
							label : 'Basic Settings',
							elements :
							[
								{
									type : 'textarea',
									id : 'filetext',
									label : 'File',
									validate : CKEDITOR.dialog.validate.notEmpty( "file field cannot be empty" )
								},
								{
								    type : 'button',
								    hidden : true,
								    id : 'id0',
								    label : editor.lang.common.browseServer,
								    filebrowser :
								    {
							            action : 'Browse',
							            params : { type : 'file'},
							            onSelect : function( fileUrl, data ) 
							            {
						                    var dialog = this.getDialog();
						                    dialog.getContentElement( 'tab1', 'filetext' ).setValue( fileUrl );
						                    // Do not call the built-in onSelect command 
						                    return false;
							            }
								    }
								}
							]
						}
					],
					onOk : function()
					{
						var dialog = this;
					    // var video = editor.document.createElement( 'videotext' );

					    // video.setText( dialog.getValueOf( 'tab1', 'video' ) );

					    editor.insertHtml( dialog.getValueOf( 'tab1', 'filetext' ) );
					}
				};
			} );
		}
	}
);