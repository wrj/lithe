CKEDITOR.plugins.add( 'video',
	{
		icons:'video',
		init: function( editor )
		{
			editor.addCommand( 'videoDialog',new CKEDITOR.dialogCommand( 'videoDialog' ) );
			editor.ui.addButton( 'Video',
			{
				label: 'Insert Video',
				command: 'videoDialog',
				toolbar: 'insert'
			});
			CKEDITOR.dialog.add( 'videoDialog', function( editor )
			{
				return {
					title : 'Video Properties',
					minWidth : 400,
					minHeight : 200,
					contents :
					[
						{
							id : 'tab1',
							label : 'Basic Settings',
							elements :
							[
								{
									type : 'textarea',
									id : 'videotext',
									label : 'Video',
									validate : CKEDITOR.dialog.validate.notEmpty( "Video field cannot be empty" )
								},
								{
								    type : 'button',
								    hidden : true,
								    id : 'id0',
								    label : editor.lang.common.browseServer,
								    filebrowser :
								    {
							            action : 'Browse',
							            params : { type : 'video'},
							            onSelect : function( fileUrl, data ) 
							            {
						                    var dialog = this.getDialog();
						                    dialog.getContentElement( 'tab1', 'videotext' ).setValue( fileUrl );
						                    // Do not call the built-in onSelect command 
						                    return false;
							            }
								    }
								}
							]
						}
					],
					onOk : function()
					{
						var dialog = this;
					    // var video = editor.document.createElement( 'videotext' );

					    // video.setText( dialog.getValueOf( 'tab1', 'video' ) );

					    editor.insertHtml( dialog.getValueOf( 'tab1', 'videotext' ) );
					}
				};
			} );
		}
	}
);