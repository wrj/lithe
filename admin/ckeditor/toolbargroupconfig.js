var toolbargroupconfig = [
    { name: 'document',    groups: [ 'mode', 'document', 'doctools' ] },
    { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
    '/',
    { name: 'links' },

    { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align' ] },
    { name: 'insert' },
    '/',
    { name: 'styles' },
    { name: 'colors' },
    { name: 'tools' },
    { name: 'others' }
];
var toolbarfullconfig = [
	{ name: 'document',    items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
    { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
    { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
    '/',
    { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
    { name: 'links',       items : [ 'Link','Unlink' ] },
    { name: 'insert',      items : [ 'Addlink','Image','Downloadfile','Video','Iframe','Table','HorizontalRule','PageBreak' ] },
    '/',
    { name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
    { name: 'colors',      items : [ 'TextColor','BGColor' ] },
    { name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-' ,'About'] }
]


// CKEDITOR.on( 'instanceCreated', function( ev ){
// 	var editor = ev.editor;
// 	editor.addCommand( 'videoDialog', new CKEDITOR.dialogCommand( 'videoDialog' ) );
// 	editor.ui.addButton( 'Video',
// 	{
// 	    label: 'Insert Video',
// 	    command: 'videoDialog',
// 	    icon: 'plugins/video/images/icon.png'
// 	} );
// });

CKEDITOR.on( 'dialogDefinition', function( ev )
    {
        // Take the dialog name and its definition from the event data.
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;
 
        // Check if the definition is from the dialog we're
        // interested on (the Link dialog).
        if ( dialogName == 'link' )
        {
            // FCKConfig.LinkDlgHideAdvanced = true
            dialogDefinition.removeContents( 'advanced' );
 
            // FCKConfig.LinkDlgHideTarget = true
            dialogDefinition.removeContents( 'target' );
/*
Enable this part only if you don't remove the 'target' tab in the previous block.
 
            // FCKConfig.DefaultLinkTarget = '_blank'
            // Get a reference to the "Target" tab.
            var targetTab = dialogDefinition.getContents( 'target' );
            // Set the default value for the URL field.
            var targetField = targetTab.get( 'linkTargetType' );
            targetField[ 'default' ] = '_blank';
*/
        }
 
        if ( dialogName == 'image' )
        {
            // FCKConfig.ImageDlgHideAdvanced = true    
            dialogDefinition.removeContents( 'advanced' );
            // FCKConfig.ImageDlgHideLink = true
            dialogDefinition.removeContents( 'Link' );
        }
 
        if ( dialogName == 'flash' )
        {
            // FCKConfig.FlashDlgHideAdvanced = true
            dialogDefinition.removeContents( 'advanced' );
        }
 
    }); 