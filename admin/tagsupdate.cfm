<cfinclude template="include/header.cfm">
<cfif isdefined('ERROR')>
    <cfset tag = getstoredata()>
    <cfif structIsEmpty(tag)>
        <cfset tag = mongonew('tag')>
    </cfif>
<cfelse>
    <cfset objdata = MongoCollectionfindone(application.applicationname,'tag',{"_id"= newid(key)})>
    <cfset tag = mongonew("tag",objdata)>
</cfif>
<cfoutput><div class="row-fluid">
    <legend>Edit tag - #tag['name']#</legend>
    #startform(action='tagscontroller')#
    #hiddenfield(name='mode',value='update')#
    #hiddenfield(name='key',value=key)#
    #textfield(name='name',label='name',value=tag['name'])#
    #submit(label=get('labelbtnedit'))#
    #endform()#
</div>
</cfoutput>
<cfinclude template="include/footer.cfm">