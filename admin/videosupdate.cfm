<cfinclude template="include/header.cfm">
<cfif isdefined('ERROR')>
    <cfset video = getstoredata()>
    <cfif structIsEmpty(video)>
        <cfset video = mongonew('video')>
    </cfif>
<cfelse>
    <cfset objdata = MongoCollectionfindone(application.applicationname,'video',{"_id"= newid(key)})>
    <cfset video = mongonew("video",objdata)>
</cfif>
<cfoutput>
<div class="row-fluid">
    <legend class="titlepage">Edit video - #video['filename']#</legend>
    #startform(action='videoscontroller')#
    #hiddenfield(name='mode',value='update')#
    #hiddenfield(name='key',value=key)#
	#hiddenfield(name='displaystart',value=start)#
    #textfield(name='filename',label='video title',value=video['filename'],require=true)#
    #textarea(name='videolink',label='embeded code',value=video['videolink'],require=true)#
    #submit(label=get('labelbtnedit'))#
    #endform()#
</div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<!--- BACK BUTTON DETECT --->
<script type="text/javascript">
history.pushState({page: 123}, "update", "");
var _firstload = true;
var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
var isIE = /*@cc_on!@*/false;                            // At least IE6
if (isFirefox){_firstload = false;}
		window.onpopstate = function(e){
		if(_firstload){_firstload = false;}
else{_firstload = false;
	<cfoutput>window.location.href = 'videos.cfm?start=#start#';</cfoutput>}
};
</script>