<cfinclude template="system/loadplugins.cfm">
<cfset structDelete(form,'FIELDNAMES')>
<cfset result = mongovalidate('user','update',form)>
<cfif result['result'] eq true>
    <cfset updateobj = result['value']>
    <cfset olddata = MongoCollectionfindone(application.applicationname,'user',{'_id'=newid(session['userid'])})>
    <cfset oldpassword = olddata['password']>
    <cfset dataupdate = mongomapvalue('user',olddata,updateobj,'default')>
    <cfif dataupdate['PASSWORD'] eq "">
        <cfset dataupdate['PASSWORD'] = oldpassword>
    </cfif>
    <cfset dataupdate['SECRET'] = "S#replaceNoCase(createuuid(),'-','','all')#">
    <cfset MongoCollectionsave(application.applicationname,'user',dataupdate)>
    <cfset flashinsert('success','Update profile complete')>
    <cflocation url="profile.cfm" addtoken="false">
<cfelse>
    <cfset flashinsert('error','There was an error update profile.')>
    <cfset storedata(result)>
    <cflocation url="profile.cfm" addtoken="false">
</cfif>