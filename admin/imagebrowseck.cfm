<cfimport taglib="lib" prefix="ui">
<cfoutput>
    <div class="row-fluid selectfile">
        <div class="pull-left">
            <h4>Image</h4>
        </div>
        <div class="pull-right selectbtn">
	        <input type="button" class="btn" value="Close" onClick="javascript:window.close();">
            <input type="button" class="btn btn-primary" value="Select" onClick="selectfile()">
        </div>
    </div>
    <div class="row-fluid">
	    <div class="span4">
		    <p>Categories Images</p>
			#treecategoryoutput()#
		</div>
		<div class="span8">
			<p class="headpage">Images</p>
            <ui:datatable field="id,#capitalize('thumbnail')#,#capitalize('filename')#" tool=false size="0,100,0"/>
		</div>
    </div>
</cfoutput>
<!---    Preview Thumb--->
<div class="modal hide fade" id="pModal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Preview Image</h3>
	</div>
	<div class="modal-body" id="previewimage">

	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	</div>
</div>
<script type="text/javascript">
var visitfirst = true;
var category = '';

$(function(){
	createcollectionstree('tree','categoryimage');
	$('.radiocheck').live('click',function(e){
		changetype();
	});
	if ($('input:radio[name=typelink]:checked').val() == '3')
	{
		changetype();
	}
})
function reportkey(key){
	category = key;
	inittable();
}
function inittable()
{
	if(visitfirst == true)
	{
		visitfirst = false;
	}else{
		clearall();
	}
	var linkvalue = '?category='+category;
	createdatatablebrowse('usetable','imagesbrowsegrid',linkvalue);
}
function clearall()
{
	oTable.fnDestroy();
	gaiSelected = [];
}
$('#usetable tbody tr').live('click', function () {
    var aData = oTable.fnGetData( this );
    var iId = aData[0];
        gaiSelected = [];
        if ( $(this).hasClass('row_selected') ) {
            $(this).removeClass('row_selected');
        }else{
            oTable.$('tr.row_selected').removeClass('row_selected');
            $(this).addClass('row_selected');
            gaiSelected.push(iId);
        }
} );

function selectfile() {
    if (gaiSelected != "") {
        <cfoutput>
        var filearr = gaiSelected[0].split("|")
        var fileUrl = '#get('pathuploadfolder')#image/'+filearr[0];
        window.top.opener.CKEDITOR.tools.callFunction(#CKEditorFuncNum#, fileUrl);
        </cfoutput>
        closewindow();
    }else{
        alert("Please select image");
    }
}

function previewimage(imagename)
{
	var pathpreview = '<div class="thumbnail"><img src="'+imagename+'"/></div>';
	$('#previewimage').html(pathpreview);
}

$('#pModal').on('hidden', function () {
	$("#previewimage").html('');
});

function closewindow()
{
    window.top.close();
}
</script>    
</body>
</html>