<cfinclude template="system/loadplugins.cfm">
<cfset structDelete(form,'FIELDNAMES')>
<cfset structDelete(form,'GALLERYDETAIL')>
<cfset structDelete(form,'GALLERYIMAGE')>
<cfset structDelete(form,'GALLERYLINK')>
<cfset structDelete(form,'GALLERYTITLE')>
<cfswitch expression="#mode#">
    <cfcase value="create">
        <cfset structDelete(form,'mode')>
        <cfif form['GALLERYJSON'] neq "">
            <cfset galleryarray = deserializeJSON(form['GALLERYJSON'])>
        <cfelse>
            <cfset galleryarray = arrayNew()>
        </cfif>
        <cfset structDelete(form,'GALLERYJSON')>
        <cfset result = mongovalidate('slide','create',form)>
        <cfif result['result'] eq true>
            <!---            Language--->
            <cfset slidedata = assignlang('slide',result['value'])>
            <cfset slidedata['GALLERY'] = galleryarray>
            <cfif slidedata['newsimage'] neq ''>
                <cfset slidedata['STHUMB'] = slidedata['newsimage']>
            </cfif>
            <cfif slidedata['newlimage'] neq ''>
                <cfset slidedata['LTHUMB'] = slidedata['newlimage']>
            </cfif>
            <cfset slidedata['USER'] = dbref('user',session['userid'])>
            <cfset galleryid = MongoCollectioninsert(application.applicationname,'slide',slidedata)>
            <cfset flashinsert('success','Create slide complete')>
            <cflocation url="slides.cfm" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error creating the slide.')>
            <cfset storedata(result)>
            <cflocation url="slidesnew.cfm" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="update">
        <cfset keyid = key>
        <cfset structDelete(form,'KEY')>
        <cfset structDelete(form,'MODE')>
        <cfif form['GALLERYJSON'] neq "">
            <cfset galleryarray = deserializeJSON(form['GALLERYJSON'])>
        <cfelse>
            <cfset galleryarray = arrayNew()>
        </cfif>
        <cfset structDelete(form,'GALLERYJSON')>
        <cfset result = mongovalidate('slide','update',form)>
        <cfif result['result'] eq true>
            <cfif form['NEWSIMAGE'] neq ''>
                <cfif form['NEWSIMAGE'] neq 'remove'>
                    <cfset result['value']['STHUMB'] = form['NEWSIMAGE']>
                <cfelse>
                    <cfset result['value']['STHUMB'] = ''>
                </cfif>
            </cfif>
            <cfif form['NEWLIMAGE'] neq ''>
                <cfif form['NEWLIMAGE'] neq 'remove'>
                    <cfset result['value']['LTHUMB'] = form['NEWLIMAGE']>
                <cfelse>
                    <cfset result['value']['LTHUMB'] = ''>
                </cfif>
            </cfif>
            <cfset olddata = MongoCollectionfindone(application.applicationname,'slide',{'_id'=newid(keyid)})>
            <cfset dataupdate = mongomapvalue('slide',olddata,result['value'],lang)>
            <cfset structDelete(dataupdate, "GALLERY")>
            <cfset dataupdate['GALLERY'] = galleryarray>
            <cfset dataupdate['USER'] = dbref('user',session['userid'])>
            <cfset MongoCollectionsave(application.applicationname,'slide',dataupdate)>
            <cfset flashinsert('success','Update slide complete')>
            <cflocation url="slides.cfm?start=#displaystart#" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error update the slide.')>
            <cfset storedata(result)>
            <cflocation url="slidesupdate.cfm?key=#keyid#&start=#displaystart#&langinput=#lang#" addtoken="false">
        </cfif>
    </cfcase>

    <cfcase value="delete">
        <cfset MongoCollectionremove(application.applicationname,'slide',{'_id'=newid(key)})>
        <cfset result = structNew()>
        <cfset result['result'] = 'true'>
        <cfset result['message'] = "Delete slide complete">
        <cfoutput>
            #serializeJSON(result)#
        </cfoutput>
    </cfcase>

</cfswitch>