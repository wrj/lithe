<cfinclude template="include/header.cfm">
<cfimport taglib="lib" prefix="ui">
<cfoutput>
    <div class="row-fluid">
        <div class="span12">
            <p class="headpage">Orders</p>
            <ui:datatable field="Email,Status,Order date"/>
        </div>
    </div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<cfoutput>#javaScriptInclude("jquery.dataTables,DT_bootstrap,createdatatable,deletedialog")#</cfoutput>
<script type="text/javascript">
    createdatatable('usetable','ordersgrid','','3/desc');

    function goedit(id)
    {
        window.location = "ordersupdate.cfm?key="+id;
    }
</script>