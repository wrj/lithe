<cfinclude template="include/header.cfm">
<cfif isdefined('ERROR')>
    <cfset file = getstoredata()>
    <cfif structIsEmpty(file)>
        <cfset file = mongonew('file')>
    </cfif>
    <cfelse>
    <cfset file = mongonew('file')>
</cfif>
<cfoutput>
<div class="row-fluid">
    <legend>Choose file(s) to upload</legend>
    #startform(action='filescontroller')#
    #hiddenfield(name='mode',value='create')#
    #hiddenfield(name='filename')#
    #hiddenfield(name='newfilename')#
    #genUpload('#get('filesizefile')#','uploadprocess','file')#
    #button(label='Upload',onclick='doupload()')#
    #endform()#
</div>
</cfoutput>
<script type="text/javascript">
    function upfilecomplete(serverfile,clientfile)
    {
        $('#filename').val(clientfile);
        $('#newfilename').val(serverfile);
        $('form').submit();
    }

    function doupload()
    {
        $('#file_upload').uploadifive('upload')
    }
</script>
<cfinclude template="include/footer.cfm">