<cfinclude template="include/header.cfm">
<cfset cateraw = MongoCollectionfind(application.applicationname,'categoryimage',{})>
<cfset caterawquery = treequery(cateraw)/>
<cfset catequery = arraynew()>
<cfloop array="#caterawquery#" index="item">
	<cfset rowquery = structNew()>
	<cfset rowquery["_id"]=item['_id']>
	<cfset rowquery["TITLE"]=item['TITLE']>
	<cfset arrayAppend(catequery,rowquery)>
</cfloop>
<cfif isdefined('ERROR')>
    <cfset image = getstoredata()>
    <cfif structIsEmpty(image)>
        <cfset image = mongonew('image')>
    </cfif>
    <cfelse>
    <cfset image = mongonew('image')>
</cfif>
<cfoutput>
<div class="row-fluid">
    <legend class="titlepage">New image</legend>
    #startform(action='imagescontroller')#
    #hiddenfield(name='mode',value='create')#
    #hiddenfield(name='filename')#
    #hiddenfield(name='newfilename')#
    #genUpload('#get('filesizeimage')#','uploadprocess','image')#
	#select(name='category',label='category',select='',option=catequery,field='title',key='_id',require=true)#
    #button(label='Upload',onclick='doupload()')#


	<cfinclude template="imagedefaultsize.cfm">
        <div class="control-group">
            <div class="controls">
				<div class="span4 addimage_showdefaultsize">
<!---   slideshow   largeimage  thumbnail   categoryimage   menuimage   otherimage	--->
                    #$showimagedefaultsize("slideshow,thumbnail,largeimage,categoryimage,menuimage,otherimage")#
				</div>
			</div>
		</div>

	#endform()#
</div>
</cfoutput>
<script type="text/javascript">
    function upfilecomplete(serverimage,clientimage)
    {
        $('#filename').val(clientimage);
        $('#newfilename').val(serverimage);
        $('form').submit();
    }

    function doupload()
    {
	    if(window.validateform($("form"))){
            $('#file_upload').uploadifive('upload')
	    }
    }
</script>
<cfinclude template="include/footer.cfm">