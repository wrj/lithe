<cfinclude template="include/header.cfm">
<cfif isdefined('ERROR')>
    <cfset member = getstoredata()>
    <cfif structIsEmpty(member)>
        <cfset member = mongonew('member')>
    </cfif>
    <cfelse>
    <cfset objdata = MongoCollectionfindone(application.applicationname,'member',{"_id"= newid(key)})>
    <cfset member = mongonew("member",objdata)>
</cfif>
<cfoutput>
    <div class="row-fluid">
    <legend>Edit member - #member['name']#</legend>
    #startform(action='memberscontroller')#
    #hiddenfield(name='mode',value='update')#
    #hiddenfield(name='key',value=key)#
    <ul class="nav nav-tabs" id="tabs">
        <li class="active"><a href="##tabs-general" data-toggle="tab">Customer</a></li>
        <li><a href="##tabs-shipping" data-toggle="tab">Shipping</a></li>
        <li><a href="##tabs-payment" data-toggle="tab">Payment</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tabs-general">
            #textfield(name='name',label='name',value=member['name'],readonly=true)#
            #textfield(name='email',label='email',value=member['email'],readonly=true)#
            <div class="control-group">
                <label class="control-label">Image</label>
                <div class="controls">
                    <cfif member['image'] neq ''>
                        <img src="#member['image']#">
                    </cfif>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="tabs-shipping">
            <cfset shipping = objdata['SHIPPING']>
            #textfield(name='type',label='type',value=shipping['type'],readonly=true)#
            <div class="control-group">
                <div class="controls">
                    <h5>Shipping Address</h5>
                </div>
            </div>
            <cfloop list="#structkeylist(shipping['SHIPPING_ADDRESS'],',')#" delimiters="," index="shippingfield">
                #textfield(name=shippingfield,label=shippingfield,value=shipping['SHIPPING_ADDRESS'][shippingfield],readonly=true)#
            </cfloop>
            <div class="control-group">
                <div class="controls">
                    <h5>Option</h5>
                </div>
            </div>
            <cfif isstruct(shipping['OPTIONS'])>
                <cfloop list="#structkeylist(shipping['OPTIONS'],',')#" delimiters="," index="shippingfield">
                    #textfield(name=shippingfield,label=shippingfield,value=shipping['OPTIONS'][shippingfield],readonly=true)#
                </cfloop>
            </cfif>
        </div>
        <div class="tab-pane" id="tabs-payment">
            <cfset payment = objdata['payment']>
            #textfield(name='type',label='type',value=payment['type'],readonly=true)#
            <div class="control-group">
                <div class="controls">
                    <h5>Billing Address</h5>
                </div>
            </div>
            <cfloop list="#structkeylist(payment['BILLING_ADDRESS'],',')#" delimiters="," index="paymentfield">
                #textfield(name=paymentfield,label=paymentfield,value=payment['BILLING_ADDRESS'][paymentfield],readonly=true)#
            </cfloop>
            <div class="control-group">
                <div class="controls">
                    <h5>Option</h5>
                </div>
            </div>
            <cfif isstruct(payment['OPTIONS'])>
                <cfloop list="#structkeylist(payment['OPTIONS'],',')#" delimiters="," index="paymentfield">
                    #textfield(name=paymentfield,label=paymentfield,value=payment['OPTIONS'][paymentfield],readonly=true)#
                </cfloop>
            </cfif>
        </div>
    </div>
    #button(label='Back',onclick="back()")#
    #endform()#
    </div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<script type="text/javascript">
    function back()
    {
        window.location = "members.cfm";
    }
</script>