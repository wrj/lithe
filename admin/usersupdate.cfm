<cfinclude template="include/header.cfm">
<cfif isdefined('ERROR')>
    <cfset user = getstoredata()>
    <cfif structIsEmpty(user)>
        <cfset user = mongonew('user')>
    </cfif>
<cfelse>
    <cfset objdata = MongoCollectionfindone(application.applicationname,'user',{"_id"= newid(key)})>
    <cfset user = mongonew("user",objdata)>
</cfif>
<cfset logq = arraynew()>
<cfset logst = structNew()>
<cfset logst['id'] = 'user'>
<cfset logst['text'] = 'user'>
<cfset arrayappend(logq,logst)>
<cfset logst = structNew()>
<cfset logst['id'] = 'admin'>
<cfset logst['text'] = 'admin'>
<cfset arrayappend(logq,logst)>
<cfoutput>
<div class="row-fluid">
    <legend class="titlepage">Edit User - #user['username']#</legend>
    #startform(action='userscontroller')#
    #hiddenfield(name='mode',value='update')#
    #hiddenfield(name='key',value=key)#
	#hiddenfield(name='displaystart',value=start)#
    #textfield(name='username',label='username',value=user['username'],readonly=true)#
    #passwordfield(name='password',label='password')#
    #textfield(name='email',label='email',value=user['email'],readonly=true)#
    #textfield(name='firstname',label='firstname',value=user['firstname'],class="span11 between5-40",require=true)#
    #textfield(name='lastname',label='lastname',value=user['lastname'],class="span11 between5-40",require=true)#
    #select(name='rule',label='rule',select=user['rule'],option=logq,field='text',key='id')#
    #submit(label=get('labelbtnedit'))#
    #endform()#
</div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<!--- BACK BUTTON DETECT --->
<script type="text/javascript">
history.pushState({page: 123}, "update", "");
var _firstload = true;
var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
var isIE = /*@cc_on!@*/false;                            // At least IE6
if (isFirefox){_firstload = false;}
		window.onpopstate = function(e){
		if(_firstload){_firstload = false;}
else{_firstload = false;
	<cfoutput>window.location.href = 'users.cfm?start=#start#';</cfoutput>}
};
</script>