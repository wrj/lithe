<cfinclude template="system/loadplugins.cfm">
<cfset structDelete(form,'FIELDNAMES')>
<cfset structDelete(form,'GALLERYDETAIL')>
<cfset structDelete(form,'GALLERYIMAGE')>
<cfset structDelete(form,'GALLERYLINK')>
<cfset structDelete(form,'GALLERYTITLE')>
<cfswitch expression="#mode#">
    <cfcase value="create">
        <cfset structDelete(form,'mode')>
        <cfif form['GALLERYJSON'] neq "">
            <cfset galleryarray = deserializeJSON(form['GALLERYJSON'])>
        <cfelse>
            <cfset galleryarray = arrayNew()>
        </cfif>
        <cfset structDelete(form,'GALLERYJSON')>
        <cfset result = mongovalidate('gallery','create',form)>
        <cfif result['result'] eq true>
            <!---            Language--->
            <cfset gallerydata = assignlang('gallery',result['value'])>
            <cfset gallerydata['GALLERY'] = galleryarray>
            <cfif gallerydata['newsimage'] neq ''>
                <cfset gallerydata['STHUMB'] = gallerydata['newsimage']>
            </cfif>
            <cfif gallerydata['newlimage'] neq ''>
                <cfset gallerydata['LTHUMB'] = gallerydata['newlimage']>
            </cfif>
            <cfset gallerydata['USER'] = dbref('user',session['userid'])>
            <cfset galleryid = MongoCollectioninsert(application.applicationname,'gallery',gallerydata)>
            <cfset flashinsert('success','Create gallery complete')>
            <cflocation url="galleries.cfm" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error creating the gallery.')>
            <cfset storedata(result)>
            <cflocation url="galleriesnew.cfm" addtoken="false">
        </cfif>
    </cfcase>
    <cfcase value="update">
        <cfset keyid = key>
        <cfset structDelete(form,'KEY')>
        <cfset structDelete(form,'MODE')>
        <cfif form['GALLERYJSON'] neq "">
            <cfset galleryarray = deserializeJSON(form['GALLERYJSON'])>
        <cfelse>
            <cfset galleryarray = arrayNew()>
        </cfif>
        <cfset structDelete(form,'GALLERYJSON')>
        <cfset result = mongovalidate('gallery','update',form)>
        <cfif result['result'] eq true>
            <cfif form['NEWSIMAGE'] neq ''>
                <cfif form['NEWSIMAGE'] neq 'remove'>
                    <cfset result['value']['STHUMB'] = form['NEWSIMAGE']>
                <cfelse>
                    <cfset result['value']['STHUMB'] = ''>
                </cfif>
            </cfif>
            <cfif form['NEWLIMAGE'] neq ''>
                <cfif form['NEWLIMAGE'] neq 'remove'>
                    <cfset result['value']['LTHUMB'] = form['NEWLIMAGE']>
                <cfelse>
                    <cfset result['value']['LTHUMB'] = ''>
                </cfif>
            </cfif>
            <cfset olddata = MongoCollectionfindone(application.applicationname,'gallery',{'_id'=newid(keyid)})>
            <cfset dataupdate = mongomapvalue('gallery',olddata,result['value'],lang)>
            <cfset structDelete(dataupdate, "GALLERY")>
            <cfset dataupdate['GALLERY'] = galleryarray>
            <cfset dataupdate['USER'] = dbref('user',session['userid'])>
            <cfset MongoCollectionsave(application.applicationname,'gallery',dataupdate)>
            <cfset flashinsert('success','Update gallery complete')>
            <cflocation url="galleries.cfm?start=#displaystart#" addtoken="false">
        <cfelse>
            <cfset flashinsert('error','There was an error update the gallery.')>
            <cfset storedata(result)>
            <cflocation url="galleriesupdate.cfm?key=#keyid#&start=#displaystart#&langinput=#lang#" addtoken="false">
        </cfif>
    </cfcase>

    <cfcase value="delete">
        <cfset MongoCollectionremove(application.applicationname,'gallery',{'_id'=newid(key)})>
        <cfset result = structNew()>
        <cfset result['result'] = 'true'>
        <cfset result['message'] = "Delete gallery complete">
        <cfoutput>
            #serializeJSON(result)#
        </cfoutput>
    </cfcase>

</cfswitch>