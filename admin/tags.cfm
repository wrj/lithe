<cfinclude template="include/header.cfm">
<cfimport taglib="lib" prefix="ui">
<cfoutput>
<div class="row-fluid">
    <div class="span12">
        <ui:datatable field="#capitalize('name')#"/>
    </div>
</div>
<div class="modal hide fade bigModal" id="delModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Delete Tag</h3>
    </div>
    <div class="modal-body">
        <p id="shownamedelete"></p>
        <h2>Please note</h2>
        <p>After you delete an Tag.It'll not collect usage data anymore.</p>
        <input type="hidden" id="iddelete" name="iddelete">
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        <a onclick="confirmdelete('tags');" class="btn btn-danger">OK</a>
    </div>
</div>
</cfoutput>
<cfinclude template="include/footer.cfm">
<cfoutput>#javaScriptInclude("jquery.dataTables,DT_bootstrap,createdatatable,deletedialog")#</cfoutput>
<script type="text/javascript">
    createdatatable('usetable','tagsgrid','');

    function gonew()
    {
        window.location = "tagsnew.cfm";
    }

    function goedit(id)
    {
        window.location = "tagsupdate.cfm?key="+id;
    }
</script>
