<cfinclude template="system/loadplugins.cfm">
<cfset sortlabel = ""/>
<cfset sortvalue = 1/>
<cfset searchtxt = ""/>
<cfset listColumns = "CUSTOMER.EMAIL,STATUS,CREATEDAT" />
<cfset listSearch = "CUSTOMER.EMAIL,STATUS">
<cfif not isDefined('iSortCol_0')>
    <cfset iSortCol_0 = 0>
    <cfset sSortDir_0 = 1>
    <cfset sSearch = "">
    <cfset sEcho = 1>
    <cfset iDisplayStart =0>
    <cfset iDisplayLength = 10>
</cfif>
<cfset sortlabel = listGetAt(listColumns,(Val(iSortCol_0) + 1))/>
<cfif sSortDir_0 eq "desc">
    <cfset sortvalue = -1/>
</cfif>
<cfset sortst = structNew()>
<cfset sortst[sortlabel] = sortvalue>
<cfoutput>
    <cfset defaultsearch = structnew()>
<!---    <cfset defaultsearch["SECRETKEY"] = session['secret']>--->
    <cfif sSearch neq "">
        <cfset searchtxt = structnew()>
        <cfset searchtxt["$regex"] = ".*#Trim(sSearch)#.*">
        <cfset searchtxt["$options"] = "i">
        <cfset fieldarray = arraynew()>
        <cfloop list="#listSearch#" delimiters="," index="item">
            <cfset fieldsearch = structNew()>
            <cfset fieldsearch[item] = searchtxt>
            <cfset arrayappend(fieldarray,fieldsearch)>
        </cfloop>
        <cfset defaultsearch["$or"] = fieldarray>
    </cfif>
    <cfset mydatatotal = MongoCollectioncount(datasource=application.applicationname,collection='order',query=defaultsearch)>
    <cfset mydata = MongoCollectionFind(datasource=application.applicationname,collection='order',query=defaultsearch,skip=iDisplayStart,size=iDisplayLength,sort=sortst)>
    <cfsavecontent variable="sourcedatatable">
        {"sEcho": #val(sEcho)#,
    "iTotalRecords": #mydatatotal#,
    "iTotalDisplayRecords": #mydatatotal#,
    "aaData": [
        <cfset i = 1/>
        <cfloop array="#mydata#" index="rowitem">
            [
            <cfloop list="#listColumns#" index="thisColumn">
                <cfif thisColumn neq listFirst(listColumns)>,</cfif>
                <cfif ListFind("CREATEDAT,UPDATEDAT",thisColumn,',') gt 0>
                    "#jsStringFormat(DateFormat(rowitem[thisColumn],'dd/mm/yy'))#"
                <cfelseif thisColumn eq "CUSTOMER.EMAIL">
                    "#jsStringFormat(rowitem['CUSTOMER']['EMAIL'])#"
                <cfelse>
                    "#jsStringFormat(checkdata(rowitem,thisColumn))#"
                </cfif>
            </cfloop>
            ,"<a onclick=\"javascript:goedit('#rowitem['_id'].toString()#')\" href=\"javascript:void(0)\" class=\"btn\">#get('labeledit')#</a>"]
            <cfif i neq arraylen(mydata)>,</cfif>
            <cfset i++/>
        </cfloop>]}
    </cfsavecontent>
    <cfoutput>
        #sourcedatatable#
    </cfoutput>
</cfoutput>