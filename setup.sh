#!/bin/bash
echo "----------------------------------------------------------------"
cp -rf ../openbdbackup/WEB-INF ./
echo "Move WEB-INF						: Completed"
echo "----------------------------------------------------------------"
cp -rf ../openbdbackup/bluedragon ./
echo "Move bluedragon					: Completed"
echo "----------------------------------------------------------------"
cp -rf ../openbdbackup/manual ./
echo "Move manual						: Completed"
echo "----------------------------------------------------------------"
rm -rf ../openbdbackup
echo "Delete backup						: Completed"
echo "----------------------------------------------------------------"
cp start.cfm index.cfm
echo "Set a default index.cfm 			: Completed"
echo "Your website use '/index.cfm' as a default index file."
echo "All of web front end pages is located inside '/web' folder."
echo "----------------------------------------------------------------"
touch admin/include/superadminmenucustom.cfm
touch admin/include/adminmenucustom.cfm
touch admin/include/usermenucustom.cfm
echo "Set custom menues 				: Completed"
echo "----------------------------------------------------------------"
mkdir assets
mkdir menu
echo "Create directories 				: Completed"
echo "----------------------------------------------------------------"
chmod -R 777 assets
chmod -R 777 menu
echo "Change Permission 				: Completed"
echo "----------------------------------------------------------------"
