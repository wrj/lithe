$(function(){
	$('.lithe-ajax-upload').each(function(){
		$this = $(this);
		$inputFile = $this.find('.ajaxUploadInputFile');
		$inputFileHidden = $this.find('.ajaxUploadInputFileHidden');
		$inputFileText = $this.find('.ajaxUploadInputFileText');
		$inputFileButton = $this.find('.ajaxUploadInputFileButton');
		$inputFileText.val('');
		$inputFileButton.click(function(){
			$inputFile.click();
		});
		$inputFile.change(function(){
            $inputFileText.val($inputFile.val());
            var fd = new FormData(), xhr = new XMLHttpRequest();
            fd.append("displayPic", $inputFile[0].files[0]);
            xhr.open("POST", "/cfc/services/order.cfc?method=uploadFile");
            xhr.addEventListener("load", function(evt){
               $inputFileHidden.val(evt.target.responseText);
            }, false)
            xhr.send(fd);
        });
	});
});