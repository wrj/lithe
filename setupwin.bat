echo "----------------------------------------------------------------"
xcopy ..\openbdbackup\WEB-INF\* .\WEB-INF /s /i
echo "Move WEB-INF						: Completed"
echo "----------------------------------------------------------------"
xcopy ..\openbdbackup\bluedragon\* .\bluedragon /s /i
echo "Move bluedragon					: Completed"
echo "----------------------------------------------------------------"
xcopy ..\openbdbackup\manual\* .\manual /s /i
echo "Move manual						: Completed"
echo "----------------------------------------------------------------"
RD /S /Q ..\openbdbackup
echo "Delete backup						: Completed"
echo "----------------------------------------------------------------"
copy start.cfm index.cfm
echo "Set a default index.cfm 			: Completed"
echo "Your website use '/index.cfm' as a default index file."
echo "All of web front end pages is located inside '/web' folder."
echo "----------------------------------------------------------------"
type NUL > admin\include\superadminmenucustom.cfm
type NUL > admin\include\adminmenucustom.cfm
type NUL > admin\include\usermenucustom.cfm
echo "Set custom menues 				: Completed"
echo "----------------------------------------------------------------"
MD assets
MD menu
echo "Create directories 				: Completed"
echo "----------------------------------------------------------------"
chmod -R 777 assets
chmod -R 777 menu
echo "Change Permission 				: Completed"
echo "----------------------------------------------------------------"
